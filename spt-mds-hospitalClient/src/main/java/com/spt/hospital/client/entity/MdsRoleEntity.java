package com.spt.hospital.client.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;
/**
 * @author hanson
 * 实体：角色表
 * */
@Entity
@Table(name = "tbl_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsRoleEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 角色编号*/
	private String roleCode; 
	/* 角色名称*/
	private String roleName; 
	/* 备注*/
	private String remark;
	
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
