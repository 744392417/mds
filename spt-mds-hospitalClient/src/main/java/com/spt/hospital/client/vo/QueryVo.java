package com.spt.hospital.client.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="查询参数vo")
public class QueryVo {
	/* keyword */
	private String keyWord;
	/* 操作类型 */
	private String operType;
	/* 用户微信登录唯一标识 */
	private String openid;
	/* 产品编号 */
	private String productCode;
	/* 产品二维码 */
	private String productQrcode;
	/* 品类编号 */
	private Long categoryId;
	/* 品类编号 */
	private Long leaseInfoId;
	/* 品类名称 */
	private String categoryName;
	/* 设备类别图片 */
	private String categoryFileId;
	/* 门店ID */
	private Long propertyId;
	/* 物业名称 */
	private String propertyName;
	/* 医院ID */
	private Long hospitalId;
	/* 医院名称 */
	private String hospitalName;
	/* 所属科室 */
	private String hospitalDepart;
	/* 产品名称 */
	private String productName;
	/* 产品类型 */
	private String productType;
	/* 产品ID */
	private Long productId;
	/* 消费状态|交易类型 */
	private String consumStatus;
	/* 登录名称 */
	private String loginName;
	/* 登录密码 */
	private String loginPwd;
	/* wx登录密码 */
	private String code;
	/* wx */
	private String encryptedData;
	/* wx */
	private String iv;
	/* token */
	private String token;
	/* 状态 */
	private String status = "1";
	/* 类型 :医院H 物业 P 代理商 A 平台M */
	private String type;
	/* 订单数量 */
	private String orderNumber;
	/* 租赁费用(收入) */
	private BigDecimal leaseCost = new BigDecimal(0.00);
	/* 预付款 */
	private BigDecimal leaseDeposit = new BigDecimal(0.00);
	/* 退还 */
	private BigDecimal intoBalance = new BigDecimal(0.00);
	/* 租金 元/小时 */
	private BigDecimal leaseUnit = new BigDecimal(0.00);
	/* 每次封顶价格 20 元/天 */
	private BigDecimal topBalance = new BigDecimal(0.00);
	/* 递减价格(元/天) */
	private BigDecimal decreasPrice = new BigDecimal(0.00);
	/* 最低(元/天) */
	private BigDecimal minimumPrice = new BigDecimal(0.00);

	/* web退款价格 */
	private BigDecimal intoWBalance = new BigDecimal(0.00);

	private String hospitalWard;

	/* web端退款密码 */
	private String refusePwd;

	/* 手机 */
	private String mobilePhone;

	/* 产品数量 */
	private Integer productNumber = 1;
	/* 开锁密码 */
	private String unlockPwd;
	// 关锁密码
	private String shutPwd;
	/* 设备状态 ：6 异常 7 低电量 8 逾期 9 维修中 */
	private String productStatus;
	/* 设备使用状态 ：1备选 2 空闲 3 暂停 4 使用中 5 报修 */
	private String productUseStatus;
	/* 起步小时 */
	private Integer minHour = 0;
	/* 收费表Id */
	private Long hospitalPriceId;
	/* 设备的Id数组 */
	private String productIdArray;
	/* 设备批量处理的类型 */
	private String batchType;
	/* 收支类型 */
	private String budgetType;
	/* 商户订单号 */
	private String outTradeNo;
	/* 微信支付订单号 */
	private String transactionId;
	/* 用户id */
	private Long userId;
	/* 物业ID */
	private String properId;
	/* 订单状态 */
	private String orderStatus;

	/* 分配 */
	private String allocated;
	/* 未分配 */
	private String undistributed;

	/* 最大值 */
	private Long maximum;
	/* 最小值 */
	private Long minimum;

	/* 报修中正常 与维修状态 */
	private String repairsType;

	/* 多个设备ID字符串 */
	private String equipmentIds;


	/* 省 */
	private String provinceName;
	/* 市 */
	private String cityName;
	/* 县 */
	private String countyName;

	/*运维人员*/
	private String operationStaff;
	/*运营人员*/
	private String operatingPerson;
	
	/*运维联系方式*/
	private String operationPhone ;
	
	/*运营人员联系方式*/
	private String operatingPhone;
	
	/*查询类型标示*/
	private Long marking;

	/*用户名*/
	private String nickName;
	
	/*用户类型 医院H 物业P 代理商A  平台 M  用户 U*/
	private String userType;
	
	/*用户手机号*/
	private String userPhone;
	
	/***物业代理商***/
	private Long userHospId;
	
	/*客服意见*/
	private String serviceReason; 
	

	/***设备是否报修*/
	private String productUse;
	
	//完成退款
	private String completeFefund; 

	
	/* 消费状态|交易类型 */
	List<String> consumStatusList;
	

	public List<String> getConsumStatusList() {
		return consumStatusList;
	}
	public void setConsumStatusList(List<String> consumStatusList) {
		this.consumStatusList = consumStatusList;
	}
	public String getCompleteFefund() {
		return completeFefund;
	}
	public void setCompleteFefund(String completeFefund) {
		this.completeFefund = completeFefund;
	}
	public String getProductUse() {
		return productUse;
	}
	public void setProductUse(String productUse) {
		this.productUse = productUse;
	}

	
	

	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}

	
	public Long getUserHospId() {
		return userHospId;
	}
	public void setUserHospId(Long userHospId) {
		this.userHospId = userHospId;
	}
	public String getServiceReason() {
		return serviceReason;
	}
	public void setServiceReason(String serviceReason) {
		this.serviceReason = serviceReason;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	
	public Long getMarking() {
		return marking;
	}
	public void setMarking(Long marking) {
		this.marking = marking;
	}
	
	public String getOperationPhone() {
		return operationPhone;
	}

	public void setOperationPhone(String operationPhone) {
		this.operationPhone = operationPhone;
	}

	public String getOperatingPhone() {
		return operatingPhone;
	}

	public void setOperatingPhone(String operatingPhone) {
		this.operatingPhone = operatingPhone;
	}

	
	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getRepairsType() {
		return repairsType;
	}

	public String getEquipmentIds() {
		return equipmentIds;
	}

	public void setEquipmentIds(String equipmentIds) {
		this.equipmentIds = equipmentIds;
	}

	public void setRepairsType(String repairsType) {
		this.repairsType = repairsType;
	}

	public Long getMaximum() {
		return maximum;
	}

	public void setMaximum(Long maximum) {
		this.maximum = maximum;
	}

	public Long getMinimum() {
		return minimum;
	}

	public void setMinimum(Long minimum) {
		this.minimum = minimum;
	}

	public String getAllocated() {
		return allocated;
	}

	public void setAllocated(String allocated) {
		this.allocated = allocated;
	}

	public String getUndistributed() {
		return undistributed;
	}

	public void setUndistributed(String undistributed) {
		this.undistributed = undistributed;
	}

	/**
	 * TODO Page
	 */
	@ApiModelProperty(value = "排序类型(asc desc)", example = "asc")
	private String order = "desc";
	@ApiModelProperty(value = "一页显示多少条数(默认十条)", example = "10")
	private int rows = 10;
	@ApiModelProperty(value = "当前页", example = "1")
	private int page = 1;
	@ApiModelProperty(value = "排序字段", example = "applyTime")
	private String sort = "updatedDate";
	/**
	 * TODO Time
	 */
	/* 申请时间 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	@JsonFormat(pattern = "yyyy/MM/dd", timezone = "GMT+08:00")
	private Date startDate;
	/* 结束时间 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	@JsonFormat(pattern = "yyyy/MM/dd", timezone = "GMT+08:00")
	private Date endDate;

	List<String> orderStatusList;

	/* 反馈内容 */
	private String feedbackContent;

	/* 原因 */
	private String reason;
	/* 详情 */
	private String remark;

	private String accountName;
	private int outHour;

	public String getFeedbackContent() {
		return feedbackContent;
	}

	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/* 图片ID */
	private String fileId;

	/* 是否正常 */
	private String isWrong;

	public String getOperationStaff() {
		return operationStaff;
	}

	public void setOperationStaff(String operationStaff) {
		this.operationStaff = operationStaff;
	}

	public String getOperatingPerson() {
		return operatingPerson;
	}

	public void setOperatingPerson(String operatingPerson) {
		this.operatingPerson = operatingPerson;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getOutHour() {
		return outHour;
	}

	public void setOutHour(int outHour) {
		this.outHour = outHour;
	}

	public String getIsWrong() {
		return isWrong;
	}

	public void setIsWrong(String isWrong) {
		this.isWrong = isWrong;
	}

	public List<String> getOrderStatusList() {
		return orderStatusList;
	}

	public void setOrderStatusList(List<String> orderStatusList) {
		this.orderStatusList = orderStatusList;
	}

	/**
	 * TODO getter & setter
	 */
	public BigDecimal getIntoWBalance() {
		return intoWBalance;
	}

	public void setIntoWBalance(BigDecimal intoWBalance) {
		this.intoWBalance = intoWBalance;
	}

	public String getRefusePwd() {
		return refusePwd;
	}

	public void setRefusePwd(String refusePwd) {
		this.refusePwd = refusePwd;
	}

	public Integer getMinHour() {
		return minHour;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getProperId() {
		return properId;
	}

	public void setProperId(String properId) {
		this.properId = properId;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getBatchType() {
		return batchType;
	}

	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}

	public String getProductIdArray() {
		return productIdArray;
	}

	public void setProductIdArray(String productIdArray) {
		this.productIdArray = productIdArray;
	}

	public void setMinHour(Integer minHour) {
		this.minHour = minHour;
	}

	public String getProductUseStatus() {
		return productUseStatus;
	}

	public void setProductUseStatus(String productUseStatus) {
		this.productUseStatus = productUseStatus;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public BigDecimal getLeaseUnit() {
		return leaseUnit;
	}

	public void setLeaseUnit(BigDecimal leaseUnit) {
		this.leaseUnit = leaseUnit;
	}

	public BigDecimal getTopBalance() {
		return topBalance;
	}

	public void setTopBalance(BigDecimal topBalance) {
		this.topBalance = topBalance;
	}

	public BigDecimal getDecreasPrice() {
		return decreasPrice;
	}

	public void setDecreasPrice(BigDecimal decreasPrice) {
		this.decreasPrice = decreasPrice;
	}

	public BigDecimal getMinimumPrice() {
		return minimumPrice;
	}

	public void setMinimumPrice(BigDecimal minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getShutPwd() {
		return shutPwd;
	}

	public void setShutPwd(String shutPwd) {
		this.shutPwd = shutPwd;
	}

	public String getUnlockPwd() {
		return unlockPwd;
	}

	public void setUnlockPwd(String unlockPwd) {
		this.unlockPwd = unlockPwd;
	}

	public Integer getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(Integer productNumber) {
		this.productNumber = productNumber;
	}

	public String getHospitalDepart() {
		return hospitalDepart;
	}

	public void setHospitalDepart(String hospitalDepart) {
		this.hospitalDepart = hospitalDepart;
	}

	public BigDecimal getIntoBalance() {
		return intoBalance;
	}

	public void setIntoBalance(BigDecimal intoBalance) {
		this.intoBalance = intoBalance;
	}

	public BigDecimal getLeaseCost() {
		return leaseCost;
	}

	public void setLeaseCost(BigDecimal leaseCost) {
		this.leaseCost = leaseCost;
	}

	public BigDecimal getLeaseDeposit() {
		return leaseDeposit;
	}

	public void setLeaseDeposit(BigDecimal leaseDeposit) {
		this.leaseDeposit = leaseDeposit;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCategoryFileId() {
		return categoryFileId;
	}

	public void setCategoryFileId(String categoryFileId) {
		this.categoryFileId = categoryFileId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Long propertyId) {
		this.propertyId = propertyId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductQrcode() {
		return productQrcode;
	}

	public void setProductQrcode(String productQrcode) {
		this.productQrcode = productQrcode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getConsumStatus() {
		return consumStatus;
	}

	public void setConsumStatus(String consumStatus) {
		this.consumStatus = consumStatus;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public Long getLeaseInfoId() {
		return leaseInfoId;
	}

	public void setLeaseInfoId(Long leaseInfoId) {
		this.leaseInfoId = leaseInfoId;
	}

	public Long getHospitalPriceId() {
		return hospitalPriceId;
	}

	public void setHospitalPriceId(Long hospitalPriceId) {
		this.hospitalPriceId = hospitalPriceId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getHospitalWard() {
		return hospitalWard;
	}

	public void setHospitalWard(String hospitalWard) {
		this.hospitalWard = hospitalWard;
	}
}
