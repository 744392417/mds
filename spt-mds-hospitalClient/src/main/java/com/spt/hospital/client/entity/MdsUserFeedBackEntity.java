package com.spt.hospital.client.entity;



import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;
/**
 * @author hanson
 * 实体：用户反馈
 * */
@Entity
@Table(name = "tbl_user_feedback")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsUserFeedBackEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;

	/* 微信id*/
	private String openid; 
	/* 手机*/
	private String mobilePhone;
	/*订单编号*/
	private String orderNumber;
	/* 状态 1 解决 0 未解决*/
	private String feedbackStatus="0";
	/*反馈内容*/
	private String feedbackContent;
	/*描述*/
	private String remark; 
	
	
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFeedbackStatus() {
		return feedbackStatus;
	}
	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}
	public String getFeedbackContent() {
		return feedbackContent;
	}
	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


}