package com.spt.hospital.client.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：门店设备价格关系
 * */
@Entity
@Table(name = "tbl_hospital_price")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsHospitalPriceEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/*设备类别ID*/
	private Long categoryId; 
	/* 设备类别图片*/
	private String categoryFileId;
	/* 类别code*/
	private String categoryCode; 
	/* 类别名称*/
	private String categoryName; 
	/* 医院ID*/
	private Long hospitalId; 
	/* 医院编号*/
	private String hospitalCode;
	/* 医院名称*/
	private String hospitalName;
	/*状态*/
	private String status ="1";
	/* 备注*/
	private String remark; 
	/* 预付款*/
	private BigDecimal leaseDeposit = new BigDecimal(0.00); 
	/* 租金 元/小时*/
	private BigDecimal leaseUnit = new BigDecimal(0.00); 
	/* 每次封顶价格  20 元/天*/
	private BigDecimal topBalance = new BigDecimal(0.00); 
	/*递减价格(元/天)*/
	private BigDecimal decreasPrice = new BigDecimal(0.00);
	/*最低(元/天)*/
	private BigDecimal minimumPrice = new BigDecimal(0.00);
	/*起步小时*/
	private Integer minHour = 0;
	
	private Integer outHour = 1;//超时加收
	

	
	public Integer getOutHour() {
		return outHour;
	}
	public void setOutHour(Integer outHour) {
		this.outHour = outHour;
	}
	public Integer getMinHour() {
		return minHour;
	}
	public void setMinHour(Integer minHour) {
		this.minHour = minHour;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getDecreasPrice() {
		return decreasPrice;
	}
	public void setDecreasPrice(BigDecimal decreasPrice) {
		this.decreasPrice = decreasPrice;
	}
	public BigDecimal getMinimumPrice() {
		return minimumPrice;
	}
	public void setMinimumPrice(BigDecimal minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryFileId() {
		return categoryFileId;
	}
	public void setCategoryFileId(String categoryFileId) {
		this.categoryFileId = categoryFileId;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public BigDecimal getLeaseDeposit() {
		return leaseDeposit;
	}
	public void setLeaseDeposit(BigDecimal leaseDeposit) {
		this.leaseDeposit = leaseDeposit;
	}
	public BigDecimal getLeaseUnit() {
		return leaseUnit;
	}
	public void setLeaseUnit(BigDecimal leaseUnit) {
		this.leaseUnit = leaseUnit;
	}
	public BigDecimal getTopBalance() {
		return topBalance;
	}
	public void setTopBalance(BigDecimal topBalance) {
		this.topBalance = topBalance;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
