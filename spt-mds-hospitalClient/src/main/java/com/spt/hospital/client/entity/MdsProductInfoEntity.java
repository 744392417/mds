package com.spt.hospital.client.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：设备信息表
 * */
@Entity
@Table(name = "tbl_product_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsProductInfoEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 设备ID */
	private String productCode;
	/* 设备名称 */
	private String productName;
	/* 设备二维码 */
	private String productQrcode;
	/* 设备兼容状态 ： 1 默认 ,2异常 ,3低电量 ,4余额不足,5报修  */
	private String productStatus="1";
	/* 设备使用状态 ：1备选 ,3 暂停,  2 空闲, 4 使用中, 5  维修中 */
	private String productUseStatus="3";
	/* 设备版本号 */
	private String productVersion;
	/* 类别ID */
	private Long categoryId;
	/* 类别图片ID */
	private String fileId;
	/* 类别code */
	private String categoryCode;
	/* 类别名称 */
	private String categoryName;
	/* 医院ID */
	private Long hospitalId;
	/* 医院编号 */
	private String hospitalCode;
	/* 医院名称 */
	private String hospitalName;
	/* 医院科室 */
	private String hospitalDepart;
	/* 医院病房 */
	private String hospitalWard;
	/* 原因 */
	private String reason;
	/* 备注 */
	private String remark;
	/* 请求升级时间 */
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date requestUpgradeDate;
	/* 成功升级时间 */
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date successUpgradeDate;
	//二维码ID 数字类型
	private Long serial;
	

	public Long getSerial() {
		return serial;
	}
	public void setSerial(Long serial) {
		this.serial = serial;
	}
	public String getHospitalDepart() {
		return hospitalDepart;
	}
	public String getHospitalWard() {
		return hospitalWard;
	}
	public void setHospitalWard(String hospitalWard) {
		this.hospitalWard = hospitalWard;
	}
	public void setHospitalDepart(String hospitalDepart) {
		this.hospitalDepart = hospitalDepart;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductQrcode() {
		return productQrcode;
	}
	public void setProductQrcode(String productQrcode) {
		this.productQrcode = productQrcode;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getProductVersion() {
		return productVersion;
	}
	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}
	public Date getRequestUpgradeDate() {
		return requestUpgradeDate;
	}
	public void setRequestUpgradeDate(Date requestUpgradeDate) {
		this.requestUpgradeDate = requestUpgradeDate;
	}
	public Date getSuccessUpgradeDate() {
		return successUpgradeDate;
	}
	public void setSuccessUpgradeDate(Date successUpgradeDate) {
		this.successUpgradeDate = successUpgradeDate;
	}
	public String getProductUseStatus() {
		return productUseStatus;
	}
	public void setProductUseStatus(String productUseStatus) {
		this.productUseStatus = productUseStatus;
	}
}
