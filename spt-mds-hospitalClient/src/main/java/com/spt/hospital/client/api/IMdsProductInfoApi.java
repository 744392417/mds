package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.ReceiveProductVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;
import com.spt.hospital.client.vo.product.ProductInfoByStoreManagerVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME
		+ "/api/mdsProductInfo", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsProductInfoApi extends BaseClient<MdsProductInfoEntity> 
{
	@PostMapping("findByCode")
	public MdsProductInfoEntity findByCode(@RequestBody QueryVo vo);
	
	@PostMapping("findByCodeAndId")
	public MdsProductInfoEntity findByCodeAndId(@RequestBody QueryVo vo);
	
	@PostMapping("findByQrcode")
	public MdsProductInfoVo findByQrcode(@RequestBody QueryVo vo);
	
	@PostMapping("findProductInfoListPage")
	public PageDown<ProductInfoByCategoryVo> findProductInfoListPage(@RequestBody QueryVo vo);
	
	@PostMapping("sendVersionUpgradeNews")
	public void sendVersionUpgradeNews(@RequestBody ReceiveProductVo receiveProductVo);
	
	@PostMapping("findProductListByStore")
	public PageDown<ProductInfoByStoreManagerVo> findProductListByStore(@RequestBody QueryVo vo);
	
	@PostMapping("findProductInfoByQrcode")
	public MdsProductInfoEntity findProductInfoByQrcode(@RequestBody QueryVo vo);
	
	@PostMapping("batchReceiveProductStatus")
	public void batchReceiveProductStatus(@RequestBody QueryVo vo);
	
//	@PostMapping("moeys")
//	public MdsHospitalPriceEntity moeys(@RequestBody Long id);
	
	@PostMapping("undistributedList")
	public List<MdsProductInfoEntity> undistributedList(@RequestBody QueryVo vo);
	@PostMapping("allocatedList")
	public List<MdsProductInfoEntity> allocatedList(@RequestBody QueryVo vo);
	@PostMapping("upDatestatus")
	public void upDatestatus(@RequestBody QueryVo vo);
	
	@PostMapping("getByHospitalId")
	public MdsProductInfoEntity getByHospitalId(@RequestBody QueryVo vo);
	
	@PostMapping("findByHospitalId")
	public List<MdsProductInfoEntity> findByHospitalId(@RequestBody QueryVo vo);
	
	
	@PostMapping("getByHospitalId")
	public int findByhospitalId(@RequestBody Long hospitalId);
	
	@PostMapping("findProductUseStatusByhospitalId")
	public List<MdsProductInfoEntity> findProductUseStatusByhospitalId(@RequestBody MdsProductInfoVo vo);
	
	@PostMapping("mdsProductStatus")
	public void mdsProductStatus(@RequestBody QueryVo vo);
	
	
}
