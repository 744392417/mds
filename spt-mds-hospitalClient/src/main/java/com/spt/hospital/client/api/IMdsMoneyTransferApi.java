package com.spt.hospital.client.api;

import org.springframework.cloud.openfeign.FeignClient;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsMoneyTransferEntity;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME
		+ "/api/mdsMoneyTransfer", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsMoneyTransferApi extends BaseClient<MdsMoneyTransferEntity> {

}
