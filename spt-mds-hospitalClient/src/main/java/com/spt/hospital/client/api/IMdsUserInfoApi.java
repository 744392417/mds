package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.shiro.ShiroUser;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.user.MdsUserInfoMngVo;


@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME	+ "/api/mdsUserInfo", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsUserInfoApi extends BaseClient<MdsUserInfoEntity> 
{
	@PostMapping("login")
	public ShiroUser login(@RequestBody MdsUserInfoVo mdsUserInfoVo) throws ApplicationException;
	
	@PostMapping(value = "logout")
	public void logout(@RequestBody Long userId) throws ApplicationException;

	@PostMapping("findPageByUserInfoMng")
	public PageDown	<MdsUserInfoMngVo> findPageByUserInfoMng(@RequestBody QueryVo vo);
	
	@PostMapping("findPageByUser")
	public PageDown	<MdsUserInfoMngVo> findPageByUser(@RequestBody QueryVo vo);
	
	@PostMapping("findUserInfoDetail")
	public MdsUserInfoMngVo findUserInfoDetail(@RequestBody QueryVo vo);
	
	@PostMapping("findByToken")
	public MdsUserInfoEntity findByToken(@RequestBody String token);

	@PostMapping("updateHospRole")
	public void updateHospRole(@RequestBody MdsUserInfoVo mdsUserInfoVo);
	
	@PostMapping("updateUserLoginInfo")
	public void updateUserLoginInfo(@RequestBody MdsUserInfoVo mdsUserInfoVo);

	@PostMapping("updateStatus")
	public void updateStatus(@RequestBody MdsUserInfoVo mdsUserInfoVo);
	
	@PostMapping("findByOpenid")
	public MdsUserInfoEntity findByOpenid(@RequestBody String openid);
	
	@PostMapping("findListByOpenid")
	public List<MdsUserInfoEntity> findListByOpenid(@RequestBody String openid);
	
	@PostMapping("findByloginName")
	public MdsUserInfoEntity findByloginName(@RequestBody QueryVo queryVo);
	
	@PostMapping("verifyUserName")
	public MdsUserInfoEntity verifyUserName(@RequestBody QueryVo queryVo);
	
	@PostMapping("updatePhone")
	public  void updatePhone(@RequestBody MdsUserInfoVo mdsUserInfoVo);
}
