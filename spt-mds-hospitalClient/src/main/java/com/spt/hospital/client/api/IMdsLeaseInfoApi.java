package com.spt.hospital.client.api;


import java.math.BigDecimal;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.QueryVo;


@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME
+ "/api/mdsLeaseInfo", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsLeaseInfoApi extends BaseClient<MdsLeaseInfoEntity> {

	@PostMapping("findByOpenid")
	public PageDown<MdsLeaseInfoVo> findByOpenid(@RequestBody QueryVo vo);
	
	@PostMapping("findMdsLeaseInfoPage")
	public PageDown<MdsLeaseInfoVo> findMdsLeaseInfoPage(@RequestBody QueryVo queryVo);

	@PostMapping("findLeaseInfoPageByProperty")
	public PageDown<MdsLeaseInfoVo> findLeaseInfoPageByProperty(QueryVo queryVo);
	
	@PostMapping("saveMdsLeaseInfo")
	public void saveMdsLeaseInfo(@RequestBody MdsLeaseInfoVo vo);
	
	@PostMapping("findLeaseByOrderNumber")
	public List<MdsLeaseInfoEntity> findLeaseByOrderNumber(@RequestBody QueryVo queryVo);
	
	@PostMapping("returnLeaseInfo")
	public void returnLeaseInfo(@RequestBody MdsLeaseInfoEntity entity);
	
	@PostMapping("findListByOpenid")
	public List<MdsLeaseInfoEntity> findListByOpenid(@RequestBody QueryVo vo);
	
	@PostMapping("findLeaseInfoByOrderNumber")
	public MdsLeaseInfoVo findLeaseInfoByOrderNumber(@RequestBody QueryVo vo);
	
	@PostMapping("findByOrderNumber")
	public MdsLeaseInfoEntity findByOrderNumber(@RequestBody QueryVo vo);
	
	@PostMapping("info")
	public MdsLeaseInfoEntity info(@RequestBody Long productId);
	
	@PostMapping("findInuseByProductCode")
	public List<MdsLeaseInfoEntity> findInuseByProductCode(@RequestBody QueryVo queryVo);
	
	@PostMapping("findByCodeAndId")
	public List<MdsLeaseInfoEntity> findByCodeAndId(@RequestBody QueryVo queryVo);
	
	@PostMapping("findOrderNumber")
	public MdsLeaseInfoEntity findOrderNumber(@RequestBody String orderNumber);
	
	
	@PostMapping("categoryNumber")
	public Long categoryNumber(@RequestBody String openid);
	@PostMapping("orderNumber")
	public Long orderNumber(@RequestBody String openid);
	@PostMapping("prepareCost")
	public BigDecimal prepareCost(@RequestBody String openid);
	@PostMapping("leaseCost")
	public BigDecimal leaseCost( String openid);
	

}
