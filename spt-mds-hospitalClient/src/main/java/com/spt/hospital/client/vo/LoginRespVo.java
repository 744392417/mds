package com.spt.hospital.client.vo;

public class LoginRespVo {

	private String ukRealSerialNo;//uk序列号
	private String cfcaStatus;//cfca开户状态1已开户，0或空未开户
    
    private boolean registStatus = false;//登录状态
    private String message;
    private String errorId;
    
	public String getUkRealSerialNo() {
		return ukRealSerialNo;
	}
	public void setUkRealSerialNo(String ukRealSerialNo) {
		this.ukRealSerialNo = ukRealSerialNo;
	}
	public String getCfcaStatus() {
		return cfcaStatus;
	}
	public void setCfcaStatus(String cfcaStatus) {
		this.cfcaStatus = cfcaStatus;
	}
	public boolean isRegistStatus() {
		return registStatus;
	}
	public void setRegistStatus(boolean registStatus) {
		this.registStatus = registStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getErrorId() {
		return errorId;
	}
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
    
}
