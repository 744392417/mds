package com.spt.hospital.client.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：设备类别表
 * */
@Entity
@Table(name = "tbl_product_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsProductCategoryEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/*设备类别图片*/
	private String categoryFileId;
	/* 类别code*/
	private String categoryCode; 
	/* 类别名称*/
	private String categoryName; 
	/* 状态*/
	private String status = "1"; 
	/* 备注*/
	private String remark; 
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCategoryFileId() {
		return categoryFileId;
	}
	public void setCategoryFileId(String categoryFileId) {
		this.categoryFileId = categoryFileId;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
