
package com.spt.hospital.client.api;

import java.math.BigDecimal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.QueryVo;


@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME
		+ "/api/mdsRecord", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsRecordsConptionApi extends BaseClient<MdsRecordsConptionEntity> {

	@PostMapping("findByOpenid")
	public PageDown<MdsRecordsConptionEntity> findByOpenid(@RequestBody QueryVo vo);

	@PostMapping("findPageMdsRecords")
	public PageDown<MdsRecordsConptionEntity> findPageMdsRecords(@RequestBody QueryVo queryVo);
	
	@PostMapping("findRecordsPage")
	public PageDown<MdsRecordsConptionVo> findRecordsPage(@RequestBody QueryVo queryVo);

	@PostMapping("sumAmount")
	public BigDecimal sumAmount(@RequestBody QueryVo queryVo);

	@PostMapping("findRecordsPageByUser")
	public PageDown<MdsRecordsConptionVo> findRecordsPageByUser(@RequestBody QueryVo queryVo);
	
	@PostMapping("saveMdsRecords")
	public void saveMdsRecords(@RequestBody MdsRecordsConptionEntity en);
	
	@PostMapping("findByIOrderNumber")
	public MdsRecordsConptionEntity findByIOrderNumber(@RequestBody QueryVo vo);

}
