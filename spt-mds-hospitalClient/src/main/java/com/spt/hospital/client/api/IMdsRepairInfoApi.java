package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME	
		+ "/api/mdsRepairInfo", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsRepairInfoApi extends BaseClient<MdsRepairInfoEntity> 
{
	@PostMapping("findByProductCode")
	MdsRepairInfoEntity findByProductCode(@RequestBody QueryVo vo);
	@PostMapping("findListByProductCode")
	List<MdsRepairInfoEntity> findListByProductCode(@RequestBody String productCode);
	
	@PostMapping("findByOrderNumber")
	List<MdsRepairInfoEntity> findByOrderNumber(@RequestBody QueryVo vo);
}
