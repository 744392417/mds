package com.spt.hospital.client.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：租赁信息列表
 */
@Entity
@Table(name = "tbl_lease_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsLeaseInfoEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 订单编号*/
	private String orderNumber; 
	/* 订单状态*/
	private String orderStatus = "0"; //订单状态  0 使用中    1 已完成     2待付款   4 报修中 5余额不足   
	/* 微信用户ID*/
	private String openid; 
	/* 医院ID*/
	private Long hospitalId; 
	/* 医院名称*/
	private String hospitalName; 
	/* 科室*/
	private String hospitalDepart; 
	/* 类别ID*/
	private Long categoryId; 
	/* 类别名称*/
	private String categoryName; 
	/* 设备ID*/
	private Long productId; 
	/* 设备名称*/
	private String productName; 
	/* 设备code*/
	private String productCode; 
	/* 设备二维码*/
	private String productQrcode;
	/* 设备状态*/
	private String productStatus; 
	/* 设备使用状态 ：1备选 2 空闲 3 暂停 4 使用中 5 报修 */
	private String productUseStatus;
	/* 设备数量*/
	private Integer productNumber = 1;
	
	/* 医院病房 */
	private String hospitalWard;

	/* 租赁时长*/
	private Long leaseTime; 
	/* 租赁费用(收入)*/
	private BigDecimal leaseCost = new BigDecimal(0.00); 
	/* 预付款*/
	private BigDecimal leaseDeposit = new BigDecimal(0.00); 
	/* 退还*/
	private BigDecimal intoBalance = new BigDecimal(0.00); 
	/* 支付系统W 微信支付*/
	private String paySystem = "WX"; 
	/* 消费状态 预付款S 退款 I 收入P*/
	private String consumStatus;
	/* 返佣类型：医院H,医院+物业HP,医院+代理HA,自营ALL*/
	private String distributionType;
	/* 医院返佣比例*/
	private Integer hospitalDistribution = 0;
	/* 医院返佣金额*/
	private BigDecimal hospitalFashion = new BigDecimal(0.00);
	/* 所属物业*/
	private String properName;
	/* 物业ID*/
	private String properId;
	/* 物业返佣比例*/
	private Integer properDistribution = 0;
	/* 物业返佣金额*/
	private BigDecimal properFashion = new BigDecimal(0.00);
	/* 所属代理商*/
	private String agentName;
	/* 代理商ID*/
	private String agentId;
	/* 代理商返佣比例*/
	private Integer agentDistribution = 0;
	/* 代理商返佣金额*/
	private BigDecimal agentFashion = new BigDecimal(0.00);
	/* 描述*/
	private String orderDescribe; 
	/* 预付款流水(平台)*/
	private String platIncomeNumber; 
	/* 预付款流水(微信支付订单号)*/
	private String wxIncomeNumber; 
	/* 预付款状态(微信返回成功失败)*/
	private String incomeSatus; 
	/* 退款流水(平台)*/
	private String platSpendNumber; 
	/* 退款状态(微信返回成功失败)*/
	private String wxSpendStatus; 
	/* remark*/
	private String remark; 
	/* 平台收入分配比例*/
	private Integer platDistribution = 0;
	/* 平台收入分配金额*/
	private BigDecimal platFashion = new BigDecimal(0.00);
	/* 退款流水(WX)*/
	private String wxSpendNumber; 
	/* 开锁密码*/
	private String unlockPwd;
	/* 关锁密码*/
	private String shutPwd;
	
	private String keyId;
	
	/* 用户手机 */
	private String userPhone;
	
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	/* 租赁开始时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseStartDate; 
	
	/* 租赁结束时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseEndDate; 
	
	/* 预付款交易时间(平台发起)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date platIncomeDate;
	
	/* 预付款交易时间(微信返回)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date wxIncomeDate; 
	
	/* 退款时间(平台发起)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date platSpendDate; 
	
	/* 退款交易时间(微信返回)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date wxSpendDate; 

	
	
	public String getHospitalWard() {
		return hospitalWard;
	}
	public void setHospitalWard(String hospitalWard) {
		this.hospitalWard = hospitalWard;
	}
	
	public String getPaySystem() {
		return paySystem;
	}
	public void setPaySystem(String paySystem) {
		this.paySystem = paySystem;
	}
	public String getUnlockPwd() {
		return unlockPwd;
	}
	public void setUnlockPwd(String unlockPwd) {
		this.unlockPwd = unlockPwd;
	}
	public String getShutPwd() {
		return shutPwd;
	}
	public void setShutPwd(String shutPwd) {
		this.shutPwd = shutPwd;
	}
	public BigDecimal getHospitalFashion() {
		return hospitalFashion;
	}
	public void setHospitalFashion(BigDecimal hospitalFashion) {
		this.hospitalFashion = hospitalFashion;
	}
	public BigDecimal getProperFashion() {
		return properFashion;
	}
	public void setProperFashion(BigDecimal properFashion) {
		this.properFashion = properFashion;
	}
	public BigDecimal getAgentFashion() {
		return agentFashion;
	}
	public void setAgentFashion(BigDecimal agentFashion) {
		this.agentFashion = agentFashion;
	}
	public Integer getPlatDistribution() {
		return platDistribution;
	}
	public void setPlatDistribution(Integer platDistribution) {
		this.platDistribution = platDistribution;
	}
	public BigDecimal getPlatFashion() {
		return platFashion;
	}
	public void setPlatFashion(BigDecimal platFashion) {
		this.platFashion = platFashion;
	}
	public String getDistributionType() {
		return distributionType;
	}
	public void setDistributionType(String distributionType) {
		this.distributionType = distributionType;
	}
	public String getProperName() {
		return properName;
	}
	public void setProperName(String properName) {
		this.properName = properName;
	}
	public String getProperId() {
		return properId;
	}
	public void setProperId(String properId) {
		this.properId = properId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Integer getHospitalDistribution() {
		return hospitalDistribution;
	}
	public void setHospitalDistribution(Integer hospitalDistribution) {
		this.hospitalDistribution = hospitalDistribution;
	}
	public Integer getProperDistribution() {
		return properDistribution;
	}
	public void setProperDistribution(Integer properDistribution) {
		this.properDistribution = properDistribution;
	}
	public Integer getAgentDistribution() {
		return agentDistribution;
	}
	public void setAgentDistribution(Integer agentDistribution) {
		this.agentDistribution = agentDistribution;
	}
	public Date getWxIncomeDate() {
		return wxIncomeDate;
	}
	public void setWxIncomeDate(Date wxIncomeDate) {
		this.wxIncomeDate = wxIncomeDate;
	}
	public String getIncomeSatus() {
		return incomeSatus;
	}
	public void setIncomeSatus(String incomeSatus) {
		this.incomeSatus = incomeSatus;
	}
	public Date getWxSpendDate() {
		return wxSpendDate;
	}
	public void setWxSpendDate(Date wxSpendDate) {
		this.wxSpendDate = wxSpendDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public String getWxSpendStatus() {
		return wxSpendStatus;
	}
	public void setWxSpendStatus(String wxSpendStatus) {
		this.wxSpendStatus = wxSpendStatus;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalDepart() {
		return hospitalDepart;
	}
	public void setHospitalDepart(String hospitalDepart) {
		this.hospitalDepart = hospitalDepart;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductQrcode() {
		return productQrcode;
	}
	public void setProductQrcode(String productQrcode) {
		this.productQrcode = productQrcode;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public Integer getProductNumber() {
		return productNumber;
	}
	public void setProductNumber(Integer productNumber) {
		this.productNumber = productNumber;
	}
	public Date getLeaseStartDate() {
		return leaseStartDate;
	}
	public void setLeaseStartDate(Date leaseStartDate) {
		this.leaseStartDate = leaseStartDate;
	}
	public Date getLeaseEndDate() {
		return leaseEndDate;
	}
	public void setLeaseEndDate(Date leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	}
	public Long getLeaseTime() {
		return leaseTime;
	}
	public void setLeaseTime(Long leaseTime) {
		this.leaseTime = leaseTime;
	}
	public BigDecimal getLeaseCost() {
		return leaseCost;
	}
	public void setLeaseCost(BigDecimal leaseCost) {
		this.leaseCost = leaseCost;
	}
	public BigDecimal getLeaseDeposit() {
		return leaseDeposit;
	}
	public void setLeaseDeposit(BigDecimal leaseDeposit) {
		this.leaseDeposit = leaseDeposit;
	}
	public BigDecimal getIntoBalance() {
		return intoBalance;
	}
	public void setIntoBalance(BigDecimal intoBalance) {
		this.intoBalance = intoBalance;
	}
	public String getConsumStatus() {
		return consumStatus;
	}
	public void setConsumStatus(String consumStatus) {
		this.consumStatus = consumStatus;
	}
	public String getPlatIncomeNumber() {
		return platIncomeNumber;
	}
	public void setPlatIncomeNumber(String platIncomeNumber) {
		this.platIncomeNumber = platIncomeNumber;
	}
	public String getWxIncomeNumber() {
		return wxIncomeNumber;
	}
	public void setWxIncomeNumber(String wxIncomeNumber) {
		this.wxIncomeNumber = wxIncomeNumber;
	}
	public Date getPlatIncomeDate() {
		return platIncomeDate;
	}
	public void setPlatIncomeDate(Date platIncomeDate) {
		this.platIncomeDate = platIncomeDate;
	}
	public Date getPlatSpendDate() {
		return platSpendDate;
	}
	public void setPlatSpendDate(Date platSpendDate) {
		this.platSpendDate = platSpendDate;
	}
	public String getPlatSpendNumber() {
		return platSpendNumber;
	}
	public void setPlatSpendNumber(String platSpendNumber) {
		this.platSpendNumber = platSpendNumber;
	}
	public String getWxSpendNumber() {
		return wxSpendNumber;
	}
	public void setWxSpendNumber(String wxSpendNumber) {
		this.wxSpendNumber = wxSpendNumber;
	}
	public String getOrderDescribe() {
		return orderDescribe;
	}
	public void setOrderDescribe(String orderDescribe) {
		this.orderDescribe = orderDescribe;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getProductUseStatus() {
		return productUseStatus;
	}
	public void setProductUseStatus(String productUseStatus) {
		this.productUseStatus = productUseStatus;
	}
}