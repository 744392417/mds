/*package com.spt.credit.client.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hsoft.commutil.cache.LocalCacheManager;
import com.hsoft.commutil.easyui.EasyTreeNode;
import com.hsoft.commutil.util.SpringContextHolder;
import com.spt.credit.client.api.IBsProductTypeApi;
import com.spt.credit.client.entity.BsProductType;

public class ProductTypeUtility {
	
	private final static Logger log = LoggerFactory.getLogger(ProductTypeUtility.class);

	private static LoadingCache<String, List<BsProductType>> productTypeCache;
	
	private static String typeCdCache="typeCdCache";
	
	public static void init() {
		productTypeCache = CacheBuilder.newBuilder().refreshAfterWrite(10, TimeUnit.MINUTES)
				.build(new CacheLoader<String, List<BsProductType>>() {
					@Override
					public List<BsProductType> load(String parentTypeCode) throws Exception {
						IBsProductTypeApi service = SpringContextHolder.getBean(IBsProductTypeApi.class);
						return service.findAll();
					}
				});
		LocalCacheManager.register(productTypeCache);
	}
	
	public static void refresh(String category) {
		productTypeCache.refresh(category);
	    log.info("refresh {} finished.", category);
	}
	*//**
	 * 清空缓存
	 *//*
	public static void cleanUp() {
		if (productTypeCache != null) {
			productTypeCache.cleanUp();
		}
	}
	*//**
	 * 查询所有节点，只有根级节点+最孩子节点
	 * @return
	 *//*
	public static List<EasyTreeNode> getAllTreeNode() {
		// TODO Auto-generated method stub
		List<EasyTreeNode> nodes = new ArrayList<EasyTreeNode>();
		Map<String,List<BsProductType>> typeCdMap = getTypeCdMap();
		for(String cd:typeCdMap.keySet()){
			List<BsProductType> list = typeCdMap.get(cd);
			for(BsProductType type:list){
				if(cd.equals(type.getTypeCode())){
					//根节点
					EasyTreeNode node = new EasyTreeNode();
					node.setId(cd);
					node.setText(type.getTypeName());
					node.setChildren(rtnListChildren(list));
					nodes.add(node);
				}
			}
			
		}
			
			
		
		return nodes;
	}
	
	
	*//**
	 * 查询某父类下的所有孩子
	 * @param parentTypeCode
	 * @param level
	 * @throws ExecutionException 
	 *//*
	public static List<EasyTreeNode> getAllChilren(List<BsProductType> parentList){
		List<EasyTreeNode> nodes = new ArrayList<EasyTreeNode>();
		try {
			for(BsProductType parent:parentList){
				List<BsProductType> typeList = productTypeCache.get(typeCdCache);
				Map<Integer,List<BsProductType>> map = getLevelMap(typeList);
				nodes.add(getTreeNode(map,parent));
			}
			
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nodes;
		
	}
	
	public static EasyTreeNode getParent(String chilrenTypeCode){
		EasyTreeNode node = new EasyTreeNode();
		try {
			List<BsProductType> allList= productTypeCache.get(typeCdCache);
			Map<Integer,List<BsProductType>> map = getLevelMap(allList);
			//获得父类序号 最顶级的父类层级为零
			int curLevel=0;
			//获得所有的顶级父类列表
			List<BsProductType> parentList= map.get(curLevel);
			if(parentList!=null){
				for(BsProductType entity:parentList){ 
					String parentTypeCode=entity.getTypeCode();
					if(chilrenTypeCode.contains(parentTypeCode)){
						node.setId(String.valueOf(entity.getId()));
						node.setText(entity.getTypeName());
						break;
						//只获得一个父类
					}
				}
			}
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return node;
	}
	
	public static EasyTreeNode getChilren(BsProductType parent){
		EasyTreeNode node = new EasyTreeNode();
		try {
			List<BsProductType> typeList = productTypeCache.get(parent.getTypeCode());
			Map<Integer,List<BsProductType>> map = getLevelMap(typeList);
			node=getTreeNode(map,parent);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return node;
		
	}
	*//**
	 * 将缓存的所有商品类型按层级分类
	 * @param typeList
	 * @return
	 *//*
	private static Map<Integer,List<BsProductType>> getLevelMap(List<BsProductType> typeList){
		Map<Integer,List<BsProductType>> rtn = new HashMap<Integer,List<BsProductType>>();
		for(BsProductType type: typeList){
			Integer level = type.getLevel();
			List<BsProductType> typeLevelList = rtn.get(level);
			if(typeLevelList==null){
				typeLevelList = new ArrayList<BsProductType>();
			}
			typeLevelList.add(type);
			rtn.put(level, typeLevelList);
		}
		return rtn;
	}
	
	private static EasyTreeNode getTreeNode(Map<Integer,List<BsProductType>> map,BsProductType parent){
		EasyTreeNode node = new EasyTreeNode();
		node.setText(parent.getTypeName());
		String parentTypeCode = parent.getTypeCode();
		node.setId(String.valueOf(parentTypeCode));
		List<EasyTreeNode> children = new ArrayList<EasyTreeNode>();
		Integer curLevel = parent.getLevel()+1;
		List<BsProductType> levelList = map.get(curLevel);
		if(levelList!=null){
			for(BsProductType type: levelList){
				String typeCode = type.getTypeCode();
	            if(typeCode.contains(parentTypeCode)){
	            	EasyTreeNode childrenNode = getTreeNode(map,type);
	            	children.add(childrenNode);
	            }
			}
		}
		node.setChildren(children);
		
		return node;
	}

	public static List<EasyTreeNode> getAllTree() {
		// TODO Auto-generated method stub
		List<EasyTreeNode> nodes = new ArrayList<EasyTreeNode>();
		try {
			List<BsProductType> typeList = productTypeCache.get("productTypeCache");
			Map<Integer,List<BsProductType>> map = getLevelMap(typeList);
			List<BsProductType> parentList = map.get(0);
			if(parentList!=null){
				for(BsProductType parent:parentList){
					nodes.add(getTreeNode(map,parent));
				}
			}
			
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nodes;
	}
    //获取parentCode下所有的孩子节点
	public static List<EasyTreeNode> findListChildren(String parentCode){
		List<BsProductType> typeList = getTypeCdMap().get(parentCode);
		return rtnListChildren(typeList);
		
	}
	public static List<EasyTreeNode> rtnListChildren(List<BsProductType> typeList){
		List<EasyTreeNode> rtnList = new ArrayList<EasyTreeNode>();
		for(BsProductType type: typeList){
			String compareType = type.getTypeCode();
			if(!haveChildrenNode(compareType,typeList)){
				EasyTreeNode node = new EasyTreeNode();
				node.setId(compareType);
				node.setText(type.getTypeName());
				rtnList.add(node);
			}
		}
		return rtnList;
	}
	*//**
	 * 判断是否有孩子节点
	 * @param typeCd
	 * @param list
	 * @return
	 *//*
	private static boolean haveChildrenNode(String typeCd,List<BsProductType> list){
		boolean flg = false;
		for(BsProductType pt:list){
			String compareCode = pt.getTypeCode();
			if(!typeCd.equals(compareCode)&&pt.getTypeCode().contains(typeCd)){
				flg = true;
				break;
			}
		}
		return flg;
	}
	*//**
	 * 将所有的商品类型按父节点分类
	 * @param typeList
	 * @return
	 *//*
	private static Map<String,List<BsProductType>> getTypeCdMap(){
		Map<String,List<BsProductType>> rtn = new HashMap<String,List<BsProductType>>();
		List<BsProductType> typeList = new ArrayList<BsProductType>();
		try {
			typeList = productTypeCache.get(typeCdCache);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(BsProductType type: typeList){
			String typeCode = type.getTypeCode();
			String[] str = typeCode.split("_");
			String indexStr = str[0];
			List<BsProductType> typeLevelList = rtn.get(indexStr);
			if(typeLevelList==null){
				typeLevelList = new ArrayList<BsProductType>();
			}
			typeLevelList.add(type);
			rtn.put(indexStr, typeLevelList);
		}
		return rtn;
	}
}
*/