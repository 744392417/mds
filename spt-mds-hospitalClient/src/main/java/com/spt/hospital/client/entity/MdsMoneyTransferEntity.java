package com.spt.hospital.client.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：资金划转表
 * */
@Entity
@Table(name = "tbl_money_transfer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsMoneyTransferEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 医院/代理商/物业 ID*/
	private Long hospitalId; 
	/* 医院/代理商/物业 编号*/
	private String hospitalCode; 
	/* 医院/代理商/物业 名称*/
	private String hospitalName; 
	/* 医院/代理商/物业 账户类型*/
	private String bankCode; 
	/* 医院/代理商/物业 账户名称*/
	private String bankName; 
	/* 医院/代理商/物业资金账号*/
	private String capitalAccount; 
	/* 医院/代理商/物业 开户行*/
	private String openBankAccount; 
	/* 收入分配(返佣比例)*/
	private Integer incomeDistrib; 
	/* 金额*/
	private BigDecimal transferAmount = new BigDecimal(0.00); 
	/* 平台划转流水*/
	private String platSettlement; 
	/* 银行流水*/
	private String bankSettlement; 
	
	/* 转账日期*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date transferDate; 
	
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCapitalAccount() {
		return capitalAccount;
	}
	public void setCapitalAccount(String capitalAccount) {
		this.capitalAccount = capitalAccount;
	}
	public String getOpenBankAccount() {
		return openBankAccount;
	}
	public void setOpenBankAccount(String openBankAccount) {
		this.openBankAccount = openBankAccount;
	}
	public Integer getIncomeDistrib() {
		return incomeDistrib;
	}
	public void setIncomeDistrib(Integer incomeDistrib) {
		this.incomeDistrib = incomeDistrib;
	}
	public BigDecimal getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}
	public Date getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
	public String getPlatSettlement() {
		return platSettlement;
	}
	public void setPlatSettlement(String platSettlement) {
		this.platSettlement = platSettlement;
	}
	public String getBankSettlement() {
		return bankSettlement;
	}
	public void setBankSettlement(String bankSettlement) {
		this.bankSettlement = bankSettlement;
	}
}
