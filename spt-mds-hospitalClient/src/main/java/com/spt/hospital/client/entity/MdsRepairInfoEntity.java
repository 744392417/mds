package com.spt.hospital.client.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;
/**
 * @author hanson
 * 实体：报修列表
 * */
@Entity
@Table(name = "tbl_repair_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsRepairInfoEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/*报修单编号*/
	private String repairNumber;
	/*订单编号*/
	private String orderNumber;

	/* 微信id*/
	private String openid; 
	/* 设备类别ID*/
	private Long   categoryId; 
	/* 类别code*/
	private String categoryCode; 
	/* 类别名称*/
	private String categoryName; 
	/*门店ID*/
	private String propertyId;
	/*设备ID*/
	private String productId;
	/*设备编码*/
	private String productCode;
	/*设备名称*/
	private String productName;
	/* 图片ID*/
	private String   fileId; 
	/* 医院ID*/
	private Long   hospitalId; 
	/* 医院名称*/
	private String hospitalName;
	/* 医院地址*/
	private String hospitalAddress;
	/* 医院部门*/
	private String hospitalDepart;
	/* 医院病房 */
	private String hospitalWard;
	/*描述*/
	private String remark; 

	/* 状态 1 解决 0 未解决*/
	private String repairStatus = "0"; 
	/* 原因*/
	private String reason; 
	
	public String getServiceReason() {
		return serviceReason;
	}
	public void setServiceReason(String serviceReason) {
		this.serviceReason = serviceReason;
	}
	private String serviceReason; 
	 

	/* 手机*/
	private String mobilePhone; 
	
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/*报修时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date repairDate;
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getRepairNumber() {
		return repairNumber;
	}
	public void setRepairNumber(String repairNumber) {
		this.repairNumber = repairNumber;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	
	
	public String getHospitalWard() {
		return hospitalWard;
	}
	public void setHospitalWard(String hospitalWard) {
		this.hospitalWard = hospitalWard;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getHospitalDepart() {
		return hospitalDepart;
	}
	public void setHospitalDepart(String hospitalDepart) {
		this.hospitalDepart = hospitalDepart;
	}
	public Date getRepairDate() {
		return repairDate;
	}
	public void setRepairDate(Date repairDate) {
		this.repairDate = repairDate;
	}
	public String getRepairStatus() {
		return repairStatus;
	}
	public void setRepairStatus(String repairStatus) {
		this.repairStatus = repairStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}