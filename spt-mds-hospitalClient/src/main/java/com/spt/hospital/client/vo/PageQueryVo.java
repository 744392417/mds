package com.spt.hospital.client.vo;

import com.hsoft.commutil.web.PageSearchVo;

public class PageQueryVo extends PageSearchVo{

	String bankName;

	String companyName;
	
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

}
