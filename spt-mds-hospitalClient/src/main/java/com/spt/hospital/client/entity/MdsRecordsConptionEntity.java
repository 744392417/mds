package com.spt.hospital.client.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：资金流水
 */
@Entity
@Table(name = "tbl_records_conption")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsRecordsConptionEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 订单编号*/
	private String orderNumber; 
	/* WX账号*/
	private String openid; 
	/* 支付系统*/
	private String paySystem; 
	/* 支付金额*/
	private BigDecimal leaseCost = new BigDecimal(0.00); 
	/* 消费状态状态 预付款S 退款 I 收入P , 分账 F*/
	private String consumStatus; 
	/* 平台资金收入流水*/
	private String platPayNumber; 
	/* 收入流水(WX返回)*/
	private String wxPayNumber; 
	/* 平台收入状态(微信返回成功失败)*/
	private String incomeSatus; 
	/* 物业ID*/
	private Long hospitalId;
	/* 医院编号/代理商编号/物业编号*/
	private String hospitalCode;
	/* 医院编号/代理商编号/物业名称*/
	private String hospitalName;
	/* 订单描述*/
	private String orderDescribe; 
	/*医院H 物业 P 代理商 A 平台M*/
	private String type;
	/* 备注*/
	private String remark; 
	
	/* 收入时间(平台发起)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date platPayDate; 
	/* 收入时间(WX返回)*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date wxPayDate; 

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getPaySystem() {
		return paySystem;
	}
	public void setPaySystem(String paySystem) {
		this.paySystem = paySystem;
	}
	public BigDecimal getLeaseCost() {
		return leaseCost;
	}
	public void setLeaseCost(BigDecimal leaseCost) {
		this.leaseCost = leaseCost;
	}
	public String getConsumStatus() {
		return consumStatus;
	}
	public void setConsumStatus(String consumStatus) {
		this.consumStatus = consumStatus;
	}
	public String getPlatPayNumber() {
		return platPayNumber;
	}
	public void setPlatPayNumber(String platPayNumber) {
		this.platPayNumber = platPayNumber;
	}
	public Date getPlatPayDate() {
		return platPayDate;
	}
	public void setPlatPayDate(Date platPayDate) {
		this.platPayDate = platPayDate;
	}
	public String getWxPayNumber() {
		return wxPayNumber;
	}
	public void setWxPayNumber(String wxPayNumber) {
		this.wxPayNumber = wxPayNumber;
	}
	public Date getWxPayDate() {
		return wxPayDate;
	}
	public void setWxPayDate(Date wxPayDate) {
		this.wxPayDate = wxPayDate;
	}
	public String getIncomeSatus() {
		return incomeSatus;
	}
	public void setIncomeSatus(String incomeSatus) {
		this.incomeSatus = incomeSatus;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getOrderDescribe() {
		return orderDescribe;
	}
	public void setOrderDescribe(String orderDescribe) {
		this.orderDescribe = orderDescribe;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}