package com.spt.hospital.client.vo.user;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;

public class MdsUserInfoMngVo extends MdsLeaseInfoEntity
{
	private static final long serialVersionUID = 3613568742412169588L;
	/* 登录名称*/
	private String loginName; 
	/* 登录密码*/
	private String loginPwd; 
	/* openId*/
	private String openid; 
	/*用户类型 医院H 物业P 代理商A  平台 M  用户 U*/
	private String userType;
	/* 用户code*/
	private String userCode; 
	/* 用户名*/
	private String userName; 
	/* 角色ID*/
	private Long roleId; 
	/* 角色CODE*/
	private String roleCode; 
	/*角色名称*/
	private String roleName;
	/*token*/
	private String token;
	/* 昵称*/
	private String nickName; 
	//注册时间
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date registerDate;
	//预计收入
	private BigDecimal prepareCost;
	//报修理由
	private String reason; 
	/* 用户手机*/
	private String userPhone; 
	//报修图片
	private String repairFileId;
	
	//设备种类(指用户用过多少种设备)
	private Long categoryCount;
	//订单数量
	private Long orderCount;
	
	/*预计收入*/
	private BigDecimal prepareCostCount;
	/*实际收入*/
	private BigDecimal leaseCostCount;
	
	
	private String registerDateStr;
	
	
	public String getRegisterDateStr() {
		return registerDateStr;
	}
	public void setRegisterDateStr(String registerDateStr) {
		this.registerDateStr = registerDateStr;
	}
	public BigDecimal getLeaseCostAll() {
		return leaseCostAll;
	}
	public void setLeaseCostAll(BigDecimal leaseCostAll) {
		this.leaseCostAll = leaseCostAll;
	}
	private BigDecimal leaseCostAll =BigDecimal.ZERO;
	
	List<MdsLeaseInfoVo> leaseInfoVo;

	public List<MdsLeaseInfoVo> getLeaseInfoVo() {
		return leaseInfoVo;
	}
	public void setLeaseInfoVo(List<MdsLeaseInfoVo> leaseInfoVo) {
		this.leaseInfoVo = leaseInfoVo;
	}

	public BigDecimal getPrepareCostCount() {
		return prepareCostCount;
	}
	public void setPrepareCostCount(BigDecimal prepareCostCount) {
		this.prepareCostCount = prepareCostCount;
	}
	public BigDecimal getLeaseCostCount() {
		return leaseCostCount;
	}
	public void setLeaseCostCount(BigDecimal leaseCostCount) {
		this.leaseCostCount = leaseCostCount;
	}
	public Long getCategoryCount() {
		return categoryCount;
	}
	public void setCategoryCount(Long categoryCount) {
		this.categoryCount = categoryCount;
	}
	public Long getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(Long orderCount) {
		this.orderCount = orderCount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getRepairFileId() {
		return repairFileId;
	}
	public void setRepairFileId(String repairFileId) {
		this.repairFileId = repairFileId;
	}

	public BigDecimal getPrepareCost() {
		return prepareCost;
	}
	public void setPrepareCost(BigDecimal prepareCost) {
		this.prepareCost = prepareCost;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	} 
}