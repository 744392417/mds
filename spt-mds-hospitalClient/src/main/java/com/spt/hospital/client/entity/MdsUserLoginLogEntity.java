package com.spt.hospital.client.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：用户登录日志
 * */
@Entity
@Table(name = "tbl_user_login_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsUserLoginLogEntity extends IdEntity 
{
	private static final long serialVersionUID = 4830599355632535449L;
	//TODO 用户信息
	/*用户类型 医院H 物业P 代理商A  平台 M  用户 U*/
	private String userType;
	/* 用户code*/
	private String userCode; 
	/* 用户名*/
	private String userName; 
	/* 角色ID*/
	private Long roleId; 
	/* 角色CODE*/
	private String roleCode; 
	/*角色名称*/
	private String roleName;
	/* 登录名称*/
	private String loginName; 
	/* 登录ID*/
	private Long loginId; 
	/*登录时长*/
	private BigDecimal loginHours = BigDecimal.ZERO;	
	//TODO 其他信息
	/*医院ID*/
	private Long  hospitalId; 
	/*医院编号/代理商编号/物业编号*/
	private String   hospitalCode ; 
	/*医院名称/代理商名称/物业名称*/
	private String   hospitalName; 
	/*医院代理商/代理商地址/物业地址*/
	private String   hospitalAddress; 
	/*token*/
	private String token; 
	/* 备注*/
	private String remark; 
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getLoginId() {
		return loginId;
	}
	public void setLoginId(Long loginId) {
		this.loginId = loginId;
	}
	public BigDecimal getLoginHours() {
		return loginHours;
	}
	public void setLoginHours(BigDecimal loginHours) {
		this.loginHours = loginHours;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
