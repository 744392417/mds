package com.spt.hospital.client.vo;

public class DictDataVo {

	private String dictCd;
	private String dictName;
	
	public String getDictCd() {
		return dictCd;
	}
	public void setDictCd(String dictCd) {
		this.dictCd = dictCd;
	}
	public String getDictName() {
		return dictName;
	}
	public void setDictName(String dictName) {
		this.dictName = dictName;
	}
}
