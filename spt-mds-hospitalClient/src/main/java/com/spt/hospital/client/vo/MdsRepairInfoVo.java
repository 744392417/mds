package com.spt.hospital.client.vo;


import com.spt.hospital.client.entity.MdsRepairInfoEntity;

public class MdsRepairInfoVo extends MdsRepairInfoEntity
{
	private static final long serialVersionUID = 6570787871964464063L;
	
	//图片全拼路径
	private String fileImageUrl;
	//用户名
	private String userName; 
	/* 用户手机*/
	private String userPhone; 

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	//getter && setter
	public String getFileImageUrl() {
		return fileImageUrl;
	}
	public void setFileImageUrl(String fileImageUrl) {
		this.fileImageUrl = fileImageUrl;
	}
}
