package com.spt.hospital.client.vo;


import java.math.BigDecimal;


import com.spt.hospital.client.entity.MdsPropertyInfoEntity;


public class ByHospitalManagerVo extends MdsPropertyInfoEntity 
{
	private static final long serialVersionUID = 3613568742412169588L;
	//操作类型 删除 D  修改 U  新增 A
	private String operType; 
	// 预付款
	private BigDecimal leaseDeposit = new BigDecimal(0.00); 
	// 租金 元/小时
	private BigDecimal leaseUnit = new BigDecimal(0.00); 
	//每次封顶价格  20 元/天
	private BigDecimal topBalance = new BigDecimal(0.00);
	/*递减价格(元/天)*/
	private BigDecimal decreasPrice = new BigDecimal(0.00);
	/*最低(元/天)*/
	private BigDecimal minimumPrice = new BigDecimal(0.00);
	//收费规则
	private String chargingRules;
	//开锁密码
	private String unlockPwd;
	//关锁密码
	private String shutPwd;
	//图片链接
	private String fileCategotyUrl;
	//总数
	private Integer allNum = 0;
	//最小租赁时长
	private Integer minHour = 0;
	//1、备选：在设备管理中录入，还没有分配医院的设备
	private Integer beixuanNum; 
	//2、空闲：在门店管理中配置好的设备，还没有使用，或使用后已经归还。
	private Integer kongxuanNum; 
	//3、暂停：只有空闲状态可以进入暂停状态，暂停状态设备不可使用。
	private Integer zhantingNum; 
	//4、使用中：正在使用中
	private Integer shiyongNum; 
	//5、报修：客户通过系统进行报修，平台确认后，进入报修状态，用能使用
	private Integer baoxiuNum; 
	//6、异常：如果设备处于空闲状态超过2周（暂定），设备属性为异常。
	private Integer yichangNum; 
	//7、低电量：设备通信次数*0.002（通信500次）+待机天数*0.002（待机500天）=1就为低电量状态
	private Integer didianliangNum; 
	//8、逾期：客户预计收入/押金>80%，状态就是逾期，予以标记。
	private Integer yuqiNum;
	
	/* 类别图片ID */
	private String fileId;
	/* 类别code */
	private String categoryCode;
	/* 类别名称 */
	private String categoryName;
	
	
	private String properBankName; // 物业银行名称
	private String agentBankName; // 代理商银行名称
	private String agentCapitalAccount; // 代理商资金账户
	private String distributionName;// 返佣类型名称
	private Integer platDistrib; // 平台收入百分比
	private Integer properDistribution  = 0; //物业返佣比例
	
	public Integer getProperDistribution() {
		return properDistribution;
	}
	public void setProperDistribution(Integer properDistribution) {
		this.properDistribution = properDistribution;
	}
	public Integer getMinHour() {
		return minHour;
	}
	public void setMinHour(Integer minHour) {
		this.minHour = minHour;
	}
	public String getChargingRules() {
		return chargingRules;
	}
	public void setChargingRules(String chargingRules) {
		this.chargingRules = chargingRules;
	}
	public Integer getBeixuanNum() {
		return beixuanNum;
	}
	public void setBeixuanNum(Integer beixuanNum) {
		this.beixuanNum = beixuanNum;
	}
	public Integer getKongxuanNum() {
		return kongxuanNum;
	}
	public void setKongxuanNum(Integer kongxuanNum) {
		this.kongxuanNum = kongxuanNum;
	}
	public Integer getZhantingNum() {
		return zhantingNum;
	}
	public void setZhantingNum(Integer zhantingNum) {
		this.zhantingNum = zhantingNum;
	}
	public Integer getYichangNum() {
		return yichangNum;
	}
	public void setYichangNum(Integer yichangNum) {
		this.yichangNum = yichangNum;
	}
	public Integer getDidianliangNum() {
		return didianliangNum;
	}
	public void setDidianliangNum(Integer didianliangNum) {
		this.didianliangNum = didianliangNum;
	}
	public String getUnlockPwd() {
		return unlockPwd;
	}
	public void setUnlockPwd(String unlockPwd) {
		this.unlockPwd = unlockPwd;
	}
	public String getShutPwd() {
		return shutPwd;
	}
	public void setShutPwd(String shutPwd) {
		this.shutPwd = shutPwd;
	}
	public Integer getAllNum() {
		return allNum;
	}
	public void setAllNum(Integer allNum) {
		this.allNum = allNum;
	}
	public Integer getShiyongNum() {
		return shiyongNum;
	}
	public void setShiyongNum(Integer shiyongNum) {
		this.shiyongNum = shiyongNum;
	}
	public Integer getBaoxiuNum() {
		return baoxiuNum;
	}
	public void setBaoxiuNum(Integer baoxiuNum) {
		this.baoxiuNum = baoxiuNum;
	}
	public Integer getYuqiNum() {
		return yuqiNum;
	}
	public void setYuqiNum(Integer yuqiNum) {
		this.yuqiNum = yuqiNum;
	} 
	public BigDecimal getLeaseDeposit() {
		return leaseDeposit;
	}
	public void setLeaseDeposit(BigDecimal leaseDeposit) {
		this.leaseDeposit = leaseDeposit;
	}
	public BigDecimal getLeaseUnit() {
		return leaseUnit;
	}
	public void setLeaseUnit(BigDecimal leaseUnit) {
		this.leaseUnit = leaseUnit;
	}
	public BigDecimal getTopBalance() {
		return topBalance;
	}
	public void setTopBalance(BigDecimal topBalance) {
		this.topBalance = topBalance;
	}
	public String getOperType() {
		return operType;
	}
	public void setOperType(String operType) {
		this.operType = operType;
	}
	 public String getFileCategotyUrl() {
		return fileCategotyUrl;
	}
	public void setFileCategotyUrl(String fileCategotyUrl) {
		this.fileCategotyUrl = fileCategotyUrl;
	}
	public BigDecimal getDecreasPrice() {
		return decreasPrice;
	}
	public void setDecreasPrice(BigDecimal decreasPrice) {
		this.decreasPrice = decreasPrice;
	}
	public BigDecimal getMinimumPrice() {
		return minimumPrice;
	}
	public void setMinimumPrice(BigDecimal minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
	public String getProperBankName() {
		return properBankName;
	}
	public void setProperBankName(String properBankName) {
		this.properBankName = properBankName;
	}
	public String getAgentBankName() {
		return agentBankName;
	}
	public void setAgentBankName(String agentBankName) {
		this.agentBankName = agentBankName;
	}
	public String getAgentCapitalAccount() {
		return agentCapitalAccount;
	}
	public void setAgentCapitalAccount(String agentCapitalAccount) {
		this.agentCapitalAccount = agentCapitalAccount;
	}
	public String getDistributionName() {
		return distributionName;
	}
	public void setDistributionName(String distributionName) {
		this.distributionName = distributionName;
	}
	public Integer getPlatDistrib() {
		return platDistrib;
	}
	public void setPlatDistrib(Integer platDistrib) {
		this.platDistrib = platDistrib;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}