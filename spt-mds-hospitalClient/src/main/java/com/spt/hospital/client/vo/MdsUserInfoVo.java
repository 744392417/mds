package com.spt.hospital.client.vo;

import com.spt.hospital.client.entity.MdsUserInfoEntity;

public class MdsUserInfoVo extends MdsUserInfoEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6760621246993152819L;
	
	private String session_key;
	
	private String loginTime;
	
	
	private String plainPassword;
	
	
	
	
	/*用过的种类数量*/
	private Long categoryNumber;

	/*预计总收入*/
	private Long prepareCost;
	/*实际总收入*/
	private Long leaseCost;

	
	public Long getCategoryNumber() {
		return categoryNumber;
	}

	public void setCategoryNumber(Long categoryNumber) {
		this.categoryNumber = categoryNumber;
	}

	public Long getPrepareCost() {
		return prepareCost;
	}

	public void setPrepareCost(Long prepareCost) {
		this.prepareCost = prepareCost;
	}

	public Long getLeaseCost() {
		return leaseCost;
	}

	public void setLeaseCost(Long leaseCost) {
		this.leaseCost = leaseCost;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getSession_key() {
		return session_key;
	}

	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
}
