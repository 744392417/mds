package com.spt.hospital.client.vo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;

@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class MdsProductCategoryVo extends MdsProductCategoryEntity
{
	private static final long serialVersionUID = 6570787871964464063L;
	
	 // 操作类型 删除 D 修改 U 新增 A
	private String operType;
	//1、备选：在设备管理中录入，还没有分配医院的设备
	private Integer beixuanNum; 
	//2、空闲：在门店管理中配置好的设备，还没有使用，或使用后已经归还。
	private Integer kongxuanNum; 
	//3、暂停：只有空闲状态可以进入暂停状态，暂停状态设备不可使用。
	private Integer zhantingNum; 
	//4、使用中：正在使用中
	private Integer shiyongNum; 
	//5、报修：客户通过系统进行报修，平台确认后，进入报修状态，用能使用
	private Integer baoxiuNum; 
	//6、异常：如果设备处于空闲状态超过2周（暂定），设备属性为异常。
	private Integer yichangNum; 
	//7、低电量：设备通信次数*0.002（通信500次）+待机天数*0.002（待机500天）=1就为低电量状态
	private Integer didianliangNum; 
	//8、逾期：客户预计收入/押金>80%，状态就是逾期，予以标记。
	private Integer yuqiNum;
	//9.维修
	private Integer weixiuzhong;
	//10.默认
	private Integer moren;
	//总数量
	private Integer countNumber;
	//类别图片路径
	private String categoryImage;
	//报修信息
	private MdsRepairInfoEntity repairInfo;
	
	public Integer getWeixiuzhong() {
		return weixiuzhong;
	}
	public void setWeixiuzhong(Integer weixiuzhong) {
		this.weixiuzhong = weixiuzhong;
	}
	public Integer getMoren() {
		return moren;
	}
	public void setMoren(Integer moren) {
		this.moren = moren;
	}
	public String getOperType() {
		return operType;
	}
	public void setOperType(String operType) {
		this.operType = operType;
	}
	public Integer getBeixuanNum() {
		return beixuanNum;
	}
	public void setBeixuanNum(Integer beixuanNum) {
		this.beixuanNum = beixuanNum;
	}
	public Integer getKongxuanNum() {
		return kongxuanNum;
	}
	public void setKongxuanNum(Integer kongxuanNum) {
		this.kongxuanNum = kongxuanNum;
	}
	public Integer getZhantingNum() {
		return zhantingNum;
	}
	public void setZhantingNum(Integer zhantingNum) {
		this.zhantingNum = zhantingNum;
	}
	public Integer getShiyongNum() {
		return shiyongNum;
	}
	public void setShiyongNum(Integer shiyongNum) {
		this.shiyongNum = shiyongNum;
	}
	public Integer getBaoxiuNum() {
		return baoxiuNum;
	}
	public void setBaoxiuNum(Integer baoxiuNum) {
		this.baoxiuNum = baoxiuNum;
	}
	public Integer getYichangNum() {
		return yichangNum;
	}
	public void setYichangNum(Integer yichangNum) {
		this.yichangNum = yichangNum;
	}
	public Integer getDidianliangNum() {
		return didianliangNum;
	}
	public void setDidianliangNum(Integer didianliangNum) {
		this.didianliangNum = didianliangNum;
	}
	public Integer getYuqiNum() {
		return yuqiNum;
	}
	public void setYuqiNum(Integer yuqiNum) {
		this.yuqiNum = yuqiNum;
	}
	public Integer getCountNumber() {
		return countNumber;
	}
	public void setCountNumber(Integer countNumber) {
		this.countNumber = countNumber;
	}
	public String getCategoryImage() {
		return categoryImage;
	}
	public void setCategoryImage(String categoryImage) {
		this.categoryImage = categoryImage;
	}
	public MdsRepairInfoEntity getRepairInfo() {
		return repairInfo;
	}
	public void setRepairInfo(MdsRepairInfoEntity repairInfo) {
		this.repairInfo = repairInfo;
	}
}
