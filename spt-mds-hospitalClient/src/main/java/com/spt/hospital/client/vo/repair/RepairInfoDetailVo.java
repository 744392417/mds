package com.spt.hospital.client.vo.repair;

import com.spt.hospital.client.entity.MdsRepairInfoEntity;
/**
 * 报修详情Vo
 * */
public class RepairInfoDetailVo  extends MdsRepairInfoEntity
{
	private static final long serialVersionUID = 7508751618255546746L;
	/* 昵称*/
	private String nickName; 
	/* 用户名*/
	private String userName; 
	/* 用户手机*/
	private String userPhone; 
	/* 登录名称*/
	private String loginName;
	/*文件地址*/
	private String imageUrl;
	
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	} 
}
