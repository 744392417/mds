package com.spt.hospital.client.constant;



public interface HospConstants 
{
	static final String PROJECT_CODE = "mds";

	static final String GATE_PRIVATEKEY = "privateKey";
	
	static final String GATE_SUBJECT_PP = "pp";// 公私钥
	
	static final String GATE_APP_CODE = "AppCode";// header参数
	
	static final String GATE_ACCESSTOKEN = "AccessToken";//用户Token

	static final String SERVER_NAME = "hospitalServer";

	/** 指定服务地址，默认为空字符串 */
	static final String SERVER_URL = "";

	static final String INIT_PASSWORD = "123456";

	/** 配置管理-配置类型 **/
	static final String REQ_CONTENT_CONFTYPE = "REQ_CONTENT_CONFTYPE";

	static final String RESULT_SUCCESS = "success";

	static final String RESULT_FAIL = "fail";

	static final String SPT_GATEWAY_URL_KEY = "spt.gateWay.url";

	static final String SPT_APP_APPCODE = "spt.app.appCode";

	static final String SPT_COMPANY_URL = "spt.company.url";

	static final String SPT_COMPANY_PK = "spt.company.privateKey";

	static final String USER_ROLE = "saaszsUserRole";


	public static final String SUBJECT_USER = "user";// Token 用户登录信息

	public static final String SECRETKEY = "hsoft_req_180306";// Token

	public static final String GATE_USER_INFO = "user-info";// Token
	
	/**返佣类型****字典表****/
	static final String DISTRIBUTION_TYPE="distributionType";
	
	/**设备状态****字典表****/
	static final String PRODUCT_STATUS="productStatus";

	static final String EXCP_APP_APPCODE = "excp.app.appCode";
	/** 私钥 */
	static final String EXCP_PRIVATEKEY = "excp.app.privateKey";

	static final String BAS_APP_APPCODE = "bas.app.appCode";
	/** 私钥 */
	static final String BAS_PRIVATEKEY = "bas.app.privateKey";

	static final String BAS_GATEWAY_URL_LOGIN_KEY = "bas.gateWay.loginurl";

	static final String CONSUM_STATUS_S = "S";// 支出 返回余款

	static final String CONSUM_STATUS_I = "I";// 汇入 预付款

	static final String CONSUM_STATUS_P = "P";// 租金 支付租金
	
    static final String CONSUM_STATUS_F = "F";// 分账
	
	static final String CONSUM_STATUS_R = "R";// 留存
	
	
	static final String CONSUM_STATUS_C = "C";//完成退款
	

	static final String OPER_TYPE_D = "D";// 删除

	static final String OPER_TYPE_A = "A";// 新增

	static final String OPER_TYPE_U = "U";// 修改

	static final String STATUS_1 = "1";// 有效

	static final String STATUS_0 = "0";// 无效
	
	static final String ROLE_MANAGER = "M";//管理员
	
	//物业 类型 :医院H 物业 P 代理商 A 平台M
	
	static final String PROPERTY_TYPE_H = "H";
	
	static final String PROPERTY_TYPE_A = "A";
	
	static final String PROPERTY_TYPE_P = "P";
	
	static final String PROPERTY_TYPE_M = "M";
	
	
	static final String DISTRI_TYPE_H  = "H";//  返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL
	
	static final String DISTRI_TYPE_P  = "P";//  返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL
	
	static final String DISTRI_TYPE_A  = "A";//  返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL
	
	static final String DISTRI_TYPE_HP = "HP";// 返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL
	
	static final String DISTRI_TYPE_HA = "HA";// 返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL
	
	static final String DISTRI_TYPE_HAP = "HAP";//返佣类型 医院H,医院+物业HP,医院+代理HA,医院+物业+代理HPA, 自营ALL

    //	  用户类型 医院H 物业P 代理商A  平台 M  用户 U
	static final String USER_TYPE_H = "H";
	
	static final String USER_TYPE_A = "A";
	
	static final String USER_TYPE_P = "P";
	
	static final String USER_TYPE_M = "M";
	
	static final String USER_TYPE_U = "U";
	
	//设备状态 ：备选  空闲 暂停   使用中   报修  异常  低电量  逾期  维修中
	
	static final String PRODUCT_USE_STATUS_1 = "1";//备选
	
	static final String PRODUCT_USE_STATUS_2 = "2";//空闲
	
	static final String PRODUCT_USE_STATUS_3 = "3";//暂停
	
	static final String PRODUCT_USE_STATUS_4 = "4";//使用中
	
	static final String PRODUCT_USE_STATUS_5 = "5";//维修中
	
	static final String PRODUCT_STATUS_1 = "1";//默认
	
	static final String PRODUCT_STATUS_2 = "2";//异常
	
	static final String PRODUCT_STATUS_3 = "3";//低电量 
	
	static final String PRODUCT_STATUS_4 = "4";//逾期
	
	static final String PRODUCT_STATUS_5 = "5";//报修
	
	static final String PRODUCT_STATUS_DELETE = "-1";//删除
	
	
	static final String PRODUCT_USE_Y = "Y";//报修
	
	
	static final String PRODUCT_USE_N = "N";//不是报修
	
    // 36 对密码
	static final String PWD_STATUS_1 = "1";// 使用中
	
	static final String PWD_STATUS_2 = "2";//已使用
	
	static final String PWD_STATUS_0 = "0";//未使用
	
	public static final Integer PWD_NUMBER = 36;
	
	
	//订单状态
	static final  String ORDER_STATUS_0 = "0"; //使用中
	
	static final  String ORDER_STATUS_1 = "1"; //完成
	
	static final  String ORDER_STATUS_2 = "2"; //待付款
	
	static final  String ORDER_STATUS_4 = "4"; //报修中
	
	static final  String ORDER_STATUS_5 = "5"; //余额不足
	
	
	
	

	//医疗平台交互类型
	static final  String INTERACTIVE_RECEIVE__PWD = "P";//接收密码
	
	static final  String INTERACTIVE__VERSION_SEND = "VS";//版本升级发送请求
	
	static final  String INTERACTIVE_VERSION_RECEIVE = "VR";//接收版本升级
	
	static final  String INTERACTIVE_RECEIVE_STATUS = "S";//接收设备状态
	
	//用户不修改登录密码
	static final String LOGIN_PWD_NO_UPDATE  = "******";
	
	//批量操作类型 S暂停  U升级
	static final String BATCH_TYPE_SUSPEND  = "S";
	static final String BATCH_TYPE_UPGRADE  = "U";
	
	static final String WX_RESULT_SUCCESS = "1";

	static final String WX_RESULT_FAIL = "0";
	
	static final int ORDER_EXPIRE_SECONDS = 3*60;//待付款 3分钟失效
	
	static final String ORDER_STATUS_REPAIRS_TYPE_0 = "0";//正常
	
	static final String ORDER_STATUS_REPAIRS_TYPE_1 = "1";//维修
	
	static final Integer PLATE_DISTRIBUTION = 100;//
	
	static final String BEARCH_RECEIVE_S = "S";//暂停S，启用U
	
	static final String BEARCH_RECEIVE_U = "U";//暂停S，启用U
		
	static final String IN_USE_Y = "Y";
	
	static final String IN_USE_N = "N";
	
	
	static final String IN_COME_STATUS_1 = "1";//资金流水分账无效
	
	static final int BALANCE_EXPIRE_SECONDS = 30*60*60;//余额不足 300分钟失效

	
}
