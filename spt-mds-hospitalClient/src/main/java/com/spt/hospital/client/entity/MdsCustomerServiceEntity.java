package com.spt.hospital.client.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;
/**
 * @author hanson
 * 实体：我的客服
 * */
@Entity
@Table(name = "tbl_customer_service")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsCustomerServiceEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 类型*/
	private String serviceType;
	/* 标题*/
	private String serviceTitle; 
	/* 内容*/
	private String serviceContent; 
	/* 备注*/
	private String remark; 

	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getServiceTitle() {
		return serviceTitle;
	}
	public void setServiceTitle(String serviceTitle) {
		this.serviceTitle = serviceTitle;
	}
	public String getServiceContent() {
		return serviceContent;
	}
	public void setServiceContent(String serviceContent) {
		this.serviceContent = serviceContent;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}