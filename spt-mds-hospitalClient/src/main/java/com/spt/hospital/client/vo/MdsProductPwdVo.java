package com.spt.hospital.client.vo;

import com.spt.hospital.client.entity.MdsProductPwdEntity;


public class MdsProductPwdVo extends MdsProductPwdEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6583979117097156563L;

	/* 微信用户ID*/
	private String openid;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	} 
}
