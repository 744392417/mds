package com.spt.hospital.client.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;



public class MdsLeaseInfoVo extends MdsLeaseInfoEntity
{

	private static final long serialVersionUID = 6583979117097156563L;

	private BigDecimal leaseCostSum = new BigDecimal(0.00); // 租赁费用(收入总计)
	private BigDecimal leaseDepositSum = new BigDecimal(0.00); // 预付款(总计)
	private BigDecimal intoBalanceSum = new BigDecimal(0.00); // 退还(归还总计)
	private BigDecimal retaineAmountSum = new BigDecimal(0.00); // 留存总计)
	private BigDecimal fashionAmountSum = new BigDecimal(0.00); // 分账总计
	private BigDecimal expectedLeaseCost = new BigDecimal(0.00); // 预计租金
	private BigDecimal leaseUnit = new BigDecimal(0.00); // 租金 元/小时
	private BigDecimal topBalance = new BigDecimal(0.00); // 每次封顶价格 20 元/天
	
	/* 租赁开始时间*/
	private String leaseStartDateStr; 
	/* 租赁结束时间*/
	private String leaseEndDateStr;
	
	/* 操作时间*/
	private String updatedDateStr;

	private String orderStatusValue;

	// 费用占比
	private String costProportion;
	/* 起步小时 */
	private Integer minHour = 0;
	
	/* 起步小时 */
	private Integer outHour =1;

	// 图片全拼路径
	private String fileImageUrl;
	// 用户名
	private String userName;

	
	private String nickName;
//	/* 描述 */
//	private String remark;



	// 图片全拼路径
	private String repairfileId;
	
	/*描述*/
	private String repairRemark; 

	/* 原因*/
	private String repairReason; 
	
	/* 报修时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date repairDate; 
	
	private String serviceReason; 
	
	//预计收入
	private BigDecimal prepareCost;
	

	/* 状态 1 解决 0 未解决 */
	private String repairStatus = "0";
	/* 原因 */
	private String reason;

	/* 手机 */
	private String mobilePhone;
	
	// 收费规则
	private String chargingRules;
	
	/*反馈内容*/
	private String feedbackContent;
	
	/*描述*/
	private String describe;
	
	public String getLeaseStartDateStr() {
		return leaseStartDateStr;
	}

	public void setLeaseStartDateStr(String leaseStartDateStr) {
		this.leaseStartDateStr = leaseStartDateStr;
	}

	public String getLeaseEndDateStr() {
		return leaseEndDateStr;
	}

	public void setLeaseEndDateStr(String leaseEndDateStr) {
		this.leaseEndDateStr = leaseEndDateStr;
	}

	public String getUpdatedDateStr() {
		return updatedDateStr;
	}

	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	public BigDecimal getPrepareCost() {
		return prepareCost;
	}

	public void setPrepareCost(BigDecimal prepareCost) {
		this.prepareCost = prepareCost;
	}

	public String getServiceReason() {
		return serviceReason;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setServiceReason(String serviceReason) {
		this.serviceReason = serviceReason;
	}

	public Date getRepairDate() {
		return repairDate;
	}

	public void setRepairDate(Date repairDate) {
		this.repairDate = repairDate;
	}

	public String getRepairRemark() {
		return repairRemark;
	}

	public void setRepairRemark(String repairRemark) {
		this.repairRemark = repairRemark;
	}

	public String getRepairReason() {
		return repairReason;
	}

	public void setRepairReason(String repairReason) {
		this.repairReason = repairReason;
	}

	public String getRepairfileId() {
		return repairfileId;
	}

	public void setRepairfileId(String repairfileId) {
		this.repairfileId = repairfileId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public String getFeedbackContent() {
		return feedbackContent;
	}

	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getFileImageUrl() {
		return fileImageUrl;
	}

	public void setFileImageUrl(String fileImageUrl) {
		this.fileImageUrl = fileImageUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


//	public String getRemark() {
//		return remark;
//	}
//
//	public void setRemark(String remark) {
//		this.remark = remark;
//	}

	public String getRepairStatus() {
		return repairStatus;
	}

	public void setRepairStatus(String repairStatus) {
		this.repairStatus = repairStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}



	public Integer getMinHour() {
		return minHour;
	}

	public void setMinHour(Integer minHour) {
		this.minHour = minHour;
	}

	public Integer getOutHour() {
		return outHour;
	}

	public void setOutHour(Integer outHour) {
		this.outHour = outHour;
	}


	public String getChargingRules() {
		return chargingRules;
	}

	public void setChargingRules(String chargingRules) {
		this.chargingRules = chargingRules;
	}

	public BigDecimal getLeaseUnit() {
		return leaseUnit;
	}

	public void setLeaseUnit(BigDecimal leaseUnit) {
		this.leaseUnit = leaseUnit;
	}

	public BigDecimal getTopBalance() {
		return topBalance;
	}

	public void setTopBalance(BigDecimal topBalance) {
		this.topBalance = topBalance;
	}

	public BigDecimal getExpectedLeaseCost() {
		return expectedLeaseCost;
	}

	public void setExpectedLeaseCost(BigDecimal expectedLeaseCost) {
		this.expectedLeaseCost = expectedLeaseCost;
	}

	public BigDecimal getLeaseCostSum() {
		return leaseCostSum;
	}

	public void setLeaseCostSum(BigDecimal leaseCostSum) {
		this.leaseCostSum = leaseCostSum;
	}

	public BigDecimal getLeaseDepositSum() {
		return leaseDepositSum;
	}

	public void setLeaseDepositSum(BigDecimal leaseDepositSum) {
		this.leaseDepositSum = leaseDepositSum;
	}

	public BigDecimal getIntoBalanceSum() {
		return intoBalanceSum;
	}

	public void setIntoBalanceSum(BigDecimal intoBalanceSum) {
		this.intoBalanceSum = intoBalanceSum;
	}

	public BigDecimal getRetaineAmountSum() {
		return retaineAmountSum;
	}

	public void setRetaineAmountSum(BigDecimal retaineAmountSum) {
		this.retaineAmountSum = retaineAmountSum;
	}

	public BigDecimal getFashionAmountSum() {
		return fashionAmountSum;
	}

	public void setFashionAmountSum(BigDecimal fashionAmountSum) {
		this.fashionAmountSum = fashionAmountSum;
	}

	public String getCostProportion() {
		return costProportion;
	}

	public void setCostProportion(String costProportion) {
		this.costProportion = costProportion;
	}

	public String getOrderStatusValue() {
		return orderStatusValue;
	}

	public void setOrderStatusValue(String orderStatusValue) {
		this.orderStatusValue = orderStatusValue;
	}



}
