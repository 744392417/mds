package com.spt.hospital.client.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：物业信息表
 * */
@Entity
@Table(name = "tbl_property_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsPropertyInfoEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 医院编号/代理商编号/物业编号*/
	private String hospitalCode;
	/* 医院名称/代理商名称/物业名称*/
	private String hospitalName;
	/* 医院代理商/代理商地址/物业地址*/
	private String hospitalAddress;
	/* 医院合作租赁方*/
	private String hospitalPartners; 
	/*科室*/
	private String hospitalDepart;
	
	private String hospitalWard;
	/* 医院返佣比例*/
	private Integer hospitalDistribution = 0; 
	/* 医院银行编号/代理商银行编号/ 物业银行编号*/
	private String bankCode; 
	/* 医院银行名称/代理商银行名称/ 物业银行名称*/
	private String bankName; 
	/* 医院资金账户/代理商资金账户/ 物业资金账户*/
	private String capitalAccount; 
	
	/* 医院资金账户/代理商资金账户/ 物业 开户行所属人姓名 */
	private String accountName; 

	/* 医院开户行/代理商开户行/ 物业开户行*/
	private String openBankAccount; 
	/* 返佣类型 医院H,医院+物业HP,医院+代理HA,自营ALL*/
	private String distributionType; 
	/* 所属物业*/
	private String properName; 
	/* 物业ID*/
	private Long properId;
	/* 物业返佣比例*/
	private Integer properDistribution  = 0; 
	/* 所属代理商*/
	private String agentName; 
	/* 代理商ID*/
	private Long agentId;
	/* 代理商返佣比例*/
	private Integer agentDistribution = 0; 
	/* 类型 :医院H 物业 P 代理商 A 平台M*/
	private String hospitalType;
	/* 联系人*/
	private String contactPerson; 
	/* 电话*/
	private String contactTelephone; 
	/* 手机号*/
	private String contactPhone; 
	/* 地址*/
	private String contactAddress; 
	/* 经度*/
	private BigDecimal lng; 
	/* 纬度*/
	private BigDecimal lat; 
	/* 物业状态 1 有效 0 无效*/
	private String status ="1"; 
	/* 备注*/
	private String remark; 
	/*平台收入比例*/
	private Integer platDistribution  = 0;
	/*设备使用时间*/
	private String propertyUsableTime;
	
	/*运维人员*/
	private String operationStaff;
	/*运营人员*/
	private String operatingPerson;
	
	/*运维联系方式*/
	private String operationPhone ;
	
	/*运营人员联系方式*/
	private String operatingPhone;
	/*省*/
	private String provinceName;
	/*市*/
    private String cityName;
    /*县*/
	private String countyName;

	
	public String getOperationPhone() {
		return operationPhone;
	}
	public void setOperationPhone(String operationPhone) {
		this.operationPhone = operationPhone;
	}
	public String getOperatingPhone() {
		return operatingPhone;
	}
	public void setOperatingPhone(String operatingPhone) {
		this.operatingPhone = operatingPhone;
	}

	
	
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getOperationStaff() {
		return operationStaff;
	}
	public void setOperationStaff(String operationStaff) {
		this.operationStaff = operationStaff;
	}
	public String getOperatingPerson() {
		return operatingPerson;
	}
	public void setOperatingPerson(String operatingPerson) {
		this.operatingPerson = operatingPerson;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getHospitalDepart() {
		return hospitalDepart;
	}
	public void setHospitalDepart(String hospitalDepart) {
		this.hospitalDepart = hospitalDepart;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getHospitalPartners() {
		return hospitalPartners;
	}
	public void setHospitalPartners(String hospitalPartners) {
		this.hospitalPartners = hospitalPartners;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCapitalAccount() {
		return capitalAccount;
	}
	public void setCapitalAccount(String capitalAccount) {
		this.capitalAccount = capitalAccount;
	}
	public String getOpenBankAccount() {
		return openBankAccount;
	}
	public void setOpenBankAccount(String openBankAccount) {
		this.openBankAccount = openBankAccount;
	}
	public String getDistributionType() {
		return distributionType;
	}
	public void setDistributionType(String distributionType) {
		this.distributionType = distributionType;
	}

	public Integer getHospitalDistribution() {
		return hospitalDistribution;
	}
	public void setHospitalDistribution(Integer hospitalDistribution) {
		this.hospitalDistribution = hospitalDistribution;
	}
	public Integer getPlatDistribution() {
		return platDistribution;
	}
	public void setPlatDistribution(Integer platDistribution) {
		this.platDistribution = platDistribution;
	}
	public String getProperName() {
		return properName;
	}
	public void setProperName(String properName) {
		this.properName = properName;
	}
	public Long getProperId() {
		return properId;
	}
	public void setProperId(Long properId) {
		this.properId = properId;
	}
	public Integer getProperDistribution() {
		return properDistribution;
	}
	public void setProperDistribution(Integer properDistribution) {
		this.properDistribution = properDistribution;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Long getAgentId() {
		return agentId;
	}
	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}
	public Integer getAgentDistribution() {
		return agentDistribution;
	}
	public void setAgentDistribution(Integer agentDistribution) {
		this.agentDistribution = agentDistribution;
	}

	public String getHospitalType() {
		return hospitalType;
	}
	public void setHospitalType(String hospitalType) {
		this.hospitalType = hospitalType;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getContactTelephone() {
		return contactTelephone;
	}
	public void setContactTelephone(String contactTelephone) {
		this.contactTelephone = contactTelephone;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public BigDecimal getLng() {
		return lng;
	}
	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}
	public BigDecimal getLat() {
		return lat;
	}
	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPropertyUsableTime() {
		return propertyUsableTime;
	}
	public void setPropertyUsableTime(String propertyUsableTime) {
		this.propertyUsableTime = propertyUsableTime;
	}
	public String getHospitalWard() {
		return hospitalWard;
	}
	public void setHospitalWard(String hospitalWard) {
		this.hospitalWard = hospitalWard;
	}
}
