//package com.spt.hospital.client.vo.recordsConption;
//
//
//import java.math.BigDecimal;
//import java.util.Date;
//
//import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
///**
// * 设备信息到我的设备 设备详情 Vo
// * */
//public class RecordsConptionDetailVo extends MdsRecordsConptionEntity
//{
//	private static final long serialVersionUID = 2681766711709018316L;
//	/* 设备ID*/
//	private String productCode; 
//	/* 设备名称*/
//	private String productName; 
//	/* 租赁费用(收入)*/
//	private BigDecimal leaseCost = new BigDecimal(0.00); 
//	/* 预付款*/
//	private BigDecimal leaseDeposit = new BigDecimal(0.00); 
//	/* 退还*/
//	private BigDecimal intoBalance = new BigDecimal(0.00); 
//	/* 租赁时长*/
//	private Long leaseTime; 
//	/* 租赁开始时间*/
//	private Date leaseStartDate; 
//	/* 租赁结束时间*/
//	private Date leaseEndDate;
//	/*收费规则*/
//	private String chargingRules;
//	/* 医院ID*/
//	private Long hospitalId; 
//	/* 医院名称*/
//	private String hospitalName; 
//	
//	public Long getLeaseTime() {
//		return leaseTime;
//	}
//	public void setLeaseTime(Long leaseTime) {
//		this.leaseTime = leaseTime;
//	}
//	public Date getLeaseStartDate() {
//		return leaseStartDate;
//	}
//	public void setLeaseStartDate(Date leaseStartDate) {
//		this.leaseStartDate = leaseStartDate;
//	}
//	public Date getLeaseEndDate() {
//		return leaseEndDate;
//	}
//	public void setLeaseEndDate(Date leaseEndDate) {
//		this.leaseEndDate = leaseEndDate;
//	}
//	public String getProductCode() {
//		return productCode;
//	}
//	public void setProductCode(String productCode) {
//		this.productCode = productCode;
//	}
//	public String getProductName() {
//		return productName;
//	}
//	public void setProductName(String productName) {
//		this.productName = productName;
//	}
//	public BigDecimal getLeaseCost() {
//		return leaseCost;
//	}
//	public void setLeaseCost(BigDecimal leaseCost) {
//		this.leaseCost = leaseCost;
//	}
//	public BigDecimal getLeaseDeposit() {
//		return leaseDeposit;
//	}
//	public void setLeaseDeposit(BigDecimal leaseDeposit) {
//		this.leaseDeposit = leaseDeposit;
//	}
//	public BigDecimal getIntoBalance() {
//		return intoBalance;
//	}
//	public void setIntoBalance(BigDecimal intoBalance) {
//		this.intoBalance = intoBalance;
//	}
//	public String getChargingRules() {
//		return chargingRules;
//	}
//	public void setChargingRules(String chargingRules) {
//		this.chargingRules = chargingRules;
//	}
//	public Long getHospitalId() {
//		return hospitalId;
//	}
//	public void setHospitalId(Long hospitalId) {
//		this.hospitalId = hospitalId;
//	}
//	public String getHospitalName() {
//		return hospitalName;
//	}
//	public void setHospitalName(String hospitalName) {
//		this.hospitalName = hospitalName;
//	} 
//}
