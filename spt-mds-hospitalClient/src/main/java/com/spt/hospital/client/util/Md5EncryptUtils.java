package com.spt.hospital.client.util;

/**
 * File: Md5Encrypt.java
 * Description: Md5加密数据
 * Copyright 2010 GamaxPay. All rights reserved
 *  
 */


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsoft.commutil.props.PropertiesUtil;
import com.hsoft.commutil.util.DateOperator;
import com.spt.hospital.client.constant.HospConstants;




/**
 * 
 * 用Md5进行数据加密 
 * @author Jacky Zhou
 */
public class Md5EncryptUtils {

	private static Logger logger = LoggerFactory.getLogger(Md5EncryptUtils.class);

	private static Md5EncryptUtils md5 = new Md5EncryptUtils();
	
	private Md5EncryptUtils(){		
	}

	public static Md5EncryptUtils getInstance(){
		return md5;
	}
	
	/**
	 * 对所有商户的参数进行加密
	 * @param map 所有将加密的参数
	 * @param privateKey 需要对参数加密的私钥
	 * @return    加密以后得到的字符串对象
	 */
	public static String sign(Map<String, String> params, String privateKey){	
		List<String> keys = new ArrayList<String>(params.keySet());
		Collections.sort(keys);
		StringBuffer content = new StringBuffer();
		for (int i = 0; i < keys.size(); i++) {
			String key = (String) keys.get(i);
			String value = "";
//			try {
//				value = URLDecoder.decode((String) params.get(key), "UTF-8");
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			value = (String) params.get(key);
			if (key == null || key.equalsIgnoreCase("sign")
					|| key.equalsIgnoreCase("sign_type")){
				continue;
			}
			content.append(key + "=" + value + "&");
		}
		String linkedContent = content.toString().substring(0, content.lastIndexOf("&"));
		String signcontent = linkedContent + privateKey;		
		return encrypt(signcontent);
		
	}	
	
	/**
	 * 签名
	 * 
	 * @param params
	 * @return
	 */
	public String getSignture(Map<String, String> params, String privateKey) {
		
		Map<String, String> sortedParams = new TreeMap<String, String>(params);
	      //先将这些请求参数以其参数名的字典序升序进行排序
	      Set<Entry<String, String>> entrys = sortedParams.entrySet();
	      // 遍历排序后的字典，将所有参数按"key=value"格式拼接在一起
	        StringBuilder basestring = new StringBuilder();
	        for (Entry<String, String> param : entrys) {
	            String key = param.getKey();
	            String value = param.getValue();
	            if(StringUtils.isEmpty(value)){
	                value = "";
	            }
	            basestring.append(key).append("=").append(value);
	        }
	        basestring.append(privateKey); 

	        // 使用MD5对待签名串求签
	        byte[] bytes = null;
	        try {
	            MessageDigest md5 = MessageDigest.getInstance("MD5");
	            bytes = md5.digest(basestring.toString().getBytes("UTF-8"));
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        // 将MD5输出的二进制结果转换为小写的十六进制
	        StringBuilder sign = new StringBuilder();
	        for (int i = 0; i < bytes.length; i++) {
	            String hex = Integer.toHexString(bytes[i] & 0xFF);
	            if (hex.length() == 1) {
	                sign.append("0");
	            }
	            sign.append(hex);
	        }
	        System.out.println(sign.toString());
	        return sign.toString();

	}
	
	
	/**
	 * 对传入的字符串数据进行MD5加密
	 * @param source	字符串数据
	 * @return   加密以后的数据
	 */
	public static String encrypt(String source) {
		MessageDigest md = null;		
		byte[] bt = null;
		try {
			bt = source.getBytes("UTF-8");
			md = MessageDigest.getInstance("MD5");
			md.update(bt);
			return bytesToHexString(md.digest()); 
		} catch (NoSuchAlgorithmException e) {
			logger.error("非法摘要算法", e);
			throw new RuntimeException(e);	
		}catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	
	/**
	 * 把字节数组转换成16进制字符串
	 * @param bArray 传入的二进制数组
	 * @return 16进制的字符串
	 */
	public static String bytesToHexString(byte[] bArray) {
		StringBuffer sb = new StringBuffer(bArray.length);
		String sTemp;
		for (int i = 0; i < bArray.length; i++) {
			sTemp = Integer.toHexString(0xFF & bArray[i]);
			if (sTemp.length() < 2)
				sb.append(0);
			sb.append(sTemp.toUpperCase());
		}
		return sb.toString();
	}
/*  public static void main(String []args){

				
				StringBuffer accessUrl = new StringBuffer();
				accessUrl.append("http://192.168.2.55/");
				System.out.println("自营审批==生成的访问路径为："+accessUrl.toString());
  }*/
  
  /****
   * 生成商品通企业升级链接
   * @return
   */
  public static String getSptUrl(String userUuid,String host){
	  Map<String,String> params = new HashMap<String, String>();
	     params.put("accessAppCode",PropertiesUtil.getProperty(HospConstants.SPT_APP_APPCODE));
	     params.put("uuid",userUuid);
		 params.put("timesptms", DateOperator.formatDate(new Date(), DateOperator.FORMAT_STR_WITH_TIMESTAMP));
          //所有参数需要参与签名。生成一个访问的签名。
			String sysSign = Md5EncryptUtils.sign(params, PropertiesUtil.getProperty(HospConstants.SPT_COMPANY_PK));
			
			StringBuffer accessUrl = new StringBuffer();
			accessUrl.append(host+"/user/v2/acct/index.a?");
			accessUrl.append("accessAppCode="+params.get("accessAppCode"));
			accessUrl.append("&uuid=");
			accessUrl.append(params.get("uuid")); 
			accessUrl.append("&timesptms=");
			accessUrl.append(params.get("timesptms")); 
			accessUrl.append("&sign=");
			accessUrl.append(sysSign); 
			logger.info("生成的访问路径为："+accessUrl.toString());
			
			return accessUrl.toString();
  }
  
  /****
   * 生成自营审批系统登录链接
   * @return
   */
  public static String getBasUrl(String userUuid){
	  String timesptms =  DateOperator.formatDate(new Date(),"yyyyMMdd");
		String plainPwd =PropertiesUtil.getProperty(HospConstants.BAS_PRIVATEKEY)+userUuid+timesptms;
		String token = Md5EncryptUtils.encrypt(plainPwd);
		StringBuffer accessUrl = new StringBuffer();
		accessUrl.append(PropertiesUtil.getProperty(HospConstants.BAS_GATEWAY_URL_LOGIN_KEY)+"/open/user/ssoLogin?");
		accessUrl.append("appCode="+HospConstants.PROJECT_CODE);
		accessUrl.append("&userId=");
		accessUrl.append(userUuid); 
		accessUrl.append("&loginTime=");
		accessUrl.append(timesptms); 
		accessUrl.append("&token=");
		accessUrl.append(token); 
		logger.info("getBasUrl 生成的访问路径为："+accessUrl.toString());
	  return accessUrl.toString();
  }
}
