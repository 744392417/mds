package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.vo.MdsProductCategoryVo;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME + "/api/mdsCategory", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsProductCategoryApi extends BaseClient<MdsProductCategoryEntity> {

	@PostMapping("findByName")
	public MdsProductCategoryEntity findByName(@RequestBody MdsProductCategoryVo vo);

	@PostMapping("findPageProductCategory")
	public PageDown<MdsProductCategoryVo> findPageProductCategory(@RequestBody QueryVo queryVo);
	
	@PostMapping("equipmentNumber")
	public Integer equipmentNumber(@RequestBody Long id);
	
	@PostMapping("findByCategoryinfo")
	public List<MdsProductCategoryEntity> findByCategoryinfo();
	
	@PostMapping("findByCategory")
	MdsProductCategoryEntity findByCategory(@RequestBody QueryVo queryVo);
	
	
	@PostMapping("updateAllCategoryInfo")
	public void updateAllCategoryInfo(@RequestBody QueryVo queryVo);

}
