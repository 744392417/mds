package com.spt.hospital.client.entity;

import javax.persistence.Entity;

import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;
/**
 * @author hanson
 * 实体：用户信息表
 * */
@Entity
@Table(name = "tbl_user_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsUserInfoEntity extends IdEntity 
{
	private static final long serialVersionUID = 3613568742412169588L;
	//TODO 用户信息
	/* 登录名称*/
	private String loginName; 
	/* 登录密码*/
	private String loginPwd; 
	/* openId*/
	private String openid; 
	/*密码加密字段*/
	private String salt;
	/*头像链接*/
	private String avatarUrl;
	/* 订阅标志*/
	private Integer subscribe; 
	/* 订阅时间*/
	private Long subscribeTime; 
	/* 昵称*/
	private String nickName; 
	/* 性别*/
	private String gender; 
	/* 国家*/
	private String country; 
	/* 状态*/
	private String status; 
	/* 头像*/
	private String headImgUrl; 
	/* 语言*/
	private String language; 
	/* 城市*/
	private String city; 
	/* 省份*/
	private String province; 
	/*用户类型 医院H 物业P 代理商A  平台 M  用户 U*/
	private String userType;
	/* 用户code*/
	private String userCode; 
	/* 用户名*/
	private String userName; 
	/* 用户手机*/
	private String userPhone; 
	/* 角色ID*/
	private Long roleId; 
	/* 角色CODE*/
	private String roleCode; 
	/*角色名称*/
	private String roleName;
	//TODO 其他信息
	/*医院ID*/
	private Long  hospitalId; 
	/*医院编号/代理商编号/物业编号*/
	private String   hospitalCode ; 
	/*医院名称/代理商名称/物业名称*/
	private String   hospitalName; 
	/*医院代理商/代理商地址/物业地址*/
	private String   hospitalAddress; 
	/*token*/
	private String token; 
	/* 备注*/
	private String remark; 
	
	/*退款密码*/
	private String refusePwd;


	private String expirationTime;

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	public String getRefusePwd() {
		return refusePwd;
	}
	public void setRefusePwd(String refusePwd) {
		this.refusePwd = refusePwd;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public Integer getSubscribe() {
		return subscribe;
	}
	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}
	public Long getSubscribeTime() {
		return subscribeTime;
	}
	public void setSubscribeTime(Long subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}
