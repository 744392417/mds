package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME
		+ "/api/mdsPropertyInfo", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsPropertyInfoApi extends BaseClient<MdsPropertyInfoEntity> {

	@PostMapping("saveOrUpdateProperty")
	public void saveOrUpdateProperty(@RequestBody MdsPropertyInfoEntity entity);

	@PostMapping("findPropertyListPage")
	public PageDown<MdsPropertyInfoVo>  findPropertyListPage(@RequestBody QueryVo vo);
	
	@PostMapping("findBytype")
	public List<MdsPropertyInfoEntity> findBytype(@RequestBody MdsPropertyInfoVo vo);
	
	@PostMapping("findByQrcode")
	public MdsProductInfoVo findByQrcode(@RequestBody QueryVo vo);
	
	@PostMapping("findAllDepart")
	public List<String> findAllDepart();
	
	@PostMapping("byHospitalName")
	public MdsPropertyInfoEntity byHospitalName(@RequestBody MdsPropertyInfoVo vo);
	
	@PostMapping("findByIsfashionable")
	public List<MdsPropertyInfoEntity> findByIsfashionable(@RequestBody QueryVo vo);
	
	
}
