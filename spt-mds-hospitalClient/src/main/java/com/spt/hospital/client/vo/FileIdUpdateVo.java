/**
 * 
 */
package com.spt.hospital.client.vo;

/**
 * @author wlddh
 *
 */
public class FileIdUpdateVo {

	private Long id;
	private String fileId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

}
