package com.spt.hospital.client.vo.product;


import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
/**
 * 设备信息到门店管理  Vo
 * */
public class ProductInfoByStoreManagerVo extends MdsProductInfoEntity
{
	private static final long serialVersionUID = 3613568742412169588L;
	/* 租赁时长*/
	private Long leaseTime; 
	/* 租赁开始时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseStartDate; 
	/* 租赁结束时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseEndDate;
	/* 医院代理商/代理商地址/物业地址*/
	private String hospitalAddress;
	/* 医院合作租赁方*/
	private String hospitalPartners; 
	
	public String getHospitalPartners() {
		return hospitalPartners;
	}
	public void setHospitalPartners(String hospitalPartners) {
		this.hospitalPartners = hospitalPartners;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public Long getLeaseTime() {
		return leaseTime;
	}
	public void setLeaseTime(Long leaseTime) {
		this.leaseTime = leaseTime;
	}
	public Date getLeaseStartDate() {
		return leaseStartDate;
	}
	public void setLeaseStartDate(Date leaseStartDate) {
		this.leaseStartDate = leaseStartDate;
	}
	public Date getLeaseEndDate() {
		return leaseEndDate;
	}
	public void setLeaseEndDate(Date leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	} 
}
