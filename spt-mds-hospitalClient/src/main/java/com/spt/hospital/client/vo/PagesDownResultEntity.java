package com.spt.hospital.client.vo;

import java.util.List;


/**
 * 分页一览数据结果集
 * 
 * 下行应答Entity
 * 
 * @author liuxl
 */
public class PagesDownResultEntity<T>
{

	 private List<T> dataList;         // 数据
	    private long     totalCount;       // 数据总数
	    private int     currentPageNumber; // 当前页数
	    private long pageCount;
	    private int numberPerPage=10;
	    public long getTotalCount()
	    {
	        return totalCount;
	    }

	    public void setTotalCount(long totalCount)
	    {
	        this.totalCount = totalCount;
	    }

	    public int getCurrentPageNumber()
	    {
	        return currentPageNumber;
	    }

	    public void setCurrentPageNumber(int currentPageNumber)
	    {
	        this.currentPageNumber = currentPageNumber;
	    }

	    public List<T> getDataList()
	    {
	        return dataList;
	    }

	    public void setDataList(List<T> dataList)
	    {
	        this.dataList = dataList;
	    }

		public long getPageCount() {
			pageCount = totalCount % numberPerPage == 0 ? (totalCount / numberPerPage) : (totalCount / numberPerPage) + 1 ;
			
			return pageCount;
		}

		public void setPageCount(long pageCount) {
			this.pageCount = pageCount;
		}

		public int getNumberPerPage() {
			return numberPerPage;
		}

		public void setNumberPerPage(int numberPerPage) {
			this.numberPerPage = numberPerPage;
		}
}
