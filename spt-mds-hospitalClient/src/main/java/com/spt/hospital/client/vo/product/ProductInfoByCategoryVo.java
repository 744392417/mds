package com.spt.hospital.client.vo.product;


import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
/**
 * 设备信息到我的设备 设备详情 Vo
 * */
public class ProductInfoByCategoryVo extends MdsProductInfoEntity
{
	private static final long serialVersionUID = 3613568742412169588L;
	/* 租赁时长*/
	private Long leaseTime; 
	/* 租赁开始时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseStartDate; 
	/* 租赁结束时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date leaseEndDate;
	
	
	/* 租赁开始时间*/
	private String leaseStartDateStr; 
	/* 租赁结束时间*/
	private String leaseEndDateStr;
	
	/* 操作时间*/
	private String updatedDateStr;
	
	
	
	public String getUpdatedDateStr() {
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	public String getLeaseStartDateStr() {
		return leaseStartDateStr;
	}
	public void setLeaseStartDateStr(String leaseStartDateStr) {
		this.leaseStartDateStr = leaseStartDateStr;
	}
	public String getLeaseEndDateStr() {
		return leaseEndDateStr;
	}
	public void setLeaseEndDateStr(String leaseEndDateStr) {
		this.leaseEndDateStr = leaseEndDateStr;
	}
	/* 设备兼容状态 ： 1 默认 ,2异常 ,3低电量 ,4余额不足,5报修  */
	private String productStatusValue;
	/* 设备使用状态 ：1备选 ,3 暂停,  2 空闲, 4 使用中, 5  维修中 */
	private String productUseStatusValue;
	
	public String getProductStatusValue() {
		return productStatusValue;
	}
	public void setProductStatusValue(String productStatusValue) {
		this.productStatusValue = productStatusValue;
	}
	public String getProductUseStatusValue() {
		return productUseStatusValue;
	}
	public void setProductUseStatusValue(String productUseStatusValue) {
		this.productUseStatusValue = productUseStatusValue;
	}
	public Long getLeaseTime() {
		return leaseTime;
	}
	public void setLeaseTime(Long leaseTime) {
		this.leaseTime = leaseTime;
	}
	public Date getLeaseStartDate() {
		return leaseStartDate;
	}
	public void setLeaseStartDate(Date leaseStartDate) {
		this.leaseStartDate = leaseStartDate;
	}
	public Date getLeaseEndDate() {
		return leaseEndDate;
	}
	public void setLeaseEndDate(Date leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	} 
}
