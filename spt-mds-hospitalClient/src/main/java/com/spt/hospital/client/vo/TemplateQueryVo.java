package com.spt.hospital.client.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("模板请求参数vo")
public class TemplateQueryVo {
	@ApiModelProperty(value = "商品cd")
	private String typeCd;
	@ApiModelProperty(hidden=true)
	private String templateCat="matchFormConfig";
	@ApiModelProperty(hidden=true)
	private String lang = "CH";//默认中文
	@ApiModelProperty(value="数据列表")
	private List<String> dictCdList;//content字段对应要取出的数据字典
	@ApiModelProperty(value = "数据字典值")
	private String dictCd;//content字段对应的数据字典，只取一个
	
	@ApiModelProperty(value = "公司ID")
	private String companyId;
	
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getTypeCd() {
		return typeCd;
	}
	public void setTypeCd(String typeCd) {
		this.typeCd = typeCd;
	}
	public String getTemplateCat() {
		return templateCat;
	}
	public void setTemplateCat(String templateCat) {
		this.templateCat = templateCat;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public List<String> getDictCdList() {
		return dictCdList;
	}
	public void setDictCdList(List<String> dictCdList) {
		this.dictCdList = dictCdList;
	}
	public String getDictCd() {
		return dictCd;
	}
	public void setDictCd(String dictCd) {
		this.dictCd = dictCd;
	}
	
}
