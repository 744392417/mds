package com.spt.hospital.client.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description
 * @Author yh
 * @createTime 2020/02/28 18:00
 */
public class EmojiUtil {
    public static String filterEmoji(String nickName) {
        //nickName 所获取的用户昵称
        if (nickName == null) {
            return nickName;
        }
        Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
                Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
        Matcher emojiMatcher = emoji.matcher(nickName);
        if (emojiMatcher.find()) {
            //将所获取的表情转换为*
            nickName = emojiMatcher.replaceAll("(表情)");
            return nickName;
        }
        return nickName;
    }
}
