package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME + "/api/mdsUserFeedBack", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsUserFeedBackApi extends BaseClient<MdsUserFeedBackEntity> {

	@PostMapping("findByOpenidOrdernum")
	public List<MdsUserFeedBackEntity> findByOpenidOrdernum(@RequestBody QueryVo vo);
	
	@PostMapping("findByOrderNum")
	public List<MdsUserFeedBackEntity> findByOrderNum(@RequestBody QueryVo vo);
}
