/*
 * Created on 2005-6-20
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.spt.hospital.client.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ParseException;
/**
 *  计算金额
 * 
 * @author
 * 
 * 单价 ：priceUnit  封顶价 ： topBalance   递减参数 ：decreasPrice  最低租赁租金 ：minimumPrice
 */
public class PriceUtils {
	private static Logger logger = LoggerFactory.getLogger(PriceUtils.class);
	public static Integer calculatePrice(Integer priceUnit, Integer topBalance, Integer decreasPrice,
			Integer minimumPrice, Date startDate, Date endDate) {
		logger.info("calculatePrice=");
		logger.info(DateParser.getDistanceTimeHM(startDate, endDate));
		
		long day = 0;
		long hour = 0;
		long min = 0;
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		long diff = time2 - time1;
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);

		Integer price = 0;
		// logger.info("day==" +day);
		// 十分钟内免费
		Integer topBalanceLijia = topBalance;

		// 十分钟内免费
		if (day == 0 && hour == 0 && min <= 10) {
			price = 0;
		}
		Integer thours = topBalance / priceUnit;// 封顶小时
		// 第一天 十小时内 算法
		if (day == 0 && hour >= 0 && hour <= thours && min > 10) {
			price = (int) ((hour + 1) * priceUnit);
		}

		for (int d = 0; d < day; d++) {
			// logger.info("d==" + d);
			// logger.info("==="+(d == ((int) (day - 1))));
			// logger.info("topBalanceLijia==" + topBalanceLijia);
			// logger.info("topBalance==" + topBalance);

			// 十分钟内免费
			if (day == 0 && hour == 0 && min <= 10) {
				price = 0;
			} else {

				if ((d == ((int) (day - 1)))) {
					// logger.info("mmmm");
					Integer thour = topBalance / priceUnit;// 封顶小时
					// logger.info("thour>>>>>"+thour);
					// logger.info("hour>>"+hour);
					if (thour >= hour) {
						// logger.info("nnnnn");
						price += topBalance + (int) ((hour + 1) * priceUnit);
					} else {
						// logger.info("dddd");
						price += topBalance * 2;
					}
				} else {
					// logger.info("lll"+topBalanceLijia);
					price = topBalanceLijia;
				}
				topBalance = topBalance - decreasPrice;
				// logger.info("decreasPrice==" + topBalance);
				if (topBalance <= minimumPrice) {
					topBalance = minimumPrice;
				}
				topBalanceLijia += topBalance;

			}
			// logger.info("priceeeeeee==" + price);
		}

		return price;
	}
	
	/**
	 *  计算金额 小于 minHour; topBalance 元/单   超出 3元/单
	 * @author 《=12 20 
	 * minHour : <= minHour ; 25 topBalance ： 起步价 ; leaseUnit:3 单价
	 * 
	 */
	public static Integer calculateTwoPrice(Integer leaseUnit, Integer topBalance, Integer minHour, Date startDate,
			Date endDate) {
		Integer price = 0;
		logger.info(DateParser.getDistanceTimeHM(startDate, endDate));
		long day = 0;
		long hour = 0;
		long min = 0;
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		long diff = time2 - time1;
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - (day * 24 * 60 - hour * 60));
		logger.info("day=" + day + "hour=" + hour + "min=" + min);

		

		if (day == 0 && hour < minHour) {
			// 十分钟内免费
			if (day == 0 && hour == 0 && min < 10) {
				price = 0;
			} else {
				price = topBalance;
			}

		} else {

			if (day == 0) {
				price = topBalance + (int) (hour - 11) * leaseUnit;
			} else {
				int hourall = ((int) day * 24 + (int) (hour + 1)) - minHour;
				logger.info("hourall==="+hourall);
				price = topBalance + hourall * leaseUnit;

			}
		}
		logger.info("price==="+price);
		return price;
	}
	
	
	/**
	 *  计算金额 小于 minHour; topBalance 元/单   超出 3元/单
	 * @author 《=12 20 
	 * minHour : <= minHour ; 25 topBalance ： 起步价 ; leaseUnit:3 单价
	 * 
	 */
	public static BigDecimal calculatePriceNew(BigDecimal leaseUnit, BigDecimal topBalance, Integer minHour, Date startDate,
			Date endDate,Integer outHour) {
		BigDecimal price = BigDecimal.ZERO;
		logger.info(DateParser.getDistanceTimeHM(startDate, endDate));
		long day = 0;
		long hour = 0;
		long min = 0;
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		long diff = time2 - time1;
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		logger.info("day=" + day + "hour=" + hour + "min=" + min);

	//	BigDecimal b = new BigDecimal((float)leaseUnit/outHour);
		
		BigDecimal b = BigDecimal.ZERO;
		BigDecimal outHourb = new BigDecimal(outHour);
		 b = leaseUnit.divide(outHourb);
		
	  	BigDecimal setScale = b.setScale(2,BigDecimal.ROUND_UP);
	  	logger.info("outHour=" + outHourb + "leaseUnit=" + leaseUnit + "setScale=" + setScale);

		
	//	BigDecimal topBalanceb = new BigDecimal(topBalance);
		logger.info("c1b==topBalanceb="+topBalance);
		if (day == 0 && hour < minHour) {
			// 十分钟内免费
			if (day == 0 && hour == 0 && min < 10) {
				price = BigDecimal.ZERO;
			} else {
				price = topBalance;
			}

		} else {
	
			if (day == 0) {
		
				int haourall = (int) (hour+1);//总小时
				int c1 =haourall-minHour;
			//	logger.info("c1==="+c1);
				BigDecimal c1b = new BigDecimal(c1);
//				logger.info("c1b==="+c1b);
//				logger.info("c1b==multiply="+c1b.multiply(setScale));
//				logger.info("c1b==topBalanceb="+topBalance);
				price = topBalance.add(c1b.multiply(setScale));
			//	logger.info("c1b==price="+price);
			} else {
			//	BigDecimal leaseUnitb = new BigDecimal(leaseUnit);
				int hourall = ((int) day * 24 + (int) (hour + 1)) - minHour;
				BigDecimal hourallb = new BigDecimal(hourall);
				price = topBalance.add(hourallb.multiply(leaseUnit));

			}
		}
		logger.info("price==="+price);
		return price;
	}
	// public static Integer calculatePrice(Integer priceUnit,Date startDate,
	// Date endDate) {
	// // logger.info("单价="+priceUnit);
	// logger.info(DateParser.getDistanceTimeHM(startDate,endDate));
	// long day = 0;
	// long hour = 0;
	// long min = 0;
	// long time1 = startDate.getTime();
	// long time2 = endDate.getTime();
	// long diff = time2 - time1;
	// day = diff / (24 * 60 * 60 * 1000);
	// hour = (diff / (60 * 60 * 1000) - day * 24);
	// min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
	// logger.info("day="+day+"hour="+hour+"min="+min);
	// Integer price = 0;
	//
	// // 十分钟内免费
	// if (day == 0 && hour == 0 && min <= 10) {
	// price = 0;
	// }
	//
	// // 第一天 十小时内 算法
	// if (day == 0 && hour>=0 && hour <=9 && min >10) {
	// price = (int) ((hour + 1) * priceUnit);
	// }
	//
	// //第一天 十小时外 算法
	// if (day == 0 && hour > 9 && min > 0) {
	// price = 20;
	// }
	//
	//
	// // 第二天 9小时内算法
	// if (day == 1 && hour <= 8 && min > 0) {
	// price = 20 + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第二天 9小时外算法
	// if (day == 1 && hour > 8 && min > 0) {
	// price = 20 + 18;
	// }
	//
	//
	// // 第三天 8小时内算法
	// if (day == 2 && hour <=7 && min > 0) {
	// price = 20 + 18 + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第三天 8小时外算法
	// if (day == 2 && hour > 7 && min > 0) {
	// price = 20 + 18 + 16;
	// }
	//
	//
	// // 第四天 7小时内算法
	// if (day == 3 && hour <=6 && min > 0) {
	// price = 20 + 18 + 16 + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第四天 7小时外算法
	// if (day == 3 && hour >6 && min > 0) {
	// price = 20 + 18 + 16+ 14;
	// }
	//
	// // 第五天 6小时内算法
	// if (day == 4 && hour <=5 && min > 0) {
	// price = 20 + 18 + 16 + 14 + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第五天 6小时外算法
	// if (day == 4 && hour >5 && min > 0) {
	// price = 20 + 18 + 16+ 14 + 12;
	// }
	//
	// // 第六天 5小时内算法
	// if (day == 5 && hour <=4 && min > 0) {
	// price = 20 + 18 + 16 + 14 + 12 + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第六天 5小时外算法
	// if (day == 5 && hour >4 && min > 0) {
	// price = 20 + 18 + 16+ 14 + 12 + 10;
	// }
	//
	//
	// Integer dasy =20 + 18 + 16+ 14 + 12 + 10;//90
	// // 第七天开始 5 小时内算法
	// if (day >=6 && hour <=4 && min > 0) {
	// for(int d = 6;d<day;d++){
	// dasy+=10;
	// }
	// price = dasy + (int) ((hour + 1) * priceUnit);
	// }
	//
	// // 第七天开始 外算法
	// if (day >=6 && hour >4 && min > 0) {
	// for(int d = 6;d<=day;d++){
	// dasy+=10;
	// }
	// price = dasy;
	// }
	//
	// return price;
	// }

	//
	//
	//
	//
	
//	
//	 public static void main(String[] args) throws ParseException, java.text.ParseException {
//	 String s = "2019/01/31 18:16:30";
//	 //使用SimpleDateFormat实现字符串和日期的相互转换
//	 //BigDecimal leaseUnit, BigDecimal topBalance, Integer minHour, Date startDate,		Date endDate,Integer outHour
//	 SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//	 // logger.info("转换前的日期:"+s);
//	 //字符串转换数字，parse函数
//	 Date date = sdf2.parse(s);
//	 // logger.info("字符串类型的日期："+date);
//
//	 
//	 
//	 Integer topBalance = 5;
//		BigDecimal topBalanceb = new BigDecimal(topBalance);
//
//	 Integer minHour =1;
//
//		 Integer priceUnit = 2;
//		 BigDecimal priceUnitb = new BigDecimal(priceUnit);
//	 Integer outHour =1;
//
//	// logger.info("mainjiage=++++==================================="+PriceUtils.calculateTwoPrice(priceUnit,topBalance,minHour,date,new Date()));
//	 logger.info("mainjiage=++++===================11================"+PriceUtils.calculatePriceNew(priceUnitb,topBalanceb,minHour,date,new Date(),outHour));
//	 }

}
