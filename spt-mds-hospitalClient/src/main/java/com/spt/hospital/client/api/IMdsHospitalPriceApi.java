package com.spt.hospital.client.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME	+ "/api/mdsHospitalPrice", 
url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsHospitalPriceApi extends BaseClient<MdsHospitalPriceEntity> 
{
	@PostMapping("findPrice")
	public MdsHospitalPriceEntity findPrice(@RequestBody QueryVo queryVo);
	
	@PostMapping("findListByHospitalId")
	public List<MdsHospitalPriceEntity> findListByHospitalId(@RequestBody QueryVo queryVo);
}
