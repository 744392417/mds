package com.spt.hospital.client.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.spt.hospital.client.entity.MdsRecordsConptionEntity;

public class MdsRecordsConptionVo extends MdsRecordsConptionEntity 
{
	private static final long serialVersionUID = 1374713432731664772L;
	private Integer platDistrib; //平台收入百分比
	private Long categoryId; // 类别ID
	private String categoryName; // 类别名称
	private Long properId; //物业
	private String properName; //物业
	private Integer properDistribution;// 物业返佣比例
	private BigDecimal properFashion  = new BigDecimal(0.00);// 物业返佣金额
	private String agentId;// 代理商ID
	private String agentName;// 所属代理商
	private Integer agentDistribution;// 代理商返佣比例
	private BigDecimal agentFashion = new BigDecimal(0.00);// 代理商返佣金额
	private Integer hospitalDistribution = 0;// 医院分账返佣比例
	private BigDecimal hospitalFashion = new BigDecimal(0.00);// 医院分账金额
	//设备名称
	private String productName;
	//项目
	private String recordsProduct;
	//支出
	private BigDecimal expenditure= new BigDecimal(0.00);
	//收入
	private BigDecimal income= new BigDecimal(0.00);
	//附加
	private BigDecimal addition= new BigDecimal(0.00);
	//收到租金
	private BigDecimal obtain= new BigDecimal(0.00);
	//交易类型
	private String dealType;
	
	private String consumStatusValue;//状态
	
	
	
	//TODO getter && setter
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	private String createdDateStr;
	

	public BigDecimal getAddition() {
		return addition;
	}
	public void setAddition(BigDecimal addition) {
		this.addition = addition;
	}
	public BigDecimal getObtain() {
		return obtain;
	}
	public void setObtain(BigDecimal obtain) {
		this.obtain = obtain;
	}
	public String getConsumStatusValue() {
		return consumStatusValue;
	}
	public void setConsumStatusValue(String consumStatusValue) {
		this.consumStatusValue = consumStatusValue;
	}

	public String getRecordsProduct() {
		return recordsProduct;
	}
	public BigDecimal getExpenditure() {
		return expenditure;
	}
	public void setExpenditure(BigDecimal expenditure) {
		this.expenditure = expenditure;
	}
	public BigDecimal getIncome() {
		return income;
	}
	public void setIncome(BigDecimal income) {
		this.income = income;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public void setRecordsProduct(String recordsProduct) {
		this.recordsProduct = recordsProduct;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getHospitalDistribution() {
		return hospitalDistribution;
	}
	public void setHospitalDistribution(Integer hospitalDistribution) {
		this.hospitalDistribution = hospitalDistribution;
	}
	public BigDecimal getHospitalFashion() {
		return hospitalFashion;
	}
	public void setHospitalFashion(BigDecimal hospitalFashion) {
		this.hospitalFashion = hospitalFashion;
	}
	public Integer getPlatDistrib() {
		return platDistrib;
	}
	public void setPlatDistrib(Integer platDistrib) {
		this.platDistrib = platDistrib;
	}
	public Integer getProperDistribution() {
		return properDistribution;
	}
	public void setProperDistribution(Integer properDistribution) {
		this.properDistribution = properDistribution;
	}
	public Integer getAgentDistribution() {
		return agentDistribution;
	}
	public void setAgentDistribution(Integer agentDistribution) {
		this.agentDistribution = agentDistribution;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Long getProperId() {
		return properId;
	}
	public void setProperId(Long properId) {
		this.properId = properId;
	}
	public String getProperName() {
		return properName;
	}
	public void setProperName(String properName) {
		this.properName = properName;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public BigDecimal getProperFashion() {
		return properFashion;
	}
	public void setProperFashion(BigDecimal properFashion) {
		this.properFashion = properFashion;
	}
	public BigDecimal getAgentFashion() {
		return agentFashion;
	}
	public void setAgentFashion(BigDecimal agentFashion) {
		this.agentFashion = agentFashion;
	}
}
