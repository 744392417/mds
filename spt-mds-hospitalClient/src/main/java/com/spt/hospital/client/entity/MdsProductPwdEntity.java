package com.spt.hospital.client.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;

/**
 * @author hanson
 * 实体：设备密码表
 * */
@Entity
@Table(name = "tbl_product_pwd")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsProductPwdEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/*设备ID*/
	private Long productId;
	/* 设备名称*/
	private String productName; 
	/* 设备编号*/
	private String productCode; 
	/*二维码   */
	private String productQrcode; 
	/*开锁密码*/
	private String unlockPwd;
	
	/*关锁密码*/
	private String shutPwd;
	/*密码状态 ：未使用 0,使用中 1, 完成 2 */
	private String pwdStatus="1"; 

	private String keyId;
	
	private String remark;
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductQrcode() {
		return productQrcode;
	}
	public void setProductQrcode(String productQrcode) {
		this.productQrcode = productQrcode;
	}
	public String getUnlockPwd() {
		return unlockPwd;
	}
	public void setUnlockPwd(String unlockPwd) {
		this.unlockPwd = unlockPwd;
	}
	public String getShutPwd() {
		return shutPwd;
	}
	public void setShutPwd(String shutPwd) {
		this.shutPwd = shutPwd;
	}
	public String getPwdStatus() {
		return pwdStatus;
	}
	public void setPwdStatus(String pwdStatus) {
		this.pwdStatus = pwdStatus;
	}
}
