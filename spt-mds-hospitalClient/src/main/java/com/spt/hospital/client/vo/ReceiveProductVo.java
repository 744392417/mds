package com.spt.hospital.client.vo;

import java.util.List;

import com.spt.hospital.client.entity.MdsProductInfoEntity;

/**
 * @author zhangyibo
 * 接收参数VO
 * */
public class ReceiveProductVo 
{
	/* 200代表成功*/
	private Integer code = 200; 
	/*value = "中文信息",example="success"*/
	private String message ="success";
	/*交互类型*/
	private String interactiveType;
	
	/* 设备列表*/
	private List<MdsProductInfoEntity> productList;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getInteractiveType() {
		return interactiveType;
	}
	public void setInteractiveType(String interactiveType) {
		this.interactiveType = interactiveType;
	}
	public List<MdsProductInfoEntity> getProductList() {
		return productList;
	}
	public void setProductList(List<MdsProductInfoEntity> productList) {
		this.productList = productList;
	}
}
