//package com.spt.credit.client.api;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//
//import com.hsoft.commutil.base.BaseClient;
//import com.hsoft.commutil.client.FeignConfig;
//import com.spt.offer.client.constant.OfferConstants;
//import com.spt.offer.client.entity.BsTemplateConfig;
//import com.spt.offer.client.vo.DictDataVo;
//import com.spt.offer.client.vo.TemplateQueryVo;
//
//
//@FeignClient(name = OfferConstants.SERVER_NAME,path= OfferConstants.SERVER_NAME+"/api/bs/templateConfig",url=OfferConstants.SERVER_URL,configuration=FeignConfig.class)
//public interface IBsTemplateConfigApi extends BaseClient<BsTemplateConfig> {
//	@PostMapping("templateMap")
//	Map<String, Object> getTemplateMap(@RequestBody TemplateQueryVo queryVo);
//	@PostMapping("templateByDictCd")
//	List<DictDataVo> getTemplateByDictCd(@RequestBody TemplateQueryVo queryVo);
//	@PostMapping("queryTemplate")
//	BsTemplateConfig queryTemplate(@RequestBody TemplateQueryVo queryVo);
//	
//}
//
