package com.spt.hospital.client.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hsoft.commutil.base.IdEntity;

@Entity
@Table(name = "tbl_bank_master")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsBankMasterEntity extends IdEntity
{
	/**
	 * 银行基本信息
	 */
	private static final long serialVersionUID = -337558401472358073L;
	/*名称*/
	private String bankName;
	/*编号*/
	private String bankCode;
	/*启用状态*/
	private String enableFlg;
	/*备注*/
	private String memo;
	
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getEnableFlg() {
		return enableFlg;
	}
	public void setEnableFlg(String enableFlg) {
		this.enableFlg = enableFlg;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
