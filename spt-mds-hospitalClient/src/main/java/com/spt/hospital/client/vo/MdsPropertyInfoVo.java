package com.spt.hospital.client.vo;


import java.math.BigDecimal;
import java.util.List;


import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;


public class MdsPropertyInfoVo extends MdsPropertyInfoEntity 
{
	private static final long serialVersionUID = 6027198473612921351L;
	private String operType; // 操作类型 删除 D 修改 U 新增 A
	private String properBankName; // 物业银行名称
	private String properCapitalAccount; // 物业资金账户
	private String agentBankName; // 代理商银行名称
	private String agentCapitalAccount; // 代理商资金账户
	private String distributionName;// 返佣类型名称
	private Integer platDistrib; // 平台收入百分比
	private BigDecimal leaseDeposit = new BigDecimal(0.00); // 预付款
	private BigDecimal leaseUnit = new BigDecimal(0.00); // 租金 元/小时
	private BigDecimal topBalance = new BigDecimal(0.00); // 每次封顶价格 20 元/天
	private BigDecimal decreasPrice = new BigDecimal(0.00);// 递减价格(元/天)
	private BigDecimal minimumPrice = new BigDecimal(0.00);// 最低(元/天)
	private List<MdsProductInfoVo> mdsProductList;//设备列表
	private List<MdsUserInfoEntity> userInfoList;//用户列表
	private Long hospitalId;//医院ID
	
	
	
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	//统计数量
	//1、备选：在设备管理中录入，还没有分配医院的设备
	private Integer beixuanAllNum; 
	//2、空闲：在门店管理中配置好的设备，还没有使用，或使用后已经归还。
	private Integer kongxuanAllNum; 
	//3、暂停：只有空闲状态可以进入暂停状态，暂停状态设备不可使用。
	private Integer zantingAllNum; 
	//4、使用中：正在使用中
	private Integer shiyongAllNum; 
	//5、报修：客户通过系统进行报修，平台确认后，进入报修状态，用能使用
	private Integer baoxiuAllNum; 
	//6、异常：如果设备处于空闲状态超过2周（暂定），设备属性为异常。
	private Integer yichangAllNum; 
	//7、低电量：设备通信次数*0.002（通信500次）+待机天数*0.002（待机500天）=1就为低电量状态
	private Integer didianliangAllNum; 
	//8、逾期：客户预计收入/押金>80%，状态就是逾期，予以标记。
	private Integer yuqiAllNum;
	
	
	private String pwdDeleteId;
	
	
	public String getPwdDeleteId() {
		return pwdDeleteId;
	}
	public void setPwdDeleteId(String pwdDeleteId) {
		this.pwdDeleteId = pwdDeleteId;
	}
	public BigDecimal getLeaseDeposit() {
		return leaseDeposit;
	}
	public void setLeaseDeposit(BigDecimal leaseDeposit) {
		this.leaseDeposit = leaseDeposit;
	}
	public BigDecimal getLeaseUnit() {
		return leaseUnit;
	}
	public void setLeaseUnit(BigDecimal leaseUnit) {
		this.leaseUnit = leaseUnit;
	}
	public BigDecimal getTopBalance() {
		return topBalance;
	}
	public void setTopBalance(BigDecimal topBalance) {
		this.topBalance = topBalance;
	}
	public BigDecimal getDecreasPrice() {
		return decreasPrice;
	}
	public void setDecreasPrice(BigDecimal decreasPrice) {
		this.decreasPrice = decreasPrice;
	}
	public BigDecimal getMinimumPrice() {
		return minimumPrice;
	}
	public void setMinimumPrice(BigDecimal minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
	public Integer getPlatDistrib() {
		return platDistrib;
	}
	public void setPlatDistrib(Integer platDistrib) {
		this.platDistrib = platDistrib;
	}
	public String getDistributionName() {
		return distributionName;
	}
	public void setDistributionName(String distributionName) {
		this.distributionName = distributionName;
	}
	public String getProperBankName() {
		return properBankName;
	}
	public void setProperBankName(String properBankName) {
		this.properBankName = properBankName;
	}
	public String getProperCapitalAccount() {
		return properCapitalAccount;
	}
	public void setProperCapitalAccount(String properCapitalAccount) {
		this.properCapitalAccount = properCapitalAccount;
	}
	public String getAgentBankName() {
		return agentBankName;
	}
	public void setAgentBankName(String agentBankName) {
		this.agentBankName = agentBankName;
	}
	public String getAgentCapitalAccount() {
		return agentCapitalAccount;
	}
	public void setAgentCapitalAccount(String agentCapitalAccount) {
		this.agentCapitalAccount = agentCapitalAccount;
	}
	public String getOperType() {
		return operType;
	}
	public void setOperType(String operType) {
		this.operType = operType;
	}
	public List<MdsProductInfoVo> getMdsProductList() {
		return mdsProductList;
	}
	public void setMdsProductList(List<MdsProductInfoVo> mdsProductList) {
		this.mdsProductList = mdsProductList;
	}
	public List<MdsUserInfoEntity> getUserInfoList() {
		return userInfoList;
	}
	public void setUserInfoList(List<MdsUserInfoEntity> userInfoList) {
		this.userInfoList = userInfoList;
	}
	public Integer getBeixuanAllNum() {
		return beixuanAllNum;
	}
	public void setBeixuanAllNum(Integer beixuanAllNum) {
		this.beixuanAllNum = beixuanAllNum;
	}
	public Integer getKongxuanAllNum() {
		return kongxuanAllNum;
	}
	public void setKongxuanAllNum(Integer kongxuanAllNum) {
		this.kongxuanAllNum = kongxuanAllNum;
	}
	public Integer getZantingAllNum() {
		return zantingAllNum;
	}
	public void setZantingAllNum(Integer zantingAllNum) {
		this.zantingAllNum = zantingAllNum;
	}
	public Integer getShiyongAllNum() {
		return shiyongAllNum;
	}
	public void setShiyongAllNum(Integer shiyongAllNum) {
		this.shiyongAllNum = shiyongAllNum;
	}
	public Integer getBaoxiuAllNum() {
		return baoxiuAllNum;
	}
	public void setBaoxiuAllNum(Integer baoxiuAllNum) {
		this.baoxiuAllNum = baoxiuAllNum;
	}
	public Integer getYichangAllNum() {
		return yichangAllNum;
	}
	public void setYichangAllNum(Integer yichangAllNum) {
		this.yichangAllNum = yichangAllNum;
	}
	public Integer getDidianliangAllNum() {
		return didianliangAllNum;
	}
	public void setDidianliangAllNum(Integer didianliangAllNum) {
		this.didianliangAllNum = didianliangAllNum;
	}
	public Integer getYuqiAllNum() {
		return yuqiAllNum;
	}
	public void setYuqiAllNum(Integer yuqiAllNum) {
		this.yuqiAllNum = yuqiAllNum;
	}
}