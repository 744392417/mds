package com.spt.hospital.client.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseClient;
import com.hsoft.commutil.client.FeignConfig;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.vo.MdsProductPwdVo;
import com.spt.hospital.client.vo.QueryVo;

@FeignClient(name = HospConstants.SERVER_NAME, path = HospConstants.SERVER_NAME+ "/api/mdsProductPwd", url = HospConstants.SERVER_URL, configuration = FeignConfig.class)
public interface IMdsProductPwdApi extends BaseClient<MdsProductPwdEntity> {

	@PostMapping("getMdsProductPwdInfo")
	public MdsProductPwdEntity getMdsProductPwdInfo(@RequestBody QueryVo queryVo);
	
	@PostMapping("updatePwdStatus")
	public void updatePwdStatus(@RequestBody MdsProductPwdVo vo);
	
	@PostMapping("getPwdByProductCode")
	public void getPwdByProductCode(@RequestBody MdsProductPwdVo vo);
	
	@PostMapping("getPushKey")
	public MdsProductPwdEntity getPushKey(@RequestBody MdsProductPwdVo vo);
	
	
	@PostMapping("sendReturnKey")
	public void sendReturnKey(@RequestBody MdsProductPwdVo vo);
	
}
