package com.spt.hospital.client.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hsoft.commutil.base.IdEntity;

/**
 * @author zhangyibo
 * 实体：设备交互记录表
 * */
@Entity
@Table(name = "tbl_interactive_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MdsInteractiveLogEntity extends IdEntity 
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 设备ID*/
	private String productCode;
	/*交互类型 接收密码  P  版本升级发送 VS   版本升级发送 VR   设备状态  S*/
	private String interactive_type;
	/* 设备状态 ：1备选 2 空闲 3 暂停 4 使用中 5 报修 相互冲突     6异常 7 低电量 8 逾期。 可以并行*/
	private String productStatus;
	/*设备版本号*/
	private String productVersion;
	/*设备密码*/
	private String productPwd;
	/*交互时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "GMT+08:00")
	private Date interactiveTime;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getInteractive_type() {
		return interactive_type;
	}
	public void setInteractive_type(String interactive_type) {
		this.interactive_type = interactive_type;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public String getProductVersion() {
		return productVersion;
	}
	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}
	public String getProductPwd() {
		return productPwd;
	}
	public void setProductPwd(String productPwd) {
		this.productPwd = productPwd;
	}
	public Date getInteractiveTime() {
		return interactiveTime;
	}
	public void setInteractiveTime(Date interactiveTime) {
		this.interactiveTime = interactiveTime;
	}
}
