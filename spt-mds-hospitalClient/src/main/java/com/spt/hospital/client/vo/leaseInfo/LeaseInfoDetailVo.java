package com.spt.hospital.client.vo.leaseInfo;


import com.spt.hospital.client.entity.MdsLeaseInfoEntity;

/**
 * 租赁订单详情Vo
 * */
public class LeaseInfoDetailVo extends MdsLeaseInfoEntity
{
	private static final long serialVersionUID = 2504878480554966113L;
	/* 昵称*/
	private String nickName; 
	/* 用户名*/
	private String userName; 
	/* 用户手机*/
	private String userPhone; 
	/* 登录名称*/
	private String loginName; 
	/* 登录密码*/
	private String loginPwd;
	
	/*反馈内容*/
	private String feedbackContent;
	/*描述*/
	private String describe;
	
	
	
	public String getFeedbackContent() {
		return feedbackContent;
	}
	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	} 
}
