/**
 * 
 */
package test.hsoft.admin.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import com.hsoft.commutil.config.CommConfig;
import com.spt.hospital.client.config.HospClientConfig;


/**
 * @author huangjian
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages={"com.hsoft.admin.client.remote"})
@Import({CommConfig.class,HospClientConfig.class})
public class App {
	
}
