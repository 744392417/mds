<#--${pojo.getPackageDeclaration()}-->
package com.spt.offer.client.remote;
<#assign classbody>
<#assign declarationName = pojo.importType(pojo.getDeclarationName())>
<#assign offerConstants = pojo.importType("com.spt.offer.client.constant.OfferConstants")>
<#assign serverName =offerConstants+".SERVER_NAME">
<#assign url = offerConstants+".SERVER_URL">

@${pojo.importType("org.springframework.cloud.netflix.feign.FeignClient")}(name = ${serverName},path= ${serverName}+"/bs/path",url=${url},configuration=${pojo.importType("com.hsoft.commutil.client.FeignConfig")+".class"})
public interface I${declarationName}Client extends ${pojo.importType("com.hsoft.commutil.base.BaseClient")}<${declarationName}> {
	
}
</#assign>

${pojo.generateImports()}
import ${pojo.getQualifiedDeclarationName()};

${classbody}
