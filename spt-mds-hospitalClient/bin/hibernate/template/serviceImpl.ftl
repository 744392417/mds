<#--${pojo.getPackageDeclaration()}-->
package com.spt.offer.server.service.impl;
<#assign classbody>
<#assign declarationName = pojo.importType(pojo.getDeclarationName())>
<#assign entityName = declarationName?uncap_first>
<#assign daoName = declarationName+"Dao">
<#assign serviceName = "I"+declarationName+"Service">
@${pojo.importType("org.springframework.stereotype.Component")}
@${pojo.importType("org.springframework.transaction.annotation.Transactional")}(readOnly = true)
public class ${declarationName}ServiceImpl extends ${pojo.importType("com.hsoft.commutil.base.BaseService")}<${declarationName}> implements ${pojo.importType("com.spt.offer.server.service."+serviceName)} {
	@${pojo.importType("org.springframework.beans.factory.annotation.Autowired")}
	private ${pojo.importType("com.spt.offer.server.dao."+daoName)} ${entityName}Dao;
	
	@Override
	public ${pojo.importType("com.hsoft.commutil.base.BaseDao")}<${declarationName}> getBaseDao() {
		return ${entityName}Dao;
	}
	
	@Override
	public Class<${declarationName}> getEntityClazz() {
		return ${declarationName}.class;
	}
	
}
</#assign>

${pojo.generateImports()}
import ${pojo.getQualifiedDeclarationName()};

${classbody}
