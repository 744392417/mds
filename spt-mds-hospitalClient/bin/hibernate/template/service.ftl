<#--${pojo.getPackageDeclaration()}-->
package com.spt.offer.server.service;
<#assign classbody>
<#assign declarationName = pojo.importType(pojo.getDeclarationName())>
<#assign daoName = declarationName+"Dao">
public interface I${declarationName}Service extends ${pojo.importType("com.hsoft.commutil.base.IBaseService")}<${declarationName}> {
	
}
</#assign>

${pojo.generateImports()}
import ${pojo.getQualifiedDeclarationName()};

${classbody}
