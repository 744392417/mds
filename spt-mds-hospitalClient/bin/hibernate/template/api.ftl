<#--${pojo.getPackageDeclaration()}-->
package com.spt.offer.server.api;
<#assign classbody>
<#assign declarationName = pojo.importType(pojo.getDeclarationName())>
<#assign entityName = declarationName?uncap_first>
<#assign daoName = declarationName+"Dao">
<#assign wordlist = declarationName?word_list>
<#assign serviceName = "I"+declarationName+"Service">

@${pojo.importType("org.springframework.web.bind.annotation.RestController")}
@${pojo.importType("org.springframework.web.bind.annotation.RequestMapping")}(value = "bs/path")
public class ${declarationName}Api extends ${pojo.importType("com.hsoft.commutil.base.BaseApi")}<${declarationName}> {
	@${pojo.importType("org.springframework.beans.factory.annotation.Autowired")}
	private ${pojo.importType("com.spt.offer.server.service."+serviceName)} ${entityName}Service;
	
	@Override
	public ${pojo.importType("com.hsoft.commutil.base.IBaseService")}<${declarationName}> getService() {
		return ${entityName}Service;
	}
	
	
	
}
</#assign>

${pojo.generateImports()}
import ${pojo.getQualifiedDeclarationName()};

${classbody}
