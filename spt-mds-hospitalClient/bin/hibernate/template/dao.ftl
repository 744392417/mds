<#--${pojo.getPackageDeclaration()}-->
package com.spt.offer.server.dao;
<#assign classbody>
<#assign declarationName = pojo.importType(pojo.getDeclarationName())>
<#assign entityName = declarationName?uncap_first>
public interface ${declarationName}Dao extends ${pojo.importType("com.hsoft.commutil.base.BaseDao")}<${declarationName}> {
}
</#assign>

${pojo.generateImports()}
import ${pojo.getQualifiedDeclarationName()};

${classbody}
