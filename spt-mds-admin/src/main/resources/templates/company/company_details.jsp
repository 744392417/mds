<style>
.control
{
    padding-left: 10px;
    width: 190px;
}
.nofile{color: #3b3b43!important;text-decoration:none!important;cursor: default!important;}
.showAttach{cursor: pointer;}
.show{color: #20aae9;text-decoration: underline;}
.show:hover{opacity: 0.8;filter: alpha(opacity=80);}
span.attach{padding-left: 110px;}
.webuploader-pick{background: #ffffff!important;}
.webuploader-pick-hover{background: #ffffff!important;}
.t1{margin-left: 110px!important;float:right;}
.t1 span img{width: 60px;height: 60px;border: 1px solid #e5e5e5;cursor:pointer;}
.t1 span a{font-size: 12px;text-align: left;cursor: pointer;display: block;margin-top: 2px;height: 16px;}
</style>
<div class="easyui-layout" fit="true">	
	<div region="center" style="padding:5px 20px 5px 5px;position: relative;overflow:hidden;" border="false">
		<form id="comInfo" method="post"  class="editForm" style="overflow:hidden;">
			<p>
			<span>
			<label class="control-label">企业TAG:</label>
			<input class="control" id="companyTag" name=companyTag type="text"  th: th:value="${companyInfo.companyTag}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">企业名称:</label>
			<input class="control" id="companyName" name="companyName" type="text"  th: th:value="${companyInfo.companyName}" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">联系人:</label>
			<input class="control" id="contactPerson" name="contactPerson" type="text"  th: th:value="${companyInfo.contactPerson}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">电话:</label>
			<input class="control" name="mobileNumber" id="mobileNumber" type="text"  th:value="${companyInfo.mobileNumber}" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">企业邮箱:</label>
			<input class="control" name="email" id="email" type="text"  th:value="${companyInfo.email}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">营业地址:</label>
			<input class="control" id="address" name="address" type="text"  th:value="${companyInfo.address}" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">行业类别:</label>
			<input class="control" style="text-overflow:ellipsis;" id="companyIndustry" name="companyIndustry" type="text"  th:value="${companyInfo.companyIndustry}" title="${companyInfo.companyIndustry}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">注册时间:</label>
			<input class="control" id="submitTime" name="submitTime" type="text"  th:value="<fmt:formatDate  th:value='${companyInfo.submitTime }' pattern='yyyy-MM-dd HH:mm'/>" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">法人证件有效期 :</label>
			<input class="control" style="text-overflow:ellipsis;" id="legalPersonCertValid" name="legalPersonCertValid" type="text"  th:value="<fmt:formatDate  th:value='${companyInfo.legalPersonCertValid }' pattern='yyyy-MM-dd HH:mm'/>" title="${companyInfo.legalPersonCertValid}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">营业执照有效期 :</label>
			<input class="control" style="text-overflow:ellipsis;" id="businessLicenseValid" name="businessLicenseValid" type="text"  th:value="<fmt:formatDate  th:value='${companyInfo.businessLicenseValid }' pattern='yyyy-MM-dd HH:mm'/>" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">是否三证合一 :</label>
			<select class="control" id="threeFlag" name="threeFlag" style="width:240px;height:29px;"disabled="disabled">
				    <option  th:value="true" >是</option>
				    <option  th:value="false" >否</option>
			</select >
			<%-- <input class="control" style="text-overflow:ellipsis;width: 240px;height: 22px;" id="threeFlag" name="threeFlag" type="text"  th:value="${companyInfo.threeFlag}" title="${companyInfo.threeFlag}" disabled="disabled"/> --%>
			</span>
			<span>
			<label class="control-label">法人证件类型 :</label>
				 <select class="control" id="legalPersonType" name="legalPersonType" style="width:240px;height:29px;"disabled="disabled">
				    <option  th:value="1" >身份证</option>
				    <option  th:value="2" >港澳通行证</option>
				    <option  th:value="3" >护照</option>
				    <option  th:value="4" >台胞证</option>
			   </select>
			<%-- <input class="control" style="text-overflow:ellipsis;width: 240px;height: 22px;" id="legalPersonType" name="legalPersonType" type="text"  th:value="${companyInfo.legalPersonType }" disabled="disabled"/> --%>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">营业执照:</label>
			<input class="control" id="licenseCode" name="licenseCode" type="text"  th:value="${companyInfo.licenseCode}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">其他联系方式:</label>
			<input class="control" id="phoneNumber" name="phoneNumber" type="text"  th:value="${companyInfo.phoneNumber}" disabled="disabled"/>
			</span>
			</p>
			<p>
			<span>
			<label class="control-label">组织机构代码:</label>
			<input class="control" style="text-overflow:ellipsis;" id="organizationCode" name="organizationCode" type="text"  th:value="${companyInfo.organizationCode}" title="${companyInfo.organizationCode}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">注册资本:</label>
			<input class="control" style="text-overflow:ellipsis;" id="registCapital" name="registCapital" type="text"  th:value="${companyInfo.registCapital}" title="${companyInfo.registCapital}" disabled="disabled"/>
			</span>
			</p> 
 			<p>
			<span>
			<label class="control-label">营业范围:</label>
			<input class="control" style="text-overflow:ellipsis;" id="businessScope" name="businessScope" type="text"  th:value="${companyInfo.businessScope}" title="${companyInfo.businessScope}" disabled="disabled"/>
			</span>
			<span>
			<label class="control-label">法人代表:</label>
			<input class="control" style="text-overflow:ellipsis;" id="representative" name="representative" type="text"  th:value="${companyInfo.representative}" title="${companyInfo.representative}" disabled="disabled"/>
			</span>
			</p> 
			<p>
			<label class="control-label">法人身份证:</label>
			<input class="control" style="text-overflow:ellipsis;" id="legalPersonCertNo" name="legalPersonCertNo" type="text"  th:value="${companyInfo.legalPersonCertNo}" title="${companyInfo.legalPersonCertNo}" disabled="disabled"/>
			<label class="control-label">注册地:</label>
			<input class="control" style="text-overflow:ellipsis;" id="registPlace" name="registPlace" type="text"  th:value="${companyInfo.registPlace}" title="${companyInfo.registPlace}" disabled="disabled"/>
			</p> 
			<c:if test="${companyInfo.threeFlag==false}">
			<p class="p1">
			<label class="control-label">组织机构代码证:</label>
			<span class="fileShow" id="${companyInfo.organizationCodePicUrl}">
				<c:if test="${not empty companyInfo.organizationCodePicUrl }">
						<embed src="${ctx }/file/small/${companyInfo.organizationCodePicUrl}" alt=""  height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
				</c:if>
				&nbsp;
				</span>
			<label class="control-label">税务登记证:</label>
			<span class="fileShow" id="${companyInfo.taxRegisterPicUrl}">
				<c:if test="${not empty companyInfo.taxRegisterPicUrl }">
						<embed src="${ctx }/file/small/${companyInfo.taxRegisterPicUrl}" alt="" height=70 width=70 />
						<a class="colorlan checkImg">查看</a>
					</c:if>
				</span>
			</p>
			</c:if>
			<p class="p1">
			<label class="control-label">法人身份证:</label>
				<span class="fileShow" id="${companyInfo.legalPersonPicUrl}">
				<c:if test="${not empty companyInfo.legalPersonPicUrl }">
						<embed src="${ctx }/file/small/${companyInfo.legalPersonPicUrl}" alt=""  height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
				</c:if>
				&nbsp;
				</span>
<%-- 			<label class="control-label">法人身份证反面:</label>
				<span class="fileShow" id="${companyInfo.legalPersonPicUrl}">
				<c:if test="${not empty companyInfo.legalPersonPicUrl }">
						<embed src="${ctx }/file/small/${companyInfo.legalPersonPicUrl}" alt=""  height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
				</c:if>
				</span> --%>
			</p>
			<p class="p1">
			<label class="control-label">营业执照:</label>
				
				<span class="fileShow" id="${companyInfo.businessLicenseUrl}">
				<c:if test="${not empty companyInfo.businessLicenseUrl }">
						<embed src="${ctx }/file/small/${companyInfo.businessLicenseUrl}" alt=""  height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
						</c:if>
						&nbsp;
				</span>
					
			<c:if test="${companyInfo.threeFlag==false}">	
			<label class="control-label">社会信用证:</label>
				<span class="fileShow" id="${companyInfo.communityCreditPicUrl}">
				<c:if test="${not empty companyInfo.communityCreditPicUrl }">
						<embed src="${ctx }/file/small/${companyInfo.communityCreditPicUrl}" alt=""  height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
				</c:if>
				</span>
			</c:if>
			</p>
						<p  class="p1">
			<label class="control-label">委托函:</label>
				<span class="fileShow" id="${companyInfo.entrustAttachId}">
				<c:if test="${not empty companyInfo.entrustAttachId }">
					<embed src="${ctx }/file/small/${companyInfo.entrustAttachId}"  alt="PDF文件请点击查看"   height=70 width=70/>
						<a class="colorlan checkImg">查看</a>
				</c:if>
				</span>
			</p>
		</form>
	</div>
</div>
<script>
var _legalPersonType = '${companyInfo.legalPersonType}'; 
var _threeFlag = '${companyInfo.threeFlag}'; 
	$(function(){
		$("#threeFlag").combobox({
			valueField: 'value',
		    textField: 'text',
			editable:false,
			multiple:true,
		});
		setCombobox1();
		$("#legalPersonType").combobox({
			valueField: 'value',
		    textField: 'text',
			editable:false,
			multiple:true,
		});
		setCombobox2();
		/*  var threeFlagjson = [{name:'是',value:'true'},{name:'否',value:'false'}];
		$('#threeFlag').combobox({
			data:threeFlagjson,
			textField:'name',
			valueField:'value',
		});
		 //1，身份证；2，港澳通行证；3，护照；4，台胞证
		var legalPersonTypejson = [{name:'身份证',value:'1'},{name:'港澳通行证',value:'2'},{name:'护照',value:'3'},{name:'台胞证',value:'4'}];
		$('#legalPersonType').combobox({
			data:legalPersonTypejson,
			textField:'name',
			valueField:'value',
		});   */
		//查看图片
		$(".p1").delegate("span img,.checkImg","click",function(){
			var id = $(this).parent().attr("id");
			window.open(_ctx + "/file/show/"+id, "查看三证");
		});
	})
	function setCombobox1(){
	//取消默认选择第一行
	$("#threeFlag").combobox("clear");
	var _params = $("#threeFlag").combobox('getData');
	$.each(_params,function(index,obj){
			if(obj.value ==_threeFlag){
				$("#threeFlag").combobox("select",obj.value);
			}
	});
}
function setCombobox2(){
		//取消默认选择第一行
		$("#legalPersonType").combobox("clear");
		var _params = $("#legalPersonType").combobox('getData');
		$.each(_params,function(index,obj){
				if(obj.value ==_legalPersonType){
					$("#legalPersonType").combobox("select",obj.value);
				}
		});
	}
</script>