FilePlugin = function(domId,bizId,url_updateFileId,hasEditFile) {
	this.domFileId = domId;
	this.bizId = bizId;
	this.url_updateFileId = url_updateFileId;
	if (hasEditFile==undefined){
		hasEditFile=false;
	}
	this.hasEditFile=hasEditFile;
};
FilePlugin.prototype = {
	load : function(fileIds, _showDomId) {
//		console.log(fileIds, _showDomId);
		$.post(_ctx + '/file/loadFiles?hasEditFile='+this.hasEditFile, {
			fileIds : fileIds
		}, function(data) {

			$(_showDomId).html(data);

		});
	},
	uploadSuccess:function(fileId){
		var _domFileId =this.domFileId;
		var _url_updateFileId =this.url_updateFileId;
		var _bizId =this.bizId;
		
		var oldFileId = $(_domFileId).val();
		var newFileId = oldFileId + fileId + ",";
//		console.log(oldFileId,newFileId);
		$(_domFileId).val(newFileId);
		$.post(_ctx+_url_updateFileId,{id:_bizId,fileId:newFileId},function(d){
//			console.log(d);
		})
		
	},
	del : function(fileId) {
		var _domFileId =this.domFileId;
		var _url_updateFileId =this.url_updateFileId;
		var _bizId =this.bizId;
		$.post(_ctx + '/file/delete', {
			fileId : fileId
		}, function(r) {
			if (r.success) {
				var oldFileId = $(_domFileId).val();
				var newFileId = oldFileId.replace(fileId + ",", '');
//				console.log(oldFileId,newFileId);
				$(_domFileId).val(newFileId);
				$('#file'+fileId).remove();
				$.post(_ctx+_url_updateFileId,{id:_bizId,fileId:newFileId},function(d){
//					console.log(d);
				})
				
			}
		});
	}

}

function fileUpload(fileUploadId, fuSaveId) {
	var $this = $('#' + fuSaveId);
	// fileUploadId:点击弹出选择文件事件的dom对象
	// fusaveId:文件上传成功后，保存的服务文件Id
	// --start-文件上传---
	var uploader = WebUploader.create({
		auto : true,
		// swf文件路径
		swf : _ctx + '/static/webuploader-0.1.5/Uploader.swf',
		// 文件接收服务端。
		server : _ctx + '/file/uploadFile',
		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick : '#' + fileUploadId,
		// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
		resize : false,
		duplicate :true ,//true可重复上传，默认不可多次重复上传
		// 只允许选择图片文件。
		accept : {
			title : 'file',
			extensions : 'gif,jpg,jpeg,bmp,png,pdf,rar,zip,doc,docx',
//				mimeTypes :	"image/*"
			mimeTypes :	"image/jpg,image/jpeg,image/gif,image/bmp,image/png,application/pdf,application/rar,application/zip,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
		}
	});
	// 去掉文件上传组件样式，使用上传dom 自己样式
	$(".webuploader-pick").removeClass();
	// 当有文件添加进来的时候
	uploader.on('fileQueued', function(file) {
		$.messager.progress(); 
		//var $li = $('<div id="' + file.id + '" class="file-item thumbnail">' + '<img>' + '<div class="info">' + file.name + '</div>' + '</div>'), $img = $li.find('img');
	});

	// 文件上传成功，给item添加成功class, 用样式标记上传成功。
	uploader.on('uploadSuccess', function(file, response) {
		$.messager.progress('close');
		if(file &&file.size > 10485760){
			$.messager.alert('tip',"上传文件不能超过10M!",'info');
			return;
		}
		if(response && response.result){//上传成功
			var fileId = response.fileId;
		
			filePlugin.uploadSuccess(fileId);
			//加载附件列表
			filePlugin.load($this.val(),'#span_'+fuSaveId);
			
		}else{
			$.messager.alert('tip',"上传文件失败，失败信息："+ response._raw,'info');
		}
	
	});
	uploader.on('error',function(file){
		$.messager.progress('close');
		$.messager.alert('tip','请选择图片','info');
	})
	// 文件上传失败，显示上传出错。
	uploader.on('file', function(file) {
		$.messager.progress('close');
		$.messager.alert('tip','文件上传失败,请重试!','info');
	});
	// --end-文件上传---
}
