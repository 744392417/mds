/**
 * 
 */
package com.spt.mds.admin.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.hsoft.admin.client.entity.SysApp;
import com.hsoft.admin.client.entity.SysDept;
import com.hsoft.admin.client.entity.SysMenu;
import com.hsoft.admin.client.entity.SysPermission;
import com.hsoft.admin.client.entity.SysResource;
import com.hsoft.admin.client.entity.SysRole;
import com.hsoft.admin.client.entity.SysUser;
import com.hsoft.commutil.easyui.EasyTreeNode;
import com.hsoft.commutil.util.CollectionUtil;

/**
 * @author jianhuang
 * 
 */
public class EasyTreeUtil {
	private static final String PARENT_ID="parentId";
	public static EasyTreeNode getRoot(String name) {
		EasyTreeNode root = new EasyTreeNode();
		root.setId("0");
		root.setText(name);
		root.setState(EasyTreeNode.STATE_OPEN);
		return root;
	}

	/** 菜单树 */
	public static EasyTreeNode getMenuTree(List<SysMenu> menus) {
		return getCheckMenuTree(menus, null);
	}

	/** 资源权限树 */
	public static EasyTreeNode gePermCheckedTree(List<SysPermission> permissions, List<SysPermission> lstSelect) {
		EasyTreeNode root = getRoot("All Permissions");
		for (SysPermission permission : permissions) {
			EasyTreeNode resultVo = new EasyTreeNode();
			resultVo.setText(permission.getPermName() + "(" + permission.getPermCd() + ")");
			resultVo.setId(String.valueOf(permission.getId()));
			boolean isContain = CollectionUtil.contain(lstSelect, "id", permission.getId());
			resultVo.setChecked(isContain);
			resultVo.setNodeType("permission");
			root.getChildren().add(resultVo);
		}
		return root;
	}

	/** 权限角色树 */
	public static EasyTreeNode getPermRoleTree(List<SysRole> roles, List<SysRole> lstSelect) {
		EasyTreeNode root = getRoot("All Roles");
		for (SysRole role : roles) {
			EasyTreeNode resultVo = new EasyTreeNode();
			resultVo.setText(role.getRoleName() + "(" + role.getRoleCd() + ")");
			resultVo.setId(String.valueOf(role.getId()));
			boolean isContain = CollectionUtil.contain(lstSelect, "id", role.getId());
			resultVo.setChecked(isContain);
			resultVo.setNodeType("role");
			root.getChildren().add(resultVo);
		}
		return root;
	}

	/** 权限资源树 */
	public static EasyTreeNode getPermResourceTree(List<SysResource> resources, List<SysResource> lstSelect) {
		EasyTreeNode root = getRoot("All Resources");
		for (SysResource resource : resources) {
			EasyTreeNode resultVo = new EasyTreeNode();
			resultVo.setText(resource.getUrl());
			resultVo.setId(String.valueOf(resource.getId()));
			boolean isContain = CollectionUtil.contain(lstSelect, "id", resource.getId());
			resultVo.setChecked(isContain);
			resultVo.setNodeType("resource");
			root.getChildren().add(resultVo);
		}
		return root;
	}

	private static EasyTreeNode createMeneNode(SysMenu menu, List<SysMenu> lstSelect) {
		EasyTreeNode resultVo = new EasyTreeNode();
		resultVo.setText(menu.getMenuName());
		resultVo.setState(EasyTreeNode.STATE_OPEN);
		resultVo.setId(String.valueOf(menu.getId()));
		resultVo.addAttr("url", menu.getUrl());
		resultVo.setNodeType("menu");
		resultVo.addAttr("appId", menu.getApp().getId());
		resultVo.addAttr(PARENT_ID, menu.getParent() == null ? "" : menu.getParent().getId());
		if (lstSelect != null) {
			boolean isContain = CollectionUtil.contain(lstSelect, "id", menu.getId());
			resultVo.setChecked(isContain);
		}
		return resultVo;
	}

	private static EasyTreeNode createAppNode(SysApp app) {
		EasyTreeNode resultVo = new EasyTreeNode();
		resultVo.setText(app.getAppName());
		resultVo.setState(EasyTreeNode.STATE_OPEN);
		resultVo.setId(String.valueOf("app" + app.getId()));
		resultVo.addAttr("appCode", app.getAppCode());
		resultVo.setNodeType("app");
		return resultVo;
	}

	private static void initMapMenus(List<SysMenu> lstAll, SysMenu menu) {
		if (!CollectionUtil.contain(lstAll, "id", menu.getId())) {
			lstAll.add(menu);
			if (menu.getParent() != null) {
				initMapMenus(lstAll, menu.getParent());
			}
		}
	}

	/** 权限菜单树 */
	public static EasyTreeNode getCheckMenuTree(List<SysMenu> menus, List<SysMenu> lstSelect) {
		EasyTreeNode root = getRoot("All Menus");
		List<SysMenu> lstAll = new ArrayList<SysMenu>();
		for (SysMenu menu : menus) {
			initMapMenus(lstAll, menu);
		}
		CollectionUtil.sortList(lstAll, "dispOrderNo");
		// Collections.sort(list, c)
		Map<Long, EasyTreeNode> mapId2Vo = new LinkedHashMap<Long, EasyTreeNode>();
		Map<Long, EasyTreeNode> mapId2AppNode = new LinkedHashMap<Long, EasyTreeNode>();
		Map<Long, EasyTreeNode> mapAppId2AppNode = new LinkedHashMap<Long, EasyTreeNode>();
		for (SysMenu menu : lstAll) {
			EasyTreeNode resultVo = createMeneNode(menu, lstSelect);
			mapId2Vo.put(menu.getId(), resultVo);
			if (!mapId2AppNode.containsKey(menu.getId()) && menu.getParent() == null) {
				EasyTreeNode appNode;
				if (!mapAppId2AppNode.containsKey(menu.getApp().getId())) {
					appNode = createAppNode(menu.getApp());
					root.getChildren().add(appNode);
					mapAppId2AppNode.put(menu.getApp().getId(), appNode);
				} else {
					appNode = mapAppId2AppNode.get(menu.getApp().getId());
				}
				mapId2AppNode.put(menu.getId(), appNode);
			}
		}

		for (SysMenu menu : lstAll) {
			EasyTreeNode resultVo = mapId2Vo.get(menu.getId());
			if (menu.getParent() == null) {
				EasyTreeNode appNode = mapId2AppNode.get(menu.getId());
				appNode.getChildren().add(resultVo);
				// root.getChildren().add(resultVo);
			} else {
				EasyTreeNode parentVo = mapId2Vo.get(menu.getParent().getId());
				parentVo.getChildren().add(resultVo);
				parentVo.setState(EasyTreeNode.STATE_CLOSED);
			}
		}
		for (EasyTreeNode node : root.getChildren()) {
			node.setState(EasyTreeNode.STATE_OPEN);
		}
		return root;
	}

	/** 机构树 */
	public static EasyTreeNode getDeptTree(List<SysDept> depts) {
		return getDeptTree(depts, false,false, null);
	}

	public static EasyTreeNode getDeptTree(List<SysDept> depts, boolean initUser) {
		return getDeptTree(depts, initUser,false, null);
	}
	public static EasyTreeNode getDeptTree(List<SysDept> depts, boolean initUser,boolean onlyUserId) {
		return getDeptTree(depts, initUser,onlyUserId,null);
	}

	/** 机构人员树 */
	public static EasyTreeNode getDeptTree(List<SysDept> depts, boolean initUser,boolean onlyUserId, List<SysUser> lstSelect) {
		EasyTreeNode root = getRoot("All Depts");

		Map<Long, EasyTreeNode> mapId2Vo = new LinkedHashMap<Long, EasyTreeNode>();
		for (SysDept dept : depts) {
			EasyTreeNode resultVo = new EasyTreeNode();
			resultVo.setText(dept.getDeptName());
			resultVo.setState(EasyTreeNode.STATE_OPEN);
			if (!onlyUserId){
				resultVo.setId(String.valueOf(dept.getId()));
			}else{
				resultVo.setId("dept"+dept.getId());
			}
			resultVo.addAttr(PARENT_ID, dept.getParent() == null ? "" : dept.getParent().getId());
			resultVo.setNodeType("dept");
			if (initUser) {
				// 生成人员节点
				for (SysUser user : dept.getUsers()) {
					EasyTreeNode userNode = new EasyTreeNode();
					userNode.setText(user.getName());
					if (onlyUserId){
						userNode.setId(String.valueOf(user.getId()));
					}else{
						userNode.setId("user" + String.valueOf(user.getId()));
					}
					userNode.setNodeType("user");

					if (lstSelect != null) {
						boolean isContain = CollectionUtil.contain(lstSelect, "id", user.getId());
						userNode.setChecked(isContain);
					}
					resultVo.getChildren().add(userNode);
					userNode.addAttr(PARENT_ID, resultVo.getId());
				}
			}
			mapId2Vo.put(dept.getId(), resultVo);
		}

		for (SysDept dept : depts) {
			EasyTreeNode resultVo = mapId2Vo.get(dept.getId());
			if (dept.getParent() == null) {
				resultVo.addAttr(PARENT_ID, resultVo.getId());
				root.getChildren().add(resultVo);
			} else {
				EasyTreeNode parentVo = mapId2Vo.get(dept.getParent().getId());
				parentVo.getChildren().add(resultVo);
				parentVo.setState(EasyTreeNode.STATE_CLOSED);
			}
		}
		for (EasyTreeNode node : root.getChildren()) {
			node.setState(EasyTreeNode.STATE_OPEN);
		}
		return root;
	}

}
