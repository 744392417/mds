/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.spt.mds.admin.shiro;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.hsoft.admin.client.entity.SysPermission;
import com.hsoft.admin.client.entity.SysRole;
import com.hsoft.admin.client.entity.SysUser;
import com.hsoft.admin.client.open.IAdminOpenFacade;
import com.hsoft.admin.client.remote.ISysUserClient;
import com.hsoft.admin.client.vo.UserLoginVo;
import com.hsoft.commutil.encrypt.Encodes;
import com.hsoft.commutil.shiro.ShiroUser;
import com.hsoft.commutil.util.CollectionUtil;

public class ShiroDbRealm extends AuthorizingRealm {
	@Autowired
	private IAdminOpenFacade adminOpenClient;

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		UserLoginVo loginVo =new UserLoginVo();
		loginVo.setAppCode(ShiroUtil.appCd);
		loginVo.setLoginName(token.getUsername());
		SysUser user = adminOpenClient.findUserByLoginName(loginVo);
		if (user != null) {
			byte[] salt = Encodes.decodeHex(user.getSalt());
			ShiroUser shiroUser = new ShiroUser(user.getId(), user.getLoginName(), user.getName());
			shiroUser.addProp("appId", user.getAppId());
			return new SimpleAuthenticationInfo(shiroUser, user.getPassword(), ByteSource.Util.bytes(salt), getName());
		} else {
			return null;
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
		UserLoginVo loginVo =new UserLoginVo();
		loginVo.setAppCode(ShiroUtil.appCd);
		loginVo.setLoginName(shiroUser.loginName);
		SysUser user = adminOpenClient.findUserByLoginName(loginVo);
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		List<SysRole> roles = adminOpenClient.findRoleByUserId(user.getId());

		List<String> roleCds = CollectionUtil.getPropList(roles, "roleCd");
		info.addRoles(roleCds);
		info.addStringPermissions(getAllPerms(roles));
		return info;
	}

	private List<String> getAllPerms(List<SysRole> roles) {
		List<SysPermission> permissions = new ArrayList<SysPermission>();
		for (SysRole role : roles) {
			if (role.getPermissions() != null) {
				permissions.addAll(role.getPermissions());
			}
		}
		List<String> permCds = CollectionUtil.getPropList(permissions, "permCd");
		return permCds;
	}

	/**
	 * 设定Password校验的Hash算法与迭代次数.
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(ISysUserClient.HASH_ALGORITHM);
		matcher.setHashIterations(ISysUserClient.HASH_INTERATIONS);

		setCredentialsMatcher(matcher);
	}
	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(Object principal) {

		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {

		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if ( cache != null ) {
			for ( Object key : cache.keys() ) {
				cache.remove(key);
			}
		}
	}
}
