/**
 * 
 */
package com.spt.mds.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hsoft.admin.client.entity.SysMenu;
import com.hsoft.admin.client.open.IAdminOpenFacade;
import com.hsoft.admin.client.vo.SysMenuSearchVo;
import com.hsoft.admin.client.vo.UserChangeVo;
import com.hsoft.commutil.constants.TmsConstants;
import com.hsoft.commutil.easyui.EasyTreeNode;
import com.hsoft.commutil.util.RenderUtil;
import com.spt.mds.admin.shiro.ShiroUtil;
import com.spt.mds.admin.util.EasyTreeUtil;

/**
 * @author huangjian
 * 
 */
@Controller
@RequestMapping(value = "/index")
public class IndexController {
	@Autowired
	private IAdminOpenFacade adminOpenClient;


	@RequestMapping(value = "")
	public String index(Model model) {
		List<SysMenu> menus;
		SysMenuSearchVo vo=new SysMenuSearchVo();
		vo.setAppCode(ShiroUtil.appCd);
		if (SecurityUtils.getSubject().hasRole(TmsConstants.Role.ADMIN)) {
		} else {
			Long userId = ShiroUtil.getCurrentUserId();
			vo.setUserId(userId);
		}
		menus = adminOpenClient.findAllMenu(vo);
		EasyTreeNode treeNode = EasyTreeUtil.getMenuTree(menus);
		if (treeNode.getChildren().size() > 0) {
			EasyTreeNode appNode =treeNode.getChildren().get(0);
			for (EasyTreeNode tmp :treeNode.getChildren()) {
				if(tmp.getId().substring(3).equals(ShiroUtil.getCurrAppId()+"")) {
					appNode = tmp;
					break;
				}
			}
			if (appNode!=null) {
				model.addAttribute("menus", appNode.getChildren());
			}
		}
		return "admin/index";
	}
	

	@RequestMapping(value = "changePwd", method = RequestMethod.POST)
	public void changePwd(@Valid @RequestParam("id")  Long userId, @RequestParam("oldPwd") String oldPwd,
			@RequestParam("newPwd") String newPwd,HttpServletRequest request,  HttpServletResponse response) {
		try {
			UserChangeVo vo=new UserChangeVo();
			vo.setPassword(oldPwd);
			vo.setUserId(userId);
			if (!adminOpenClient.isPwdEqual(vo)) {
				RenderUtil.renderText("error", response);
				return;
			}
			String result = "success";
			vo.setPassword(newPwd);
			adminOpenClient.updateUser(vo);
			RenderUtil.renderText(result, response);
		} catch (Exception e) {
			RenderUtil.renderFailure("保存失败", response);
		}
	}
}
