package com.spt.mds.admin.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hsoft.commutil.task.ITask;
import com.spt.mds.admin.shiro.ShiroUtil;

@Component
public class ShiroHeartJob implements ITask {

	/** 10秒钟执行一次 */
	@Override
	@Scheduled(cron = "0/10 * * * * *")
	public void run() {
		if (!ShiroUtil.filterInited) {
			ShiroUtil.clean();
		}
	}

	@Override
	public String getTaskName() {
		return "ShiroHeartJob";
	}
}
