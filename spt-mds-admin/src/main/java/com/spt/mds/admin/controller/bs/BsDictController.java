/**
 * 
 */
package com.spt.mds.admin.controller.bs;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.admin.client.entity.AppDictData;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.commutil.util.RenderUtil;

/**
 * @author wlddh
 *
 */
@Controller
@RequestMapping(value = "/bs/dict")
public class BsDictController {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "loadCommbo/{dictTypeCd}", method = RequestMethod.POST)
	public void loadCommbo(@PathVariable("dictTypeCd") String dictTypeCd, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			List<AppDictData> lstData = DictUtil.getListByCategory(dictTypeCd);
			RenderUtil.renderSuccess(JsonUtil.obj2Json(lstData), response);
		} catch (Exception e) {
			logger.error("loadCommbo", e);
			RenderUtil.renderFailure("errorId:" + e.getMessage(), response);
		}
	}
}
