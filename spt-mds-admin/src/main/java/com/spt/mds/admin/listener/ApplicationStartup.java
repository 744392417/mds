/**
 * 
 */
package com.spt.mds.admin.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.commutil.CommandExecutor;
import com.spt.mds.admin.shiro.ShiroUtil;

/**
 * 应用启动监听器
 * 
 * @author huangjian
 *
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private CommandExecutor executor;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		new Thread(executor).start();
		// 初始化缓存
		DictUtil.init(ShiroUtil.appCd);
		logger.info("---Application已启动---");
	}

}