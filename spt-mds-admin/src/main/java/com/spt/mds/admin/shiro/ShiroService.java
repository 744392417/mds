/**
 * 
 */
package com.spt.mds.admin.shiro;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.config.Ini;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.admin.client.entity.SysMenu;
import com.hsoft.admin.client.entity.SysPermission;
import com.hsoft.admin.client.entity.SysResource;
import com.hsoft.admin.client.open.IAdminOpenFacade;
import com.hsoft.admin.client.vo.SysMenuSearchVo;
import com.hsoft.commutil.util.CollectionUtil;

/**
 * @author huangjian
 * 
 */
@Component
// 默认将类中的所有public函数纳入事务管理.
@Transactional(readOnly = true)
public class ShiroService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 默认premission字符串
	 */
	public static final String PREMISSION_STRING = "perms[\"{0}\"]";
	public static final String ROLE_STRING = "roles[{0}]";
	@Autowired
	private IAdminOpenFacade adminOpenClient;
	private void initResource(Ini.Section section,String appCd){
		logger.info(">>>>>>init resource to permission<<<<<<");
		List<SysResource> list = (List<SysResource>) adminOpenClient.findResourceByApp(appCd);
		// 循环Resource的url,逐个添加到section中。section就是filterChainDefinitionMap,
		// 里面的键就是链接URL,值就是存在什么条件才能访问该链接
		for (Iterator<SysResource> it = list.iterator(); it.hasNext();) {
			SysResource resource = it.next();
			// 如果不为空值添加到section中
			if (StringUtils.isNotBlank(resource.getAuth())){
				//如果授权方式不为空，直接添加到section中，
				section.put(resource.getUrl(), resource.getAuth());
			}else{
				//如果授权方式为空，根据权限表配置
				List<String> perms = new ArrayList<String>();
				if (StringUtils.isNotEmpty(resource.getUrl())) {
					for (SysPermission permission : resource.getPermissions()) {
						perms.add(permission.getPermCd());
					}
				}
				String strPerms = CollectionUtil.array2String(perms);
				if (StringUtils.isNotBlank(strPerms)) {
					section.put(resource.getUrl(), MessageFormat.format(PREMISSION_STRING, strPerms));
				}
			}
		}
	}
	private void initMenu(Ini.Section section,String appCd){
		logger.info(">>>>>>init menu to permission<<<<<<");
		SysMenuSearchVo vo=new SysMenuSearchVo();
		vo.setAppCode(appCd);
		List<SysMenu> menus = adminOpenClient.findAllMenu(vo);
		for (SysMenu menu : menus) {
			List<String> perms = new ArrayList<String>();
			if (StringUtils.isNotEmpty(menu.getUrl())) {
				for (SysPermission permission : menu.getPermissions()) {
					perms.add(permission.getPermCd());
				}
			}
			String strPerms = CollectionUtil.array2String(perms);
			if (StringUtils.isNotBlank(strPerms)) {
				section.put(menu.getUrl(), MessageFormat.format(PREMISSION_STRING, strPerms));
			}
		}
	}
	public boolean initSection(Ini.Section section,String appCd) {
		try {
			initMenu(section,appCd);
			initResource(section,appCd);
			ShiroUtil.filterInited = true;
			return true;
		} catch (Exception e) {
			logger.warn("初始化权限资源失败,err:{}",e.getMessage());
		}
		return false;
	}

}
