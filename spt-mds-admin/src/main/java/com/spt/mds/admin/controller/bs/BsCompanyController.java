///**
// * 
// */
//package com.spt.saas.admin.controller.bs;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import com.hsoft.admin.client.cache.DictUtil;
//import com.hsoft.commutil.base.BaseClient;
//import com.hsoft.commutil.base.BaseVo;
//import com.hsoft.commutil.easyui.JsonEasyUI;
//import com.hsoft.commutil.props.PropertiesUtil;
//import com.hsoft.commutil.util.JsonUtil;
//import com.hsoft.commutil.util.RenderUtil;
//import com.hsoft.commutil.web.PageSearchVo;
//import com.hsoft.commutil.web.SingleCrudControll;
//import com.spt.credit.client.api.IBsCompanyApi;
//import com.spt.credit.client.api.IBsUserAccountApi;
//import com.spt.credit.client.constant.OfferConstants;
//import com.spt.credit.client.entity.BsCompany;
//import com.spt.credit.client.vo.UserAccountVo;
//import com.spt.credit.client.vo.UserCompanyVo;
//
//import net.sf.json.JSONObject;
//
//
//
///**
// * 企业管理
// * 
// * @author wlddh
// *
// */
//@Controller
//@RequestMapping(value = "/bs/company")
//public class BsCompanyController extends SingleCrudControll<BsCompany, BaseVo> {
//	@Autowired
//	private IBsCompanyApi bsCompanyApi;
//
//	
//	@Autowired
//	private IBsUserAccountApi bsUserAccountApi;
//
//	@Override
//	public BaseClient<BsCompany> getService() {
//		return bsCompanyApi;
//	}
//
//	@RequestMapping(value = "")
//	public String index(Model model) {
//		model.addAttribute("statusJson",
//				JsonUtil.obj2Json(DictUtil.getListByCategory(OfferConstants.SAAS_STATUS)));
//		model.addAttribute("companyTypeJson",
//				JsonUtil.obj2Json(DictUtil.getListByCategory(OfferConstants.SAAS_COMPANYTYPE)));
//		model.addAttribute("mappingTypeJson",
//				JsonUtil.obj2Json(DictUtil.getListByCategory(OfferConstants.SAAS_MAPPINGTYPE)));
//		return "bs/company";
//	}
//	
//	@RequestMapping(value = "companyDetail/{targetId}")
//	public String companyDetail(@PathVariable("targetId") String targetId, Model model,HttpServletRequest request) {
//		String id = request.getParameter("id");
//		BsCompany companyInfo = getEntity(Long.valueOf(id));
//		UserCompanyVo vo = new UserCompanyVo();
//		BeanUtils.copyProperties(companyInfo, vo);
//	
//		String attachFileId = companyInfo.getAttacheFileId();
//		
//		JSONObject fileJson = new JSONObject();
//		
//		if(StringUtils.isNotBlank(attachFileId)&&attachFileId.indexOf("{")==0&&attachFileId.indexOf("}")>0){
//			fileJson = JSONObject.fromObject(attachFileId);
//		}
//		
//		//拆分企业保存的图片
//		if(fileJson!=null){
//
//		    vo.setBusinessLicenseUrl(fileJson.get("businessLicenseUrl")==null?"":String.valueOf(fileJson.get("businessLicenseUrl")));
//
//		    vo.setCommunityCreditPicUrl(fileJson.get("communityCreditPicUrl")==null?"":String.valueOf(fileJson.get("communityCreditPicUrl")));
//
//		    vo.setOrganizationCodePicUrl(fileJson.get("organizationCodePicUrl")==null?"":String.valueOf(fileJson.get("organizationCodePicUrl")));
//
//		    vo.setTaxRegisterPicUrl(fileJson.get("taxRegisterPicUrl")==null?"":String.valueOf(fileJson.get("taxRegisterPicUrl")));
//
//		    vo.setLegalPersonOppositePicUrl(fileJson.get("legalPersonOppositePicUrl")==null?"":String.valueOf(fileJson.get("legalPersonOppositePicUrl")));
//
//		    vo.setLegalPersonPicUrl(fileJson.get("legalPersonPicUrl")==null?"":String.valueOf(fileJson.get("legalPersonPicUrl")));
//
//		    vo.setBankOpenAcctLicenseUrl(fileJson.get("bankOpenAcctLicenseUrl")==null?"":String.valueOf(fileJson.get("bankOpenAcctLicenseUrl")));
//			String fileRootPath = PropertiesUtil.getProperty(OfferConstants.FILE_SHOW_URL);
//			String filePath = fileRootPath;
//			model.addAttribute("filePath", filePath+"show/");
//			vo.setFilePath(filePath+"small/");
//		}
//		model.addAttribute("entity", vo);
//	
//		model.addAttribute("legalPersonTypeJson",JsonUtil.obj2Json(DictUtil.getListByCategory(OfferConstants.COMPANY_LEGALPERSONTYPE)));
//		model.addAttribute("threeFlagTypeJson",	JsonUtil.obj2Json(DictUtil.getListByCategory(OfferConstants.COMPANY_THREEFLAG)));
//		String mark = null;
//		
//		if(OfferConstants.SPT_COMPANY_AUDIT_STATUS_W.equals(companyInfo.getStatus())){
//			mark = "N";
//		}
//		if(OfferConstants.SPT_COMPANY_AUDIT_STATUS_R.equals(companyInfo.getStatus())){
//			mark = "Y";
//		}
//		model.addAttribute("mark", mark);	
//		if(targetId.equals("audit")){
//			return "company/identify_window";
//		}else{
//			
//		return "company/company_edit";
//		
//		}
//	
//	}
//
//	
//	@ModelAttribute("preload")
//	public BsCompany getEntity(@RequestParam(value = "id", required = false) Long id) {
//		if (id != null) {
//			if (id > 0)
//				return getService().getEntity(id);
//			else {
//				BsCompany entity = new BsCompany();
//				entity.setId(0l);
//				return entity;
//			}
//		}
//		return null;
//	}
//	
//	/**
//	 * 审核
//	 * @param id
//	 * @param response
//	 */
//	@RequestMapping(value = "companyAudit/{id}", method = RequestMethod.POST)
//	public void audit(@PathVariable("id") Long id,HttpServletRequest request, HttpServletResponse response) {
//		try{
//			BsCompany entity = getEntity(id);
//			if(entity!=null){
//				String status = request.getParameter("status");
//				entity.setStatus(status);
//				bsCompanyApi.save(entity);
//				RenderUtil.renderText("success", response);
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//			RenderUtil.renderText("fail", response);
//		}
//		
//	}
//
//	@RequestMapping(value = "updateCompany")
//	public void updateCompany(Model model,BsCompany company,HttpServletRequest request, HttpServletResponse response) {
//		try{
//			String 	businessLicenseUrl = request.getParameter("businessLicenseUrl");//营业执照
//		    String 	legalPersonPicUrl = request.getParameter("legalPersonPicUrl");//身份证正面
//		    String 	legalPersonOppositePicUrl = request.getParameter("legalPersonOppositePicUrl");//身份证反面
//		    String 	bankOpenAcctLicenseUrl = request.getParameter("bankOpenAcctLicenseUrl");//
//		
//		    
///*			String 	communityCreditPicUrl = request.getParameter("communityCreditPicUrl");
//			String 	organizationCodePicUrl = request.getParameter("organizationCodePicUrl");
//		    String 	taxRegisterPicUrl = request.getParameter("taxRegisterPicUrl");
//*/		    
//
//		    
//			JSONObject obj = new JSONObject();
//			if(StringUtils.isNotBlank(legalPersonOppositePicUrl)){
//				obj.put("legalPersonOppositePicUrl", legalPersonOppositePicUrl);
//			}
//			if(StringUtils.isNotBlank(legalPersonPicUrl)){
//				obj.put("legalPersonPicUrl", legalPersonPicUrl);
//			}
//			if(StringUtils.isNotBlank(businessLicenseUrl)){
//				obj.put("businessLicenseUrl", businessLicenseUrl);
//			}
//			if(StringUtils.isNotBlank(bankOpenAcctLicenseUrl)){
//				obj.put("bankOpenAcctLicenseUrl", bankOpenAcctLicenseUrl);
//			}
///*			if(StringUtils.isNotBlank(communityCreditPicUrl)){
//				obj.put("communityCreditPicUrl", communityCreditPicUrl);
//			}
//			if(StringUtils.isNotBlank(organizationCodePicUrl)&&!organizationCodePicUrl.equals(businessLicenseUrl)){
//				obj.put("organizationCodePicUrl", organizationCodePicUrl);
//			}
//			if(StringUtils.isNotBlank(taxRegisterPicUrl)&&!taxRegisterPicUrl.equals(businessLicenseUrl)){
//				obj.put("taxRegisterPicUrl", taxRegisterPicUrl);
//			}*/
//
//			System.out.println(">>>>>>>>>>>>>>>>>>>="+obj.toString());
//			if(obj!=null){
//				company.setAttacheFileId(obj.toString());
//			}
//
//			BsCompany companyold = bsCompanyApi.getEntity(company.getId());
//			BeanUtils.copyProperties(company, companyold);
//			bsCompanyApi.save(companyold);
//			RenderUtil.renderText("success", response);
//		}catch(Exception e){
//			e.printStackTrace();
//			RenderUtil.renderText("fail", response);
//		}		
//	}	
//
//	/**
//	 * 获取未审核用户列表
//	 * @param id
//	 * @param response
//	 */
//	@RequestMapping(value = "toAuditedList")
//	public String toAuditedList(Model model, HttpServletRequest request, HttpServletResponse response) {
//		List<BsCompany> companyNList = bsCompanyApi.findByStatus(OfferConstants.SPT_COMPANY_AUDIT_STATUS_W);
//		model.addAttribute("companyNListJson", companyNList);	
//		return "company/identify";
//	}
//	
//	@RequestMapping(value = "companyNList", method = RequestMethod.POST)
//	public void companyNList(PageSearchVo searchVo, HttpServletRequest request, HttpServletResponse response) {
//		Map<String,Object> map = searchVo.getSearchParams();
//		if(map==null){
//			map =new HashMap<String, Object>();
//		}
//		map.put("EQS_status", OfferConstants.SPT_COMPANY_AUDIT_STATUS_W);
//		searchVo.setSearchParams(map);
//		Page<BsCompany> page = bsCompanyApi.findPage(searchVo);
//		JsonEasyUI.renderJson(response, page);
//	}
//	
//
//	
//	@RequestMapping(value = "detail/{id}", method = RequestMethod.GET)
//	public String detail(@PathVariable("id") Long id, Model model) {
//		BsCompany entity = getEntity(id);
//		model.addAttribute("entity", entity);
//		return "bs/company-detail";
//	}
//	
//	
//	@RequestMapping(value ="ajaxRegisterStatus")
//	public void registerStatus(HttpServletRequest request, HttpServletResponse response){
//
//		String mark = request.getParameter("mark");
//		String companyInfoId = request.getParameter("companyInfoId");
//		String attacheFileId = request.getParameter("attacheFileId");
//		UserAccountVo vo= new UserAccountVo();
//		vo.setCompanyId(Long.valueOf(companyInfoId));vo.setMark(mark);vo.setAttacheFileId(attacheFileId);
//		bsUserAccountApi.updateCompanyStatus(vo);
//		
//			RenderUtil.renderSuccess("审核通过", response);
//		
//	
//	}
//	/**
//	 * 审核
//	 * @param companyId
//	 * @param response
//	 */
//	@RequestMapping(value = "activateCompany/{companyId}", method = RequestMethod.GET)
//	public void activateCompany(@PathVariable("companyId") Long companyId, HttpServletRequest request, HttpServletResponse response){
//		try {
//			
//			String activate = request.getParameter("activate");
//			System.out.println("activate===="+activate);
//			System.out.println("companyId===="+companyId);
//			BsCompany bsCompany = new BsCompany();
//			  bsCompany.setId(companyId);
//			  bsCompany.setStatus(activate);
//			bsCompanyApi.activateCompany(bsCompany);
//			
//		} catch (Exception e) {
//			logger.info(e.getMessage(),e);
//			RenderUtil.renderFailure("failure", response);
//		}
//		
//		RenderUtil.renderSuccess("success", response);
//	}
//
//	/**
//	 * 获取修改用户信息列表
//	 * @param id
//	 * @param response
//	 */
//	@RequestMapping(value = "toIdentifyUpdate")
//	public String toIdentifyUpdate(Model model, HttpServletRequest request, HttpServletResponse response) {
//		return "company/identify_update";
//	}
//	
//}
