/**
 * 
 */
package com.spt.mds.admin.command;

import org.springframework.stereotype.Component;

import com.hsoft.commutil.ICommandExecutor;
import com.spt.mds.admin.shiro.ShiroUtil;

/**
 * @author wlddh
 *
 */
@Component
public class AdminCommandExecutor implements ICommandExecutor {

	/* (non-Javadoc)
	 * @see com.hsoft.commutil.ICommandExecutor#executeCommand(java.lang.String)
	 */
	@Override
	public boolean executeCommand(String commandline) throws Exception {
		if (commandline.trim().equalsIgnoreCase("clean")) // 刷新shiro
		{
			ShiroUtil.clean();
			return true;
		}
		return false;
	}

}
