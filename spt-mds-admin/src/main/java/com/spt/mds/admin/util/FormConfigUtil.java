/*package com.spt.saas.admin.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.alibaba.druid.support.json.JSONUtils;
import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.admin.client.entity.AppDictData;
import com.spt.credit.client.entity.BsTemplateConfig;


public class FormConfigUtil {

	public static List<AppDictData> findByTypeCd(Map<String, Map<String, BsTemplateConfig>> map, String templateContentKey) {
		// TODO Auto-generated method stub
		java.util.Iterator it = map.entrySet().iterator();
        java.util.Map.Entry entry = (java.util.Map.Entry)it.next();
        String key =(String)entry.getKey();     //返回对应的键
		BsTemplateConfig tc = map.get(key).get(key);
		String content = tc.getContent();
		Map<String, String> obj = (Map<String, String>)JSONUtils.parse(content);

		String values =(String) obj.get(templateContentKey);
		JSONObject json= new JSONObject();
		List<AppDictData> dataList = new ArrayList<AppDictData>();
		for(String value:values.split(",")){
			AppDictData dict= new AppDictData();
			dict.setDictCd(value);
			dict.setDictName(DictUtil.getValue(templateContentKey, value));
			dataList.add(dict);
		}
		
		return dataList;
	}

}
*/