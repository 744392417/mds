/**
 * 
 */
package com.spt.mds.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.hsoft.admin.client.config.AdminClientConfig;
import com.hsoft.file.client.config.FileClientConfig;

/**
 * @author huangjian
 *
 */
// 这里的配置文件必须另外起个名字，不能直接使用application.properties，框架自动会以application.properties里面的配置项最为最高优先级
@PropertySource(value = { "classpath:/config.properties","file:d:/deploy/config.properties",
"file:/data/product/deploy/config.properties"}, ignoreResourceNotFound = true)
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients(basePackages={"com.spt.hospital.client.api"})
@Import({AdminClientConfig.class,FileClientConfig.class})
//@ImportResource("classpath*:/applicationContext-shiro.xml")
public class Admin {
	public static void main(String[] args) {
		SpringApplication.run(Admin.class, args);
	}
}
