package com.hsoft.common.util.test;
/**
 * 
 */

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.hsoft.commutil.encrypt.TokenUtil;
import com.hsoft.commutil.util.HTTPUtility;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author wlddh
 *
 */
public class HTTPUtilityTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();
		map.put("secretKey", "hsoftwms171010");
		map.put("userName", "商品通");
		map.put("appCode", "spt");
		try {
			String token = TokenUtil.createToken("spt", "hsoftwms171018");
			Map<String, String> headerMap = new HashMap<>();
			headerMap.put("AccessToken", token);
			headerMap.put("AppCode", "spt");
			HTTPUtility.doPostBody("http://localhost:8080/imServer/im/app/findAll", null, headerMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	// 生成token
	public static String createToken(String userId, String userName) {
		String userToken = Jwts.builder().setSubject("wms")
				.setExpiration(new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 60)).claim("userId", userId)
				.claim("userName", userName).signWith(SignatureAlgorithm.HS256, "hsoft_wms_171010").compact();

		return userToken;
	}

}
