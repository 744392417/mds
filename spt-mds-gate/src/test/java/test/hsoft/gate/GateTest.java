package test.hsoft.gate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hsoft.commutil.constants.Constant;
import com.hsoft.commutil.encrypt.TokenUtil;
import com.hsoft.commutil.util.HTTPUtility;

public class GateTest {

	
	//@org.junit.Test
	public void test() {
		try {
			String token = TokenUtil.createToken("商品通", "ptRvglMCJX8rjquHOyjoSRMyUfDgwekr");
			Map<String, String> headerMap = new HashMap<>();
			headerMap.put(Constant.GATE_ACCESSTOKEN, token);
			headerMap.put("userId","SPTU0000002196");
			HTTPUtility.doPostBody("http://localhost:8080/file/api/uploadBase64", null, headerMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//@org.junit.Test
	public void test2() {
		try {
//			String token = TokenUtil.createToken(Constant.GATE_SUBJECT_PP, "hsoft_req_180306");
			Map<String, String> headerMap = new HashMap<>();
			String token ="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTIyMjM3MTI1LCJ1c2VySW5mbyI6IntcInJvbGVDZFwiOlwiTlwiLFwiZGVwdElkXCI6XCIxXCIsXCJjb21wYW55VXVpZFwiOlwiMVwiLFwidXNlck5hbWVcIjpcIumCk-iJs-WNjlwiLFwidXNlcklkXCI6MjJ9In0.W9F4spME4Da5SlwAA1jt4QUMTVmpBjI2Vn0xPVcDEbyNuU3EoJy-PGbtVxjSdZ3Hk7jAhZKAbREfPMF4flbbCw";
			headerMap.put(Constant.GATE_ACCESSTOKEN, token);
			HTTPUtility.doPostBody("http://localhost:8088/open/bs/userAccount/getEntity", 22L, headerMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@org.junit.Test
	public void findMyOfferPirceList() {
		try {
//			String token = TokenUtil.createToken(Constant.GATE_SUBJECT_PP, "hsoft_req_180306");
			Map<String, String> headerMap = new HashMap<>();
			String token ="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTIzOTUzNzEzLCJ1c2VySW5mbyI6IntcInJvbGVDZFwiOlwiTlwiLFwiY29tcGFueVR5cGVcIjpcIkNcIixcImRlcHRJZFwiOlwiMVwiLFwidXNlclV1aWRcIjpcIlNQVFUwMDAwMDA1NjcyXCIsXCJjb21wYW55VXVpZFwiOlwiMVwiLFwidXNlck5hbWVcIjpcImNlc2hpMlwiLFwidXNlcklkXCI6MTh9In0.r5akz--UzmrpPPwlOKCWXzdwLx8rsrzX6qZ8_aKEziliE6c1monG6Jq0Qiah6PTNge77bl2IeeJe7Ib07UnhYw";
			headerMap.put(Constant.GATE_ACCESSTOKEN, token);
			Map<String,Object> vo = new HashMap<>();
			vo.put("type", "noRelease");
			HTTPUtility.doPostBody("http://192.168.1.34:8088/open/off/offPrice/findMyOfferPirceList", vo, headerMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//@org.junit.Test
	public void login() {
		try {
			Map<String, String> param = new HashMap<>();
			param.put("loginName", "dengyanhua@autrade.com.cn");
			param.put("loginPwd", "123456");
			HTTPUtility.doPostBody("http://localhost:8088/open/bs/userAccount/login", param, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	@org.junit.Test
//	public void sptOpenSaasAccount( ) {
//		try {
//			OpenSassAccountRequestVo request = new OpenSassAccountRequestVo();
//			List<SptOpenSaasAccountVo> userList = new ArrayList<SptOpenSaasAccountVo>();
//			SptOpenSaasAccountVo user = new SptOpenSaasAccountVo();
//			user.setLoginName("18392403745");
//			user.setLoginPwd("123456");
//			user.setMobile("18392403745");
//			user.setEmail("1050839423@qq.com");
//			user.setUserName("哈哈");
//			user.setRoleCd("N");
//			user.setOutAccountId("spt1256722000UU");
//			user.setOutCompanyId("spt1256722000UC");
//			user.setAddress("上海");
//			user.setCompanyName("公司名称");
//			user.setCompanyCode("ABC");
//			userList.add(user);
//			request.setVoList(userList);
//			HTTPUtility.doPostBody("http://localhost:8088/open/bs/userAccount/sptOpenSaasAccount", request, null);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
