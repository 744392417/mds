package test.hsoft.gate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="报价查询参数vo")
public class OfferPriceQueryVo {

	@ApiModelProperty(value = "当为管理员时 需要所在部门下的用户id 多个时用，隔开", example="")
	private String userId;		//用户id	
	/*@ApiModelProperty(value = "查询类别", example="3",position=1)
	private String type;		//查询类别
*/	@ApiModelProperty(value = "排序字段(productCode:商品(默认),buySell(买卖))", example="productCode")
	private String socrt;		//排序字段
	@ApiModelProperty(value = "排序类型(asc desc)", example="asc")
	private String order;
	@ApiModelProperty(value = "一页显示多少条数(默认十条)", example="10")
	private int rows = 10;	
	@ApiModelProperty(value = "检索关键字", example="")
	private String content;			//检索关键字
	@ApiModelProperty(value = "当前页", example="1")
	private int page = 1;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSocrt() {
		return socrt;
	}

	public void setSocrt(String socrt) {
		this.socrt = socrt;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	
	
}
