package com.hsoft.gate.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.hsoft.commutil.constants.Constant;
import com.hsoft.commutil.encrypt.TokenUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.spt.hospital.client.constant.HospConstants;

import io.jsonwebtoken.Claims;

@Component
public class AuthenticationHeaderFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(AuthenticationHeaderFilter.class);
	

	private static final String[][] preAuthenticationIgnoreUris = { 
			{ "/open/mds/fileUploadFile","*"},
			{ "/open/mdsBasicInfo/getMdsBasicInfoList","*"},
			{ "/open/mdsUserInfo/login","*"},
			{ "/open/mdsWxUser/weiXinLogin","*"},
			{ "/open/mdsWxUser/wxPhoneNumber","*"},
			{ "/open/mdsWxUser/wxNotify","*"},
			{ "/open/mdsProperty/findPropertyListPage","*"},
			{ "/open/mdsProperty/modifyProperty","*"},
			{ "/open/mdsProduct/findProductCategoryPage","*"},
			{ "/open/mdsHospital/findhospitalProduct","*"},
			{ "/open/mdsHospital/saveHospitalPrice","*"},
			{ "/open/mdsLeaseInfo/findLeaseInfoPage","*"}
	};

	// 所有request都需要验证Token，除了上面ignore的
	private static final String[][] preAuthenticationMustUris = { { "*", "*" } };

	// If request method is POST
	// private static final String[][] preAuthenticationMustUris = {
	// {"/user", "POST"}
	// };
	
//	@Autowired
//	private IBsUserAccountClient accountClient;

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		try {
			request.setCharacterEncoding("UTF-8");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		String uri = request.getRequestURI().toString();
		String method = request.getMethod();

		log.info("url :{}  method: {} ", uri, method);

		try {
			ServletInputStream is = request.getInputStream();
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) != -1) {
			    result.write(buffer, 0, length);
			}
			log.info("bodyParam :{} ", result.toString());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0; i < preAuthenticationIgnoreUris.length; i++) {
			if (uri.startsWith(preAuthenticationIgnoreUris[i][0])
					&& (preAuthenticationIgnoreUris[i][1].equals("*")
							|| method.equalsIgnoreCase(preAuthenticationIgnoreUris[i][1]))) {
				log.info("this will not use filter");
				parseAccessToken(request,ctx);
				return false;
			}
		}
		
		
		for (int i = 0; i < preAuthenticationMustUris.length; i++) {
			if (uri.startsWith(preAuthenticationMustUris[i][0])
					&& (preAuthenticationMustUris[i][1].equals("*")
							|| method.equalsIgnoreCase(preAuthenticationMustUris[i][1]))) {
				log.info("this will use filter");
				return true;
			}
		}
		
		parseAccessToken(request,ctx);
		return false;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		
		// 1. clear userInfo from HTTP header to avoid fraud attack
		ctx.addZuulRequestHeader("user-info", "");

		String accessToken = request.getHeader(Constant.GATE_ACCESSTOKEN);
		log.info("AccessToken: {}", accessToken);
		boolean isParsed = parseAccessToken(request,ctx);
		
		if (!isParsed) {
			stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
					"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
		}else {
			if (StringUtils.isBlank(accessToken)) {
				this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
						"AccessToken is needed for the API (" + request.getRequestURI().toString() + ")");
			}
		}
		
		// 2. verify the passed user token
//		String accessToken = request.getHeader(Constant.GATE_ACCESSTOKEN);
//		log.info("AccessToken: {}", accessToken);
//		Claims claims = null;
//		if (StringUtils.isNotBlank(accessToken)) {
//			try {
//				log.info("AccessToken has value");
//				claims = TokenUtil.parseJWT(accessToken, OfferConstants.SECRETKEY);
//				if (claims == null) {
//					this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//							"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
//					return null;
//				}
//				log.info("claims is:{}", claims);
//				if (claims.getSubject().equals(Constant.GATE_SUBJECT_PP)) {
//					// 公私钥
//					String secretId = claims.getId();
//					log.info("secretId is:{}", secretId);
//					if (secretId == null) {
//						this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//								"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
//						return null;
//					}
//				} else if (claims.getSubject().equals(OfferConstants.SUBJECT_USER)){
//
//					String userInfo = (String) claims.get("userInfo");
////					ShiroUser user = JsonUtil.json2Object(ShiroUser.class, userInfo);
//					log.info("userInfo:{}", userInfo);
//					// 3. set userInfo to HTTP header
//					String encodeUserInfo = userInfo;
//					try {
//						encodeUserInfo = URLEncoder.encode(userInfo, "UTF-8");
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					ctx.addZuulRequestHeader(OfferConstants.GATE_USER_INFO, encodeUserInfo);
//					log.info("Header userInfo: {}", request.getHeader("user-info"));
//					log.info("ZuulRequestHeaders userInfo: {}", ctx.getZuulRequestHeaders().get("user-info"));
//
//				}
//
//				// log.debug("userInfo========="+userInfo);
//			} catch (Exception e) {
//				this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//						"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
//				return null;
//			}
//		} else {
//			this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//					"AccessToken is needed for the API (" + request.getRequestURI().toString() + ")");
//		}

		return null;
	}
	
	
	private boolean parseAccessToken(HttpServletRequest request,RequestContext ctx) {
		// 2. verify the passed user token
		String accessToken = request.getHeader(Constant.GATE_ACCESSTOKEN);
		Claims claims = null;
		if (StringUtils.isNotBlank(accessToken)) {
			try {
				log.info("AccessToken has value");
				claims = TokenUtil.parseJWT(accessToken, HospConstants.SECRETKEY);
				if (claims == null) {
//							this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//									"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
					return false;
				}
				log.info("claims is:{}", claims);
				if (claims.getSubject().equals(Constant.GATE_SUBJECT_PP)) {
					// 公私钥
					String secretId = claims.getId();
					log.info("secretId is:{}", secretId);
					if (secretId == null) {
//								this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//										"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
						return false;
					}
				} else if (claims.getSubject().equals(HospConstants.SUBJECT_USER)){

					String userInfo = (String) claims.get("userInfo");
//							ShiroUser user = JsonUtil.json2Object(ShiroUser.class, userInfo);
					log.info("userInfo:{}", userInfo);
					// 3. set userInfo to HTTP header
					String encodeUserInfo = userInfo;
					try {
						encodeUserInfo = URLEncoder.encode(userInfo, "UTF-8");
					} catch (Exception e) {
						e.printStackTrace();
					}
					ctx.addZuulRequestHeader(HospConstants.GATE_USER_INFO, encodeUserInfo);
					log.info("Header userInfo: {}", request.getHeader("user-info"));
					log.info("ZuulRequestHeaders userInfo: {}", ctx.getZuulRequestHeaders().get("user-info"));

				}

				// log.debug("userInfo========="+userInfo);
			} catch (Exception e) {
				log.error("parseAccessToken",e);
//						this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
//								"Error AccessToken for the API (" + request.getRequestURI().toString() + ")");
				return false;
			}
		} /*else {
			this.stopZuulRoutingWithError(ctx, HttpStatus.UNAUTHORIZED,
					"AccessToken is needed for the API (" + request.getRequestURI().toString() + ")");
		}*/
		return true;
	}

	private void stopZuulRoutingWithError(RequestContext ctx, HttpStatus status, String responseText) {

		ctx.removeRouteHost();
		ctx.setResponseStatusCode(status.value());
		ctx.setResponseBody(responseText);
		ctx.setSendZuulResponse(false);
	}
}
