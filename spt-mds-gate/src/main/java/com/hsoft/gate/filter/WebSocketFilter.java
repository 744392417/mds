/**
 * 
 */
package com.hsoft.gate.filter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

/**
 * @author wlddh
 *
 */
@Component
public class WebSocketFilter extends ZuulFilter {
	private static Logger log = LoggerFactory.getLogger(WebSocketFilter.class);
	// 所有request都需要验证Token，除了上面ignore的
	private static final String[][] preAuthenticationMustUris = { { "/websocket", "*" } };

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netflix.zuul.IZuulFilter#run()
	 */
	@Override
	public Object run() throws ZuulException {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();
		String upgradeHeader = request.getHeader("Upgrade");
		if (null == upgradeHeader) {
			upgradeHeader = request.getHeader("upgrade");
		}
		if (null != upgradeHeader && "websocket".equalsIgnoreCase(upgradeHeader)) {
			context.addZuulRequestHeader("connection", "Upgrade");
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netflix.zuul.IZuulFilter#shouldFilter()
	 */
	@Override
	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		try {
			request.setCharacterEncoding("UTF-8");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		String uri = request.getRequestURI().toString();
		String method = request.getMethod();
		for (int i = 0; i < preAuthenticationMustUris.length; i++) {
			if (uri.startsWith(preAuthenticationMustUris[i][0])
					&& (preAuthenticationMustUris[i][1].equals("*")
							|| method.equalsIgnoreCase(preAuthenticationMustUris[i][1]))) {
				log.info("this will use filter");
				return true;
			}
		}
		
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netflix.zuul.ZuulFilter#filterOrder()
	 */
	@Override
	public int filterOrder() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netflix.zuul.ZuulFilter#filterType()
	 */
	@Override
	public String filterType() {
		return "pre";
	}

}
