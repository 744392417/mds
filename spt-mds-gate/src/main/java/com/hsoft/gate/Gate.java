package com.hsoft.gate;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.hsoft.commutil.CommandExecutor;
import com.hsoft.commutil.config.CommWebConfig;
import com.spt.hospital.client.config.HospClientConfig;

//这里的配置文件必须另外起个名字，不能直接使用application.properties，框架自动会以application.properties里面的配置项最为最高优先级
@PropertySource(value = { "classpath:/config.properties","file:d:/deploy/config.properties",
		"file:/data/product/deploy/config.properties"}, ignoreResourceNotFound = true)
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
@Controller
@EnableZuulProxy
@EnableFeignClients(basePackages={"com.spt.req.client.remote"})
@Import({HospClientConfig.class,CommWebConfig.class})
public class Gate extends WebMvcConfigurerAdapter {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Gate.class, args);
		new Thread(new CommandExecutor()).start();
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("12800000KB");
		factory.setMaxRequestSize("12800000KB");
		return factory.createMultipartConfig();
	}

	@Bean
	public FilterRegistrationBean characterEncodingFilter() {
		FilterRegistrationBean filter = new FilterRegistrationBean();
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		filter.setFilter(characterEncodingFilter);
		return filter;
	}

	@Bean
	public CorsFilter corsFilter() {

		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration config = new CorsConfiguration();

		config.setAllowCredentials(true);
//		config.addAllowedOrigin("http://wechat.totrade.cn");
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		config.addAllowedOrigin("http://api.totrade.cn");

		source.registerCorsConfiguration("/**", config);

		return new CorsFilter(source);

	}

}
