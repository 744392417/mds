package com.spt.mds.web.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * cookie������ع���
 * @author dp
 *
 */
public class CookieUtility 
{
	/**
	 * �������ֻ�ȡcookie
	 * @param request
	 * @param name cookie����
	 * @return
	 */
	public static Cookie getCookieByName(HttpServletRequest request,String name){
	    Map<String,Cookie> cookieMap = ReadCookieMap(request);
	    if(cookieMap.containsKey(name)){
	        Cookie cookie = cookieMap.get(name);
	        return cookie;
	    }else{
	        return null;
	    }  
	}
	
	/**
	 * ��cookie��װ��Map����
	 * @param request
	 * @return
	 */
	private static Map<String,Cookie> ReadCookieMap(HttpServletRequest request){ 
	    Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
	    Cookie[] cookies = request.getCookies();
	    if(null!=cookies){
	        for(Cookie cookie : cookies)
	        {
	            cookieMap.put(cookie.getName(), cookie);
	        }
	    }
	    return cookieMap;
	}
	
	public static void addCookie(String key,String value,HttpServletResponse response,String path, Integer... maxAge){
		Cookie coo = new Cookie(key, value);
		coo.setPath(path);
		int ma = (maxAge != null && maxAge.length > 0)? maxAge[0] : 1800;
		coo.setMaxAge(ma);//30����
		response.addCookie(coo);
	}
	public static void deleteCookie(String key,HttpServletResponse response,String path){
		Cookie coo = new Cookie(key,null);
		coo.setMaxAge(0);//
		response.addCookie(coo);
	}
}
