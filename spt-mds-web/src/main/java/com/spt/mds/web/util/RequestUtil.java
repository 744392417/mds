package com.spt.mds.web.util;

import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import com.spt.mds.open.vo.UserInfo;


public class RequestUtil {
	
	private static Logger log = LoggerFactory.getLogger(RequestUtil.class);

	private static ThreadLocal<Object[]> userInfoKeeper = new ThreadLocal<Object[]>();
	
	public static void setupUserInfo(String userInfoJson, UserInfo userInfo) {
		
		log.info(String.format("RequestUtil setupUserInfo - userInfoJson: %s", userInfoJson));
		log.info(String.format("RequestUtil setupUserInfo - userInfo: %s", userInfo));
		
		userInfoKeeper.set(new Object[]{userInfoJson, userInfo});
	}
	
	public static UserInfo getUserInfo() {
		
		Object[] data = (Object[])userInfoKeeper.get();
		UserInfo userInfo = null;
		if (data != null) {
			userInfo = (UserInfo)data[1];
		}
		
		log.info(String.format("RequestUtil getUserInfo: %s", userInfo));
    	
    	return userInfo;
	}
	
	public static Long getCurrentUserId() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return Long.valueOf(userInfo.getUserId());
		}
		return 0l;
	}
	
	public static String getCurrentUserName() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getUserName();
		}
		return null;
	}
	
	public static String getCurrentLoginName() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getLoginName();
		}
		return null;
	}
	
	public static String getCurUserUuid() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getUserUuid();
		}
		return null;
	}
	public static String getCurCompanyType() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getCompanyType();
		}
		return null;
	}
	
	public static String getCurUserDeptId() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getDeptId();
		}
		return null;
	}
	
	public static String getCurUserRoleCd() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			return userInfo.getRoleCd();
		}
		return null;
	}
	
	public static String getCurUserCompanyUuid() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			if(userInfo.getCompanyId()!=null)
			return String.valueOf(userInfo.getCompanyId());
		}
		return null;
	}
	
	public static Long getCurUserCompanyId() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			if(userInfo.getCompanyId()!=null)
			return userInfo.getCompanyId();
		}
		return null;
	}
	
	public static String getCurUserCompanyName() {
		UserInfo userInfo = getUserInfo();
		if (userInfo != null) {
			if(StringUtils.isNotBlank(userInfo.getCompanyName()))
			return userInfo.getCompanyName();
		}
		return null;
	}
	
	public static void setUserInfoHeader(HttpHeaders headers) {
		
		Object[] data = (Object[])userInfoKeeper.get();
		
		if (data != null) {
			String userInfoJson = (String)data[0];
			String encodedUserInfoJson = userInfoJson;
			
			try {
				encodedUserInfoJson = URLEncoder.encode(userInfoJson,"UTF-8");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info(String.format("RequestUtil setUserInfoHeader - encodedUserInfoJson: %s", encodedUserInfoJson));
			
			headers.set("user-info", encodedUserInfoJson);
		}
	}
	
	public static void main(String[] args) {
		
    }
	
}

