/**
 * 
 */
package com.spt.mds.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wlddh
 *
 */
@Controller
@RequestMapping(value = "/siteMesh")
public class SiteMeshController {
	
	@RequestMapping(value = "index")
	public String index(Model model) {
		return "siteMesh/index";
	}
	@RequestMapping(value = "userIndex")
	public String userIndex(Model model) {
		
		return "siteMesh/userIndex";
		
	}
}
