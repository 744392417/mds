package com.spt.mds.web.controller;
//package com.spt.ucs.web.controller;
//
//import java.io.IOException;
//
//import javax.imageio.ImageIO;
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import com.spt.credit.client.api.IBsUserAccountApi;
//import com.spt.ucs.web.util.RandomNumUtil;
///**
// * 修改密码controller
// *
// */
//@Controller
//@RequestMapping(value = "/pwd")
//public class PasswordController {
//	@Autowired
//	private IBsUserAccountApi userAccountClient;
//	
//	@RequestMapping(value = "gotouserValidate", method = RequestMethod.GET)
//	public String gotouserValidate(){
//		return "changepwd/userValidate";
//	}
//	
//	@RequestMapping(value = "gotolayer", method = RequestMethod.GET)
//	public String gotolayer(){
//		return "changepwd/layer";
//	}
//	/**
//	 * 输入登录名校验
//	 * @param response
//	 */
//	/*@RequestMapping(value = "/userValidate", method = RequestMethod.POST)
//	public String userValidate(HttpServletRequest request, HttpServletResponse response, Model model){
//		String username=request.getParameter("username");
//		String picvalue=request.getParameter("picvalue");
//		HttpSession session = request.getSession();
//		String random = (String) session.getAttribute("random");
//		if(picvalue.equals(random)){
//			BsUserAccount userAccount = userAccountClient.findUserByLoginName(username);
//			if(userAccount!=null){
//				return "changepwd/msgValidate";
//			}else{
//				model.addAttribute("error", "登录名错误！");
//				return "changepwd/userValidate";
//			}
//		}else{
//			model.addAttribute("error", "验证码错误，请重新输入");
//			return "changepwd/userValidate";
//		}
//	}*/
//	/**
//	 * 发送手机号
//	 * @param mobile
//	 * @param response
//	 */
//	/*@RequestMapping(value = "sendSms", method = RequestMethod.POST)
//	public void sendSms(@Valid @RequestParam("mobile")  String mobile, HttpServletResponse response){
//		BsUserAccount userAccount = userAccountClient.findUserByLoginName(mobile);
//		if(userAccount!=null){
//			userAccountClient.sendSms(userAccount);
//			RenderUtil.renderText(OfferConstants.RESULT_SUCCESS, response);
//		}
//	}*/
//	/**
//	 * 短信验证
//	 * @param mobile
//	 * @param sms
//	 * @param response
//	 */
//	/*@RequestMapping(value = "smsValidate", method = RequestMethod.POST)
//	public void smsValidate(@Valid @RequestParam("mobile")  String mobile, @Valid @RequestParam("sms")  String sms, HttpServletResponse response){
//		UserAccountVo vo = new UserAccountVo();
//		vo.setLoginName(mobile);
//		vo.setPassword(sms);
//		boolean bl = userAccountClient.smsValidate(vo);
//		if(bl){
//			RenderUtil.renderText(OfferConstants.RESULT_SUCCESS, response);
//		}else{
//			RenderUtil.renderText(OfferConstants.RESULT_FAIL, response);
//		}
//	}*/
//	/**
//	 * 修改密码
//	 * @param username
//	 * @param password
//	 * @param response
//	 */
//	/*@RequestMapping(value = "resetPassword", method = RequestMethod.POST)
//	public void resetPassword(@Valid @RequestParam("username")  String username, @Valid @RequestParam("password")  String password, HttpServletResponse response){
//		BsUserAccount userAccount = userAccountClient.findUserByLoginName(username);
//		if(userAccount!=null){
//			userAccount.setPlainPassword(password);
//			userAccountClient.save(userAccount);
//			RenderUtil.renderText(OfferConstants.RESULT_SUCCESS, response);
//		}else{
//			RenderUtil.renderText(OfferConstants.RESULT_FAIL, response);
//		}
//	}*/
//	/**
//	 * 获取图片验证码
//	 * @param req
//	 * @param resp
//	 */
//	@RequestMapping("/getPicValidate")
//	public void getPicValidate(HttpServletRequest req,HttpServletResponse resp) {
//		ServletOutputStream sos = null;
//		RandomNumUtil rdnu = RandomNumUtil.Instance();
//		HttpSession session = req.getSession();
//		session.setAttribute("random", rdnu.getString());
//		// 禁止图像缓存。
//		resp.setHeader("Pragma", "no-cache");
//		resp.setHeader("Cache-Control", "no-cache");
//		resp.setDateHeader("Expires", 0);
//		resp.setContentType("image/jpeg");
//		try {
//			// 将图像输出到Servlet输出流中。
//			sos = resp.getOutputStream();
//			ImageIO.write(rdnu.getImage(), "jpeg", sos);
//		} catch (Exception e) {
//
//		} finally {
//			try {
//				sos.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//	
//}
