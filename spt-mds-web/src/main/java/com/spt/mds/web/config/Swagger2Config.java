/**
 * 
 */
package com.spt.mds.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author wlddh
 *
 */
@Configuration
@EnableSwagger2
@ComponentScan(basePackages = { "com.spt.mds.open" })
public class Swagger2Config extends WebMvcConfigurerAdapter {
	/**
	 * 这个地方要重新注入一下资源文件，不然不会注入资源的，也没有注入requestHandlerMappping,相当于xml配置的
	 * <!--swagger资源配置-->
	 * <mvc:resources location="classpath:/META-INF/resources/" mapping=
	 * "swagger-ui.html"/>
	 * <mvc:resources location="classpath:/META-INF/resources/webjars/" mapping=
	 * "/webjars/**"/> 不知道为什么，这也是spring boot的一个缺点（菜鸟觉得的）
	 * 
	 * @param registry
	 */
	@Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.spt.mds.open"))
                .paths(PathSelectors.any())
                .build();
    }
    //构建 api文档的详细信息函数,注意这里的注解引用的是哪个
    private ApiInfo apiInfo() {
    	return new ApiInfoBuilder().title("医院共享租赁平台1.0").termsOfServiceUrl("www.totrade.cn")
				.contact(new Contact("huangjian", "http://www.totrade.cn", "huangjian@autrade.com.cn")).version("v1.0")
				.build();
    }
    
    @Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}