/**
 * 
 */
package com.spt.mds.web.command;

import org.springframework.stereotype.Component;

import com.hsoft.commutil.ICommandExecutor;
import com.spt.mds.web.shiro.ShiroUtil;

/**
 * @author wlddh
 *
 */
@Component
public class WebCommandExecutor implements ICommandExecutor {

	/* (non-Javadoc)
	 * @see com.hsoft.commutil.ICommandExecutor#executeCommand(java.lang.String)
	 */
	@Override
	public boolean executeCommand(String commandline) throws Exception {
		if (commandline.trim().equalsIgnoreCase("clean")) // 刷新shiro
		{
			ShiroUtil.clean();
			return true;
		}
		return false;
	}

}
