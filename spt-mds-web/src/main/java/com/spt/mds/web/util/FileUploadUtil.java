package com.spt.mds.web.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.hsoft.commutil.props.PropertiesUtil;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.file.client.constant.FileConstant;
import com.hsoft.file.client.remote.FileRemote;
import com.hsoft.file.client.vo.Base64DataVo;
import com.hsoft.file.client.vo.FileRespVo;
import com.hsoft.file.client.vo.FileUploadBase64Request;

import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class FileUploadUtil{
	
	@Autowired
	private static FileRemote fileRemote;
	
	@Value("${filePath}")
	private static String filePath;
	
	public static  FileRespVo uploadFileImg(InputStream input, String filename) {
		FileRespVo vo = new FileRespVo();
		try {
			FileUploadBase64Request fileRequest = new FileUploadBase64Request();
			fileRequest.setFilePath(filePath);
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			List<Base64DataVo> dataList = new ArrayList<Base64DataVo>();
			// 获取rar包字节流
			String imgData = getBytes(input);
			// 将上传的文件转为base64
			Base64DataVo dataVo = new Base64DataVo();
			dataVo.setFileName(filename);
			dataVo.setBase64Data(imgData);
			dataList.add(dataVo);
			fileRequest.setDataList(dataList);
			vo = fileRemote.uploadBase64(fileRequest);
			System.out.println(JsonUtil.obj2Json(vo));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}
	
	private static String getBytes(InputStream input) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024 * 1024];
		int byteread = 0;
		while ((byteread = input.read(buffer)) != -1) {
			out.write(buffer, 0, byteread);
			out.flush();
		}
		byte[] buffer_read = out.toByteArray();

		return new BASE64Encoder().encode(buffer_read);
	}
	

	
}
