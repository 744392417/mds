package com.spt.mds.web.config;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hsoft.commutil.exception.ErrorResp;
import com.hsoft.commutil.exception.WebApplicationException;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class BasicErrorController extends AbstractErrorController {

	private final ErrorProperties errorProperties;

	public BasicErrorController(ErrorAttributes errorAttributes, ErrorProperties errorProperties) {
		this(errorAttributes, errorProperties, Collections.<ErrorViewResolver>emptyList());
	}

	public BasicErrorController(ErrorAttributes errorAttributes, ErrorProperties errorProperties,
			List<ErrorViewResolver> errorViewResolvers) {
		super(errorAttributes, errorViewResolvers);
		Assert.notNull(errorProperties, "ErrorProperties must not be null");
		this.errorProperties = errorProperties;
	}

	@Override
	public String getErrorPath() {
		return this.errorProperties.getPath();
	}

	@RequestMapping(produces = "text/html")
	public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
		HttpStatus status = getStatus(request);
		Map<String, Object> model = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.TEXT_HTML));
		ErrorResp resp = getErrorResp(request);
		if (resp != null) {
			model.put("errorId", resp.getErrorId());
			model.put("errorMsg", resp.getMessage());	
		}
		response.setStatus(status.value());
		ModelAndView modelAndView = resolveErrorView(request, response, status, model);
		// insertError(request);
		String errorPage ="error";
		if (status==HttpStatus.BAD_REQUEST){
			errorPage ="error/400";
		}else if (status==HttpStatus.NOT_FOUND){
			errorPage ="error/404";
		}else if (status==HttpStatus.UNAUTHORIZED){
			errorPage ="error/401";
		}else if (status==HttpStatus.INTERNAL_SERVER_ERROR){
			errorPage ="error/500";
		}
		
		return modelAndView == null ? new ModelAndView(errorPage, model) : modelAndView;
	}

	@SuppressWarnings("unused")
	@RequestMapping
	@ResponseBody
	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
		HttpStatus status = getStatus(request);
		ErrorResp resp = getErrorResp(request);
		if (resp != null) {
			body.put("errorId", resp.getErrorId());
			body.put("errorMsg", resp.getMessage());
		}

		return new ResponseEntity<Map<String, Object>>(body, HttpStatus.OK);
	}

	private ErrorResp getErrorResp(HttpServletRequest request) {
		Throwable e = (Throwable) request.getAttribute("javax.servlet.error.exception");
		ErrorResp resp = getErrorResp(e);
		return resp;
	}

	
	public static ErrorResp getErrorResp(Throwable e) {
		WebApplicationException ex = null;
		if (e != null && e.getCause() != null) {
			if (e.getCause() instanceof WebApplicationException) {
				ex = (WebApplicationException) e.getCause();
			} else if (e.getCause().getCause() != null) {
				if (e.getCause().getCause() instanceof WebApplicationException) {
					ex = (WebApplicationException) e.getCause().getCause();
				}
			}
		}
		ErrorResp resp;
		if (ex != null) {
			resp = ex.getResp();
		}else{
			resp = new ErrorResp();
			if (e!=null) {
				resp.setMessage(e.getMessage());
			}
		}
		return resp;
	}
	/**
	 * Determine if the stacktrace attribute should be included.
	 *
	 * @param request
	 *            the source request
	 * @param produces
	 *            the media type produced (or {@code MediaType.ALL})
	 * @return if the stacktrace attribute should be included
	 */
	protected boolean isIncludeStackTrace(HttpServletRequest request, MediaType produces) {
		ErrorProperties.IncludeStacktrace include = getErrorProperties().getIncludeStacktrace();
		if (include == ErrorProperties.IncludeStacktrace.ALWAYS) {
			return true;
		}
		if (include == ErrorProperties.IncludeStacktrace.ON_TRACE_PARAM) {
			return getTraceParameter(request);
		}
		return false;
	}

	/**
	 * Provide access to the error properties.
	 *
	 * @return the error properties
	 */
	protected ErrorProperties getErrorProperties() {
		return this.errorProperties;
	}
}