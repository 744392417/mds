/**
 * 
 */
package com.spt.mds.web.shiro;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

/**
 * @author jian
 * 
 */
public class MyFormAuthenticationFilter extends FormAuthenticationFilter {

	
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
			ServletResponse response) throws Exception {
		return super.onLoginSuccess(token, subject, request, response);
	}
	
	@Override
	protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		UsernamePasswordToken upToken = (UsernamePasswordToken)token;
		return super.onLoginFailure(token, e, request, response);
	}
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		if(this.isLoginRequest(request, response)) {
            if(this.isLoginSubmission(request, response)) {
                return this.executeLogin(request, response);
            } else {
                return true;
            }
        }else {
        	//判断是否是ajax请求
            if(isAjax(request)){
            	//设置超时
            	out(response);
            }else{
            	  //保存Request和Response 到登录后的链接
                this.saveRequestAndRedirectToLogin(request, response);
            }
            return false;
        }
	}
	
	/**
	 * 是否是ajax 请求
	 * @param request
	 * @return
	 */
	 public static boolean isAjax(ServletRequest request){
	        String header = ((HttpServletRequest) request).getHeader("X-Requested-With");
	        //为ajax请求
	        if("XMLHttpRequest".equalsIgnoreCase(header)){
	            return Boolean.TRUE;
	        }
	        return Boolean.FALSE;
	    }
	    /**
	     * response 设置超时
	     * @param hresponse
	     * @param resultMap
	     * @throws IOException
	     */
	    public static void out(ServletResponse servletResponse){
	        HttpServletResponse response = (HttpServletResponse) servletResponse;        
	        response.setCharacterEncoding("UTF-8");
	        //在响应头设置session状态
	        response.setHeader("session-status", "timeout");
	    }
}
