/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.spt.mds.web.shiro;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsoft.admin.client.remote.ISysUserClient;
import com.hsoft.commutil.encrypt.Digests;
import com.hsoft.commutil.encrypt.Encodes;
import com.hsoft.commutil.shiro.ShiroUser;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.LoginRespVo;
import com.spt.hospital.client.vo.MdsUserInfoVo;

public class ShiroDbRealm extends AuthorizingRealm {
	private Logger logger = LoggerFactory.getLogger(ShiroDbRealm.class);
	private static final int SALT_SIZE = 8;
	public static final int HASH_INTERATIONS = 1024;

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		LoginRespVo respVo = loginSpt(token.getUsername(),token.getPassword());
		
		//BsUserAccount user = userAccountClient.findUserByLoginName(token.getUsername());
		MdsUserInfoEntity user = new  MdsUserInfoEntity();
		if (user != null) {
			String userSalt = user.getSalt();
			byte[] salt;
			if(StringUtils.isBlank(userSalt)){
				salt = Digests.generateSalt(SALT_SIZE);;
			}else{
				salt = Encodes.decodeHex(userSalt);
			}
			String loginPwd = user.getLoginPwd();
			//判断是否是免密登录
			if(respVo.isRegistStatus()){
				loginPwd = getEncodePwd(String.valueOf(token.getPassword()), salt);
			}
			ShiroUser shiroUser = new ShiroUser(user.getId(), user.getLoginName(), user.getUserName());
			//修改authcToken得密码，免密登录
			SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(shiroUser, loginPwd, ByteSource.Util.bytes(salt), getName());
			Map<String, Object> prop = shiroUser.getProp();
			if(prop==null){
				prop=new HashMap<String, Object>();
			}
			
			//Long companyId = user.getCompanyId();
		//	String companyUiid = "";
//			if(companyId!=null){/*
//				BsCompanyMapping company = companyMappingClient.findByCompanyId(companyId);
//				if(company!=null){
//					companyUiid = company.getOutCompanyId();
//				}
//			*/}
//			
//			prop.put("companyUuid", companyUiid);
//			prop.put("companyId", String.valueOf(companyId));
//			prop.put("roleCd", user.getRoleCd());
//			prop.put("deptId", user.getDeptId()+"");
/*			BsCompany company = bsCompanyClient.findByCompanyId(companyId);
			String companyType = null;
			String companyName = null;
			if(company!=null){
				companyType = company.getCompanyType();
				companyName = company.getCompanyName();
			}
			prop.put("companyType", companyType);
			prop.put("companyName", companyName);*/
		/*	BsAccountMapping am = accountMappingClient.findByAccountId(user.getId());
			if(am!=null){
				prop.put("userUuid", am.getOutAccountId());
			}*/
			shiroUser.setProp(prop);
			
			return info;
		}
		
		
		return null;
	}
	
	/**
	 * 加密算法
	 */
	private String getEncodePwd(String plainPwd, byte[] salt) {
		byte[] hashPassword = Digests.sha1(plainPwd.getBytes(), salt, HASH_INTERATIONS);
		return Encodes.encodeHex(hashPassword);
	}
	/**
	 * 登录商品通用户名密码判断
	 * @param loginName
	 * @param passWord
	 * @return
	 */
	private LoginRespVo loginSpt(String loginName,char[] passWord){
		LoginRespVo respVo = new LoginRespVo();
		
		MdsUserInfoVo mdsUserInfoVo = new MdsUserInfoVo();
		mdsUserInfoVo.setLoginName(loginName);
		mdsUserInfoVo.setLoginPwd(String.valueOf(passWord));
	//	SptLoginUserVo loginVo;
		return respVo;
		
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
	//	BsUserAccount user = userAccountClient.findUserByLoginName(shiroUser.loginName);
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		
		return info;
	}

	/**
	 * 设定Password校验的Hash算法与迭代次数.
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(ISysUserClient.HASH_ALGORITHM);
		matcher.setHashIterations(ISysUserClient.HASH_INTERATIONS);

		setCredentialsMatcher(matcher);
	}
	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(Object principal) {

		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {

		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if ( cache != null ) {
			for ( Object key : cache.keys() ) {
				cache.remove(key);
			}
		}
	}
}
