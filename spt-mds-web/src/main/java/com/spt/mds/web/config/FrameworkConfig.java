package com.spt.mds.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.hsoft.commutil.CommandExecutor;
import com.hsoft.commutil.interceptor.PageInterceptor;
import com.hsoft.commutil.interceptor.TaskInterceptor;
import com.hsoft.commutil.kafka.ProducerBase;
import com.hsoft.commutil.redis.RedisDataSource;
import com.hsoft.commutil.remote.IRemoteService;
import com.hsoft.commutil.remote.RemoteServiceImpl;
import com.hsoft.commutil.util.SpringContextHolder;
import com.spt.mds.web.shiro.ShiroUtil;

@Configuration
public class FrameworkConfig {
//	private Logger logger = LoggerFactory.getLogger(FrameworkConfig.class);
	@Autowired
	private Environment env;
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate template = new RestTemplate();
		SimpleClientHttpRequestFactory factory = (SimpleClientHttpRequestFactory) template.getRequestFactory();
		factory.setConnectTimeout(5000);
		factory.setReadTimeout(30000);
		return template;
	}
	
	@Bean
	public TaskInterceptor taskInterceptor() {
		return new TaskInterceptor();
	}
	
	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
		ThreadPoolTaskExecutor executor =new ThreadPoolTaskExecutor();
		return executor;
	}
	@Bean
	public RedisDataSource redisDataSource(){
		RedisDataSource dataSource =new RedisDataSource();
		dataSource.setHost(env.getProperty("spring.redis.host"));
		dataSource.setPort(Integer.valueOf(env.getProperty("spring.redis.port")));
		return dataSource;
	}
	
	@DependsOn("springContextHolder")
	@Bean("shiroUtil")
	public ShiroUtil shiroUtil(){
		ShiroUtil shiroUtil =new ShiroUtil();
		return shiroUtil;
	}
	//----------commonutil------------------
	@Bean
	public SpringContextHolder springContextHolder(ApplicationContext applicationContext){
		SpringContextHolder contextHolder=  new SpringContextHolder();
		contextHolder.setApplicationContext(applicationContext);
		return contextHolder;
	}
	
	@Bean
	public ProducerBase producerBase(){
		ProducerBase producerBase = new ProducerBase();
//		producerBase.init();
		return producerBase;
	}
	
	@Bean
	public PageInterceptor pageInterceptor(){
		PageInterceptor interceptor = new PageInterceptor();
		return interceptor;
	}
	
	@Bean
	public IRemoteService remoteService(){
		return new RemoteServiceImpl();
	}
	//----------commonutil------------------
	

	@Bean
	public CommandExecutor commandExecutor() {
		return new CommandExecutor();
	}
}
