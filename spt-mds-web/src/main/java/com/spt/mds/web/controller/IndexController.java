/**
 * 
 */
package com.spt.mds.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hsoft.admin.client.open.IAdminOpenFacade;
import com.hsoft.admin.client.vo.UserChangeVo;
import com.hsoft.commutil.encrypt.TokenUtil;
import com.hsoft.commutil.util.DateOperator;
import com.hsoft.commutil.util.RenderUtil;
import com.spt.mds.web.util.CookieUtility;

/**
 * @author huangjian
 * 
 */
@Controller
@RequestMapping(value = "/index")
public class IndexController {
	@Autowired
	private IAdminOpenFacade adminOpenClient;



	@RequestMapping(value = "")
	public String index(Model model) {
		return "index";
	}
//	@RequestMapping(value = "success")
//	public String sucess(){
//		String companyType = ShiroUtil.getCurrentCompanyType();
///*		if(RobotConstants.COMPANY_TYPE_C.equals(companyType)){
//			return "redirect:/rbt/treaty/findAll"; 
//		}else{*/
//			return "redirect:/rbt/singleShot/findAll"; 
////		}
//	}
	

	@RequestMapping(value = "changePwd", method = RequestMethod.POST)
	public void changePwd(@Valid @RequestParam("id")  Long userId, @RequestParam("oldPwd") String oldPwd,
			@RequestParam("newPwd") String newPwd,HttpServletRequest request,  HttpServletResponse response) {
		try {
			UserChangeVo vo=new UserChangeVo();
			vo.setPassword(oldPwd);
			vo.setUserId(userId);
			if (!adminOpenClient.isPwdEqual(vo)) {
				RenderUtil.renderText("error", response);
				return;
			}
			String result = "success";
			vo.setPassword(newPwd);
			adminOpenClient.updateUser(vo);
			RenderUtil.renderText(result, response);
		} catch (Exception e) {
			RenderUtil.renderFailure("保存失败", response);
		}
	}
	
	
	@RequestMapping(value = "abountUs", method = RequestMethod.GET)
	public String abountUs(HttpServletResponse response) throws Exception {
		System.out.println(111);
	    String appcode =  "fz";
	    String secretKey = "5ldAjnhlTeyDETnDikGpP0EG07W1tcYD";
	    //数据签名ptRvglMCJX8rjquHOyjoSRMyUfDgwekr
	    Map<String,Object> params = new HashMap<String,Object>();
	    params.put("userId","SPTU0000002091");
	    params.put("timestamp", DateOperator.formatDate(new Date(), "yyyyMMddHHmmss"));
	    String token =  TokenUtil.createToken("CuoCuoOAuth", params,secretKey);
	    String  gateWay = "http://192.168.1.52:8088/";
	    String path = "/";
	    int maxAge = 24 * 60 * 60 * 1000;// 24小时候
	    CookieUtility.addCookie("AppCode",appcode, response, path, maxAge);
	    CookieUtility.addCookie("IMGATEWAY",gateWay, response, path, maxAge);
	    CookieUtility.addCookie("AccessToken", token, response, path, maxAge);	    
	    return "cuocuo/test";
	  }
}
