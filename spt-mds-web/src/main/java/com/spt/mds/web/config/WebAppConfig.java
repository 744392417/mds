/**
 * 
 */
package com.spt.mds.web.config;

import java.util.List;

import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsoft.commutil.interceptor.PageInterceptor;
import com.hsoft.commutil.util.JsonUtil;
import com.spt.mds.open.interceptor.AuthenticationHeaderInterceptor;

/**
 * @author wlddh
 *
 */
@Configuration
public class WebAppConfig extends WebMvcConfigurerAdapter {
	@Autowired
	private PageInterceptor pageInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new AuthenticationHeaderInterceptor()).addPathPatterns("/open/**");
		registry.addInterceptor(pageInterceptor);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/index/success");
		super.addViewControllers(registry);
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		ObjectMapper objectMapper = JsonUtil.getObjectMapper();
		converter.setObjectMapper(objectMapper);
		converters.add(converter);
		super.configureMessageConverters(converters);
	}

	@Bean
	public FilterRegistrationBean characterEncodingFilter() {
		FilterRegistrationBean filter = new FilterRegistrationBean();
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		filter.setFilter(characterEncodingFilter);
		return filter;
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// 设置文件大小限制 ,超出设置页面会抛出异常信息，
		// 这样在文件上传的地方就需要进行异常信息的处理了;
		factory.setMaxFileSize("10MB"); // KB,MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize("100MB");
		// Sets the directory location where files will be stored.
		// factory.setLocation("路径地址");
		return factory.createMultipartConfig();
	}
	
	@Autowired(required = false)
	private List<ErrorViewResolver> errorViewResolvers;

	@Bean
	public BasicErrorController basicErrorController(ErrorAttributes errorAttributes,
			ServerProperties serverProperties) {
		return new BasicErrorController(errorAttributes, serverProperties.getError(), this.errorViewResolvers);
	}
	
	/**  
     * 装饰器  
     */  
    @Bean  
    public FilterRegistrationBean siteMeshFilter(){  
        FilterRegistrationBean fitler = new FilterRegistrationBean();  
        WebSiteMeshFilter siteMeshFilter = new WebSiteMeshFilter();  
        fitler.setFilter(siteMeshFilter);  
        fitler.setOrder(1);
        return fitler;  
    }  
}
