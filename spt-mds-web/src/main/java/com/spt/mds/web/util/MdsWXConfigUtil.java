package com.spt.mds.web.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.wxpay.sdk.WXPayConfig;

import ch.qos.logback.classic.Logger;
 
@Component
public class MdsWXConfigUtil implements WXPayConfig {
	private Logger logger = (Logger) LoggerFactory.getLogger(getClass());
	
    private byte[] certData;

    public static String certificateFile;
    
    public String getCertificateFile() {
		return certificateFile;
	}
    @Value("${wx.refund.cert.path}")
	public void setCertificateFile(String certificateFile) {
    	logger.info("certificateFile.========="+certificateFile);
		this.certificateFile = certificateFile;
	}

	public MdsWXConfigUtil() throws Exception {
   
      //从微信商户平台下载的安全证书存放的目录
	//	String certPath = "";
     if(certificateFile!=null){
    	 logger.info("certificateFile====MdsWXConfigUtil===="+certificateFile);
		// File file = ResourceUtils.getFile("classpath:mds/apiclient_cert.p12");
		//System.out.println("certPath>>>>>>>>>>>>>>="+certPath+"apiclient_cert.p12");
       File file = new File(certificateFile);
      
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
     }

        
    }
 
    @Override
    public String getAppID() {
        return "wx83289454fe7a15b1";
    }
 
    //parnerid
    @Override
    public String getMchID() {
        return "1520601861";
    }
 
    @Override
    public String getKey() {
        return "GRMd42LnZBf0ce1tuiKo4cTUsXYMB8aX";
    }
 
    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }
 
    @Override
    public int getHttpConnectTimeoutMs() {
        return 8000;
    }
 
    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}
