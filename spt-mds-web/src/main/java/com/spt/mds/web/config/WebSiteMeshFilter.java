package com.spt.mds.web.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

public class WebSiteMeshFilter extends ConfigurableSiteMeshFilter {
	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
			builder/*.addDecoratorPath("/*", "/siteMesh/index")*/
			   .addDecoratorPath("/*Price/*", "/siteMesh/userIndex")
			   .addDecoratorPath("/*order/*", "/siteMesh/userIndex")
			   .addDecoratorPath("/rbt/treaty/findAll*", "/siteMesh/userIndex")
			   .addDecoratorPath("/rbt/singleShot/findAll*", "/siteMesh/userIndex")
			   .addDecoratorPath("/login", "/siteMesh/index")
				.addDecoratorPath("/setting/resetPassword", "/siteMesh/index")
			   .addDecoratorPath("/setting/resetPassword1", "/siteMesh/index")
				.addExcludedPath("/admin/index")
				.addExcludedPath("/plugin/*")
				.addExcludedPath("/bs/templateConfig/*")
				.addExcludedPath("/file/loadFiles*")
				.addExcludedPath("/offPrice/findMyOfferPirceList*")
				.addExcludedPath("/commonPrice/add")
				.addExcludedPath("/commonPrice/deleteCommonPrice")
				.addExcludedPath("/offPrice/createNewOfferPrice*")
				.addExcludedPath("/comparePrice/findCompareList*")
				.addExcludedPath("/comparePrice/findCompareMany*")
				.addExcludedPath("/offPrice/offerDelete*")
				.addExcludedPath("/order/dealDetail*")
				.addExcludedPath("/order/findMyOrderList*")
				.addExcludedPath("/commonPrice/findAllCommonPrice*");
		
	}
}
