package com.spt.mds.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/regist")
public class RegistControll  {
	
	
	/*@RequestMapping(value = "register",method = RequestMethod.GET)
	public String register(){
		return "saas_regist/regist_1";
	}*/
	
	/**
	 * 验证码 邮箱验证
	 */
	/*@RequestMapping(value = "code",method = RequestMethod.POST)
	public String code(HttpServletRequest request,HttpServletResponse response,Model model){
		String random=(String) request.getSession().getAttribute("random");
		String inputcheckCode=request.getParameter("inputcheckCode");
		String tbEmail=request.getParameter("tbEmail");
		if (!random.equals(inputcheckCode)) {
			model.addAttribute("mess", "验证码不正确");
			model.addAttribute("emilaCink", tbEmail);
			model.addAttribute("inputcheckCode", inputcheckCode);
			return "saas_regist/regist_1";
		}else{
			BsUserAccount userAccount = userAccountClient.findUserByLoginName(tbEmail);	
			if(userAccount==null){
				BsUserAccount user=userAccountClient.findByEmailLoginName(tbEmail);
				if(user==null){
					user=new BsUserAccount();
				}
				user.setLoginName(tbEmail);
				user.setEmail(tbEmail);
				userAccountClient.register(user);
				model.addAttribute("tbEmail", tbEmail);
				request.getSession().setAttribute("tbEmail", tbEmail);
				return "saas_regist/regist_2";				
			}else{
				model.addAttribute("emila", "该邮箱已存在");
				request.setAttribute("emilaCink", tbEmail);
				return "saas_regist/regist_1";
			}			
		}		
	}*/
		
	
	
	/**
	 * 返回重新填写
	 */
	/*@RequestMapping(value = "returnRegist_index",method = RequestMethod.GET)
	public String returnRegist_index(){
		return "regist/regist_index";
	}*/
	
	/**
	 * 重新发送邮件
	 */
	/*@RequestMapping(value = "resendEmail",method = RequestMethod.GET)
	public String resendEmail(HttpServletRequest request,@RequestParam("tbEmail") String tbEmail,HttpServletResponse response,Model model){
		BsUserAccount user=userAccountClient.findByEmailLoginName(tbEmail);
		userAccountClient.register(user);
		model.addAttribute("tbEmail", tbEmail);
		return "saas_regist/regist_2";
	}*/
	
	/**
	 * 激活邮箱
	 */
	/*@RequestMapping(value = "activeEmail/{email}/{mailActiveCode}",method = RequestMethod.GET)
	public String activeEmail(@PathVariable("email") String email,@PathVariable("mailActiveCode") String mailActiveCode,HttpServletRequest request,HttpServletResponse response,Model model){
		if(StringUtils.isNotBlank(email)&&StringUtils.isNotBlank(mailActiveCode)){
			String mailActive = email+","+mailActiveCode;
			boolean result = userAccountClient.activeEmail(mailActive);
			UserAccountEmailVo userAccountEmail=new UserAccountEmailVo();
		    if(result){
		    	userAccountEmail.setLoginName(email);
		    	userAccountEmail.setEmailFlg(true);
		    	userAccountClient.updateEmailFlg(userAccountEmail);
		    }
		}
		model.addAttribute("successEmail", email);
		return "saas_regist/regist_3";
	 }*/
	
	/**
	 * 验证码发送至手机短信
	 * @param mobile
	 */
	/*@RequestMapping(value = "sendSms", method = RequestMethod.POST)
	public void sendSms(@RequestParam("mobile") String mobile, HttpServletRequest request,HttpServletResponse response){
		request.getSession().setAttribute("mobile", mobile);
		UserAccountSendRequestVo vo =new UserAccountSendRequestVo();
		vo.setMobile(mobile);
		userAccountClient.sendSms(vo);
	}*/
	
	/**
	 * 短信验证
	 * @param mobile
	 * @param sms
	 * @param response
	 */
	/*@RequestMapping(value = "smsValidate", method = RequestMethod.POST)
	public void smsValidate(@Valid @RequestParam("sms")  String sms, HttpServletRequest request, HttpServletResponse response,Model model){
		UserAccountVo vo = new UserAccountVo();
		String mobile= (String) request.getSession().getAttribute("mobile");
		vo.setMobileNumber(mobile);
		vo.setSms(sms);
		boolean flag = userAccountClient.smsValidate(vo);
		if(flag){
			RenderUtil.renderText(OfferConstants.RESULT_SUCCESS, response);
		}else{
			RenderUtil.renderText(OfferConstants.RESULT_FAIL, response);
		}
	}*/
	
	/**
	 * 获取图片验证码
	 * @param req
	 * @param resp
	 */
	/*@RequestMapping("/getPicValidate")
	public void getPicValidate(HttpServletRequest req,HttpServletResponse resp) {
		ServletOutputStream sos = null;
		RandomNumUtil rdnu = RandomNumUtil.Instance();
		HttpSession session = req.getSession();
		session.setAttribute("random", rdnu.getString());
		// 禁止图像缓存。
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", 0);
		resp.setContentType("image/jpeg");
		try {
			// 将图像输出到Servlet输出流中。
			sos = resp.getOutputStream();
			ImageIO.write(rdnu.getImage(), "jpeg", sos);
		} catch (Exception e) {

		} finally {
			try {
				sos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	
}
