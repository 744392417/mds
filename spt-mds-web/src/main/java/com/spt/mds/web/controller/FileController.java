/**
 * 
 */
package com.spt.mds.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hsoft.commutil.props.PropertiesUtil;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.commutil.util.RenderUtil;
import com.hsoft.commutil.web.FileUploadRequest;
import com.hsoft.file.client.constant.FileConstant;
import com.hsoft.file.client.entity.SysFile;
import com.hsoft.file.client.remote.FileRemote;
import com.hsoft.file.client.vo.Base64DataVo;
import com.hsoft.file.client.vo.FileRespVo;
import com.hsoft.file.client.vo.FileSearchVo;
import com.hsoft.file.client.vo.FileUploadBase64Request;

/**
 * @author wlddh
 *
 */
@Controller
@RequestMapping(value = "/file")
public class FileController {
	private final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private FileRemote fileRemote;

	
	@Value("${file.bucket}")
	private String filebucket;
	 /**
    * 上传附件
    * @param model
    * @param vo
    * @param req
    * @param resp
    * @throws Exception
    */
	@RequestMapping("/uploadFile")
	public void uploadFile(FileUploadRequest fileRequest,HttpServletRequest req, HttpServletResponse resp) throws Exception {
		try {
			fileRequest.parseFiles(req);
			fileRequest.setFilePath(filebucket);
			String[] allTypes = { ".jpg", ".gif", ".png", ".bmp",".pdf",".docx",".doc"  };
			fileRequest.setAllowTypes(allTypes);
			fileRequest.setServerName(req.getServerName());
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			FileRespVo fileRespVo=  fileRemote.uploadFile(fileRequest);
			
			RenderUtil.renderJson(JsonUtil.obj2Json(fileRespVo), resp);
		} catch (Exception e) {
			logger.error("uploadFile error!", e);
		}

	}
	
	
	/**富文本编辑框 文件上传 */
	@RequestMapping("/uploadConfigFile")
	public void uploadConfigFile(Model model, FileUploadRequest fileRequest, HttpServletRequest req, HttpServletResponse resp) {
		try {
			fileRequest.parseFiles(req);
			String[] allTypes = { ".jpg", ".gif", ".png", ".bmp",".pdf",".docx",".doc"  };
			fileRequest.setAllowTypes(allTypes);
			fileRequest.setFilePath("config/");
			fileRequest.setServerName(req.getServerName());
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			FileRespVo fileRespVo=  fileRemote.uploadFile(fileRequest);
			String fileId =fileRespVo.getFileId();
			String filePath = PropertiesUtil.getProperty(FileConstant.FILE_PATH_KEY);
			String url = filePath+"/view/show/" + fileId;
			
			Map<String, Object> map =new HashMap<String, Object>();
			map.put("state", "SUCCESS");
			map.put("url", url);
			map.put("title", fileId);
			map.put("original", "图片上传");
			RenderUtil.renderJson(map, resp);
		} catch (Exception e) {
			logger.error("uploadConfigFile error!", e);
		}

	}
	
	
	@RequestMapping(value = "/delete")
	public void delete(HttpServletResponse resp, Long fileId) {
		try {
			FileRespVo fileRespVo = fileRemote.delete(fileId);
			RenderUtil.renderSuccess(fileRespVo.getFileId(), resp);
		} catch (Exception e) {
			logger.error("delete error!", e);
		}
	}
	
	/**上传基础格式文件demo */
	@RequestMapping("/uploadbaseFile")
	public void uploadbaseFile(Model model, FileUploadBase64Request fileRequest, HttpServletRequest req, HttpServletResponse resp) {
		try {
			String[] allTypes = { ".jpg", ".gif", ".png", ".bmp",".pdf",".docx",".doc"  };
			fileRequest.setAllowTypes(allTypes);
			fileRequest.setFilePath("user");
			fileRequest.setServerName(req.getServerName());
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			fileRequest.setBizFieldName("img_file_id");
			fileRequest.setBizTableName("t_sys_user");
			fileRequest.setBizId(4l);
			//将上传的文件转为base64
			fileRequest.fileCoverBase64(req);
			FileRespVo fileRespVo=  fileRemote.uploadBase64(fileRequest);
			String fileId =fileRespVo.getFileId();
			String filePath = PropertiesUtil.getProperty(FileConstant.FILE_PATH_KEY);
			String url = filePath+"/view/show/" + fileId;
			
			Map<String, Object> map =new HashMap<String, Object>();
			map.put("state", "SUCCESS");
			map.put("url", url);
			map.put("title", fileId);
			map.put("original", "图片上传");
			RenderUtil.renderJson(map, resp);
		} catch (Exception e) {
			logger.error("uploadConfigFile error!", e);
		}

	}
	
	/**上传base64格式文件 demo*/
	@RequestMapping("/uploadbase64File")
	public void uploadbase64File(Model model, FileUploadBase64Request fileRequest, HttpServletRequest req, HttpServletResponse resp) {
		try {
			String[] allTypes = { ".jpg", ".gif", ".png", ".bmp",".pdf",".docx",".doc"  };
			fileRequest.setAllowTypes(allTypes);
			fileRequest.setFilePath("user");
			fileRequest.setServerName(req.getServerName());
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			fileRequest.setBizFieldName("img_file_id");
			fileRequest.setBizTableName("t_sys_user");
			fileRequest.setBizId(2l);
			List<Base64DataVo> dataList = new ArrayList<Base64DataVo>();
			Base64DataVo dataVo = new Base64DataVo();
			dataVo.setBase64Data("base64字符串");
			dataVo.setFileName("文件名");
			dataList.add(dataVo);
			fileRequest.setDataList(dataList);
			FileRespVo fileRespVo=  fileRemote.uploadBase64(fileRequest);
			String fileId =fileRespVo.getFileId();
			String filePath = PropertiesUtil.getProperty(FileConstant.FILE_PATH_KEY);
			String url = filePath+"/view/show/" + fileId;
			
			Map<String, Object> map =new HashMap<String, Object>();
			map.put("state", "SUCCESS");
			map.put("url", url);
			map.put("title", fileId);
			map.put("original", "图片上传");
			RenderUtil.renderJson(map, resp);
		} catch (Exception e) {
			logger.error("uploadConfigFile error!", e);
		}

	}
	
	@RequestMapping("/loadFiles")
	public String uploadbase64File(Model model, HttpServletRequest req,FileSearchVo vo) {
		List<SysFile> list = fileRemote.loadFiles(vo);
		model.addAttribute("list", list);
		String canEditFile = req.getParameter("hasEditFile");
		boolean canEdit =BooleanUtils.toBoolean(canEditFile);
		model.addAttribute("hasEditFile", canEdit);
		return "file/file-show";
	}
}
