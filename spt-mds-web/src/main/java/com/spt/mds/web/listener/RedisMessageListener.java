package com.spt.mds.web.listener;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.QueryVo;
 
@Component
public class RedisMessageListener extends KeyExpirationEventMessageListener {

	public RedisMessageListener(RedisMessageListenerContainer listenerContainer) {
		super(listenerContainer);
	}

	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	
	@Override
	public void onMessage(Message message, byte[] pattern) {
		// key 过期时调用
		logger.info("RedisMessageListener ===onPMessage pattern " + pattern + " " + " " + message);
		String channel = new String(message.getChannel());
		String str = (String) redisTemplate.getValueSerializer().deserialize(message.getBody());
		logger.info("RedisMessageListener ===onMessage=start=="+str);
		if(str.substring(0, 7).equals("pay_pre")){
			QueryVo queryVo = new QueryVo();
			logger.info("RedisMessageListener =111111111111=="+str.substring(7));
			queryVo.setOrderNumber(str.substring(7));
			MdsLeaseInfoEntity overdueLease = mdsLeaseInfoApi.findByOrderNumber(queryVo);
			//处理待付款
			if(overdueLease.getOrderStatus().equals(HospConstants.ORDER_STATUS_2)){
				logger.info("RedisMessageListener ===overdueLease=="+(overdueLease==null));
				logger.info("RedisMessageListener ===overdueLease=="+overdueLease.getUnlockPwd());
				overdueLease.setLeaseEndDate(new Date());
				overdueLease.setOrderStatus(HospConstants.ORDER_STATUS_1);//订单完成
				mdsLeaseInfoApi.save(overdueLease);
				MdsProductInfoEntity overdueProduct =mdsProductInfoApi.getEntity(overdueLease.getProductId());
				logger.info("RedisMessageListener ===overdueProduct=="+(overdueProduct==null));
				logger.info("RedisMessageListener ===overdueProduct=="+overdueProduct.getProductQrcode());
				overdueProduct.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);//设备变空闲
				mdsProductInfoApi.save(overdueProduct);
			}

			System.out.println("RedisMessageListener ===onMessage=end==");
		}


	}

}