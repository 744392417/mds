package com.spt.mds.web.controller;
//package com.spt.ucs.web.controller;
//
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import com.alibaba.fastjson.JSONObject;
//import com.hsoft.commutil.exception.ApplicationException;
//import com.hsoft.commutil.util.RenderUtil;
//import com.spt.credit.client.api.IBsUserAccountApi;
//import com.spt.credit.client.vo.LoginRespVo;
//import com.spt.credit.client.vo.SptLoginUserVo;
//import com.spt.credit.client.vo.UserAccountVo;
//import com.spt.ucs.web.shiro.ShiroUtil;
//
///**
// * LoginController负责打开登录页面(GET请求)和登录出错页面(POST请求)，
// * 
// * 真正登录的POST请求由Filter完成,
// * 
// */
//@Controller
//@RequestMapping(value = "/login")
//public class LoginController {
//	@Autowired
//	private IBsUserAccountApi userAccountClient;
//
//	@RequestMapping(method = RequestMethod.GET)
//	public String login() {
//		if (ShiroUtil.isLogin()) {
//			return "redirect:/";
//		}
//		return "index";
//	}
//
//	@RequestMapping(method = RequestMethod.POST)
//	public String fail(@RequestParam(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String userName, Model model) {
//
//		if (ShiroUtil.isLogin()) {
//			return "redirect:/";
//		}
//		model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, userName);
//		model.addAttribute("userName", userName);
//		model.addAttribute("errorInfo", "登录名或密码错误");
//		return "index";
//	}
//	/**
//	 * 商品通登录判断，是否需要cfca
//	 * @param accountVo
//	 * @param response
//	 */
//	@RequestMapping(value ="loginSpt",method = RequestMethod.POST)
//	private void isLoginSpt(UserAccountVo accountVo,HttpServletResponse response){
//		try{
//			SptLoginUserVo loginVo = userAccountClient.loginSptUser(accountVo);
//			LoginRespVo respVo = new LoginRespVo();
//			if(loginVo.isRegistStatus()){
//				respVo.setCfcaStatus(loginVo.getCfcaStatus());
//				respVo.setRegistStatus(loginVo.isRegistStatus());
//				respVo.setUkRealSerialNo(loginVo.getUkRealSerialNo());
//				RenderUtil.renderSuccess(JSONObject.toJSONString(respVo), response);
//			}else{
//				respVo.setErrorId(loginVo.getErrorId());
//				respVo.setMessage(loginVo.getMessage());
//				RenderUtil.renderFailure(JSONObject.toJSONString(respVo), response);
//			}
//			
//		}catch (ApplicationException e) {
//			RenderUtil.renderFailure(e.getMessage(), response);
//		}
//		
//		
//	}
//
//}
