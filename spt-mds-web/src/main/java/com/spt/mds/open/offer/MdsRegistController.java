package com.spt.mds.open.offer;
//package com.spt.ucs.open.offer;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.imageio.ImageIO;
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.hsoft.admin.client.cache.DictUtil;
//import com.hsoft.admin.client.entity.AppDictData;
//import com.hsoft.commutil.exception.ErrorResp;
//import com.hsoft.commutil.redis.RedisUtil;
//import com.spt.credit.client.api.IBsUserAccountApi;
//import com.spt.credit.client.constant.OfferConstants;
//import com.spt.credit.client.entity.BsUserAccount;
//import com.spt.credit.client.vo.UserAccountEmailVo;
//import com.spt.credit.client.vo.UserAccountSendRequestVo;
//import com.spt.credit.client.vo.UserAccountUpdateVo;
//import com.spt.credit.client.vo.UserAccountVo;
//import com.spt.ucs.open.vo.RespVo;
//import com.spt.ucs.web.config.BasicErrorController;
//import com.spt.ucs.web.util.RandomNumUtil;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//
//@Api(value = "注册", description = "注册")
//@RestController
//@RequestMapping(value = "open/regist/register")
//public class RegistOpenControll {
//
//	private Logger logger = LoggerFactory.getLogger(getClass());
//	@Autowired
//	private IBsUserAccountApi userAccountClient;
//	
//	/**
//	 * 验证码 邮箱验证
//	 */
//	@ApiOperation(value = "注册第一步 邮箱验证 发送激活邮箱",notes ="email(邮箱,String类型),sms(验证码,String类型)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足"),
//        @ApiResponse(code = 1050, message = "验证码输入不正确"),
//        @ApiResponse(code = 1051, message = "邮箱已存在")
//        })
//	@PostMapping(value="code")
//	public RespVo<Map<String, Object>> code(HttpServletRequest request,@RequestBody UserAccountSendRequestVo vo){
//		RespVo<Map<String, Object>> resultVo=new RespVo<Map<String, Object>>();
//		Map<String, Object> map=new HashMap<String, Object>();
//		try {			
//				// 根据邮箱 查看该邮箱是否存在有效用户
//				BsUserAccount userAccount = userAccountClient.findUserByLoginName(vo.getEmail());	
//				if(userAccount==null){
//					//查看该用户是否存在注册记录
//					BsUserAccount user=userAccountClient.findByEmailLoginName(vo.getEmail());
//					if(user==null){
//						user=new BsUserAccount();
//					}
//					//获得登录邮箱地址
//					AppDictData data = DictUtil.getListByCategory(OfferConstants.SAAS_EMAIL).get(0);
//					String companyEmailUrl = data.getRemark();
//					map.put("companyEmailUrl", companyEmailUrl);
//					
//					user.setLoginName(vo.getEmail());
//					user.setEmail(vo.getEmail());
//					userAccountClient.register(user);
//					map.put("tbEmail", vo.getEmail());
//					RedisUtil.set("registerEmail", vo.getEmail());
//				}else{
//					resultVo.setCode(1051);
//					resultVo.setMessage("邮箱已经存在");
//				}
//			resultVo.setData(map);
//		} catch (Exception e) {
//			logger.error("code", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			if(errsp.getMessage().equals("1055")){
//				resultVo.setMessage("邮箱只能为商品通企业邮箱！");
//				resultVo.setCode(1055);
//			}else{
//				resultVo.setFail(errsp);
//			}
//		}
//		return resultVo;
//	}
//						
//	
//	/**
//	 * 激活邮箱
//	 * @param voAccount
//	 * @return
//	 */
//	@ApiOperation(value = "激活邮箱",notes ="mailActiveCode(随机码,String类型)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足"),
//        @ApiResponse(code = 1052, message = "邮箱会话失效"),
//        @ApiResponse(code = 1049, message = "邮箱验证失败,验证错误!")
//			})
//	@PostMapping(value="activeEmail")
//	public RespVo<Map<String,Object>> activeEmail(HttpServletRequest request,HttpServletResponse response,@RequestBody UserAccountSendRequestVo vo){
//		RespVo<Map<String,Object>> resultVo = new RespVo<Map<String,Object>>();
//		Map<String,Object> map=new HashMap<>();
//		try {
//			String email=RedisUtil.get("registerEmail");
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱验证失败,验证错误!");
//				throw new IllegalAccessError();
//			}
//			
//			if(StringUtils.isNotBlank(email)&&StringUtils.isNotBlank(vo.getMailActiveCode())){
//				String mailActive = email+","+vo.getMailActiveCode();
//				boolean result = userAccountClient.activeEmail(mailActive);
//				UserAccountEmailVo userAccountEmail=new UserAccountEmailVo();
//			    if(result){
//			    	userAccountEmail.setLoginName(email);
//			    	userAccountEmail.setEmailFlg(true);
//			    	userAccountClient.updateEmailFlg(userAccountEmail);
//			    	map.put("email", email);
//			    }else{
//			       resultVo.setCode(1049);
//			       resultVo.setMessage("邮箱验证失败！");
//			    }
//			}
//			resultVo.setData(map);
//		} catch (Exception e) {
//			logger.error("activeEmail", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//	
//	/**
//	 * 验证码发送至手机短信
//	 * @param voAccount
//	 * @return 
//	 */
//	@ApiOperation(value = "验证码发送至手机短信",notes ="mobile(String类型,手机号)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足") })
//	@PostMapping(value="sendSms")
//	public RespVo<String> sendSms(@RequestBody UserAccountSendRequestVo voAccount){
//		RespVo<String> resultVo = new RespVo<>();	
//		try {
//			BsUserAccount account= userAccountClient.findUserByMobile(voAccount.getMobile());
//			if(account!=null){
//				throw new IllegalArgumentException();
//			}
//			if(voAccount.getMobile().equals("")){
//				throw new NullPointerException();
//			}
//			//UserAccountSendRequestVo vo=userAccountClient.sendSms(voAccount);
//			//resultVo.setData(vo.getSms());
//		}catch(IllegalArgumentException e){
//			logger.error("account is exist", e);			
//			//ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setCode(402);
//			resultVo.setMessage("账号已存在");
//		}
//		catch (Exception e) {
//			logger.error("sendSms", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//	
//	
//	/**
//	 * 短信验证
//	 * @param sms
//	 */
//	@ApiOperation(value = "短信验证",notes ="sms(String类型,输入的验证码),mobile(String类型,手机号)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足") })
//	@PostMapping(value="smsValidate")
//	public RespVo<?> smsValidate(@RequestBody UserAccountSendRequestVo voAccount){
//		RespVo<?> resultVo = new RespVo<>();	
//		try {
//			UserAccountVo vo = new UserAccountVo();
//			vo.setMobileNumber(voAccount.getMobile());
//			vo.setSms(voAccount.getSms());
//			userAccountClient.smsValidate(vo);
//		} catch (Exception e) {
//			logger.error("smsValidate", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//	
//	/**
//	 * 添加账户信息
//	 */
//	@ApiOperation(value = "添加账户信息",notes ="mobile(手机号,String类型),userName(用户名,String类型),loginPwd(密码,String类型),loginName(登录名,String类型)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足") })
//	@PostMapping(value="userAccountAdd")
//	public RespVo<?> userAccountAdd(@RequestBody UserAccountUpdateVo vo){
//		RespVo<?> resultVo = new RespVo<>();	
//		try {	
//			
//			String email=RedisUtil.get("registerEmail");
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱验证失败,验证错误!");
//				throw new IllegalArgumentException();
//			}
//			//短信验证
//			UserAccountVo vaildeVo = new UserAccountVo();
//			vaildeVo.setMobileNumber(vo.getMobile());
//			vaildeVo.setSms(vo.getMailActiveCode());
//			boolean jg=userAccountClient.smsValidate(vaildeVo);
//			if(!jg){
//				resultVo.setCode(1055);
//				resultVo.setMessage("手机验证码,输入错误!");
//				throw new IllegalArgumentException();
//			}
//			
//			BsUserAccount userAccount=new BsUserAccount();
//			userAccount.setMobile(vo.getMobile());
//			userAccount.setUserName(vo.getUserName());
//			userAccount.setPlainPassword(vo.getLoginPwd());
//			//userAccount.setLoginName(email);
//			userAccount.setEmail(email);
//			//userAccount.setRoleCd("N");
//			//List<BsCompany>  list=offCommonPriceApi.findOneBsCompany();
//			//userAccount.setCompanyUuid(list.get(0).getCompanyUuid());
//			userAccountClient.saveAccount(userAccount);
//		} catch (Exception e) {
//			logger.error("userAccountAdd", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;	
//	}
//	
//	/**
//	 * 重新发送邮件
//	 */
//	@ApiOperation(value="重新发送邮件")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足"),
//        @ApiResponse(code = 1052, message = "邮箱会话失效")})
//	@PostMapping(value = "resendEmail")
//	public RespVo<?> resendEmail(HttpServletRequest request,HttpServletResponse response){
//		RespVo<?> resultVo=new RespVo<>();
//		String email=RedisUtil.get("registerEmail");
//		try {
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱会话失效！");
//				throw new IllegalArgumentException();
//			} 
//			BsUserAccount user=userAccountClient.findByEmailLoginName(email);
//			userAccountClient.register(user);
//		} catch (Exception e) {
//			logger.error("userAccountAdd", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		
//		return resultVo;
//	}
//	
//	
//	
//	/**
//	 * 获取图片验证码
//	 */
//	@ApiOperation(value = "获取图片验证码")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足") })
//	@PostMapping(value="getPicValidate")
//	public RespVo<?> getPicValidate(HttpServletRequest req,HttpServletResponse resp) {
//		RespVo<?> resultVo = new RespVo<>();	
//		ServletOutputStream sos = null;
//		RandomNumUtil rdnu = RandomNumUtil.Instance();
//		HttpSession session = req.getSession();
//		session.setAttribute("random", rdnu.getString());
//		// 禁止图像缓存。
//		resp.setHeader("Pragma", "no-cache");
//		resp.setHeader("Cache-Control", "no-cache");
//		resp.setDateHeader("Expires", 0);
//		resp.setContentType("image/jpeg");
//		try {
//			// 将图像输出到Servlet输出流中。
//			sos = resp.getOutputStream();
//			ImageIO.write(rdnu.getImage(), "jpeg", sos);
//			sos.close();
//		} catch (Exception e) {
//			logger.error("getPicValidate", e);			
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//	
//}
