package com.spt.mds.open.offer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.admin.client.entity.AppDictData;
import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.util.DateOperator;
import com.hsoft.commutil.util.PoiExcelUtil;
import com.spt.hospital.client.api.IMdsBankMasterApi;
import com.spt.hospital.client.api.IMdsHospitalPriceApi;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductCategoryApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsUserFeedBackApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsBankMasterEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "基础信息管理", description = "基础信息管理")
@RestController
@RequestMapping(value = "/mdsBasicInfo")
public class MdsBasicInfoController 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	@Autowired
	private IMdsBankMasterApi mdsBankMasterApi;
	@Autowired
	private IMdsProductCategoryApi mdsProductCategoryApi;
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsUserFeedBackApi mdsUserFeedBackApi;
	
	@Autowired
	private IMdsHospitalPriceApi mdsHospitalPriceApi;
	
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	/**
	 * TODO 设备管理导出报表
	 * @throws IOException 
	 * */
	@ApiOperation(value = "设备管理导出报表")
	@GetMapping(value = "productExportReport")
	@ResponseBody
	public void productExportReport(QueryVo vo,HttpServletRequest request,HttpServletResponse response) throws IOException 
	{
		logger.info("this method productExportReport>>>>>>设备管理导出报表");
		if(vo.getMarking()!=null){
		if (vo.getMarking() == 1) {
			vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);//备选
		}
		if (vo.getMarking() == 2) {
			vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);//空闲
		}
		if (vo.getMarking() == 3) {
			vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);//暂停
		}
		if (vo.getMarking() == 4) {
			vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);//使用中
		}
		if (vo.getMarking() == 5) {
			vo.setProductStatus(HospConstants.PRODUCT_STATUS_5);//报修
		}
		if (vo.getMarking() == 6) {
			vo.setProductStatus(HospConstants.PRODUCT_STATUS_2);//异常
		}
		if (vo.getMarking() == 7) {
			vo.setProductStatus(HospConstants.PRODUCT_STATUS_3);//低电量 
		}
		if (vo.getMarking() == 8) {
			vo.setProductStatus(HospConstants.PRODUCT_STATUS_4);//余额不足
		}
		if (vo.getMarking() == 9) {
			vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);//维修中
		}
		}

		if (vo.getEndDate() != null) {
			vo.setEndDate(DateOperator.addDaysByDays(vo.getEndDate(), 1));
		}
		//查询页面导出的数据
		PageDown<ProductInfoByCategoryVo> productInfoVoPage = mdsProductInfoApi.findProductInfoListPage(vo);
		logger.info("this method productExportReport>>>>>>设备管理导出报表=========");
		int totalPages = productInfoVoPage.getTotalPages()+1;
		String title = "设备详情报表";
		String[] titles = new String[]{"二维码ID","设备ID","硬件版本号","所在医院","科室","病房号","租出时间","归还时间","租赁时长(小时)","使用状态","兼容状态","操作时间"};
		String[] attrs = new String[]{"productQrcode","productCode","productVersion","hospitalName","hospitalDepart","hospitalWard","leaseStartDateStr","leaseEndDateStr","leaseTime","productUseStatusValue","productStatusValue","updatedDateStr"};
		int [] widths =new int[] {15,15,20,15,15,15,15,15,15,15,15,15};
		Workbook workbook = PoiExcelUtil.newWorkbook(PoiExcelUtil.WB_TYPE_2007);
		// 生成一个表格
		Sheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为 15 个字节
		sheet.setDefaultColumnWidth(15);
		// 产生表格标题行
		// 生成一个样式
		CellStyle cellStyle = PoiExcelUtil.getCellStyle(workbook);
		/** 创建表头 */
		int[] widthes = new int[titles.length];
		for (int i = 0; i < titles.length; i++) 
		{
			widthes[i] = widths[i];
		}
		PoiExcelUtil.creatHeads(workbook, sheet, titles, widthes);
		int start =0;
		
		while (productInfoVoPage!=null && productInfoVoPage.getContent()!=null&& productInfoVoPage.getContent().size()>0) 
		{
			PoiExcelUtil.createRows(sheet, productInfoVoPage.getContent(), attrs, start, cellStyle,DateOperator.FORMAT_STR);
			productInfoVoPage.setTotalPages(totalPages);
			if (productInfoVoPage.hasNext()) 
			{
				vo.setPage(vo.getPage()+1);
				productInfoVoPage = mdsProductInfoApi.findProductInfoListPage(vo);
				start += vo.getRows();
			}else 
			{
				productInfoVoPage=null;
			}
		}
		try 
		{
			PoiExcelUtil.write(workbook, response, title);
		} catch (IOException e) 
		{
		  logger.error(e.getMessage(),e);
		}
	}

	/**
	 * TODO 设备管理导入码单
	 * */
	@ApiOperation(value = "设备管理导入码单")
	@PostMapping(value = "productImportCodeSheet")
	public RespVo<Map<String, Object>> productImportCodeSheet(@RequestBody MdsPropertyInfoVo vo) 
	{
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		
		return respVo;
	}
	/**
	 * TODO 门店-导出报表
	 * */
	@ApiOperation(value = "门店-导出报表")
	@GetMapping(value = "storeExportQuotation")
	@ResponseBody
	public void storeExportQuotation(QueryVo vo,HttpServletRequest request,HttpServletResponse response)throws IOException  
	{
		logger.info("this method storeExportQuotation>>>>>>门店导出报表");
//		if (vo.getEquipmentIds() !=null && !("".equals(vo.getEquipmentIds()))){
//			String [] paperId= vo.getEquipmentIds().split(",");
		
			List<ProductInfoByCategoryVo> ProductInfoList = new ArrayList<ProductInfoByCategoryVo>();
			List<MdsProductInfoEntity> ls = mdsProductInfoApi.findByHospitalId(vo);
			
			for (MdsProductInfoEntity productInfo:ls) {
				if (productInfo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_1)) {
					productInfo.setProductStatus("默认");
				}
				if (productInfo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_2)) {
					productInfo.setProductStatus("异常");
				}
				if (productInfo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_3)) {
					productInfo.setProductStatus("低电量");
				}
				if (productInfo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_1)) {
					productInfo.setProductStatus("余额不足");
				}
				if (productInfo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_5)) {
					productInfo.setProductStatus("报修");
				}
				
				if (productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_1)) {
					productInfo.setProductUseStatus("备选");
				}
				if (productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_2)) {
					productInfo.setProductUseStatus("空闲");
				}
				if (productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_3)) {
					productInfo.setProductUseStatus("暂停");
				}
				if (productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)) {
					productInfo.setProductUseStatus("使用中");
				}
				if (productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_5)) {
					productInfo.setProductUseStatus("维修中");
				}
				
				ProductInfoByCategoryVo pcvo= new ProductInfoByCategoryVo();
				BeanUtils.copyProperties(productInfo, pcvo);
				pcvo.setUpdatedDateStr(DateParser.formatDatesS1(productInfo.getUpdatedDate()));
				vo.setProductCode(productInfo.getProductCode());
				vo.setProductId(productInfo.getId());
			//	System.out.println("productInfo.getId()>>>>>>>>>>>>>=="+productInfo.getId());
				List<MdsLeaseInfoEntity> lsle =mdsLeaseInfoApi.findByCodeAndId(vo);
				if(lsle!=null && lsle.size()>0){
					MdsLeaseInfoEntity entity =lsle.get(0);
					pcvo.setLeaseEndDate(entity.getLeaseEndDate());
					pcvo.setLeaseStartDate(entity.getLeaseStartDate());
					if(entity.getLeaseStartDate()!=null){
						pcvo.setLeaseStartDateStr(DateParser.formatDatesS1(entity.getLeaseStartDate()));
					}
					
					
					if(entity.getLeaseEndDate()!=null){
					pcvo.setLeaseEndDateStr(DateParser.formatDatesS1(entity.getLeaseEndDate()));
					}
					if(entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)){
						pcvo.setLeaseTime(DateParser.getDateHour(entity.getLeaseEndDate(), entity.getLeaseStartDate()));
			        }else{
			        	pcvo.setLeaseTime(DateParser.getDateHour(new Date(), entity.getLeaseStartDate()));
			        }
				}
				
				
				ProductInfoList.add(pcvo);
			}
			String title = "门店设备报表";
			String[] titles = new String[]{"二维码ID","设备ID","绑定时间","租出时间","归还时间","租赁时长","使用状态","兼容状态"};
			String[] attrs = new String[]{"productQrcode","productCode","updatedDateStr","leaseStartDateStr","leaseEndDateStr","leaseTime","productUseStatus","productStatus"};
			int [] widths =new int[] {15,15,20,15,15,15,15,15};
			Workbook workbook = PoiExcelUtil.newWorkbook(PoiExcelUtil.WB_TYPE_2007);
			// 生成一个表格
			Sheet sheet = workbook.createSheet(title);
			// 设置表格默认列宽度为 15 个字节
			sheet.setDefaultColumnWidth(15);
			// 产生表格标题行
			// 生成一个样式
			CellStyle cellStyle = PoiExcelUtil.getCellStyle(workbook);
			/** 创建表头 */
			int[] widthes = new int[titles.length];
			for (int i = 0; i < titles.length; i++) 
			{
				widthes[i] = widths[i];
			}
			PoiExcelUtil.creatHeads(workbook, sheet, titles, widthes);
			int start =0;
			
			for (int i = 0; i < ProductInfoList.size(); i++) {
				PoiExcelUtil.createRows(sheet, ProductInfoList, attrs, start, cellStyle,DateOperator.FORMAT_STR);
			}
			
			try 
			{
				PoiExcelUtil.write(workbook, response, title);
			} catch (IOException e) 
			{
			  logger.error(e.getMessage(),e);
			}
		//}
}
	
	
	
	/**
	 * TODO 订单-导出报表
	 * @throws IOException 
	 * */
	@ApiOperation(value = "订单-导出报表")
	@GetMapping(value = "orderExportReport")
	@ResponseBody
	public void orderExportReport(QueryVo vo,HttpServletRequest request,HttpServletResponse response) throws IOException 
	{
		logger.info("this method orderExportReport>>>>>>订单-导出报表");
		if (vo.getEndDate() != null) {
			vo.setEndDate(DateOperator.addDaysByDays(vo.getEndDate(), 1));
		}
		//查询页面导出的数据
		PageDown<MdsLeaseInfoVo> productInfoVoPage = mdsLeaseInfoApi.findLeaseInfoPageByProperty(vo);

		int totalPages = productInfoVoPage.getTotalPages()+1;
		String title = "订单详情报表";
		String[] titles = new String[]{"订单编号","用户手机号","设备ID","租凭设备","租凭医院","平台收入","租出时间","归还时间","租赁时长(小时)","订单金额","费用占比","状态","报修信息"};
		String[] attrs = new String[]{"orderNumber","userPhone","productCode","categoryName","hospitalName","platFashion","leaseStartDateStr","leaseEndDateStr","leaseTime","leaseCost","costProportion","orderStatusValue","repairReason"};
		int [] widths =new int[] {15,15,20,15,15,15,15,15,15,15,15,15,15};
		Workbook workbook = PoiExcelUtil.newWorkbook(PoiExcelUtil.WB_TYPE_2007);
		// 生成一个表格
		Sheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为 15 个字节
		sheet.setDefaultColumnWidth(15);
		// 产生表格标题行
		// 生成一个样式
		CellStyle cellStyle = PoiExcelUtil.getCellStyle(workbook);
		/** 创建表头 */
		int[] widthes = new int[titles.length];
		for (int i = 0; i < titles.length; i++) 
		{
			widthes[i] = widths[i];
		}
		PoiExcelUtil.creatHeads(workbook, sheet, titles, widthes);
		int start =0;
		
		while (productInfoVoPage!=null && productInfoVoPage.getContent()!=null&& productInfoVoPage.getContent().size()>0) 
		{
			PoiExcelUtil.createRows(sheet, productInfoVoPage.getContent(), attrs, start, cellStyle,DateOperator.FORMAT_STR);
			productInfoVoPage.setTotalPages(totalPages);
			if (productInfoVoPage.hasNext()) 
			{
				vo.setPage(vo.getPage()+1);
				productInfoVoPage = mdsLeaseInfoApi.findLeaseInfoPageByProperty(vo);
				start += vo.getRows();
			}else 
			{
				productInfoVoPage=null;
			}
		}
		try 
		{
			PoiExcelUtil.write(workbook, response, title);
		} catch (IOException e) 
		{
		  logger.error(e.getMessage(),e);
		}
	}

	
	/**
	 * TODO 门店-盘点仓库
	 * */
	@ApiOperation(value = "门店-盘点仓库")
	@PostMapping(value = "storeInventoryWarehouse")
	public RespVo<Map<String, Object>> storeInventoryWarehouse(@RequestBody MdsPropertyInfoVo vo) 
	{
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		
		return respVo;
	}
	/**
	 *TODO 基础信息列表查询
	 */
	@ApiOperation(value = "基础信息管理  ： 医院列表   物业列表  代理商列表  银行列表 设备列表  返佣类型列表  下拉列表")
	@PostMapping(value = "getMdsBasicInfoList")
	public RespVo<Map<String, Object>> getMdsBasicInfoList(@RequestBody MdsPropertyInfoVo vo) 
	{// 类型 :医院H 物业 P 代理商 A 平台M
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();
		// 医院
		vo.setHospitalType(HospConstants.PROPERTY_TYPE_H);
		List<MdsPropertyInfoEntity> hospitalList = mdsPropertyInfoApi.findBytype(vo);
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		for(MdsPropertyInfoEntity entity:hospitalList){
			Map<String,Object> entiMap = new HashMap<String,Object>();
			entiMap.put("id", entity.getId());
			entiMap.put("name", entity.getHospitalName());
			List<Map<String,String>> mList = new ArrayList<Map<String,String>>();
			//查询科室
			// 科室
			String department = entity.getHospitalDepart();
			if(StringUtils.isNotBlank(department)){
				String[] departStr = department.split(",");
				
				for(String str:departStr){
					Map<String,String> m = new HashMap<String,String>();
					m.put("name", str);
					mList.add(m);
				}
			}
			entiMap.put("departList",mList );
			list.add(entiMap);
		}
		map.put("hospitalList", list);
		
		//状态
		List<String> statusList = new ArrayList<String>();
		statusList.add("备选");
		statusList.add("空闲");
		statusList.add("暂停");
		statusList.add("使用中");
		statusList.add("报修");
		statusList.add("异常");
		statusList.add("低电量");
		statusList.add("余额不足");
		statusList.add("维修中");
		map.put("statusList", statusList);
		// 物业
		vo.setHospitalType(HospConstants.PROPERTY_TYPE_P);
		List<MdsPropertyInfoEntity> propertyList = mdsPropertyInfoApi.findBytype(vo);
		map.put("propertyList", propertyList);
		// 代理商
		vo.setHospitalType(HospConstants.PROPERTY_TYPE_A);
		List<MdsPropertyInfoEntity> agentList = mdsPropertyInfoApi.findBytype(vo);
		map.put("agentList", agentList);
		// 银行列表
		List<MdsBankMasterEntity> bankList =  mdsBankMasterApi.findAll();
		map.put("bankList", bankList);
		// 类别列表
		List<MdsProductCategoryEntity> categoryList =  mdsProductCategoryApi.findByCategoryinfo();
		map.put("categoryList", categoryList);
		//返佣类型
		List<AppDictData> distributionTypeList = DictUtil.getListByCategory(HospConstants.DISTRIBUTION_TYPE);
		map.put("distributionTypeList", distributionTypeList);
		
		respVo.setData(map);
		return respVo;
		}
	
	
	
	/***
	 * TODO 用户反馈
	 */
	@ApiOperation(value = "用户反馈")
	@PostMapping(value = "userFeedback")
	public RespVo<Map<String, Object>> userFeedback(@RequestBody QueryVo queryVo) {
		logger.info("用户反馈 >>>>>>>>>>>>>>>>>>>>userFeedback");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		if (queryVo.getToken() != null) {

			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				queryVo.setOpenid(user.getOpenid());
				List<MdsUserFeedBackEntity> oldfeedBackls = mdsUserFeedBackApi.findByOpenidOrdernum(queryVo);
				if(oldfeedBackls!=null && oldfeedBackls.size()>0){
					
					respVo.setCode(201);
					respVo.setMessage("该订单已提交");
					return respVo;
					
				}else{
					
					if(StringUtils.isNotBlank(queryVo.getFeedbackContent())){
							MdsUserFeedBackEntity feedBack = new MdsUserFeedBackEntity();
							feedBack.setOrderNumber(queryVo.getOrderNumber());
							feedBack.setFeedbackContent(queryVo.getFeedbackContent());
							feedBack.setOpenid(user.getOpenid());
							feedBack.setMobilePhone(user.getUserPhone());
							mdsUserFeedBackApi.save(feedBack);
						}else{
							respVo.setCode(201);
							respVo.setMessage("没有反馈内容");
							return respVo;
						}
						
						return respVo;
					
				}


			} else {
				respVo.setCode(201);
				respVo.setMessage("用户信息不存在");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}

	}
	
}
