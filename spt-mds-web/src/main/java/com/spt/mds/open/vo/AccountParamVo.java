package com.spt.mds.open.vo;

public class AccountParamVo {

	private Long accountId;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}	
}
