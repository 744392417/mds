package com.spt.mds.open.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "返回实体")
public class ResultEntity {

	@ApiModelProperty(value = "返回的参数1")
	private String param1;
	@ApiModelProperty(value = "返回的参数2")
	private String param2;
	public String getParam1() {
		return param1;
	}
	public void setParam1(String param1) {
		this.param1 = param1;
	}
	public String getParam2() {
		return param2;
	}
	public void setParam2(String param2) {
		this.param2 = param2;
	}
}
