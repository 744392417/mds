package com.spt.mds.open.offer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(value = "系统配置", description = "系统配置")
@RestController
@RequestMapping(value = "/open/companyConfig")
public class BsCompanyConfigOpenController {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(getClass());
	

}
