package com.spt.mds.open.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户密码操作 vo
 * @author Administrator
 *
 */
@ApiModel(value = "根据邮箱找回密码vo")
public class UserPwdParamVo {

	@ApiModelProperty(value = "邮箱地址",example="13477086901@163.com")
	private String email;
	@ApiModelProperty(value = "手机号",example="13477086901")
	private String mobile;
	@ApiModelProperty(value = "手机短信",example="99568")
	private String smsMsg;			//手机短息

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSmsMsg() {
		return smsMsg;
	}

	public void setSmsMsg(String smsMsg) {
		this.smsMsg = smsMsg;
	}

	
	
	
	
	
}
