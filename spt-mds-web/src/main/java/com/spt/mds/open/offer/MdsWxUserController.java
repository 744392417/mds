package com.spt.mds.open.offer;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsoft.commutil.shiro.ShiroUser;
import com.hsoft.commutil.util.JsonUtil;
import com.spt.hospital.client.util.EmojiUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayUtil;
import com.hsoft.commutil.encrypt.TokenUtil;
import com.hsoft.commutil.redis.RedisUtil;
import com.hsoft.file.client.remote.FileRemote;
import com.spt.hospital.client.api.IMdsHospitalPriceApi;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductCategoryApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsProductPwdApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsRecordsConptionApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.MdsProductPwdVo;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.SendTemplateMessage;
import com.spt.hospital.client.vo.TemplateData;
import com.spt.mds.open.vo.PaySignInfo;
import com.spt.mds.open.vo.RespVo;
import com.spt.mds.web.util.Httprequests;
import com.spt.mds.web.util.MdsWXConfigUtil;
import com.spt.mds.web.util.PayUtil;
import com.spt.mds.web.util.StringUtils;
import com.spt.mds.web.util.WechatGetUserInfoUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;


@Api(value = "微信用户", description = "微信用户")
@RestController
@RequestMapping(value = "/mdsWxUser")
public class MdsWxUserController 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	@Autowired
	private IMdsProductCategoryApi mdsProductCategoryApi;
	
	@Autowired
	private IMdsProductPwdApi mdsProductPwdApi;
	@SuppressWarnings("unused")
	@Autowired
	private IMdsHospitalPriceApi mdsHospitalPriceApi;
	@Autowired
	private IMdsRecordsConptionApi mdsRecordsConptionApi;
	@SuppressWarnings("unused")
	@Autowired
	private FileRemote fileRemote;
	private MdsWXConfigUtil config;
	private WXPay wxpay;
	
	//String SEND_TEMPLATE_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=";
	
	@Value("${wx.login.url}")
	String wxloginurl;
	@Value("${wx.appid}")
	String appId;
	@Value("${wx.secret}")
	String secret;
	@Value("${wx.granttype}")
	String grantType;
	@Value("${wx.mchid}")
	String mchId;
	@Value("${wx.key}")
	String key;
	@Value("${wx.unifiedorder.url}")
	String unifiedOrder;
	@Value("${wx.tradetype}")
	String tradeType;
	@Value("${wx.notifyurl}")
	String notifyUrl;
	@Value("${wx.spbillcreateip}")
	String spbillCreateIp;
	@Value("${wx.signtype}")
	String signType;
	@Value("${wx.page.url}")
	String wxpage;
	@Value("${wx.tempamessage.url}")
	String templateMessageUrl;
	@Value("${wx.orderend.template}")
	String orderendTemplateId;
	
	
	@Value("${fileurl}")
	String fileurl;
	
	public  static final BigDecimal yb = new BigDecimal(100.00);
	
	public MdsWxUserController() 
	{
		try 
		{
			//初始化微信支付客户端
			config = new MdsWXConfigUtil();
			wxpay = new WXPay(config);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/***
	 * TODO 用户资金明细查询
	 */
	@ApiOperation(value = "用户资金明细查询")
	@PostMapping(value = "findRecordsPageByUser")
	public RespVo<Map<String, Object>> findRecordsPageByUser(@RequestBody QueryVo queryVo) 
	{
		logger.info("this controller method : findRecordsPageByUser >>>>>>>用户资金明细查询>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();
		Page<MdsRecordsConptionVo> page = null;

		if(null != queryVo.getToken())
		{
			MdsUserInfoEntity userInfoEntity = mdsUserInfoApi.findByToken(queryVo.getToken());
			//根据用户信息查询用户的资金记录
			if(StringUtils.isNotBlank(userInfoEntity.getOpenid()))
			{
				queryVo.setOpenid(userInfoEntity.getOpenid());
				page = mdsRecordsConptionApi.findRecordsPageByUser(queryVo);
				map.put("page", page);
				respVo.setData(map);
			}else
			{
				respVo.setCode(400);
				respVo.setMessage("The OpenId Is Null !");
			}
		}else
		{
			respVo.setCode(400);
			respVo.setMessage("The AccessToken Is Null !");
		}
		return respVo;
	}


	/**
	 * TODO 获取用户租赁信息 
	 * */
	@ApiOperation(value = "获取用户租赁信息")
	@PostMapping(value = "findLeaseInfoPageByOpenid")
	public RespVo<Map<String, Object>> findLeaseInfoPageByOpenid(@RequestBody QueryVo queryVo) 
	{
		logger.info("this method : findLeaseInfoPageByOpenid >>>>>>>>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		// 通过token去捞openid stayABug
//		if(StringUtils.isNotBlank(queryVo.getToken()))
//		{
			//String openid =  "oMFbm5aK-Ddb3dNfnI4DvQ5Uva0s";
			MdsUserInfoEntity userInfoEntity = mdsUserInfoApi.findByToken(queryVo.getToken());
			//String openid =userInfoEntity.getOpenid();
			queryVo.setOpenid(userInfoEntity.getOpenid());
//			if(!"".equals(queryVo.getOrderStatus())){
//				queryVo.getOrderStatus().split(",");
//			}
			Page<MdsLeaseInfoVo> page = mdsLeaseInfoApi.findByOpenid(queryVo);
			if(page!=null && page.getContent().size()>0){
				RedisUtil.set(userInfoEntity.getOpenid(), "Y");
			}
			Map<String, Object> map = new HashMap<>();
			map.put("page", page);
			respVo.setData(map);
	//	}
		return respVo;
	}
	/**
	 * TODO 微信登录
	 * @throws Exception 
	 * */
	@ApiOperation(value = "微信登录")
	@PostMapping(value = "weiXinLogin")
	public RespVo<Map<String, Object>> wxLogin(@RequestBody QueryVo vo) throws Exception {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> retrunMap = new HashMap<>();
		//	MdsUserInfoEntity entity = new MdsUserInfoEntity();


		if (vo.getCode() != null) {

			logger.info(">>>>>jsCode==" + vo.getCode());

			// 微信的接口
			String url = wxloginurl + "?appid=" + appId + "&secret=" + secret + "&js_code=" + vo.getCode()
					+ "&grant_type=" + grantType;
			logger.info(">>>>>url==" + url);
			RestTemplate restTemplate = new RestTemplate();
			// 进行网络请求,访问url接口
			ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
			// 根据返回值进行后续操作
			if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
				String sessionData = responseEntity.getBody();
				JSONObject json = JSONObject.fromObject(sessionData);

				logger.info("这里是openid和session_key" + json);
				String sessionKey = (String) json.get("session_key");
				String openid = (String) json.get("openid");
				String unionid = (String) json.get("unionid");
				// Gson gson = new Gson();
				// //解析从微信服务器获得的openid和session_key;
				// MdsUserInfoVo weChatSession =
				// gson.fromJson(sessionData,MdsUserInfoVo.class);
				// //获取用户的唯一标识
				// String openid = weChatSession.getOpenid();
				// //获取会话秘钥
				// String session_key = weChatSession.getSession_key();
				// 下面就可以写自己的业务代码了
				// 最后要返回一个自定义的登录态,用来做后续数据传输的验证
				logger.info("==openid===" + openid);
				logger.info("==sessionKey===" + sessionKey);
				logger.info("==unionid===" + unionid);
				// String encryptedData =
				// "HjJGWuAIywp9QO3iX5v73jd7uhjXPHvGwExjPzCMlm/Powh2tr80U3pWBUGmEIicALaYpMu2i0qdUX1YvOEG/8CY4pfT9g624zDYnXrw7Z5zf/njQ8+OGFAFKQOQwQ4o5kzEUAmoL3S8mYZUrHf9mCH8ZKcxR1FzkWNyUN3srKnItAPLKyOIj37UyBfBpbdlTmLevKUtmdIfgO1i2M3/aHN0IRKNxc3ukHaXdm+foCAGizyY9vjXHz6xF0BnwNBCKyORRA+Qhapzi3oB7ujHrTG/NS3spoDSd86ueJuYjZi7mNVVVCl40jVK0UmUTedpAiifQfYJqyLfK9zVka4KUd9/c3h4NnE87eBXo3thB0jRldUwQIhH5fX/rQpav0aA1bJKaVNnokqhXZPnxQUcF/xZndnH4QsIGzfh8/FelDVEFax4BST2hs3o7xF90RMF/pWMHQaDwybFpyfCJJx5jMG0dLZsk2NGLpXWtG8ii8M=";
				// String iv = "iIEmLEUPwPdN+670qyb7Ew==";

				if (StringUtils.isNotBlank(openid) && StringUtils.isNotBlank(sessionKey)) {
					MdsUserInfoEntity wxentity = mdsUserInfoApi.findByOpenid(openid);
					if (wxentity == null) {
						wxentity = new MdsUserInfoEntity();
					}
					wxentity.setOpenid(openid);
					wxentity.setRemark(sessionKey);
					if (StringUtils.isNotBlank(vo.getEncryptedData()) && StringUtils.isNotBlank(vo.getIv())) {
						// 解密获取用户信息
						com.alibaba.fastjson.JSONObject userInfoJSON = WechatGetUserInfoUtil.getUserInfo(vo.getEncryptedData(), sessionKey, vo.getIv());
						logger.info("==userInfoJSON===" + userInfoJSON);
						if (userInfoJSON != null) {
							wxentity.setNickName(EmojiUtil.filterEmoji((String) userInfoJSON.get("nickName")));
							Integer gender = (Integer) userInfoJSON.get("gender");
							wxentity.setGender(String.valueOf(gender));
							wxentity.setCity((String) userInfoJSON.get("city"));
							wxentity.setProvince((String) userInfoJSON.get("province"));
							wxentity.setCountry((String) userInfoJSON.get("country"));
							wxentity.setAvatarUrl((String) userInfoJSON.get("avatarUrl"));
						}
					}

					ShiroUser shiroUser = new ShiroUser();
					shiroUser.addProp("wxOpenId", openid);

					try {
						Map<String, Object> map = new HashMap<>();
						map.put("userInfo", JsonUtil.obj2Json(shiroUser.getProp()));
						String wxlogintoken = TokenUtil.createToken(HospConstants.SUBJECT_USER, map, HospConstants.SECRETKEY);
						wxentity.setToken(wxlogintoken);
						retrunMap.put(HospConstants.GATE_ACCESSTOKEN, wxlogintoken);
						wxentity.setUserType(HospConstants.USER_TYPE_U);
						Long expirationTime= System.currentTimeMillis()+ 1000 * 60 * 60 * 24l;
						retrunMap.put("expirationTime", expirationTime);
						wxentity.setExpirationTime(String.valueOf(expirationTime));
						logger.info("=wxentity===" + JsonUtil.obj2Json(wxentity));
						mdsUserInfoApi.save(wxentity);
					} catch (Exception e) {
						e.printStackTrace();
					}
//				} else {
//						logger.info("解密失败");
//						respVo.setCode(201);
//						respVo.setMessage("解密失败");
//					}
				} else {
					logger.info("未获取到用户openid 或 session_key");
					respVo.setCode(202);
					respVo.setMessage("未获取到用户openid 或 session_key");
				}
			} else {

				respVo.setCode(203);
				respVo.setMessage("微信返回错误" + responseEntity.getStatusCode());
			}
		} else {
			respVo.setCode(205);
			respVo.setMessage("未获取到用户 code");

		}
		logger.info(">>>>>weiXinLogin>>微信登录----retrunMap------------->>==" + JsonUtil.obj2Json(retrunMap));
		respVo.setData(retrunMap);
		return respVo;
	}
	/***
	 * TODO 发起微信支付
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ApiOperation(value = "发起微信支付")
	@PostMapping(value = "wxPayment")
	public RespVo<Map<String, Object>> wxPayment(@RequestBody QueryVo queryVo) 
	{
		logger.info("wxPayment>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//MdsProductInfoVo productInfoVo = mdsPropertyInfoApi.findByQrcode(queryVo);
		//stayABug
		MdsUserInfoEntity userInfoEntity = mdsUserInfoApi.findByToken(queryVo.getToken());
		logger.info("wxPayment>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+(userInfoEntity!=null));
		if(userInfoEntity!=null){

		String openid =userInfoEntity.getOpenid();
		logger.info("wxPayment>>>>>>>>>>>>>>openid>>>>>>>>>>>>>>>"+openid);
		//String openid =  "oMFbm5aK-Ddb3dNfnI4DvQ5Uva0s";// 通过token去捞openid
		JSONObject JsonObject = new JSONObject();
		try 
		{
			logger.info("wxPayment>>>>>>>>>>>>>>openid>>>productInfo>>>>>>>>>>>");
			MdsProductInfoEntity productInfo  = mdsProductInfoApi.findByQrcode(queryVo);
			if(!productInfo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)){
				
			
			MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoApi.getEntity(queryVo.getHospitalId());
			
			queryVo.setCategoryId(productInfo.getCategoryId());
			MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceApi.findPrice(queryVo);

			MdsProductCategoryEntity productCategory = mdsProductCategoryApi.getEntity(productInfo.getCategoryId());
			
			BigDecimal leaseDeposit = BigDecimal.ZERO;
			leaseDeposit = hospitalPrice.getLeaseDeposit().multiply(BigDecimal.valueOf(100));
			logger.info("模拟微信支付>>>>>>>>>>>>>leaseDeposit>>>>>>>>"+leaseDeposit);
			// 生成的随机字符串
			String nonce_str = StringUtils.getRandomStringByLength(32);
			logger.info("wxPayment>>>>>>>>>>>>>nonce_str>>>>>>>>");
			// 商品名称
			String body = productCategory.getCategoryName();
			// 获取本机的ip地址
			// String spbill_create_ip = IpUtils.getIpAddr("");
			String spbill_create_ip = "";
			String uuid = UUID.randomUUID().toString().replace("-", "");
				String orderNo =uuid;
				//String orderNo ="H"+DateParser.formatDateSS(new Date());
				logger.info("wxPayment>>>>>>>>>>>>>spbill_create_ip>>>>>>>>");
			// "H"+DateParser.formatDateSS(new Date());
//			String money = "2";// 支付金额，单位：分，这边需要转成字符串类型，否则后面的签名会失败
//			String totalFee = "2";
				
				
                String money =  String.valueOf(leaseDeposit.intValue());
                String totalFee = String.valueOf(leaseDeposit.intValue());
            	logger.info("wxPayment>>>>>>>>money>>>>>>>"+money);
            	logger.info("wxPayment>>>>>>>>totalFee>>>>>>>"+totalFee);
			Map<String, String> packageParams = new HashMap<String, String>();
			packageParams.put("appid", appId);//小程序ID
			packageParams.put("mch_id", mchId);//商户号
			packageParams.put("nonce_str", nonce_str);//随机字符串
			packageParams.put("body", body);//商品描述
			packageParams.put("out_trade_no", orderNo);// 商户订单号
			packageParams.put("total_fee",money);// 支付金额，这边需要转成字符串类型，否则后面的签名会失败
			//packageParams.put("spbill_create_ip", spbill_create_ip);
			packageParams.put("notify_url", notifyUrl);//微信给发的回调地址
			packageParams.put("trade_type", tradeType);
			packageParams.put("total_fee",totalFee);//总金额
			packageParams.put("openid",openid);//微信用户ID
			String json=JSON.toJSONString(packageParams); 
			logger.info(json);//
			// 除去数组中的空值和签名参数
			packageParams = PayUtil.paraFilter(packageParams);
			String prestr = PayUtil.createLinkString(packageParams); // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
			logger.info(prestr+key);//
			// MD5运算生成签名，这里是第一次签名，用于调用统一下单接口
			String mysign = PayUtil.sign(prestr, key, "utf-8").toUpperCase();
			
			logger.info("=======================第一次签名：" + mysign + "=====================");

			// 拼接统一下单接口使用的xml数据，要将上一步生成的签名一起拼接进去
			String xml = "<xml>" + "<appid>" + appId + "</appid>" + "<body><![CDATA[" + body + "]]></body>" + "<mch_id>"
					+ mchId + "</mch_id>" + "<nonce_str>" + nonce_str + "</nonce_str>" + "<notify_url>" + notifyUrl
					+ "</notify_url>" + "<openid>"+openid+"</openid>" + "<out_trade_no>" + orderNo
					+ "</out_trade_no>" + "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>"
					+ "<total_fee>" + money + "</total_fee>" + "<trade_type>" + tradeType + "</trade_type>" + "<sign>"
					+ mysign + "</sign>" + "</xml>";

			logger.info("调试模式_统一下单接口 请求XML数据：" + xml);

			// 调用统一下单接口，并接受返回的结果
			String result = PayUtil.httpRequest(unifiedOrder, "POST", xml);

			logger.info("调试模式_统一下单接口 返回XML数据：" + result);

			// 将解析结果存储在HashMap中
			Map map = PayUtil.doXMLParse(result);

			String return_code = (String) map.get("return_code");// 返回状态码

			String return_msg = (String) map.get("return_msg");//返回信息
			logger.info("=======================第一次返回 return_code ：" + return_code + "==========return_msg==========="+return_msg);
			// 返回给移动端需要的参数
			Map<String, Object> retrunMap = new HashMap<String, Object>();
			
			if (return_code == "SUCCESS" || return_code.equals(return_code)) 
			{
				PaySignInfo wxpaySign=new PaySignInfo();
				retrunMap.put("appid", appId);
				wxpaySign.setAppid(appId);
				retrunMap.put("nonceStr", nonce_str);
				wxpaySign.setNonceStr(nonce_str);
				// 业务结果
				String prepay_id = (String) map.get("prepay_id");// 返回的预付单信息
				retrunMap.put("package", "prepay_id=" + prepay_id);
				wxpaySign.setPackages("prepay_id=" + prepay_id);
				Long timeStamp = System.currentTimeMillis() / 1000;
				retrunMap.put("timeStamp", timeStamp + "");// 这边要将返回的时间戳转化成字符串，不然小程序端调用wx.requestPayment方法会报签名错误
				wxpaySign.setTimeStamp(timeStamp + "");
	
				String stringSignTemp = "appId=" + appId + "&nonceStr=" + nonce_str + "&package=prepay_id=" + prepay_id
						+ "&signType=" + signType + "&timeStamp=" + timeStamp;
				// 再次签名，这个签名用于小程序端调用wx.requesetPayment方法
				String paySign = PayUtil.sign(stringSignTemp, key, "utf-8").toUpperCase();
				logger.info("=======================第二次签名：" + paySign + "=====================");
				wxpaySign.setPaySign(paySign);
				JSONObject wxpaySignJsons = JSONObject.fromObject(wxpaySign);
				logger.info("==wxpaySignJsons="+wxpaySignJsons);
				logger.info("=================redis==satr======");
				RedisUtil.setex("pay_pre"+orderNo,HospConstants.ORDER_EXPIRE_SECONDS,wxpaySignJsons.toString());
				logger.info("=================redis==end======");
				retrunMap.put("paySign", paySign);
				// 更新订单信息
				logger.info("==pwd===START=====");
				MdsProductPwdVo productPwdVo = new MdsProductPwdVo();
				productPwdVo.setOpenid(openid);
				productPwdVo.setProductCode(productInfo.getProductCode());
				productPwdVo.setProductQrcode(productInfo.getProductQrcode());
				MdsProductPwdEntity productPwdEntity = mdsProductPwdApi.getPushKey(productPwdVo);
				logger.info("===================pwd=======END====");
				MdsLeaseInfoVo mdsLeaseInfoVo = new MdsLeaseInfoVo();
	
				
//				BigDecimal leaseDeposit = BigDecimal.ZERO;
//				BigDecimal moneyB = new BigDecimal(money) ;
//				leaseDeposit = moneyB.divide(BigDecimal.valueOf(100),2,RoundingMode.UP);
				
				logger.info("===================prepay_id==========="+prepay_id);
				mdsLeaseInfoVo.setWxIncomeNumber(prepay_id);//WX预付款ID
				mdsLeaseInfoVo.setUserPhone(userInfoEntity.getUserPhone());
				logger.info("===================prepay_id===========");
				mdsLeaseInfoVo.setOpenid(openid);
				logger.info("===================openid===========");
				mdsLeaseInfoVo.setOrderNumber(orderNo);//订单
			//	logger.info("===================orderNo==========="+hospitalPrice.getLeaseDeposit());
			//	mdsLeaseInfoVo.setLeaseDeposit(hospitalPrice.getLeaseDeposit());//预付款
				logger.info("===================leaseDeposit===========");
				mdsLeaseInfoVo.setLeaseStartDate(new Date());//开始时间
				logger.info("===================setLeaseStartDate===========");
				mdsLeaseInfoVo.setHospitalId(queryVo.getHospitalId());//医院id
				logger.info("===================getHospitalId===========");
				mdsLeaseInfoVo.setHospitalName(productInfo.getHospitalName());//医院名称
				logger.info("===================getHospitalName===========");
				mdsLeaseInfoVo.setHospitalDepart(productInfo.getHospitalDepart());//科室
				logger.info("===================getHospitalDepart===========");
				logger.info("===================getHospitalWard=======getHospitalWard===="+productInfo.getHospitalWard());
			    if(productInfo.getHospitalWard()!=null && !"".equals(productInfo.getHospitalWard())){
			    	mdsLeaseInfoVo.setHospitalWard(productInfo.getHospitalWard());//房号
			    }			
				
				if(propertyInfo.getHospitalDistribution()!=null && !"".equals(propertyInfo.getHospitalDistribution())){
					mdsLeaseInfoVo.setHospitalDistribution(propertyInfo.getHospitalDistribution());
				}else{
					mdsLeaseInfoVo.setHospitalDistribution(0);
				}
				
				mdsLeaseInfoVo.setCategoryId(productCategory.getId());//
				logger.info("===================getId===========");
				mdsLeaseInfoVo.setCategoryName(productCategory.getCategoryName());//
				logger.info("===================getCategoryName===========");
				mdsLeaseInfoVo.setProductId(productInfo.getId());//
				logger.info("===================productInfo.getId()===========");
				mdsLeaseInfoVo.setProductCode(productInfo.getProductCode());//
				logger.info("===================pproductInfo.getProductCode()==========");
				mdsLeaseInfoVo.setProductQrcode(productInfo.getProductQrcode());//
				logger.info("===================pproductInfo.getProductQrcode()==========");
				//mdsLeaseInfoVo.setProductName(mdsLeaseInfoVo.getProductName());
				mdsLeaseInfoVo.setOrderStatus(HospConstants.ORDER_STATUS_2);//代付款
				//mdsLeaseInfoVo.getProductStatus(HospConstants.PRODUCT_STATUS)
				mdsLeaseInfoVo.setProductNumber(1);//
				logger.info("===================pproductInfo.pwd.getUnlockPwd()()==========");
				mdsLeaseInfoVo.setUnlockPwd(productPwdEntity.getUnlockPwd());
				logger.info("===================pwd.getShutPwd()========");
				mdsLeaseInfoVo.setShutPwd(productPwdEntity.getShutPwd());
				mdsLeaseInfoVo.setKeyId(productPwdEntity.getKeyId());
				logger.info("==================saveMdsLeaseInfo===444====");
				//流水
				String platIncomeNumber = DateParser.formatDateSS(new Date());
				logger.info("===============platIncomeNumber===");
				mdsLeaseInfoVo.setPlatIncomeNumber(platIncomeNumber);//预付款流水
				logger.info("==================saveMdsLeaseInfo===444====");
				mdsLeaseInfoVo.setPlatIncomeDate(new Date());//平台发起时间
//				logger.info("==================setWxIncomeNumber===444====");
//				mdsLeaseInfoVo.setWxIncomeNumber("wxIncomeNumber");//微信返回流水
//				logger.info("==================setWxIncomeDate===444====");
				mdsLeaseInfoVo.setWxIncomeDate(new Date());//WX发起时间
				logger.info("==================setWxIncomeDate===444====");
				mdsLeaseInfoVo.setIncomeSatus(HospConstants.RESULT_SUCCESS);//预付款状态
				
				logger.info("==================saveMdsLeaseInfo=getAgentId==");
				if(propertyInfo.getAgentId()!=null && StringUtils.isNotBlank(propertyInfo.getAgentName())){
					mdsLeaseInfoVo.setAgentId(String.valueOf(propertyInfo.getAgentId()));
					mdsLeaseInfoVo.setAgentName(propertyInfo.getAgentName());
				}
				
				if(propertyInfo.getAgentDistribution()!=null && !"".equals(propertyInfo.getAgentDistribution())){
					mdsLeaseInfoVo.setAgentDistribution(propertyInfo.getAgentDistribution());
				}else{
					mdsLeaseInfoVo.setAgentDistribution(0);
				}
				logger.info("=================getProperId=====");
				
	          if(propertyInfo.getProperId()!=null && StringUtils.isNotBlank(propertyInfo.getProperName())){
	        		mdsLeaseInfoVo.setProperId(String.valueOf(propertyInfo.getProperId()));
					mdsLeaseInfoVo.setProperName(propertyInfo.getProperName());
				}
	          
				if(propertyInfo.getProperDistribution()!=null && !"".equals(propertyInfo.getProperDistribution())){
					mdsLeaseInfoVo.setProperDistribution(propertyInfo.getProperDistribution());
				}else{
					mdsLeaseInfoVo.setProperDistribution(0);
				}
			      logger.info("==================saveMdsLeaseInfo====platDistribution=s==");
				Integer platDistribution = 100-mdsLeaseInfoVo.getHospitalDistribution()-mdsLeaseInfoVo.getProperDistribution()-mdsLeaseInfoVo.getAgentDistribution();
			      logger.info("==================saveMdsLeaseInfo====platDistribution=e=="+platDistribution);
				mdsLeaseInfoVo.setPlatDistribution(platDistribution);
		      logger.info("==================saveMdsLeaseInfo====RESULT_SUCCESS=getHospitalWard=="+mdsLeaseInfoVo.getHospitalWard());
				//mdsLeaseInfoVo.setOrderStatus(HospConstants.ORDER_STATUS_0);
				mdsLeaseInfoApi.saveMdsLeaseInfo(mdsLeaseInfoVo);//生成订单
				logger.info("==================saveMdsLeaseInfo=======");
	
//				MdsProductPwdVo vo = new MdsProductPwdVo();
//				BeanUtils.copyProperties(pwd, vo);
//				pwd.setPwdStatus(HospConstants.PWD_STATUS_1);// 使用中
//				mdsProductPwdApi.updatePwdStatus(vo);
               logger.info("==================getProductQrcode======="+productInfo.getProductQrcode());
				productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
				logger.info("==================setProductUseStatus======="+productInfo.getProductUseStatus());
				mdsProductInfoApi.save(productInfo);
				}
			
			logger.info("==================orderNo======="+orderNo);
				
				retrunMap.put("orderNumber", orderNo);
		
			respVo.setData(retrunMap);
			}else{
				respVo.setCode(201);
				respVo.setMessage("该设备使用中");
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
			respVo.setCode(201);
			respVo.setMessage("发起失败");
		}

	}else{
		respVo.setCode(300);
		respVo.setMessage("token不正确");
		return respVo;	
	}
		logger.info("==================wxPayment=====end==");
		return respVo;
	}
	/**
	 * TODO 微信退款接口
	 * @param out_trade_no		订单号
	 * @throws Exception
	 */
	@ApiOperation(value = "微信退款接口")
	@PostMapping(value = "refund")
	public RespVo<Map<String, Object>>  refund(@RequestBody QueryVo queryVo) throws Exception {

		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> retrunMap = new HashMap<String, Object>();
		MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
		// 设置请求参数
		HashMap<String, String> data = new HashMap<String, String>(50);
		data.put("out_trade_no", mdsLeaseInfo.getOrderNumber());//
		String outRefundNo = "R" + DateParser.formatDateSS(new Date());
		data.put("out_refund_no", outRefundNo);
		//data.put("transaction_id", mdsLeaseInfo.getWxIncomeNumber());
		BigDecimal totalFee = mdsLeaseInfo.getLeaseDeposit().multiply(yb);//微信分计算
		logger.info(" 预付款>>>>>>==="+totalFee);
		///////////data.put("total_fee", String.valueOf(2));// 订单预付款
		data.put("total_fee", String.valueOf(totalFee.intValue()));
		logger.info(" 预付款>>>>>total_fee>==="+data.get("total_fee"));
		
		BigDecimal refundFee = new BigDecimal(0.00);
		if (queryVo.getType() != null && queryVo.getType().equals(HospConstants.PROPERTY_TYPE_M)) {
			// we退款
			refundFee = queryVo.getIntoWBalance().multiply(yb);
			logger.info(" we退款>>>>>>==="+refundFee);
		} else {
			// 用户归还
			refundFee = mdsLeaseInfo.getIntoBalance().multiply(yb);
			logger.info(" 小程序退款>>>>>>==="+refundFee);
		}
		
		
		logger.info("支付前参数==退款金额=refundFee="+String.valueOf(refundFee.intValue()));
		/////////data.put("refund_fee", String.valueOf(2));// 退款
		data.put("refund_fee", String.valueOf(refundFee.intValue()));
		logger.info("支付前参数===退款金额=refundFee===="+data.get("refund_fee"));
		data.put("refund_fee_type", "CNY");
		logger.info("支付前参数==333=="+config.getMchID());
		data.put("op_user_id", config.getMchID());
		logger.info("支付前参数==444");
		String json = JSON.toJSONString(data);
		logger.info("支付前参数==json"+json);
		try {
			// 调用sdk发起退款
			Map<String, String> result = wxpay.refund(data);
			logger.info("result_code===" + result.get("result_code"));
			String results = JSON.toJSONString(result);
			logger.info(results);
			if ("SUCCESS".equals(result.get("result_code"))) {
				// 更新订单
				//BigDecimal refundFeeS = new BigDecimal(2.00);
				queryVo.setOrderNumber(result.get("out_trade_no"));
				MdsLeaseInfoEntity mdsLeaseInfoEntity  = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
				// //WX返回时间
				mdsLeaseInfoEntity.setWxSpendDate(new Date());
				// WX返回付款状态
				mdsLeaseInfoEntity.setWxSpendStatus(HospConstants.WX_RESULT_SUCCESS);// 1成功 0失败
				if (StringUtils.isNotBlank(queryVo.getShutPwd())) {
					mdsLeaseInfoEntity.setShutPwd(queryVo.getShutPwd());// 关锁密码
				}
			
				mdsLeaseInfo.setIntoBalance(refundFee.divide(yb));
				logger.info("mdsLeaseInfoEntity.getLeaseDeposit()==="+mdsLeaseInfoEntity.getLeaseDeposit());
				//退款金额
				mdsLeaseInfoEntity.setIntoBalance(mdsLeaseInfo.getIntoBalance());
				logger.info("mdsLeaseInfoEntity.getIntoBalance()==="+mdsLeaseInfoEntity.getIntoBalance());
				//租金
				mdsLeaseInfoEntity.setLeaseCost((mdsLeaseInfoEntity.getLeaseDeposit().subtract(mdsLeaseInfoEntity.getIntoBalance())));
				logger.info("mdsLeaseInfoEntity.getLeaseCost()==="+mdsLeaseInfoEntity.getLeaseCost());
				//订单完成
				mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
				logger.info("mdsLeaseInfoEntity.getLeaseTime()==="+mdsLeaseInfoEntity.getOrderStatus());
				//结束时间
				mdsLeaseInfoEntity.setLeaseEndDate(new Date());
				//时长
				mdsLeaseInfoEntity.setLeaseTime(DateParser.getDateHour(mdsLeaseInfoEntity.getLeaseEndDate(), mdsLeaseInfoEntity.getLeaseStartDate()));
				logger.info("mdsLeaseInfoEntity.getLeaseTime()==="+mdsLeaseInfoEntity.getLeaseTime());
				mdsLeaseInfoApi.returnLeaseInfo(mdsLeaseInfoEntity);
				
				
				// 返回余款
				MdsRecordsConptionEntity entityp = new MdsRecordsConptionEntity();
				logger.info("mdsRecords 返回余款:========");
				entityp.setPlatPayDate(new Date());
				entityp.setPlatPayNumber("S"+DateParser.formatDateSS(new Date()));
				entityp.setWxPayDate(new Date());
				entityp.setOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
				entityp.setOpenid(mdsLeaseInfoEntity.getOpenid());
				entityp.setPaySystem("WX");
				entityp.setConsumStatus(HospConstants.CONSUM_STATUS_S);
				entityp.setOrderDescribe("返回余款");
				entityp.setLeaseCost(mdsLeaseInfoEntity.getIntoBalance());
				logger.info("mdsRecords 返回余款:" + entityp.getLeaseCost());
				entityp.setHospitalId(mdsLeaseInfoEntity.getHospitalId());
				entityp.setHospitalName(mdsLeaseInfoEntity.getHospitalName());
				entityp.setUpdatedDate(new Date());
				logger.info("mdsRecords 返回余款:=====save===");
				mdsRecordsConptionApi.save(entityp);
				
				//支付租金
				MdsRecordsConptionEntity entity = new MdsRecordsConptionEntity();
				
				logger.info("mdsRecords 支付租金:========");
				entity.setOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
				entity.setOpenid(mdsLeaseInfoEntity.getOpenid());
				logger.info("mdsRecords 支付租金 :" + mdsLeaseInfoEntity.getLeaseCost());
				entity.setLeaseCost(mdsLeaseInfoEntity.getLeaseCost());//支付租金
				entity.setConsumStatus(HospConstants.CONSUM_STATUS_P);
				entity.setPaySystem("WX");
				entity.setHospitalId(mdsLeaseInfoEntity.getHospitalId());
				entity.setHospitalName(mdsLeaseInfoEntity.getHospitalName());
				entity.setPlatPayDate(new Date());
				entity.setPlatPayNumber("p"+DateParser.formatDateSS(new Date()));
				entity.setWxPayDate(new Date());
				entity.setUpdatedDate(new Date());
				entity.setOrderDescribe("支付租金");
				logger.info("mdsRecords 支付租金:=====save===");
				mdsRecordsConptionApi.save(entity);
     			//暂时关闭密码状态
//				MdsProductPwdEntity pwd = mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
//				MdsProductPwdVo vopwd = new MdsProductPwdVo();
//				pwd.setPwdStatus(HospConstants.PWD_STATUS_2);
//				BeanUtils.copyProperties(pwd, vopwd);
//				mdsProductPwdApi.updatePwdStatus(vopwd);
				logger.info("mdsRecords 设备设置为空闲 :");
				logger.info("mdsRecords>>>>>>>>>>>>>>>>=="+mdsLeaseInfoEntity.getProductQrcode());
				queryVo.setProductQrcode(mdsLeaseInfoEntity.getProductQrcode());
				MdsProductInfoEntity productInfo = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
				logger.info("mdsRecords 设备设置为空闲 :"+queryVo.getProductUse());
				if(StringUtils.isNotBlank(queryVo.getProductUse()) && queryVo.getProductUse().equals(HospConstants.PRODUCT_USE_Y)){
					productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
				}else{
					productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
				}
				productInfo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
				logger.info("mdsRecords 设备设置为空闲 :"+productInfo.getProductStatus());
				mdsProductInfoApi.save(productInfo);
				
				
				
				if (StringUtils.isBlank(queryVo.getType())){
					
					MdsProductPwdVo productPwdVo = new MdsProductPwdVo();
					productPwdVo.setOpenid(mdsLeaseInfoEntity.getOpenid());
					productPwdVo.setProductCode(mdsLeaseInfoEntity.getProductCode());
					productPwdVo.setProductQrcode(mdsLeaseInfoEntity.getProductQrcode());
					productPwdVo.setKeyId(mdsLeaseInfoEntity.getKeyId());
			        mdsProductPwdApi.sendReturnKey(productPwdVo);
					
				}
				logger.info("订单" + result.get("out_trade_no") + "微信退款成功");
		
				retrunMap.put("LeaseInfo", mdsLeaseInfoEntity);
				logger.info("发送消息模版===satrt============");
			     this.sendTemplateMessage(mdsLeaseInfoEntity);
			 	logger.info("发送消息模版=====end==========");
				respVo.setData(retrunMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			respVo.setCode(201);
			respVo.setMessage("微信退款提交失败");
		}


		return respVo;
	}

	/**
	 * TODO 微信支付 回调
     * @Description:微信支付  回调
     * @return
     * @author dzg
     * @throws Exception
     * @throws WeixinException
     * @date 2016年12月2日
     */
	@PostMapping(value = "wxNotify")
	public void wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("接收到>>>>>>>>>入口>>>>>>>>>>>>");
		BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
		String line = null;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		br.close();
		// sb为微信返回的xml
		String notityXml = sb.toString();
		String resXml = "";
		logger.info("接收到的报文：" + notityXml);

		// Map map = PayUtil.doXMLParse(notityXml);
		Map<String, String> notifyMap = WXPayUtil.xmlToMap(notityXml);
		String json = JSON.toJSONString(notifyMap);
		logger.info(json);//
		logger.info("key>>>>>>==" + key);//
		String returnCode = (String) notifyMap.get("return_code");
		logger.info("接收到>>returnCode>>>>>>>>>" + returnCode);
		if ("SUCCESS".equals(returnCode)) {
			// 验证签名是否正确
			if (WXPayUtil.isSignatureValid(notifyMap, key)) {
				String outTradeNo = notifyMap.get("out_trade_no");
				// String refundId = notifyMap.get("refund_id");//微信退款单号
				String totalFee = notifyMap.get("total_fee");// 微信退款单号
				logger.info("接收到>>totalFee>>>>>>>>>" + totalFee);
				BigDecimal totalFeeb = new BigDecimal(totalFee);
				BigDecimal bd8 = new BigDecimal("100");
				QueryVo queryVo = new QueryVo();
				queryVo.setOrderNumber(outTradeNo);
				MdsLeaseInfoEntity mdsLeaseInfoEntity = mdsLeaseInfoApi.findByOrderNumber(queryVo);
				
				//MdsLeaseInfoEntity mdsLeaseInfoEntity =mdsLeaseInfoApi.getEntity(mdsLeaseInfoEntityold.getId());
				// WX返回时间
				mdsLeaseInfoEntity.setWxIncomeDate(new Date());
				// mdsLeaseInfoEntity.setWxSpendNumber(refundId);
				// WX返回付款状态
				mdsLeaseInfoEntity.setWxSpendStatus(HospConstants.WX_RESULT_SUCCESS);
				

				if (mdsLeaseInfoEntity.getOrderStatus().equals(HospConstants.ORDER_STATUS_2)) {
					mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_0);//使用中
				} 
				logger.info("setLeaseDeposit>>>" + totalFeeb.divide(bd8, 2, BigDecimal.ROUND_HALF_UP));
				mdsLeaseInfoEntity.setLeaseDeposit(totalFeeb.divide(bd8, 2, BigDecimal.ROUND_HALF_UP));

				logger.info("orderStatus >>>:" + mdsLeaseInfoEntity.getOrderStatus());
				// 生成订单
				 mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
				logger.info("orderStatus >>>00000000:" + mdsLeaseInfoEntity.getOrderStatus());
				// 付款流水
				MdsRecordsConptionEntity entityp = new MdsRecordsConptionEntity();
				entityp.setPlatPayDate(new Date());
				entityp.setWxPayDate(new Date());
				entityp.setOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
				entityp.setOpenid(mdsLeaseInfoEntity.getOpenid());
				entityp.setPaySystem("WX");
				logger.info("orderStatus 999999:" + mdsLeaseInfoEntity.getOrderStatus());
				MdsRecordsConptionEntity records= mdsRecordsConptionApi.findByIOrderNumber(queryVo);
				if (mdsLeaseInfoEntity.getOrderStatus().equals(HospConstants.ORDER_STATUS_0)) {
					entityp.setConsumStatus(HospConstants.CONSUM_STATUS_I);
					entityp.setOrderDescribe("预付款");
				} else if (mdsLeaseInfoEntity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)) {
					
					if(records!=null){
						entityp.setConsumStatus(HospConstants.CONSUM_STATUS_S);
						entityp.setOrderDescribe("返回余款");
					}else{
						entityp.setConsumStatus(HospConstants.CONSUM_STATUS_I);
						entityp.setOrderDescribe("预付款");
					}
	
				}
				entityp.setLeaseCost(totalFeeb.divide(bd8, 2, BigDecimal.ROUND_HALF_UP));
				entityp.setHospitalId(mdsLeaseInfoEntity.getHospitalId());
				entityp.setHospitalName(mdsLeaseInfoEntity.getHospitalName());
				// entityp.setOrderDescribe("支付租金");
				entityp.setUpdatedDate(new Date());
				mdsRecordsConptionApi.save(entityp);
				//微信支付延迟 
				if(records==null && mdsLeaseInfoEntity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)){
					logger.info("微信支付延迟 订单已结束处理====");
					this.refund(queryVo);
				}
			
				logger.info("接收到>>通知微信服务器已经支付成功>>>");
				// 通知微信服务器已经支付成功
				resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
						+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
			} else {
				logger.info("接收到>> 签名有问题>>>>>");
			}
		} else {
			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
					+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
		}
		logger.info(">>>>通知微信服务器已经支付成功>>>>>>>>>=" + resXml);
		logger.info("微信支付回调数据结束");
		BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
		out.write(resXml.getBytes());
		out.flush();
		out.close();
	}
//	@SuppressWarnings({ "rawtypes", "unused" })
//	public void wxPayResult(Map map) 
//	{
//		// 解析各种数据
//		String appid = (String) map.get("appid");//应用ID
//		String attach = (String) map.get("attach");//商家数据包
//		String bank_type = (String) map.get("bank_type");//付款银行
//		String cash_fee = (String) map.get("cash_fee");//现金支付金额
//		String fee_type = (String) map.get("fee_type");//货币种类
//		String is_subscribe = (String) map.get("is_subscribe");//是否关注公众账号
//		String mch_id = (String) map.get("mch_id");//商户号
//		String nonce_str = (String) map.get("nonce_str");//随机字符串
//		String openid = (String) map.get("openid");//用户标识
//		String out_trade_no = (String) map.get("out_trade_no");// 获取商户订单号
//		String result_code = (String) map.get("result_code");// 业务结果
//		String return_code = (String) map.get("return_code");// SUCCESS/FAIL
//		String sign = (String) map.get("sign");// 获取签名
//		String time_end = (String) map.get("time_end");//支付完成时间
//		String total_fee = (String) map.get("total_fee");// 获取订单金额
//		String trade_type = (String) map.get("trade_type");//交易类型
//		String transaction_id = (String) map.get("transaction_id");//微信支付订单号
//	}
	/***
	 * TODO 扫码
	 */
	@ApiOperation(value = "扫码")
	@PostMapping(value = "findProductCode")
	public RespVo<MdsProductInfoVo> findProductCode(@RequestBody QueryVo vo) {
		logger.info("--扫码--findProductCode----");
		RespVo<MdsProductInfoVo> respVo = new RespVo<>();
		MdsUserInfoEntity userInfoEntity = mdsUserInfoApi.findByToken(vo.getToken());
		if (userInfoEntity != null) {
			MdsProductInfoVo toPageVo = mdsProductInfoApi.findByQrcode(vo);
			
		//	logger.info("---findByQrcode--"+(toPageVo != null));
			if (toPageVo != null && toPageVo.getProductCode()!=null && !"".equals(toPageVo.getProductCode())) {
				MdsPropertyInfoEntity propertyInfoEntity = mdsPropertyInfoApi.getEntity(toPageVo.getHospitalId());
				if (toPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_2)) {
					if(propertyInfoEntity.getPropertyUsableTime()!=null &&!"".equals(propertyInfoEntity.getPropertyUsableTime())){
						//logger.info("--营业时间段-医院id---"+propertyInfoEntity.getId());
						//logger.info("--营业时间段----"+propertyInfoEntity.getPropertyUsableTime());
						if(DateParser.isInTime(propertyInfoEntity.getPropertyUsableTime())){
							toPageVo.setFileId(fileurl + toPageVo.getFileId());
							toPageVo.setImageURL(toPageVo.getFileCategotyUrl());
							respVo.setData(toPageVo);
							return respVo;
						}else{
							respVo.setCode(201);
							respVo.setMessage("该备不在可使用时间范围内!");
							respVo.setData(toPageVo);
							return respVo;
							
						}
					}else{
						toPageVo.setFileId(fileurl + toPageVo.getFileId());
						toPageVo.setImageURL(toPageVo.getFileCategotyUrl());
						respVo.setData(toPageVo);
						return respVo;
					}
				
					
				

				} else {

					if (toPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)) {
						String openid = userInfoEntity.getOpenid();
					//	logger.info("--扫码--findProductCode--openid--" + openid);
						vo.setOpenid(openid);
						vo.setProductCode(toPageVo.getProductCode());
						List<String> orderStatusList = new ArrayList<String>();
						orderStatusList.add(HospConstants.ORDER_STATUS_0);
						orderStatusList.add(HospConstants.ORDER_STATUS_2);
						vo.setOrderStatusList(orderStatusList);
						List<MdsLeaseInfoEntity> findByOpenidList = mdsLeaseInfoApi.findLeaseByOrderNumber(vo);
					//	logger.info("--扫码--findProductCode----" + findByOpenidList.size());
						if (null != findByOpenidList && findByOpenidList.size() > 0) {
							MdsLeaseInfoEntity mdsLeaseInfoEntity = findByOpenidList.get(0);
							logger.info("--扫码--findProductCode--mdsLeaseInfoEntity.getOpenid()--"
									+ mdsLeaseInfoEntity.getOpenid());
							if (openid.equals(mdsLeaseInfoEntity.getOpenid())) {
								toPageVo.setIsCurrUserUse(Boolean.TRUE);
								respVo.setData(toPageVo);
								return respVo;
							} else {
								respVo.setCode(201);
								respVo.setMessage("设备正在使用中!");
								respVo.setData(toPageVo);
								return respVo;
							}
						} else {

							respVo.setCode(201);
							respVo.setMessage("设备正在使用中!");
							return respVo;
						}

					} else {
						respVo.setCode(201);
						respVo.setMessage("设备正在升级中!");
						respVo.setData(toPageVo);
						return respVo;
					}
				}

			} else {
				respVo.setCode(201);
				respVo.setMessage("该设备二维码不在使用范围内!");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;
		}

	}
	
	@ApiOperation(value = "代付款获取paySign")
	@PostMapping(value = "findPaySign")
	public RespVo<Map<String, Object>> findPaySign(@RequestBody QueryVo queryVo) 
	{
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();

		MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
		if (user != null) {
			String payPre = RedisUtil.get("pay_pre"+ queryVo.getOrderNumber());
			logger.info("=====findPaySign=payPre===" + payPre);
			if(StringUtils.isNotBlank(payPre)){
				map.put("pay_pre", payPre);
				respVo.setData(map);
			}else{
				respVo.setCode(201);
				respVo.setMessage("订单已过期!");
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;
		}
		return respVo;
	}
	
	
	@ApiOperation(value = "小程序获取开锁密码")
	@PostMapping(value = "findProductUnlockPwd")
	public RespVo<Map<String, Object>> findProductUnlockPwd(@RequestBody QueryVo queryVo) 
	{
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();
		logger.info("=====findProductUnlockPwd==getProductQrcode====" + queryVo.getProductQrcode());
		
		MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
		if(user!=null){
		MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
		map.put("mdsLeaseInfo", mdsLeaseInfo);// 开锁密码
		respVo.setData(map);
		}else{
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;	
		}
		return respVo;
	}
	

	// 业务逻辑代码
	/***
	 * TODO 验证关锁密码
	 */
	@ApiOperation(value = "归还  验证关锁密码")
	@PostMapping("verifyPassword")
	public RespVo<?> verifyPassword(@RequestBody QueryVo queryVo) 
	{
		RespVo<?> respVo = new RespVo<>();
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) 
		{
			MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
			if (mdsLeaseInfo != null) 
			{
		
				//queryVo.setUnlockPwd(mdsLeaseInfo.getUnlockPwd());
				MdsProductPwdEntity pwd = mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
				if (mdsLeaseInfo.getShutPwd().equals(queryVo.getShutPwd())) {
					//验证成功更新密码状态
					MdsProductPwdVo vo = new MdsProductPwdVo();
					BeanUtils.copyProperties(pwd, vo);
					pwd.setPwdStatus(HospConstants.PWD_STATUS_2);
					mdsProductPwdApi.updatePwdStatus(vo);
				}else{
					respVo.setCode(201);
					respVo.setMessage("关锁密码不正确!");
				}
			} else 
			{
				respVo.setCode(201);
				respVo.setMessage("没有相关订单!");
			}
		} else 
		{
			respVo.setCode(201);
			respVo.setMessage("没有参数 orderNumber");
		}
		return respVo;
	}
	/***
	 * TODO 确认归还
	 * @throws Exception 
	 */
	@ApiOperation(value = "确认归还")
	@PostMapping("returnLeaseInfo")
	public RespVo<Map<String, Object>> returnLeaseInfo(@RequestBody QueryVo queryVo) throws Exception 
	{

		RespVo<Map<String, Object>> respVo = new RespVo<>();
		MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
		if(user!=null){
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) 
		{
			MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
			if (mdsLeaseInfo != null) 
			{
		
				//queryVo.setUnlockPwd(mdsLeaseInfo.getUnlockPwd());

				if (mdsLeaseInfo.getShutPwd().equals(queryVo.getShutPwd())) {
//				MdsUserInfoEntity userInfoEntity = mdsUserInfoApi.findByToken(queryVo.getToken());
//					vo.setOpenid(userInfoEntity.getOpenid());
//					vo.setOrderNumber(queryVo.getOrderNumber());
//					mdsLeaseInfoApi.returnLeaseInfo(vo);
					return	this.refund(queryVo);
				}else{
					respVo.setCode(201);
					respVo.setMessage("关锁密码不正确!");
				}
			} else 
			{
				respVo.setCode(201);
				respVo.setMessage("没有相关订单!");
				
			}
		} else{
			respVo.setCode(201);
			respVo.setMessage("没有参数 orderNumber");
			
		}
		}else{
			
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			
		}
		return respVo;

	}
//	/**
//	 * TODO  私有：更新微信预支付后订单信息
//	 * */
//	@SuppressWarnings("unused")
//	private void updPrePayOrder(String money,String orderNo,QueryVo queryVo,Map<String, String> resp)
//	{
//		// 更新订单信息
//		MdsLeaseInfoVo mdsLeaseInfoVo = new MdsLeaseInfoVo();
//		BigDecimal leaseDeposit = BigDecimal.ZERO;
//		BigDecimal moneyB = new BigDecimal(money) ;
//		leaseDeposit = moneyB.divide(BigDecimal.valueOf(100),2,RoundingMode.UP);
//		//订单号码
//		mdsLeaseInfoVo.setOrderNumber(orderNo);
//		//预付款
//		mdsLeaseInfoVo.setLeaseDeposit(leaseDeposit);
//		//开始时间
//		mdsLeaseInfoVo.setLeaseStartDate(new Date());
//		//医院id
//		mdsLeaseInfoVo.setHospitalId(queryVo.getHospitalId());
//		//医院名称
//		mdsLeaseInfoVo.setHospitalName(queryVo.getHospitalName());
//		//所属科室
//		mdsLeaseInfoVo.setHospitalDepart(queryVo.getHospitalDepart());
//		//品类
//		mdsLeaseInfoVo.setCategoryId(queryVo.getCategoryId());
//		//品类名称
//		mdsLeaseInfoVo.setCategoryName(queryVo.getCategoryName());
//		//产品ID
//		mdsLeaseInfoVo.setProductId(queryVo.getProductId());
//		//产品编号
//		mdsLeaseInfoVo.setProductCode(queryVo.getProductCode());
//		//产品二维码
//		mdsLeaseInfoVo.setProductQrcode(queryVo.getProductQrcode());
//		//产品数量
//		mdsLeaseInfoVo.setProductNumber(queryVo.getProductNumber());
//		//解锁密码
//		mdsLeaseInfoVo.setUnlockPwd(queryVo.getUnlockPwd());
//		//流水
//		String platIncomeNumber = DateParser.formatDateSS(new Date());
//		//预付款流水
//		mdsLeaseInfoVo.setPlatIncomeNumber(platIncomeNumber);
//		//平台发起时间
//		mdsLeaseInfoVo.setPlatIncomeDate(new Date());
//		// 预付款WX流水(微信支付订单号)
//		mdsLeaseInfoVo.setWxIncomeNumber(resp.get("transaction_id"));
//		//openid 用户openid
//		mdsLeaseInfoVo.setOpenid(resp.get("openid"));
//	}
//	
	
	
	/**
	 * TODO 获取用户租赁信息 
	 * */
	@ApiOperation(value = "获取订单详细信息")
	@PostMapping(value = "findLeaseInfoByOrderNumber")
	public RespVo<Map<String, Object>> findLeaseInfoByOrderNumber(@RequestBody QueryVo queryVo) 
	{
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
		if(user!=null){
		MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
		Map<String, Object> map = new HashMap<>();
		map.put("mdsLeaseInfo", mdsLeaseInfo);
		respVo.setData(map);
		return respVo;
		}else{
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;	
		}
	}
	
	/**
	 *TODO  验证 WEB 退款
	 * @param mdsUserInfoEntity 
	 * */
	@ApiOperation(value = "WEB 退款")
	@PostMapping(value = "webRefuse")
	public RespVo<Map<String, Object>> webRefuse(@RequestBody QueryVo queryVo) {
		logger.info(">>>>>>>>>>>webRefuse>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();

		logger.info(">>>>>>>>>>>loginName>>>>>>"+queryVo.getLoginName());

		MdsUserInfoEntity user = mdsUserInfoApi.findByloginName(queryVo);
		if(user!=null){
	
	//	if (user.getRefusePwd().equals(queryVo.getRefusePwd())) {
			try {
				queryVo.setType(user.getUserType());
				if(queryVo.getIntoWBalance().compareTo(BigDecimal.ZERO)==1){
					return	this.refund(queryVo);
				}else{
					
					MdsLeaseInfoEntity mdsLeaseInfoEntity  = mdsLeaseInfoApi.findByOrderNumber(queryVo);
				
					//退款金额
					mdsLeaseInfoEntity.setIntoBalance(BigDecimal.ZERO);
					logger.info(">>>>webRefuse>>>>getIntoBalance()==="+mdsLeaseInfoEntity.getIntoBalance());
					//租金
					mdsLeaseInfoEntity.setLeaseCost((mdsLeaseInfoEntity.getLeaseDeposit().subtract(mdsLeaseInfoEntity.getIntoBalance())));
					logger.info(">>>>webRefuse>>>>.getLeaseCost()==="+mdsLeaseInfoEntity.getLeaseCost());
					//订单完成
					mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
					logger.info(">>>>webRefuse>>>>.getLeaseTime()==="+mdsLeaseInfoEntity.getOrderStatus());
					//结束时间
					mdsLeaseInfoEntity.setLeaseEndDate(new Date());
					//时长
					mdsLeaseInfoEntity.setLeaseTime(DateParser.getDateHour(mdsLeaseInfoEntity.getLeaseEndDate(), mdsLeaseInfoEntity.getLeaseStartDate()));
					logger.info(">>>>webRefuse>>>>.getLeaseTime()==="+mdsLeaseInfoEntity.getLeaseTime());
					mdsLeaseInfoApi.returnLeaseInfo(mdsLeaseInfoEntity);
					
					logger.info(">>>>webRefuse>>>> 设备设置为空闲 :");
					logger.info(">>>>webRefuse>>>>>>>>>>>>>>>>>>>>=="+mdsLeaseInfoEntity.getProductQrcode());
					queryVo.setProductQrcode(mdsLeaseInfoEntity.getProductQrcode());
					
					MdsProductInfoEntity productInfo = mdsProductInfoApi.getEntity(mdsLeaseInfoEntity.getProductId());
					if(StringUtils.isNotBlank(queryVo.getProductUse()) && !queryVo.getProductUse().equals(HospConstants.PRODUCT_USE_Y)){
						logger.info(">>>>webRefuse>>>> 设备设置为空闲 :");
						productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
					}else{
						logger.info(">>>>webRefuse>>>> 设备设置为空闲 :");
						productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
					}
					productInfo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
					logger.info(">>>>webRefuse>>>> 设备 :"+productInfo.getProductUseStatus());
			
					mdsProductInfoApi.save(productInfo);
					
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
//		} else {
//			respVo.setCode(201);
//			respVo.setMessage("退款密码不对");
//		}
		return respVo;
	}else{
		respVo.setCode(300);
		respVo.setMessage("loginName不正确");
		return respVo;	
	}
		
	}
	
	
	
	/**
	 *TODO WEB inuse 使用中设备退款
	 * @param mdsUserInfoEntity 
	 * @throws Exception 
	 * */
	@ApiOperation(value = "WEB inuse 使用中设备退款")
	@PostMapping(value = "webInUseRefuse")
	public RespVo<Map<String, Object>> webInUseRefuse(@RequestBody QueryVo queryVo) throws Exception {
		logger.info(">>>>>>>>>>>webInUseRefuse>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		logger.info(">>>>>>>>>>>loginName>>>>>>" + queryVo.getLoginName());

//		MdsUserInfoEntity user = mdsUserInfoApi.findByloginName(queryVo);
//		if (user != null) {
			// 设备 web
			List<MdsLeaseInfoEntity> lsleaseinfo = mdsLeaseInfoApi.findLeaseByOrderNumber(queryVo);
			logger.info(">>>>>>>>>>>webInUseRefuse>lsleaseinfo>>>>>>"+lsleaseinfo.size());
			if (lsleaseinfo != null && lsleaseinfo.size()>0) {
				MdsLeaseInfoEntity entity = lsleaseinfo.get(0);
				logger.info(">>>>>>>>>>>webInUseRefuse>lsleaseinfo>>>>>>"+entity.getOrderNumber());
				queryVo.setOrderNumber(entity.getOrderNumber());
				return this.refund(queryVo);
			} else {
				respVo.setCode(201);
				respVo.setMessage("设备有问题");
				return respVo;
			}

//		} else {
//			respVo.setCode(300);
//			respVo.setMessage("loginName不正确");
//			return respVo;
//		}

	}

	/**
	 *TODO WEB inuse 使用中订单结束 
	 * @param mdsUserInfoEntity 
	 * @throws Exception 
	 * */
	@ApiOperation(value = "WEB inuse 使用中订单结束 ")
	@PostMapping(value = "webInUseLeaseRefuse")
	public RespVo<Map<String, Object>> webInUseLeaseRefuse(@RequestBody QueryVo queryVo) throws Exception {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//Map<String, Object> retrunMap = new HashMap<>();
//		MdsUserInfoEntity user = mdsUserInfoApi.findByloginName(queryVo);
//		if (user != null) {
			if (StringUtils.isNotBlank(queryVo.getOrderNumber())) {
				// 订单web
				MdsLeaseInfoEntity leaseInfo = mdsLeaseInfoApi.findByOrderNumber(queryVo);
				if (leaseInfo != null) {
					if (queryVo.getOrderStatus().equals(HospConstants.ORDER_STATUS_2)) {
						// 代付款 直接结束
						leaseInfo.setOrderStatus(HospConstants.ORDER_STATUS_1);
						mdsLeaseInfoApi.save(leaseInfo);

						queryVo.setProductQrcode(leaseInfo.getProductQrcode());
						MdsProductInfoEntity productInfo = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
						logger.info("代付款 :");
						productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
						productInfo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
						logger.info("代付款 :" + productInfo.getProductStatus());
						mdsProductInfoApi.save(productInfo);
						respVo.setCode(200);
						 return respVo;

					} else {
				
						//  维修(报修中)
						if (queryVo.getOrderStatus().equals(HospConstants.ORDER_STATUS_4 )) {
							// 维修(报修中)
							if (queryVo.getRepairsType().equals(HospConstants.ORDER_STATUS_REPAIRS_TYPE_0)) {
								// 恢复正常
								logger.info("恢复正常 getProductStatus:" + queryVo.getProductStatus());
								logger.info("恢复正常 getRepairsType:" + queryVo.getRepairsType());
								leaseInfo.setOrderStatus(HospConstants.ORDER_STATUS_0);
								mdsLeaseInfoApi.save(leaseInfo);
								MdsProductInfoEntity productInfo = mdsProductInfoApi.getEntity(leaseInfo.getProductId());
								productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
								productInfo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
								mdsProductInfoApi.save(productInfo);
								respVo.setCode(200);
								 return respVo;
							} else {
								// 维修
								leaseInfo.setOrderStatus(HospConstants.ORDER_STATUS_1);
								leaseInfo.setLeaseEndDate(leaseInfo.getUpdatedDate());
								mdsLeaseInfoApi.save(leaseInfo);
								
								 logger.info("respVos>>1111>>>>>>>==:");
								 RespVo<Map<String, Object>>  respVos =	this.refund(queryVo);
								 logger.info("respVos>>>>>>>>>==:" + respVos.getCode());
	                            if(respVos.getCode().equals(200)){
	                            	MdsProductInfoEntity productInfos = mdsProductInfoApi.getEntity(leaseInfo.getProductId());
	                    			logger.info("维修 :" + productInfos.getProductStatus());
	                    			productInfos.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
									logger.info("维修 >>:" + productInfos.getProductStatus());
									mdsProductInfoApi.save(productInfos);
	                            }
	                             respVo.setCode(200);
	                             return respVo;
							}
							
						}else{
							
							// 使用中 余额不足
							leaseInfo.setOrderStatus(HospConstants.ORDER_STATUS_1);
							leaseInfo.setLeaseEndDate(leaseInfo.getUpdatedDate());
							mdsLeaseInfoApi.save(leaseInfo);
							this.refund(queryVo);
							MdsProductInfoEntity productInfow = mdsProductInfoApi.getEntity(leaseInfo.getProductId());
							logger.info("维修 :" + productInfow.getProductStatus());
							productInfow.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
							logger.info("维修 >>:" + productInfow.getProductStatus());
							mdsProductInfoApi.save(productInfow);
							respVo.setCode(200);
							 return respVo;
							
						} 
						
					}

				} else {
					respVo.setCode(201);
					respVo.setMessage("订单有问题");
					return respVo;
				}

			} else {
				respVo.setCode(201);
				respVo.setMessage("订单号码有问题");
				return respVo;
			}
		
//		} else {
//			respVo.setCode(300);
//			respVo.setMessage("LONGNAME不正确");
//			return respVo;
//		}
	}
	
	
	
	
	
	
	
	@ApiOperation(value = "微信获取手机号")
	@PostMapping(value = "wxPhoneNumber")
	public RespVo<Map<String, Object>> wxPhoneNumber(@RequestBody QueryVo queryVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> retrunMap = new HashMap<>();
		if (queryVo.getToken() != null) {
			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				String sessionKey = null;
				if (queryVo.getCode() != null) {
					String url = wxloginurl + "?appid=" + appId + "&secret=" + secret + "&js_code=" + queryVo.getCode()
							+ "&grant_type=" + grantType;
					logger.info(">>>>>url==" + url);
					RestTemplate restTemplate = new RestTemplate();
					ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null,
							String.class);
					if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
						String sessionData = responseEntity.getBody();
						JSONObject json = JSONObject.fromObject(sessionData);
						logger.info("这里是openid和session_key" + json);

						sessionKey = (String) json.get("session_key");
						logger.info("=======session_key==loginin=====" + sessionKey);
					} else {
						respVo.setCode(201);
						respVo.setMessage("微信返回出错!");
						return respVo;
					}
				} else {
					logger.info("=======session_key==getRemark=====" + user.getRemark());
					sessionKey = user.getRemark();
				}
				logger.info("=======session_key==getRemark==s==="+sessionKey);
				logger.info("=======session_key==getEncryptedData==s==="+queryVo.getEncryptedData());
				logger.info("=======session_key==getIv==s==="+queryVo.getIv());
				com.alibaba.fastjson.JSONObject userInfoPhoneJSON = WechatGetUserInfoUtil
						.getUserInfo(queryVo.getEncryptedData(), sessionKey, queryVo.getIv());
				logger.info("=======session_key==getRemark==e==="+(userInfoPhoneJSON==null));
				if(userInfoPhoneJSON!=null){
					logger.info("userInfoJSON>>>>>>>>>>>>>>>=" + userInfoPhoneJSON);
					String phoneNumber = (String) userInfoPhoneJSON.get("phoneNumber");
					logger.info("wxPhoneNumber>>>>>>>>>>>>>>>=" + phoneNumber);
					user.setUserPhone(phoneNumber);
					user.setRemark(sessionKey);
					//mdsUserInfoApi.save(user);
					MdsUserInfoVo mdsUserInfoVo = new MdsUserInfoVo();
					BeanUtils.copyProperties(user, mdsUserInfoVo);
					mdsUserInfoApi.updatePhone(mdsUserInfoVo);
					retrunMap.put("userInfo", user);
					respVo.setData(retrunMap);
					return respVo;
			
				}else{
					logger.info("session_key>>>>>>>>>>>>>>>=");
					respVo.setCode(301);
					respVo.setMessage("session_key过期");
					return respVo;
				}

			} else {
				respVo.setCode(201);
				respVo.setMessage("没有token相关用户");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token不能为空");
			return respVo;
		}

	}
	
	/**
	 *TODO 、检查当前用户是否有使用中订单（接口）
	 * @param mdsUserInfoEntity 
	 * */
	@ApiOperation(value = "检查当前用户是否有使用中订单")
	@PostMapping(value = "findByOpenidInUse")
	public RespVo<Map<String, Object>> findByOpenidInUse(@RequestBody QueryVo queryVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> retrunMap = new HashMap<>();
		MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
		if (user != null) {
			//String inUse = RedisUtil.get(user.getOpenid());
//			if (StringUtils.isNotBlank(inUse) && inUse.equals("Y")) {
//				retrunMap.put("inUse", "Y");
//			} else {
				//订单状态  0 使用中    1 结束     2待付款   4 报修中 5余额不足   
				//String orstatus = "0,2,3,4";
				
				List<String> orderStatusList = new ArrayList<String>();
				orderStatusList.add(HospConstants.ORDER_STATUS_0);
				orderStatusList.add(HospConstants.ORDER_STATUS_2);
				orderStatusList.add(HospConstants.ORDER_STATUS_4);
				orderStatusList.add(HospConstants.ORDER_STATUS_5);
				queryVo.setOrderStatusList(orderStatusList);
				queryVo.setOpenid(user.getOpenid());
				Page<MdsLeaseInfoVo> page = mdsLeaseInfoApi.findByOpenid(queryVo);
				if (page != null && page.getContent().size() > 0) {
				//	RedisUtil.set(user.getOpenid(), "Y");
					retrunMap.put("inUse", HospConstants.IN_USE_Y);
				} else {
					retrunMap.put("inUse", HospConstants.IN_USE_N);
				}
			

			respVo.setData(retrunMap);
			return respVo;
		} else {
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;
		}

	}
	
	
	/***
	 * TODO 设备报修处理
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "设备报修处理" )
	@PostMapping(value = "reportProcessing")
	public RespVo<Map<String, Object>> reportProcessing(@RequestBody QueryVo queryVo) {
		logger.info("设备报修处理>>>>>>>>>>>>>>>>>>>>reportProcessing");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
	     
		    MdsProductInfoEntity  mdsProductInfoEntity = mdsProductInfoApi.findByCode(queryVo);
		 
			MdsLeaseInfoEntity mdsLeaseInfoEntity =mdsLeaseInfoApi.info(mdsProductInfoEntity.getId());
			if(mdsProductInfoEntity !=null && mdsLeaseInfoEntity!=null){
			  //恢复正常
				if ("true".equals(queryVo.getIsWrong())) {
					mdsProductInfoEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4); 
					mdsProductInfoEntity.setProductStatus(HospConstants.PRODUCT_STATUS_1);
					mdsProductInfoApi.save(mdsProductInfoEntity);
					mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_0);
					mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
					respVo.setCode(200);
					respVo.setMessage("设备,订单已恢复正常!");
					return respVo;
				}else{
				//改为维修中	remark:备注内容
					mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
					mdsLeaseInfoEntity.setRemark(queryVo.getRemark());
					mdsLeaseInfoEntity.setLeaseEndDate(mdsLeaseInfoEntity.getUpdatedDate());
					mdsLeaseInfoEntity.setUpdatedDate(new Date());
				    mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
				    queryVo.setOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
				    try {
						this.refund(queryVo);
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mdsProductInfoEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5); 
					mdsProductInfoEntity.setProductStatus(HospConstants.PRODUCT_STATUS_5);
					mdsProductInfoEntity.setRemark(queryVo.getRemark());
					mdsProductInfoApi.save(mdsProductInfoEntity);
					respVo.setCode(200);
					respVo.setMessage("设备,订单已恢复正常!");
					return respVo;
	
				}
				
		  }else{
			  respVo.setCode(201);
			  respVo.setMessage("没有相关设备 订单!");
			  
		  }
		return respVo;
}
	/**
	 * TODO 获取accessToken
	 * */
	private String getAccessToken()
	{
		RestTemplate restTemplate = new RestTemplate();
		// 进行网络请求,访问url接口
		String getTokenUrl="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appId+"&secret="+secret;
		ResponseEntity<String> responseEntity = restTemplate.exchange(getTokenUrl, HttpMethod.GET, null, String.class);
		String map = responseEntity.getBody();
		Map<String, Object> mapTypes = JSON.parseObject(map);  
		String token = (String) mapTypes.get("access_token");
		return token;
	}
	

	/**
     * 发送模板消息sendTemplateMessage
     * 小程序模板消息,发送服务通知
     * @param touser 接收者（用户）的 openid
     * @param template_id 所需下发的模板消息的id
     * @param page 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
     * @param formid 表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id
     * @return
     */
	public JSONObject sendTemplateMessage(MdsLeaseInfoEntity leaseInfo) {
		logger.info("sendTemplateMessage==============");
		Map<String, TemplateData> templateMap = new HashMap<>();
		templateMap.put("keyword1", new TemplateData(leaseInfo.getOrderNumber()));
		templateMap.put("keyword2", new TemplateData(leaseInfo.getCategoryName()));
		templateMap.put("keyword3", new TemplateData(String.valueOf(leaseInfo.getLeaseDeposit())));
		templateMap.put("keyword4", new TemplateData(String.valueOf(leaseInfo.getLeaseCost())));
		templateMap.put("keyword5", new TemplateData(DateParser.formatDates(leaseInfo.getLeaseStartDate())));
		templateMap.put("keyword6", new TemplateData(DateParser.formatDates(leaseInfo.getLeaseEndDate())));
		
		
		SendTemplateMessage sendTemplateMessage = new SendTemplateMessage();
		// 拼接数据
		sendTemplateMessage.setTouser(leaseInfo.getOpenid());
		sendTemplateMessage.setTemplate_id(orderendTemplateId);//订单结束模版ID
		//sendTemplateMessage.setPage(page);
		sendTemplateMessage.setForm_id(leaseInfo.getWxIncomeNumber());
		sendTemplateMessage.setData(templateMap);
		//sendTemplateMessage.setEmphasis_keyword("");
		
		
		JSONObject json = JSONObject.fromObject(sendTemplateMessage);
		logger.info("##模版发送JSON数据:  " + json.toString());
		String accessToken = getAccessToken();
		logger.info("##模版发送accessToken: " + accessToken);
		logger.info("##模版发送URL: " + templateMessageUrl + accessToken);
		String ss = Httprequests.sendPostWXMessages(templateMessageUrl + accessToken, json.toString());
		JSONObject jsonObj = JSONObject.fromObject(ss);
		logger.info("接收参数消息模版返回值:  " + jsonObj.toString());
		return jsonObj;
	}

	/**
	 * TODO 订单完成退款接口
	 * @param out_trade_no		订单号
	 * @throws Exception
	 */
	@ApiOperation(value = "订单完成退款接口")
	@PostMapping(value = "completeRefund")
	public RespVo<Map<String, Object>> completeRefund(@RequestBody QueryVo queryVo) throws Exception {
		logger.info(" we=completeRefund=========getIntoWBalance=========>>>>===" + queryVo.getIntoWBalance());
		logger.info(" we=completeRefund==>>>>getOrderNumber===" + queryVo.getOrderNumber());
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> retrunMap = new HashMap<String, Object>();

		if (queryVo.getIntoWBalance() != null && !queryVo.getIntoWBalance().equals("")
				&& queryVo.getIntoWBalance().compareTo(BigDecimal.ZERO) == 1) {

			logger.info(" we=completeRefund==>>>>===" + queryVo.getIntoWBalance());
			MdsLeaseInfoVo mdsLeaseInfo = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
			logger.info(" we=completeRefund==>>>>===" + mdsLeaseInfo.getLeaseCost());
			BigDecimal 	  intoCompleteBalance = mdsLeaseInfo.getLeaseCost().subtract(queryVo.getIntoWBalance());
			//logger.info(" we=completeRefund==>>>>===" + mdsLeaseInfo.getLeaseCost());
			if(mdsLeaseInfo.getLeaseCost().compareTo(queryVo.getIntoWBalance())!=-1){
				// 设置请求参数
				HashMap<String, String> data = new HashMap<String, String>(50);
				data.put("out_trade_no", mdsLeaseInfo.getOrderNumber());//
				String outRefundNo = "R" + DateParser.formatDateSS(new Date());
				data.put("out_refund_no", outRefundNo);
				BigDecimal totalFee = mdsLeaseInfo.getLeaseDeposit().multiply(yb);// 微信分计算
				logger.info(" 预付款>>>>>>===" + totalFee);
				data.put("total_fee", String.valueOf(totalFee.intValue()));
				logger.info(" 预付款>>>>>total_fee>===" + data.get("total_fee"));
				BigDecimal refundFee = new BigDecimal(0.00);
				refundFee = queryVo.getIntoWBalance().multiply(yb);
				logger.info(" we=completeRefund====退款>>>>>>===" + refundFee);

				logger.info("支付前参数==退款金额=refundFee=" + String.valueOf(refundFee.intValue()));
				data.put("refund_fee", String.valueOf(refundFee.intValue()));
				logger.info("支付前参数===退款金额=refundFee====" + data.get("refund_fee"));
				data.put("refund_fee_type", "CNY");
				logger.info("支付前参数==333==" + config.getMchID());
				data.put("op_user_id", config.getMchID());
				logger.info("支付前参数==444");
				String json = JSON.toJSONString(data);
				logger.info("支付前参数==json" + json);
				try {
					// 调用sdk发起退款
					Map<String, String> result = wxpay.refund(data);
					logger.info("result_code===" + result.get("result_code"));
					String results = JSON.toJSONString(result);
					logger.info(results);
					if ("SUCCESS".equals(result.get("result_code"))) {
						// 更新订单
						// BigDecimal refundFeeS = new BigDecimal(2.00);
						queryVo.setOrderNumber(result.get("out_trade_no"));
						MdsLeaseInfoEntity mdsLeaseInfoEntity = mdsLeaseInfoApi.findLeaseInfoByOrderNumber(queryVo);
						// //WX返回时间
						mdsLeaseInfoEntity.setWxSpendDate(new Date());
						// WX返回付款状态
						mdsLeaseInfoEntity.setWxSpendStatus(HospConstants.WX_RESULT_SUCCESS);// 1成功
																								// 0失败
						logger.info("mdsLeaseInfo.getIntoBalance()=原来===" + mdsLeaseInfoEntity.getIntoBalance());
						logger.info("mdsLeaseInfo.getIntoBalance()=现在===" + refundFee.divide(yb));
						logger.info("mdsLeaseInfo.getIntoBalance()=一共==="
								+ mdsLeaseInfoEntity.getIntoBalance().add(refundFee.divide(yb)));
						// mdsLeaseInfo.setIntoBalance(mdsLeaseInfo.getIntoBalance().add(refundFee.divide(yb)));
						// 退款金额
						mdsLeaseInfoEntity.setIntoBalance(mdsLeaseInfo.getIntoBalance().add(refundFee.divide(yb)));
						logger.info("mdsLeaseInfoEntity.getIntoBalance()===" + mdsLeaseInfoEntity.getIntoBalance());
						// 租金
						logger.info("mdsLeaseInfoEntity.getLeaseCost()=前==" + mdsLeaseInfoEntity.getLeaseCost());
						mdsLeaseInfoEntity.setLeaseCost(
								(mdsLeaseInfoEntity.getLeaseDeposit().subtract(mdsLeaseInfoEntity.getIntoBalance())));
						logger.info("mdsLeaseInfoEntity.getLeaseCost()=现在==" + mdsLeaseInfoEntity.getLeaseCost());
						// 订单完成
						mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
						logger.info("mdsLeaseInfoEntity.getLeaseTime()===" + mdsLeaseInfoEntity.getOrderStatus());
						// 结束时间
						mdsLeaseInfoEntity.setLeaseEndDate(new Date());
						// 时长
						mdsLeaseInfoEntity.setLeaseTime(DateParser.getDateHour(mdsLeaseInfoEntity.getLeaseEndDate(),
								mdsLeaseInfoEntity.getLeaseStartDate()));
						logger.info("mdsLeaseInfoEntity.getLeaseTime()===" + mdsLeaseInfoEntity.getLeaseTime());
						
						mdsLeaseInfoApi.returnLeaseInfo(mdsLeaseInfoEntity);

						// 退款金额
						MdsRecordsConptionEntity entityNEW = new MdsRecordsConptionEntity();
						logger.info("completeRefund 完成退款:========");
						entityNEW.setPlatPayDate(new Date());
						entityNEW.setPlatPayNumber("C" + DateParser.formatDateSS(new Date()));
						entityNEW.setWxPayDate(new Date());
						entityNEW.setOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
						entityNEW.setOpenid(mdsLeaseInfoEntity.getOpenid());
						entityNEW.setPaySystem("WX");
						entityNEW.setConsumStatus(HospConstants.CONSUM_STATUS_C);
						entityNEW.setOrderDescribe("完成退款");
						entityNEW.setLeaseCost(refundFee.divide(yb));
						logger.info("completeRefund 完成退款:" + refundFee.divide(yb));
						entityNEW.setHospitalId(mdsLeaseInfoEntity.getHospitalId());
						entityNEW.setHospitalName(mdsLeaseInfoEntity.getHospitalName());
						entityNEW.setUpdatedDate(new Date());
						logger.info("completeRefund 返回余款:=====save===");
						mdsRecordsConptionApi.saveMdsRecords(entityNEW);

						logger.info("completeRefund 订单" + result.get("out_trade_no") + "微信退款成功");
						retrunMap.put("LeaseInfo", mdsLeaseInfoEntity);
						logger.info("发送消息模版==completeRefund=satrt============");
						this.sendTemplateMessage(mdsLeaseInfoEntity);
						logger.info("发送消息模版==completeRefund===end==========");
						respVo.setData(retrunMap);
					}
				} catch (Exception e) {
					e.printStackTrace();
					respVo.setCode(201);
					respVo.setMessage("微信退款提交失败");
				}

				return respVo;
			}else{
				respVo.setCode(201);
				respVo.setMessage("退款金额大于收取租赁费用!");
				return respVo;
			}
			
		
		} else {

			respVo.setCode(201);
			respVo.setMessage("退款金额不能为空或小于0!");
			return respVo;
		}

	}


}
