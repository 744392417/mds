package com.spt.mds.open.offer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.encrypt.Digests;
import com.hsoft.commutil.encrypt.Encodes;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;

import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author hanson
 * */
@Api(value = "医院-物业-代理商 管理", description = "医院-物业-代理商 管理")
@RestController
@RequestMapping(value = "/mdsProperty")
public class MdsPropertyController
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	
	
	private static final int SALT_SIZE = 8;
	public static final int HASH_INTERATIONS = 1024;
	/**
	 * TODO 合作方管理  查询 （医院-物业-代理商） 列表
	 * @param QueryVo
	 * @return
	 */
	@ApiOperation(value = "合作方管理  查询 （医院-物业-代理商） 列表")
	@PostMapping(value = "findPropertyListPage")
	public RespVo<Map<String, Object>> findPropertyListPage(@RequestBody QueryVo vo)
	{
		logger.info("角色 管理  查询医院 物业  代理商》》》》》》》》》》》findPropertyListPage");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		// 类型 :医院H 物业 P 代理商 A 平台M
		if (vo.getType() != null && !"".equals(vo.getType())) 
		{
			PageDown<MdsPropertyInfoVo> page = mdsPropertyInfoApi.findPropertyListPage(vo);
			Map<String, Object> map = new HashMap<>(50);
			map.put("page", page);
			respVo.setData(map);
		} else 
		{
			respVo.setCode(201);
			respVo.setMessage(" type不能为空 ");
		}
		return respVo;
	}
	/***
	 * TODO (医院-管理-物业-代理商) ： (编辑 保存 删除)
	 */
	@ApiOperation(value = "(医院-管理-物业-代理商) ： (编辑 保存 删除)")
	@PostMapping(value = "modifyProperty")
	public RespVo<Map<String, Object>> modifyProperty(@RequestBody MdsPropertyInfoVo vo) 
	{
		logger.info("(医院-管理-物业-代理商) ： (编辑 保存 删除)》》》》》》》》》》》modifyProperty");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		// 类型 :医院H 物业 P 代理商 A 平台M
		if (StringUtils.isNotBlank(vo.getHospitalType())) 
		{
			// 操作类型 删除 D 修改 U 新增 A     OperType
			if (StringUtils.isNotBlank(vo.getOperType())) 
			{
				//  操作类型  新增 A
				if (vo.getOperType().equals(HospConstants.OPER_TYPE_A)) 
				{
					String logName = null;
					//logger.info("新增医院名:"+vo.getHospitalName());
					MdsPropertyInfoEntity pertyInfo = mdsPropertyInfoApi.byHospitalName(vo);
					//logger.info("新增医院名:"+(pertyInfo !=null));
					
					if (pertyInfo !=null) {
						respVo.setCode(201);
						respVo.setMessage("医院名已存在");
					}else{
						MdsPropertyInfoEntity entity = new MdsPropertyInfoEntity();
						BeanUtils.copyProperties(vo, entity);
						entity.setStatus(HospConstants.STATUS_1);
//						System.out.println("getAgentId==="+entity.getAgentId());
//						System.out.println("getProperId==="+entity.getProperId());
//						System.out.println("getProperName==="+entity.getProperName());
//						System.out.println("getAgentName==="+entity.getAgentName());
						if(entity.getHospitalDistribution()==null ||entity.getHospitalDistribution() .equals("")){
							entity.setHospitalDistribution(0);
						}
	                    if(entity.getProperDistribution()==null ||entity.getProperDistribution() .equals("")){
	                    	entity.setProperDistribution(0);
						}
	                    if(entity.getAgentDistribution()==null ||entity.getAgentDistribution() .equals("")){
	                    	entity.setAgentDistribution(0);
	                    }
						Integer platDistribution= 100-entity.getHospitalDistribution() - entity.getProperDistribution()
								- entity.getAgentDistribution();
						entity.setPlatDistribution(platDistribution);
						entity = mdsPropertyInfoApi.save(entity);
						
						//获取用户列表 批量保存信息
						List<MdsUserInfoEntity> userInfoList = vo.getUserInfoList();
						
						if(null != userInfoList)
						{
							for (MdsUserInfoEntity mdsUserInfoEntity : userInfoList) 
							{
						//		System.out.println("mdsUserInfoEntity.getLoginName()===>"+mdsUserInfoEntity.getLoginName());
								QueryVo queryVo = new QueryVo();
								queryVo.setLoginName(mdsUserInfoEntity.getLoginName());
								MdsUserInfoEntity userInfo = mdsUserInfoApi.verifyUserName(queryVo);
						//		System.out.println("==="+(userInfo == null));
								//System.out.println("userInfo.getLoginName()===>"+userInfo.getLoginName());
								if (userInfo == null) {									
									entryptPassword(mdsUserInfoEntity);
									
									MdsUserInfoEntity userInfoEntity = new MdsUserInfoEntity();
									userInfoEntity = transMdsUserInfo(entity, vo ,mdsUserInfoEntity);
									userInfoEntity.setSalt(mdsUserInfoEntity.getSalt());
									
									
									userInfoEntity.setUserType(vo.getHospitalType());
						//			System.out.println("save===>");
									mdsUserInfoApi.save(userInfoEntity);
								}else{
									logName = mdsUserInfoEntity.getLoginName()+",";
						//			System.out.println("save=logNamelogNamelogName==>"+logName);
									respVo.setCode(201);
									respVo.setMessage(logName+"账号重复!");
									return respVo;
								}
							}
						}
						if (logName == null) {
							respVo.setCode(200);
							
						}else{
							respVo.setCode(201);
							respVo.setMessage(logName+"账号重复");
							return respVo;
						}
					}	
					return respVo;
				}
				//  操作类型  修改 U 
				if (vo.getOperType().equals(HospConstants.OPER_TYPE_U)) 
				{
					MdsPropertyInfoEntity entityold = mdsPropertyInfoApi.getEntity(vo.getId());
					//BeanUtils.copyProperties(vo, entityold);
					MdsPropertyInfoEntity pertyInfo = mdsPropertyInfoApi.byHospitalName(vo);
					if (pertyInfo !=null && !vo.getHospitalName().equals(entityold.getHospitalName())) {
						respVo.setCode(201);
						respVo.setMessage("医院名已存在");
						return respVo;
					}else{
						
					
					this.transProperty(entityold, vo);
					//设置使用时间段
					entityold.setPropertyUsableTime(vo.getPropertyUsableTime());
					
					Integer plate = entityold.getAgentDistribution()+entityold.getHospitalDistribution()+entityold.getProperDistribution();

					if(plate<=HospConstants.PLATE_DISTRIBUTION){
						Integer platDistribution= 100-entityold.getHospitalDistribution() - entityold.getProperDistribution()
								- entityold.getAgentDistribution();
						entityold.setPlatDistribution(platDistribution);
						
						mdsPropertyInfoApi.save(entityold);
					}else{
						respVo.setCode(201);
						respVo.setMessage("返佣不得超过100%");
						return respVo;
					}
					
					//获取用户列表  批量修改用户信息
					List<MdsUserInfoEntity> userInfoList = vo.getUserInfoList();
					if(null != userInfoList && userInfoList.size()>0)
					{
						int c=0;
						for (MdsUserInfoEntity mdsUserInfoEntity : userInfoList) 
						{
							//如果没有修改密码
					
							if(mdsUserInfoEntity.getLoginPwd()!=null && !mdsUserInfoEntity.getLoginPwd().equals(HospConstants.LOGIN_PWD_NO_UPDATE))
							{
								MdsUserInfoVo mdsUserInfoVo = new MdsUserInfoVo();
								
								QueryVo queryVo = new QueryVo();
								queryVo.setLoginName(mdsUserInfoEntity.getLoginName());
							
					          System.out.println("===getLoginName===="+mdsUserInfoEntity.getLoginName());
					          System.out.println("====getLoginPwd==="+mdsUserInfoEntity.getLoginPwd());
								if (StringUtils.isNotBlank(mdsUserInfoEntity.getLoginName()) && StringUtils.isNotBlank(mdsUserInfoEntity.getLoginPwd())) 
								{
									if(null != mdsUserInfoEntity.getId())
									{
										mdsUserInfoVo.setId(mdsUserInfoEntity.getId());
									}
									
									MdsUserInfoEntity userInfo = mdsUserInfoApi.verifyUserName(queryVo);
									if(userInfo!=null){
										
										 System.out.println("===getLoginName=00==="+userInfo.getLoginName());
										 
										if(userInfo.getHospitalId().equals(entityold.getId()) && (c==0)){
											//登录名
											mdsUserInfoVo.setLoginName(mdsUserInfoEntity.getLoginName());
											//登录密码
											entryptPassword(mdsUserInfoEntity);
											mdsUserInfoVo.setLoginPwd(mdsUserInfoEntity.getLoginPwd());
											mdsUserInfoVo.setSalt(mdsUserInfoEntity.getSalt());
											mdsUserInfoVo.setHospitalId(vo.getId());
											mdsUserInfoVo.setRoleCode(HospConstants.ROLE_MANAGER);
											mdsUserInfoVo.setUserType(vo.getHospitalType());
											mdsUserInfoApi.save(mdsUserInfoVo);
						
											c=c+1;
										}else{
											 System.out.println("===22222==="+userInfo.getLoginName());
											respVo.setCode(201);
											respVo.setMessage("账号重复!");
											return respVo;
										}
						
									//	mdsUserInfoApi.save(mdsUserInfoVo);
									}else{
										
										//登录名
										mdsUserInfoVo.setLoginName(mdsUserInfoEntity.getLoginName());
										//登录密码
										entryptPassword(mdsUserInfoEntity);
										mdsUserInfoVo.setLoginPwd(mdsUserInfoEntity.getLoginPwd());
										mdsUserInfoVo.setSalt(mdsUserInfoEntity.getSalt());
										mdsUserInfoVo.setHospitalId(vo.getId());
										mdsUserInfoVo.setRoleCode(HospConstants.ROLE_MANAGER);
										mdsUserInfoVo.setUserType(vo.getHospitalType());
										mdsUserInfoApi.save(mdsUserInfoVo);
									}
								}else{
									respVo.setCode(201);
									respVo.setMessage("账号密码不能为空!");
								}
					        
					
							}else{
					
								respVo.setCode(200);
							}
					
						}
					
					}
					logger.info(">>>>>>>>>>>>>>>>==="+vo.getPwdDeleteId());
					if (StringUtils.isNotBlank(vo.getPwdDeleteId())) {
						if (vo.getPwdDeleteId().indexOf(",") == -1) {
							mdsUserInfoApi.delete(Long.valueOf(vo.getPwdDeleteId()));
						} else {
							String[] pwdDeleteId = vo.getPwdDeleteId().split(",");
							for (int i = 0; i < pwdDeleteId.length; i++) {
								mdsUserInfoApi.delete(Long.valueOf(pwdDeleteId[i]));
							}

						}
					}
				}
			
				}
				//  操作类型  删除 D
				if (vo.getOperType().equals(HospConstants.OPER_TYPE_D))
				{
					int productInfols = mdsProductInfoApi.findByhospitalId(vo.getId());
					if(productInfols > 0){
						respVo.setCode(201);
						respVo.setMessage("医院有设备不能删除");
					}else{
						MdsPropertyInfoEntity entityDold = mdsPropertyInfoApi.getEntity(vo.getId());
						entityDold.setStatus(HospConstants.STATUS_0);
						mdsPropertyInfoApi.save(entityDold);
						MdsUserInfoVo mdsUserInfoVo = new MdsUserInfoVo();
						mdsUserInfoVo.setHospitalId(entityDold.getId());
						mdsUserInfoVo.setRoleCode(HospConstants.ROLE_MANAGER);
						mdsUserInfoVo.setStatus(HospConstants.STATUS_0);
						mdsUserInfoApi.updateStatus(mdsUserInfoVo);
					}

				}
			} else 
			{
				respVo.setCode(201);
				respVo.setMessage("The operType is null");
			}
		} else 
		{
			respVo.setCode(201);
			respVo.setMessage("The type is null");
		}
		Map<String, Object> map = new HashMap<>(50);
		respVo.setData(map);
		return respVo;
	}
	
	
	
	
	/**
	 * 密码加密
	 * @param mdsUserInfoEntity
	 */
	@SuppressWarnings("mdsUserInfoEntity")
	private void entryptPassword(MdsUserInfoEntity mdsUserInfoEntity) 
	{
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		mdsUserInfoEntity.setSalt(Encodes.encodeHex(salt));
		mdsUserInfoEntity.setLoginPwd(getEncodePwd(mdsUserInfoEntity.getLoginPwd(), salt));
	}

	private String getEncodePwd(String plainPwd, byte[] salt)
	{
		byte[] hashPassword = Digests.sha1(plainPwd.getBytes(), salt, HASH_INTERATIONS);
		return Encodes.encodeHex(hashPassword);
	}
	
	
	
	
	/**
	 * TODO  医院管理 编辑医院返佣比例
	 */
	@ApiOperation(value = "医院管理 编辑医院返佣比例")
	@PostMapping(value = "modifyDistribution")
	public RespVo<Map<String, Object>> modifyDistribution(@RequestBody MdsPropertyInfoVo vo) 
	{
		logger.info("医院管理 编辑医院返佣比例》》》》》》》》》》》modifyDistribution");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		if (vo.getId() != null && !"".equals(vo.getId())) 
		{
			MdsPropertyInfoEntity entityold = mdsPropertyInfoApi.getEntity(vo.getId());
			// 医院名称/代理商名称/物业名称
			if (StringUtils.isNotBlank(vo.getHospitalName())) 
			{
				entityold.setHospitalName(vo.getHospitalName());
			}
			// 返佣类型 医院H,医院+物业HP,医院+代理HA,自营ALL
			if (StringUtils.isNotBlank(vo.getDistributionType())) 
			{
				entityold.setDistributionType(vo.getDistributionType());
			}
			// 所属物业
			if (StringUtils.isNotBlank(vo.getProperName()))
			{
				entityold.setProperName(vo.getProperName());
			}
			// 所属代理商
			if (StringUtils.isNotBlank(vo.getAgentName()))
			{
				entityold.setAgentName(vo.getAgentName());
			}
			// 代理商ID
			if (vo.getAgentId() != null && !"".equals(vo.getAgentId())) 
			{
				entityold.setAgentId(vo.getAgentId());
			}
			// 物业ID
			if (vo.getProperId() != null && !"".equals(vo.getProperId())) 
			{
				entityold.setProperId(vo.getProperId());
			}
			// 医院返佣比例
			if (vo.getHospitalDistribution() != null && !"".equals(vo.getHospitalDistribution())) 
			{
				entityold.setHospitalDistribution(vo.getHospitalDistribution());
			}
			// 物业返佣比例
			if (vo.getProperDistribution() != null && !"".equals(vo.getProperDistribution())) 
			{
				entityold.setProperDistribution(vo.getProperDistribution());
			}
			// 代理商返佣比例
			if (vo.getAgentDistribution() != null && !"".equals(vo.getAgentDistribution())) 
			{
				entityold.setAgentDistribution(vo.getAgentDistribution());
			}
			mdsPropertyInfoApi.save(entityold);
			Map<String, Object> map = new HashMap<>(50);
			respVo.setData(map);
		} else 
		{
			respVo.setCode(201);
			respVo.setMessage("id is null");
		}
		return respVo;
	}
	/**
	 *TODO 私有 : 属性赋值
	 * */
	public MdsPropertyInfoEntity transProperty(MdsPropertyInfoEntity entityold, MdsPropertyInfoVo vo) 
	{
		// 医院名称/代理商名称/物业名称
		if (StringUtils.isNotBlank(vo.getHospitalName())) {
			entityold.setHospitalName(vo.getHospitalName());
		}
		// 医院代理商/代理商地址/物业地址
		if (StringUtils.isNotBlank(vo.getHospitalAddress())) {
			entityold.setHospitalAddress(vo.getHospitalAddress());
		}
		// 医院合作租赁方
		if (StringUtils.isNotBlank(vo.getHospitalDepart())) {
			entityold.setHospitalDepart(vo.getHospitalDepart());
		}
		// 医院资金账户/代理商资金账户/ 物业资金账户
		if (StringUtils.isNotBlank(vo.getCapitalAccount())) {
			entityold.setCapitalAccount(vo.getCapitalAccount());
		}
		// 医院银行名称/代理商银行名称/ 物业银行名称
		if (StringUtils.isNotBlank(vo.getBankName())) {
			entityold.setBankName(vo.getBankName());
		}
		// 医院银行编号/代理商银行编号/ 物业银行编号
		if (StringUtils.isNotBlank(vo.getBankCode())) {
			entityold.setBankCode(vo.getBankCode());
		}
		
		// 医院银行 开户行/代理商银行 开户行/ 物业银行 开户行
		if (StringUtils.isNotBlank(vo.getOpenBankAccount())) {
			entityold.setOpenBankAccount(vo.getOpenBankAccount());
		}
		
		
		// 返佣类型 医院H,医院+物业HP,医院+代理HA,自营ALL
		if (StringUtils.isNotBlank(vo.getDistributionType())) {
			entityold.setDistributionType(vo.getDistributionType());
		}
		// 所属物业
		if (StringUtils.isNotBlank(vo.getProperName())) {
			entityold.setProperName(vo.getProperName());
		}else{
			entityold.setProperName("");
		}
		// 所属代理商
		if (StringUtils.isNotBlank(vo.getAgentName())) {
			entityold.setAgentName(vo.getAgentName());
		}else{
			entityold.setAgentName("");
		}
		// 代理商ID
		if (vo.getAgentId() != null && !"".equals(vo.getAgentId())) {
			entityold.setAgentId(vo.getAgentId());
		}else{
			entityold.setAgentId(Long.valueOf(0));
		}
		
		// 代理商返佣比例
		if (vo.getAgentDistribution() != null && !"".equals(vo.getAgentDistribution())) {
			entityold.setAgentDistribution(vo.getAgentDistribution());
		}else{
			entityold.setAgentDistribution(0);
		}
		// 物业返佣比例
		if (vo.getProperDistribution() != null && !"".equals(vo.getProperDistribution())) {
			entityold.setProperDistribution(vo.getProperDistribution());
		}else{
			entityold.setProperDistribution(0);
		}
		// 医院返佣比例
		if (vo.getHospitalDistribution() != null && !"".equals(vo.getHospitalDistribution())) {
			entityold.setHospitalDistribution(vo.getHospitalDistribution());
		}else{
			entityold.setHospitalDistribution(0);
		}
		// 物业ID
		if (vo.getProperId() != null && !"".equals(vo.getProperId())) {
			entityold.setProperId(vo.getProperId());
		}else{
			entityold.setProperId(Long.valueOf(0));
		}
		
		// 开户人
		if (vo.getContactPerson() != null && !"".equals(vo.getContactPerson())) {
			entityold.setContactPerson(vo.getContactPerson());
		}

		// 运维人员
		if (StringUtils.isNotBlank(vo.getContactPhone())) {
			entityold.setContactPhone(vo.getContactPhone());
		}
		// 运维人员
		if (StringUtils.isNotBlank(vo.getContactTelephone())) {
			entityold.setContactTelephone(vo.getContactTelephone());
		}
		
		

		// 开户人
		if (vo.getAccountName() != null && !"".equals(vo.getAccountName())) {
			entityold.setAccountName(vo.getAccountName());
		}

		// 运维人员
		if (StringUtils.isNotBlank(vo.getOperationStaff())) {
			entityold.setOperationStaff(vo.getOperationStaff());
		}

		// 运营人员
		if (StringUtils.isNotBlank(vo.getOperatingPerson())) {
			entityold.setOperatingPerson(vo.getOperatingPerson());
		}

		// 运营人员联系方式
		if (StringUtils.isNotBlank(vo.getOperatingPhone())) {
			entityold.setOperatingPhone(vo.getOperatingPhone());
		}

		// 运维联系方式
		if (StringUtils.isNotBlank(vo.getOperationPhone())) {
			entityold.setOperationPhone(vo.getOperationPhone());
		}

		// 省
		if (StringUtils.isNotBlank(vo.getProvinceName())) {
			entityold.setProvinceName(vo.getProvinceName());
		}

		// 市
		if (StringUtils.isNotBlank(vo.getCityName())) {
			entityold.setCityName(vo.getCityName());
		}

		// 县
		if (StringUtils.isNotBlank(vo.getCountyName())) {
			entityold.setCountyName(vo.getCountyName());
		}
		return entityold;
	}
	/**
	 *TODO 私有 : 用户属性赋值
	 * @param mdsUserInfoEntity 
	 * */
	public MdsUserInfoEntity transMdsUserInfo(MdsPropertyInfoEntity entity, MdsPropertyInfoVo vo, MdsUserInfoEntity mdsUserInfoEntity) 
	{
		MdsUserInfoEntity entityNew = new MdsUserInfoEntity();
		//登录名
		if (StringUtils.isNotBlank(mdsUserInfoEntity.getLoginName())) 
		{
			entityNew.setLoginName(mdsUserInfoEntity.getLoginName());
		}
		//登录密码
		if (StringUtils.isNotBlank(mdsUserInfoEntity.getLoginPwd())) 
		{
			entityNew.setLoginPwd(mdsUserInfoEntity.getLoginPwd());
		}
		// 医院H 物业P 代理商A 平台 M 用户 U
		if (vo.getHospitalType().equals(HospConstants.PROPERTY_TYPE_H)) 
		{
			entityNew.setUserType(HospConstants.USER_TYPE_H);
		} 
		else if (vo.getHospitalType().equals(HospConstants.PROPERTY_TYPE_A)) 
		{
			entityNew.setUserType(HospConstants.USER_TYPE_A);
		} else if (vo.getHospitalType().equals(HospConstants.PROPERTY_TYPE_P)) 
		{
			entityNew.setUserType(HospConstants.USER_TYPE_P);
		} else 
		{
			entityNew.setUserType(HospConstants.USER_TYPE_M);
		}
		//权限级别
		entityNew.setRoleCode(HospConstants.ROLE_MANAGER);
		entityNew.setHospitalId(entity.getId());
		entityNew.setHospitalName(entity.getHospitalName());
		entityNew.setHospitalAddress(entity.getHospitalAddress());
		//状态
		entityNew.setStatus(HospConstants.STATUS_1);
		return entityNew;
	}
//	public static void main(String[] args) {
//		String str="ABC_001"; 
//		if(str.indexOf("C")!=-1){  
//		System.out.println("包含"); 
//		}else{ System.out.println("不包含"); 
//		} 
//	}
}
