package com.spt.mds.open.vo;

public class UserInfo {
	private String userId;
	private String userName;
	private Long companyId;
	private String companyUuid;
	private String roleCd;
	private String deptId;
	private String userUuid;
	private String companyType;
	private String companyName;
	private String status;
	private String wxOpenId;
	private String url;
	private String cfcaStatus;
	private String cfcaUkRealSerialNo;
	private String basUrl;
	private String registerStatus; 
	private String loginName;
	private String isfashionable;
	public String getIsfashionable() {
		return isfashionable;
	}

	public void setIsfashionable(String isfashionable) {
		this.isfashionable = isfashionable;
	}

	public String getRegisterStatus() {
		return registerStatus;
	}

	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}


	
	public String getBasUrl() {
		return basUrl;
	}

	public void setBasUrl(String basUrl) {
		this.basUrl = basUrl;
	}

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyUuid() {
		return companyUuid;
	}

	public void setCompanyUuid(String companyUuid) {
		this.companyUuid = companyUuid;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCfcaStatus() {
		return cfcaStatus;
	}

	public void setCfcaStatus(String cfcaStatus) {
		this.cfcaStatus = cfcaStatus;
	}

	public String getCfcaUkRealSerialNo() {
		return cfcaUkRealSerialNo;
	}

	public void setCfcaUkRealSerialNo(String cfcaUkRealSerialNo) {
		this.cfcaUkRealSerialNo = cfcaUkRealSerialNo;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

}