package com.spt.mds.open.offer;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spt.hospital.client.api.IMdsHospitalPriceApi;
import com.spt.hospital.client.api.IMdsInteractiveLogApi;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductCategoryApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsProductPwdApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsRepairInfoApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsInteractiveLogEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.MdsProductCategoryVo;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.MdsProductPwdVo;
import com.spt.hospital.client.vo.MdsRepairInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.ReceiveProductVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;
import com.spt.hospital.client.vo.product.ProductRepairInfoVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * @author hanson
 */
@Api(value = "设备管理", description = "设备管理")
@RestController
@RequestMapping(value = "/mdsProduct")
public class MdsProductController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	@Autowired
	private IMdsProductPwdApi mdsProductPwdApi;
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsProductCategoryApi mdsProductCategoryApi;
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	@Autowired
	private IMdsInteractiveLogApi mdsInteractiveLogApi;
	@Autowired
	private IMdsRepairInfoApi mdsRepairInfoApi;

	@Autowired
	private IMdsHospitalPriceApi mdsHospitalPriceApi;

	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;

	@Value("${fileurl}")
	private String filePath;

	/**
	 * TODO 推送设备消息接口 设备版本升级请求推送
	 */
	@ApiOperation(value = "推送设备消息接口")
	@PostMapping(value = "sendProductNews")
	public RespVo<Map<String, Object>> sendProductNews(@RequestBody ReceiveProductVo receiveProductVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		logger.info("推送设备消息>>>>>>>>>>>>>>>>>>>sendProductNews");
		List<MdsProductInfoEntity> productList = receiveProductVo.getProductList();
		for (MdsProductInfoEntity mdsProductInfoEntity : productList) {
			/* 校验接收的数据 */
			if (StringUtils.isNotBlank(mdsProductInfoEntity.getProductCode())) {
				// 添加交互日志
				MdsInteractiveLogEntity logEntity = new MdsInteractiveLogEntity();
				BeanUtils.copyProperties(receiveProductVo, logEntity);
				logEntity.setInteractiveTime(new Date());
				mdsInteractiveLogApi.save(logEntity);
				// 交互类型为 请求版本升级
				if (HospConstants.INTERACTIVE__VERSION_SEND.equals(receiveProductVo.getInteractiveType())) {
					// 推送版本升级请求
					mdsProductInfoApi.sendVersionUpgradeNews(receiveProductVo);
				}
			}
		}
		return respVo;
	}

	/**
	 * TODO 接收设备消息接口 设备版本升级成功或失败 设备状态变更
	 */
	@ApiOperation(value = "接收设备消息接口")
	@PostMapping(value = "receiveProductNews")
	public RespVo<Map<String, Object>> receiveProductNews(@RequestBody ReceiveProductVo receiveProductVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		logger.info("接收消息信息>>>>>>>>>>>>>>>>>>>receiveProductNews");
		List<MdsProductInfoEntity> productList = receiveProductVo.getProductList();
		for (MdsProductInfoEntity mdsProductInfoEntity : productList) {
			/* 校验接收的数据 */
			if (StringUtils.isNotBlank(mdsProductInfoEntity.getProductCode())) {
				// 查询数据库中该Code对应的实体
				QueryVo queryVo = new QueryVo();
				queryVo.setProductCode(mdsProductInfoEntity.getProductCode());
				MdsProductInfoEntity findByCodeEntity = mdsProductInfoApi.findByCode(queryVo);
				// 添加交互日志
				MdsInteractiveLogEntity logEntity = new MdsInteractiveLogEntity();
				BeanUtils.copyProperties(mdsProductInfoEntity, logEntity);
				logEntity.setInteractiveTime(new Date());
				mdsInteractiveLogApi.save(logEntity);
				// 交互类型为 接收设备状态
				if (HospConstants.INTERACTIVE_RECEIVE_STATUS.equals(receiveProductVo.getInteractiveType())) {
					// 修改设备数据
					MdsProductInfoEntity updateStatusEntity = mdsProductInfoApi.getEntity(findByCodeEntity.getId());
					updateStatusEntity.setProductStatus(mdsProductInfoEntity.getProductStatus());
					updateStatusEntity.setUpdatedDate(new Date());
					mdsProductInfoApi.save(updateStatusEntity);
				}
				// 交互类型为 接收版本升级
				if (HospConstants.INTERACTIVE_VERSION_RECEIVE.equals(receiveProductVo.getInteractiveType())) {
					// 修改设备数据
					MdsProductInfoEntity upgradeEntity = mdsProductInfoApi.getEntity(findByCodeEntity.getId());
					upgradeEntity.setSuccessUpgradeDate(new Date());
					upgradeEntity.setProductVersion(mdsProductInfoEntity.getProductVersion());
					mdsProductInfoApi.save(upgradeEntity);
				}
			}
		}
		return respVo;
	}

	/***
	 * TODO 设备管理
	 */
	@ApiOperation(value = "设备管理-我的设备 列表查询")
	@PostMapping(value = "findProductCategoryPage")
	public RespVo<Map<String, Object>> findProductCategoryPage(@RequestBody QueryVo vo) {
		logger.info("设备 管理  我的设备>>>>>>>>>>>>>>>>>>>>findProductCategoryPage");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Page<MdsProductCategoryVo> page = mdsProductCategoryApi.findPageProductCategory(vo);
		Map<String, Object> map = new HashMap<>(50);
		map.put("page", page);
		respVo.setData(map);
		return respVo;
	}

	/***
	 * TODO 我的设备-设备详情-报修信息
	 */
	@ApiOperation(value = "我的设备-设备详情-报修信息 必传:(设备code):productCode")
	@PostMapping(value = "ProductRepairInfo")
	public RespVo<Map<String, Object>> ProductRepairInfo(@RequestBody ProductRepairInfoVo productRepairInfoVo) {
		logger.info("我的设备-设备详情-报修信息 >>>>>>>>>>>>>>>>>>>>ProductRepairInfo");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<String, Object>();
		// 如果传入参数正确
		Boolean isNull = (null != productRepairInfoVo && StringUtils.isNotBlank(productRepairInfoVo.getProductCode()));
		if (isNull) {
			// 查出来当前设备信息
			QueryVo vo = new QueryVo();
			vo.setProductId(productRepairInfoVo.getId());
			vo.setProductCode(productRepairInfoVo.getProductCode());
			MdsProductInfoEntity findByCodeEntity = mdsProductInfoApi.findByCode(vo);
			// 如果当前设备不为空，且使用状态为报修状态
			if (null != findByCodeEntity) {
				if (HospConstants.PRODUCT_USE_STATUS_5.equals(findByCodeEntity.getProductUseStatus())) {
					// 查询报修表中该设备的信息并返回
					MdsRepairInfoEntity repairInfoEntity = mdsRepairInfoApi.findByProductCode(vo);
					MdsRepairInfoVo repairVo = new MdsRepairInfoVo();
					BeanUtils.copyProperties(repairInfoEntity, repairVo);
					repairVo.setFileImageUrl(filePath + repairVo.getFileId());
					MdsUserInfoEntity userInfo = mdsUserInfoApi.findByOpenid(repairInfoEntity.getOpenid());
					repairVo.setUserName(userInfo.getUserName());
					repairVo.setUserPhone(userInfo.getUserPhone());
					if (null != repairInfoEntity) {
						map.put("repairInfo", repairVo);
						respVo.setData(map);
					}
				}
			}
		}
		return respVo;
	}

	

	
	/***
	 * TODO 我的设备-提交保修信息
	 */
	@ApiOperation(value = "我的设备-提交报修信息")
	@PostMapping(value = "submitRepairInfo")
	public RespVo<Map<String, Object>> submitRepairInfo(@RequestBody QueryVo queryVo) {
		logger.info("我的设备-提交报修信息 >>>>>>>>>>>>>>>>>>>>submitRepairInfo");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<String, Object>();
		if (queryVo.getToken() != null) {

			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				MdsLeaseInfoEntity leaseEntity = mdsLeaseInfoApi.findByOrderNumber(queryVo);

				if (leaseEntity != null) {

					MdsRepairInfoEntity repairEntity = new MdsRepairInfoEntity();

					MdsProductInfoEntity productEntity = mdsProductInfoApi.getEntity(leaseEntity.getProductId());

					MdsPropertyInfoEntity hospitalEntity = mdsPropertyInfoApi.getEntity(leaseEntity.getHospitalId());

					repairEntity.setOrderNumber(queryVo.getOrderNumber());

					String repairNumber = "R" + DateParser.formatDateSS(new Date());
					repairEntity.setRepairNumber(repairNumber);

					if (StringUtils.isNotBlank(queryVo.getReason())) {
						repairEntity.setReason(queryVo.getReason());
					}
					if (StringUtils.isNotBlank(queryVo.getRemark())) {
						repairEntity.setRemark(queryVo.getRemark());// 描述
					}

					if (StringUtils.isNotBlank(queryVo.getFileId())) {
						repairEntity.setFileId(queryVo.getFileId());// 图片
					}

					repairEntity.setCreatedDate(new Date());
					repairEntity.setRepairDate(new Date());
					repairEntity.setProductCode(leaseEntity.getProductCode());

					repairEntity.setProductId(String.valueOf(leaseEntity.getProductId()));

					repairEntity.setCategoryId(leaseEntity.getCategoryId());

					repairEntity.setCategoryName(leaseEntity.getCategoryName());

					repairEntity.setHospitalId(leaseEntity.getHospitalId());

					repairEntity.setHospitalName(leaseEntity.getHospitalName());

					repairEntity.setHospitalAddress(hospitalEntity.getHospitalAddress());

					if (StringUtils.isNotBlank(hospitalEntity.getHospitalDepart())) {
						repairEntity.setHospitalDepart(leaseEntity.getHospitalDepart());
					}
					if (StringUtils.isNotBlank(hospitalEntity.getHospitalWard())) {
						repairEntity.setHospitalWard(productEntity.getHospitalWard());
					}

					if (StringUtils.isNotBlank(user.getUserPhone())) {
						repairEntity.setMobilePhone(user.getUserPhone());
					}else{
						repairEntity.setMobilePhone(queryVo.getMobilePhone());
					}
					repairEntity.setOpenid(user.getOpenid());

					mdsRepairInfoApi.save(repairEntity);
					// 设备状态报修
					// productEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
					productEntity.setProductStatus(HospConstants.PRODUCT_STATUS_5);
					mdsProductInfoApi.save(productEntity);
					// 订单设备状态报修
					leaseEntity.setOrderStatus(HospConstants.ORDER_STATUS_4);// 报修中
					leaseEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
					leaseEntity.setUpdatedDate(new Date());
					mdsLeaseInfoApi.save(leaseEntity);

					return respVo;

				} else {

					respVo.setCode(201);
					respVo.setMessage("订单不存在");
					return respVo;
				}

			} else {

				respVo.setCode(201);
				respVo.setMessage("用户信息不存在");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}

	}

	/***
	 * TODO 设备 - 品类管理 新增+修改+删除
	 */
	@ApiOperation(value = "设备 - 品类管理 新增+修改+删除")
	@PostMapping(value = "productCatgoryManager")
	public RespVo<Map<String, Object>> productCatgoryManager(@RequestBody MdsProductCategoryVo vo) {
		logger.info("设备管理-品类  新增+修改+删除>>>>>>>>>>>>>>>>>>>>productCatgoryManager");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		// 如果操作状态不为空
		if (StringUtils.isNotBlank(vo.getOperType())) {
			// A 新增
			if (vo.getOperType().equals(HospConstants.OPER_TYPE_A)) {
				if (StringUtils.isNotBlank(vo.getCategoryName())) {
					MdsProductCategoryEntity entityold = mdsProductCategoryApi.findByName(vo);
					if (entityold == null) {
						MdsProductCategoryEntity entitynew = new MdsProductCategoryEntity();
						entitynew.setCategoryName(vo.getCategoryName());
						if (StringUtils.isNotBlank(vo.getCategoryFileId())) {
							entitynew.setCategoryFileId(vo.getCategoryFileId());
						}
						entitynew.setStatus(HospConstants.STATUS_1);
						mdsProductCategoryApi.save(entitynew);
					} else {
						respVo.setCode(201);
						respVo.setMessage(" categoryName 已存在");
					}
				} else {
					respVo.setCode(201);
					respVo.setMessage(" categoryName IS NULL");
				}
			}
			// U 修改
			if (vo.getOperType().equals(HospConstants.OPER_TYPE_U)) {
				// 修改根据ID
				MdsProductCategoryEntity entityName = mdsProductCategoryApi.getEntity(vo.getId());
				if (entityName != null) {
//					MdsProductCategoryEntity updateEntity = new MdsProductCategoryEntity();
//					BeanUtils.copyProperties(vo, updateEntity);
			
					if(StringUtils.isNotBlank(vo.getCategoryName())){
						MdsProductCategoryEntity categoryName = mdsProductCategoryApi.findByName(vo);
						if(categoryName!=null && categoryName.getId()!=entityName.getId()){
							respVo.setCode(201);
							respVo.setMessage("类别名不能重复!");
						}else{
							entityName.setCategoryName(vo.getCategoryName());
							if(StringUtils.isNotBlank(vo.getCategoryFileId())){
								entityName.setCategoryFileId(vo.getCategoryFileId());
							}
							if(StringUtils.isNotBlank(vo.getCategoryCode())){
								entityName.setCategoryCode(vo.getCategoryCode());
							}
							entityName.setUpdatedDate(new Date());
							mdsProductCategoryApi.save(entityName);
							QueryVo queryVoCate = new QueryVo();
							queryVoCate.setCategoryFileId(entityName.getCategoryFileId());
							queryVoCate.setCategoryName(entityName.getCategoryName());
							queryVoCate.setCategoryId(entityName.getId());
							mdsProductCategoryApi.updateAllCategoryInfo(queryVoCate);
						}
						
					}
					
					
				} else {
					respVo.setCode(201);
					respVo.setMessage("类别不存在!");
				}
			}
			// D 删除
			if (vo.getOperType().equals(HospConstants.OPER_TYPE_D)) {
				// 删除根据ID
				int i = mdsProductCategoryApi.equipmentNumber(vo.getId());
				if (i > 0) {
					respVo.setCode(201);
					respVo.setMessage("此设备类型下仍有设备，无法删除此类型");
				} else {
					MdsProductCategoryEntity entityold = mdsProductCategoryApi.getEntity(vo.getId());
					entityold.setStatus(HospConstants.STATUS_0);
					entityold.setId(vo.getId());
					mdsProductCategoryApi.save(entityold);
				}
			}
		} else {
			respVo.setCode(201);
			respVo.setMessage("operType is null");
		}
		return respVo;
	}

	/***
	 * TODO 设备管理 设备详情
	 */
	@ApiOperation(value = "我的设备-设备详情 列表查询  必传:(设备类别ID)categoryId", notes = "查询参数：设备状态：productStatus，设备使用状态：productUseStatus，所在科室：hospitalDepart，"
			+ "所在医院：hospitalName，设备ID等关键字：productCode，开始时间：startDate，结束时间：endDate")
	@PostMapping(value = "findProductPageList")
	public RespVo<Map<String, Object>> findProductPageList(@RequestBody QueryVo vo) {
		logger.info("设备详情 列表>>>>>>>>>>>>>>>>>>>>findProductPageList");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		
		if (vo.getCategoryId() != null && !"".equals(vo.getCategoryId())) {
			System.out.println(vo.getMarking());
			if (vo.getMarking() == 1) {
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);//备选
			}
			if (vo.getMarking() == 2) {
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);//空闲
			}
			if (vo.getMarking() == 3) {
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);//暂停
			}
			if (vo.getMarking() == 4) {
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);//使用中
			}
			if (vo.getMarking() == 5) {
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_5);//报修
			}
			if (vo.getMarking() == 6) {
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_2);//异常
			}
			if (vo.getMarking() == 7) {
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_3);//低电量 
			}
			if (vo.getMarking() == 8) {
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_4);//余额不足
			}
			if (vo.getMarking() == 9) {
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);//维修中
			}
			if (vo.getStartDate() !=null) {
				Calendar calendar = Calendar.getInstance();  
				calendar.setTime(vo.getStartDate());  
				//calendar.add(Calendar.DATE, -1);
				vo.setStartDate(calendar.getTime());
			}
			if (vo.getEndDate() !=null) {
				Calendar calendars = Calendar.getInstance();  
				calendars.setTime(vo.getEndDate());  
				calendars.add(Calendar.DATE, 1);
				vo.setEndDate(calendars.getTime());
			}
			Page<ProductInfoByCategoryVo> page = mdsProductInfoApi.findProductInfoListPage(vo);
			Map<String, Object> map = new HashMap<>(50);
			map.put("page", page);
			respVo.setData(map);
		} else {
			respVo.setCode(201);
			respVo.setMessage("categoryId is null");
		}
		return respVo;
	}

	/***
	 * TODO '运维人员' 通过小程序添加设备
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员 通过小程序添加设备")
	@PostMapping(value = "addProduct")
	public RespVo<Map<String, Object>> addProduct(@RequestBody QueryVo queryVo) {
		logger.info("'运维人员' 通过小程序添加设备>>>>>>>>>>>>>>>>>>>>addProduct");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		// 设备编码，设备二维码
		if (queryVo.getToken() != null) {

			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				MdsProductInfoEntity entity = mdsProductInfoApi.findByCode(queryVo);
				if (entity != null) {
					respVo.setCode(201);
					respVo.setMessage("设备ID已存在!");
					return respVo;
				} else {
					MdsProductCategoryEntity categroyEntity = mdsProductCategoryApi.getEntity(queryVo.getCategoryId());// 目前只有一项

					MdsProductInfoEntity entityn = new MdsProductInfoEntity();
					entityn.setProductCode(queryVo.getProductCode());
					entityn.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);

					if (categroyEntity != null && StringUtils.isNotBlank(categroyEntity.getCategoryName())) {
						entityn.setCategoryName(categroyEntity.getCategoryName());
						entityn.setCategoryId(categroyEntity.getId());
						entityn.setFileId(categroyEntity.getCategoryFileId());
						mdsProductInfoApi.save(entityn);
						return respVo;
					} else {
						respVo.setCode(201);
						respVo.setMessage("设备类别为空");
						return respVo;
					}
				}
			} else {
				respVo.setCode(300);
				respVo.setMessage("token不正确");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}

	}

	/***
	 * TODO 运维人员 通过小程序设备信息
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员 通过小程序设备信息")
	@PostMapping(value = "findProductDetail")
	public RespVo<Map<String, Object>> findProductDetail(@RequestBody QueryVo queryVo) {
		logger.info("运维人员 通过小程序设备信息>>>>>>>>>>>>>>>>>>findProductDetail");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getToken() != null) {

			if (StringUtils.isNotBlank(queryVo.getProductQrcode())) {
				MdsProductInfoVo productVo = mdsProductInfoApi.findByQrcode(queryVo);
				if (productVo != null && StringUtils.isNotBlank(productVo.getProductCode())) {
					map.put("productInfo", productVo);
					respVo.setData(map);
					return respVo;
				} else {

					respVo.setCode(201);
					respVo.setMessage("没有二维码相关设备");
					return respVo;

				}

			} else {
				respVo.setCode(201);
				respVo.setMessage("设备二维码不能为空");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}

	}

	/***
	 * TODO 运维人员 通过小程序添加设备
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员 通过小程序查看绑定设备ID和二维码")
	@PostMapping(value = "findProduct")
	public RespVo<Map<String, Object>> findProduct(@RequestBody QueryVo queryVo) {
		logger.info("运维人员通过小程序绑定设备ID和二维码>>>>>>>>>>>>>>>>>>findProduct");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getToken() != null) {

			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				// 设备编码，
				if (StringUtils.isNotBlank(queryVo.getProductCode())) {
					MdsProductInfoEntity entity = mdsProductInfoApi.findByCode(queryVo);
					if (entity != null) {
						if (StringUtils.isBlank(entity.getProductQrcode())) {
							respVo.setCode(200);
							map.put("productInfo", entity);
							respVo.setData(map);
							return respVo;
						} else {
							respVo.setCode(200);
							map.put("productInfo", entity);
							respVo.setData(map);
							respVo.setMessage("设备ID 已绑定");
						}
					} else {
						respVo.setCode(201);
						respVo.setMessage("该设备未入库请先入库");
						return respVo;
					}
				}

				if (StringUtils.isNotBlank(queryVo.getProductQrcode())) {
					MdsProductInfoEntity productVo = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
					if (productVo != null) {
						if (StringUtils.isNotBlank(productVo.getProductCode())) {
							respVo.setCode(200);
							map.put("productInfo", productVo);
							respVo.setData(map);
							respVo.setMessage("设备二维码已绑定");
							return respVo;
						} else {
							return respVo;
						}

				
					} else {
						respVo.setCode(200);
					    //map.put("productInfo", productVo);
						respVo.setMessage("设备二维码未绑定");
						return respVo;
					}
				}

			} else {
				respVo.setCode(300);
				respVo.setMessage("token不正确");
				return respVo;
			}

		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}
		return respVo;

	}

	/***
	 * TODO 运维人员 通过小程序绑定设备ID和二维码
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员 通过小程序绑定设备ID和二维码")
	@PostMapping(value = "bindingProduct")
	public RespVo<Map<String, Object>> bindingProduct(@RequestBody QueryVo queryVo) {
		logger.info("运维人员通过小程序绑定设备ID和二维码>>>>>>>>>>>>>>>>>>bindingProduct");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getToken() != null) {

			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {
				if (StringUtils.isNotBlank(queryVo.getProductCode())
						&& StringUtils.isNotBlank(queryVo.getProductQrcode())) {
					
					MdsProductInfoEntity entity = mdsProductInfoApi.findByCode(queryVo);
					if(entity!=null){
						MdsProductInfoEntity productVo = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
						if (productVo != null && StringUtils.isNotBlank(productVo.getProductCode())) {
							respVo.setCode(201);
							respVo.setMessage("二维码已使用");
							return respVo;
						} else {
							entity.setProductQrcode(queryVo.getProductQrcode());
							entity.setSerial(Long.valueOf(queryVo.getProductQrcode()));
							mdsProductInfoApi.save(entity);
						}

					}else {
						respVo.setCode(201);
						respVo.setMessage("设备ID没入库");
						return respVo;
					}
       		} else {
					respVo.setCode(201);
					respVo.setMessage("二维码 设备ID 为空");
					return respVo;
				}

			} else {
				respVo.setCode(201);
				respVo.setMessage("用户信息不存在");
				return respVo;
			}
		} else {
			respVo.setCode(300);
			respVo.setMessage("token为空");
			return respVo;
		}
		return respVo;
	}

	/***
	 * TODO 运维人员 通过小程序绑定设备ID和二维码
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员 通过小程序测试设备")
	@PostMapping(value = "findTestProduct")
	public RespVo<Map<String, Object>> findTestProduct(@RequestBody QueryVo queryVo) {
		logger.info("运维人员通过小程序绑定设备ID和二维码>>>>>>>>>>>>>>>>>>findTestProduct");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getToken() != null) {
			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {

				MdsProductInfoEntity productInfoEntity = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
				MdsProductInfoVo productInfoVo = new MdsProductInfoVo();
				BeanUtils.copyProperties(productInfoEntity, productInfoVo);
				// MdsProductPwdEntity productPwd =
				// mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
				// productInfoVo.setUnlockPwd(productPwd.getUnlockPwd());
				//logger.info("getProductCode======" + productInfoEntity.getProductCode());
				//logger.info("getProductQrcode====" + productInfoEntity.getProductQrcode());
				MdsProductPwdVo productPwdVo = new MdsProductPwdVo();
				productPwdVo.setOpenid(user.getLoginName());
				productPwdVo.setProductCode(productInfoEntity.getProductCode());
				productPwdVo.setProductQrcode(productInfoEntity.getProductQrcode());
				MdsProductPwdEntity productPwdEntity = mdsProductPwdApi.getPushKey(productPwdVo);
				//logger.info("getUnlockPwd====" + productPwdEntity.getUnlockPwd());
				productInfoVo.setUnlockPwd(productPwdEntity.getUnlockPwd());
				map.put("productInfoVo", productInfoVo);
				respVo.setData(map);
			}
		} else {
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;
		}

		return respVo;
	}

	/***
	 * TODO 运维人员 绑定医院
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = "运维人员  绑定医院")
	@PostMapping(value = "saveProductHospital")
	public RespVo<Map<String, Object>> saveProductHospital(@RequestBody QueryVo queryVo) {
		logger.info("运维人员  绑定医院>>>>>>>>>>>>>>>>>>saveProductHospital");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getToken() != null) {
			MdsUserInfoEntity user = mdsUserInfoApi.findByToken(queryVo.getToken());
			if (user != null) {

				if (StringUtils.isBlank(queryVo.getProductQrcode())) {

					respVo.setCode(201);
					respVo.setMessage("二维码为空");
					return respVo;

				}
				if (StringUtils.isBlank(queryVo.getHospitalDepart())) {

					respVo.setCode(201);
					respVo.setMessage("医院为空");
					return respVo;

				}

				MdsProductInfoEntity entity = mdsProductInfoApi.findProductInfoByQrcode(queryVo);

				MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoApi.getEntity(queryVo.getHospitalId());
				entity.setHospitalId(propertyInfo.getId());
				entity.setHospitalName(propertyInfo.getHospitalName());
				entity.setHospitalDepart(queryVo.getHospitalDepart());
				entity.setHospitalWard(queryVo.getHospitalWard());
				queryVo.setCategoryId(entity.getCategoryId());
				MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceApi.findPrice(queryVo);
				
				if (hospitalPrice != null) {
					if (isUseProduct(hospitalPrice)) {
						entity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
					}else{
						entity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
					}
				
				}else{
					entity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
				}
				
				
				
				
				mdsProductInfoApi.save(entity);
				if (StringUtils.isNotBlank(queryVo.getHospitalWard())) {
					propertyInfo.setHospitalWard(queryVo.getHospitalWard());
					mdsPropertyInfoApi.save(propertyInfo);
				}
				respVo.setData(map);
			}
		} else {
			respVo.setCode(300);
			respVo.setMessage("token不正确");
			return respVo;
		}

		return respVo;
	}
	
	

	/***
	 *小程序入库判断是否符合改变空闲条件
	 * 
	 * @param 
	 * @return
	 */
	public boolean isUseProduct(MdsHospitalPriceEntity hospitalPrice){
		if((hospitalPrice.getLeaseDeposit().compareTo(BigDecimal.ZERO)==1)
				&& (hospitalPrice.getLeaseUnit().compareTo(BigDecimal.ZERO)==1)
				&& (hospitalPrice.getMinHour() >0)){
			return true;
		}else{
			return false;
		}
	}

	/***
	 * TODO 设备 修改+删除+添加
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = " 设备  修改+删除+添加", notes = "必传：operType (添加A，修改U，删除D) productId(设备Id)")
	@PostMapping(value = "modifyProductInfo")
	public RespVo<Map<String, Object>> modifyProductInfo(@RequestBody QueryVo queryVo) {
		logger.info("设备  修改+删除>>>>>>>>>>>>>>>>>>>>modifyProductInfo");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		if (null != queryVo && StringUtils.isNotBlank(queryVo.getOperType())) {
		
			// 添加
			if (HospConstants.OPER_TYPE_A.equals(queryVo.getOperType())) {

				MdsProductInfoEntity ProductInfos = mdsProductInfoApi.findProductInfoByQrcode(queryVo);
				if (ProductInfos != null) {
					respVo.setCode(201);
					respVo.setMessage("二维码ID或设备ID已存在!");
				} else {
					MdsProductCategoryEntity productCategory = mdsProductCategoryApi.getEntity(queryVo.getCategoryId());
					MdsProductInfoEntity entity = new MdsProductInfoEntity();
					entity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);
					entity.setProductQrcode(queryVo.getProductQrcode());
					entity.setProductCode(queryVo.getProductCode());
					entity.setProductVersion("1.0.0");
					entity.setHospitalName(queryVo.getHospitalName());
					entity.setHospitalDepart(queryVo.getHospitalDepart());
					entity.setHospitalId(queryVo.getHospitalId());
					entity.setCategoryId(queryVo.getCategoryId());
					entity.setCategoryName(productCategory.getCategoryName());
					entity.setFileId(productCategory.getCategoryFileId());
					entity.setSerial(Long.valueOf(queryVo.getProductQrcode()));
					mdsProductInfoApi.save(entity);
					respVo.setCode(200);
					respVo.setMessage("设备添加成功!");
				}
			}
			// 修改 前端传值：空闲2 暂停3
			if (HospConstants.OPER_TYPE_U.equals(queryVo.getOperType())) {
				if (HospConstants.PRODUCT_USE_STATUS_2.equals(queryVo.getProductUseStatus())) {
					// 变空闲
					BigDecimal A = new BigDecimal(0.00);
					MdsProductInfoEntity entityS = mdsProductInfoApi.getEntity(queryVo.getProductId());
					queryVo.setCategoryId(entityS.getCategoryId());
					//System.out.println(">>>>>>>>>>>>>>>==="+entityS.getCategoryId());
					logger.info(">>>>>>>>>>>>>>>==="+entityS.getCategoryId());
					//System.out.println(">>>>getHospitalId>>>>>>>>>>>==="+entityS.getHospitalId());
					//logger.info(">>>>>>>>>>>.getCategoryId()>>>>==="+queryVo.getCategoryId());
					//logger.info(">>>>>>>>>>>.getHospitalId()>>>>==="+queryVo.getHospitalId());
					//System.out.println(">>>>getHospitalId>>>>>>>>>>>==="+queryVo.getHospitalId());
					//System.out.println(">>>>getHospitalId>>>>>>>>>>>==="+queryVo.getHospitalId());
					MdsHospitalPriceEntity hospit = mdsHospitalPriceApi.findPrice(queryVo);
					if (hospit != null) {
						
						if (A.compareTo(hospit.getLeaseDeposit()) == 0 || A.compareTo(hospit.getLeaseUnit()) == 0
								|| hospit.getMinHour() == 0) {
							respVo.setCode(201);
							respVo.setMessage("请完善收费价格!");
						} else {
							//entity = mdsProductInfoApi.getEntity(queryVo.getProductId());
							entityS.setProductUseStatus(queryVo.getProductUseStatus());
							mdsProductInfoApi.save(entityS);
						}
					} else {

						respVo.setCode(201);
						respVo.setMessage("没有设置收费规则!");

					}
				} else {
					// 变暂停
					logger.info(">>>>>>>>>>modifyProductInfo"+queryVo.getProductId());
					MdsProductInfoEntity	entityX = mdsProductInfoApi.getEntity(queryVo.getProductId());
					entityX.setProductUseStatus(queryVo.getProductUseStatus());
					if(queryVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_1)){
			            entityX.setHospitalName(null);
			            entityX.setHospitalId(null);
			            entityX.setHospitalWard(null);
			            entityX.setHospitalDepart(null);
			          }
					mdsProductInfoApi.save(entityX);
					respVo.setCode(200);
					respVo.setMessage("修改成功!");

				}

			}

			// 删除
			if (HospConstants.OPER_TYPE_D.equals(queryVo.getOperType())) {
				MdsProductInfoEntity	entityD = mdsProductInfoApi.getEntity(queryVo.getProductId());
				// BeanUtils.copyProperties(queryVo, entity);
				entityD.setId(queryVo.getProductId());
				entityD.setProductStatus(HospConstants.PRODUCT_STATUS_DELETE);
				entityD.setProductUseStatus(HospConstants.PRODUCT_STATUS_DELETE);
				mdsProductInfoApi.save(entityD);

			}
		} else {
			respVo.setCode(201);
			respVo.setMessage("不能为空!");
		}
		return respVo;
	}

	/***
	 * TODO 设备报修处理
	 * 
	 * @param vo
	 * @return
	 */
	@ApiOperation(value = " 设备报修处理", notes = "必传：operType")
	@PostMapping(value = "reportProcessing")
	public RespVo<Map<String, Object>> reportProcessing(@RequestBody QueryVo queryVo) {
		logger.info("设备报修处理>>>>>>>>>>>>>>>>>>>>reportProcessing");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		List<MdsRepairInfoEntity> ls = mdsRepairInfoApi.findByOrderNumber(queryVo);
		if(ls!=null && ls.size()>0){
			MdsRepairInfoEntity repairInfoEntity = ls.get(0);
			repairInfoEntity.setServiceReason(queryVo.getServiceReason());
			repairInfoEntity.setUpdatedDate(new Date());
			mdsRepairInfoApi.save(repairInfoEntity);
			return respVo;
		}else{
			respVo.setCode(201);
	     	respVo.setMessage("没有相关设备 订单!");
		    return respVo;
		}
//		MdsProductInfoEntity mdsProductInfoEntity = mdsProductInfoApi.findByCode(queryVo);
//		System.out.println(mdsProductInfoEntity.getId());
//		MdsLeaseInfoEntity mdsLeaseInfoEntity = mdsLeaseInfoApi.info(mdsProductInfoEntity.getId());
//		if (mdsProductInfoEntity != null && mdsLeaseInfoEntity != null) {
//			// 恢复正常
//			if ("true".equals(queryVo.getIsWrong())) {
//				mdsProductInfoEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
//				mdsProductInfoEntity.setProductStatus(HospConstants.PRODUCT_STATUS_1);
//				mdsProductInfoApi.save(mdsProductInfoEntity);
//				mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_0);
//				mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
//				respVo.setCode(200);
//				respVo.setMessage("设备,订单已恢复正常!");
//			} else {
//				// 改为维修中 remark:备注内容
//				mdsProductInfoEntity.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
//				mdsProductInfoEntity.setProductStatus(HospConstants.PRODUCT_STATUS_5);
//				mdsProductInfoEntity.setRemark(queryVo.getRemark());
//				mdsProductInfoApi.save(mdsProductInfoEntity);
//
//				mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
//				mdsLeaseInfoEntity.setRemark(queryVo.getRemark());
//				mdsLeaseInfoEntity.setLeaseEndDate(mdsLeaseInfoEntity.getUpdatedDate());
//				mdsLeaseInfoEntity.setUpdatedDate(new Date());
//				mdsLeaseInfoApi.returnLeaseInfo(mdsLeaseInfoEntity);
//				respVo.setCode(200);
//				respVo.setMessage("设备进入维修中,订单已结束!");
//			}
//
//		} else {
//			respVo.setCode(201);
//			respVo.setMessage("没有相关设备 订单!");
//
//		}
		
	}

	/**
	 * TODO 设备批量暂停，批量启用
	 */
	@ApiOperation(value = " 设备  暂停使用", notes = "必传：operType (暂停S，启用U) productId(设备Id)")
	@PostMapping(value = "batchReceiveProductStatus")
	public RespVo<Map<String, Object>> batchReceiveProductStatus(@RequestBody QueryVo queryVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		String batchType = queryVo.getBatchType();
		String productId = queryVo.getProductIdArray();
		int is = 0;
		if (batchType == null && "".equals(batchType) && productId == null && "".equals(productId)) {
			respVo.setCode(201);
			respVo.setMessage("请选择要操作的设备!");
		} else {
			String productIds = null;
			if (HospConstants.BEARCH_RECEIVE_S.equals(batchType)) {
				String[] product = productId.split("-");
				for (int i = 0; i < product.length; i++) {
					productIds = product[i];
					Long id = new Long(productIds);
				//	System.out.println("==id="+id);
					MdsProductInfoEntity info = mdsProductInfoApi.getEntity(id);
				//	System.out.println("==="+(info != null));
				//	System.out.println("==info.getProductUseStatus()="+info.getProductUseStatus());
					if (info != null  && !info.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)) {
				//		System.out.println("==id====="+id);
						QueryVo queryVos = new QueryVo();
						queryVos.setBatchType(batchType);
						queryVos.setProductId(id);
				//		System.out.println("==id====="+id);
						mdsProductInfoApi.batchReceiveProductStatus(queryVos);
					} 

				}
//				if (is > 0) {
//					respVo.setCode(200);
//					respVo.setMessage("有设备正在使用中!");
//				} else {
//					respVo.setCode(200);
//					respVo.setMessage("设备批量暂停操作成功!");
//				}
				respVo.setCode(200);
				respVo.setMessage("设备批量暂停操作成功!");
			} else {
				String[] product = productId.split("-");
				for (int i = 0; i < product.length; i++) {
					productIds = product[i];
					Long id = new Long(productIds);
					QueryVo queryVos = new QueryVo();
					queryVos.setBatchType(batchType);
					queryVos.setProductId(id);
					MdsProductInfoEntity mdsProductInfoEntity =	mdsProductInfoApi.getEntity(id);
					if (mdsProductInfoEntity != null  && !mdsProductInfoEntity.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)) {
					queryVos.setCategoryId(mdsProductInfoEntity.getCategoryId());
					if(mdsProductInfoEntity.getHospitalId()!=null){
						queryVos.setHospitalId(mdsProductInfoEntity.getHospitalId());
						MdsHospitalPriceEntity price = mdsHospitalPriceApi.findPrice(queryVos);
						 if(price.getLeaseDeposit().compareTo(BigDecimal.ZERO) == 1 && price.getLeaseUnit().compareTo(BigDecimal.ZERO) == 1&& price.getMinHour()> 0){
							 mdsProductInfoApi.batchReceiveProductStatus(queryVos);
						 }
					}
				}
				}
				respVo.setCode(200);
				respVo.setMessage("设备批量启用操作成功!");
			}
		}
		return respVo;
	}

	@ApiOperation(value = "设备新增查找", notes = "必传： hospitalId (医院ID)")
	@PostMapping(value = "equipmentList")
	public RespVo<Map<String, Object>> equipmentList(@RequestBody QueryVo queryVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//System.out.println(queryVo.getHospitalId());
		Map<String, Object> map = new HashMap<>(50);
		if (queryVo.getHospitalId() == null && "".equals(queryVo.getHospitalId())) {
			respVo.setCode(201);
			respVo.setMessage("出错了!");
		} else {
			List<MdsProductInfoEntity> undistributedList = mdsProductInfoApi.undistributedList(queryVo); //未分配设备
			List<MdsProductInfoEntity> allocatedList = mdsProductInfoApi.allocatedList(queryVo); //关联医院已分配设备
		
			map.put("undistributedList", undistributedList);
			map.put("allocatedList", allocatedList);
			respVo.setData(map);
		}
		return respVo;
	}

	@ApiOperation(value = "门店新增绑定设备", notes = "必传： hospitalId (医院ID)")
	@PostMapping(value = "equipmentAllocated")
	public RespVo<Map<String, Object>> equipmentAllocated(@RequestBody QueryVo queryVo) {
		RespVo<Map<String, Object>> respVo = new RespVo<>();
	
			// 备用
			if (StringUtils.isNotBlank(queryVo.getAllocated())) {
				if(queryVo.getAllocated().indexOf(",")>-1){
					String[] product = queryVo.getAllocated().split(",");
					for (int i = 0; i < product.length; i++) {
						queryVo.setProductQrcode(product[i]);
						queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);
						queryVo.setHospitalId(null);
						queryVo.setHospitalName(null);
						queryVo.setHospitalDepart(null);
						queryVo.setHospitalWard(null);
						mdsProductInfoApi.upDatestatus(queryVo);
			
					}
					
				}else{
					queryVo.setProductQrcode(queryVo.getAllocated());
					queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);
					queryVo.setHospitalId(null);
					queryVo.setHospitalName(null);
					queryVo.setHospitalDepart(null);
					queryVo.setHospitalWard(null);
					mdsProductInfoApi.upDatestatus(queryVo);
					
				}

			}else if (StringUtils.isNotBlank(queryVo.getUndistributed())) {
				// 暂停or空闲

				///System.out.println(queryVo.getCategoryName());
				
				MdsProductCategoryEntity  category = mdsProductCategoryApi.findByCategory(queryVo);
				
				queryVo.setCategoryId(category.getId());
				
				MdsHospitalPriceEntity hospit = mdsHospitalPriceApi.findPrice(queryVo);
				BigDecimal A = new BigDecimal(0.00);
		
				if (hospit == null || (A.compareTo(hospit.getLeaseDeposit()) == 0 || A.compareTo(hospit.getLeaseUnit()) == 0
						|| hospit.getMinHour() == 0)) {
			
					if(queryVo.getUndistributed().indexOf(",")>-1){
						String[]  product = queryVo.getUndistributed().split(",");
						for (int i = 0; i < product.length; i++) {
							queryVo.setProductQrcode(product[i]);
							queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
							mdsProductInfoApi.upDatestatus(queryVo);
							
						}
					}else{
						queryVo.setProductQrcode(queryVo.getUndistributed());
						queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
						mdsProductInfoApi.upDatestatus(queryVo);
					}
				
					
		
				}else{
					
					if(queryVo.getUndistributed().indexOf(",")>-1){
					String[]  product = queryVo.getUndistributed().split(",");
					for (int i = 0; i < product.length; i++) {
						queryVo.setProductQrcode(product[i]);
						queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
						mdsProductInfoApi.upDatestatus(queryVo);
						
					}
					}else{
						queryVo.setProductQrcode(queryVo.getUndistributed());
						queryVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
						mdsProductInfoApi.upDatestatus(queryVo);
					}
				}
				
			}else{
				respVo.setCode(201);
				respVo.setMessage("请绑定设备ID和设备二维码!");
				
			}
			return respVo;
		
	}

}