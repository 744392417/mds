package com.spt.mds.open.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("报价参数请求实体")
public class PageRequestQuery {
	@ApiModelProperty(value = "模糊查询关键字", example="")
	private String content;			//模糊查询关键字
	@ApiModelProperty(value = "查询类别 (noRelease:未发布；havaRelease：已发布)", example="noRelease")
	private String type;			//区分为报价已发报价
	@ApiModelProperty(value = "排序字段(productCode:商品(默认),buySell(买卖))", example="productCode")
	private String socrt;		//排序字段
	@ApiModelProperty(value = "排序类型(asc desc)", example="asc")
	private String order;
	@ApiModelProperty(value = "一页显示多少条数(默认十条)", example="10")
	private int rows = 10;	
	
	@ApiModelProperty(value = "当前页", example="1")
	private int page = 1;
	private String userId;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSocrt() {
		return socrt;
	}
	public void setSocrt(String socrt) {
		this.socrt = socrt;
	}
	
	
	
}
