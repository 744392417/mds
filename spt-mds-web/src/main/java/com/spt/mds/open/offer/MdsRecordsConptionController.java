package com.spt.mds.open.offer;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.util.DateOperator;
import com.hsoft.commutil.util.PoiExcelUtil;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsRecordsConptionApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "资金结算", description = "资金结算")
@RestController
@RequestMapping(value = "/mdsRecords")
public class MdsRecordsConptionController 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsRecordsConptionApi mdsRecordsConptionApi;
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	
	/***
	 * TODO 资金结算 查询
	 */
	@ApiOperation(value = "资金结算分页列表")
	@PostMapping(value = "findRecordsPage")
	public RespVo<Map<String, Object>> findRecordsPage(@RequestBody QueryVo queryVo) 
	{
		logger.info("this controller method : findRecordsPage >>>>>>>资金结算分页列表>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();
		if (queryVo.getEndDate() != null) {
			queryVo.setEndDate(DateOperator.addDaysByDays(queryVo.getEndDate(), 1));
		}
		Page<MdsRecordsConptionVo> page = mdsRecordsConptionApi.findRecordsPage(queryVo);
		
		//退款
		BigDecimal iAmount = new BigDecimal(0.00);
		queryVo.setConsumStatus(HospConstants.CONSUM_STATUS_S);//
		iAmount =  mdsRecordsConptionApi.sumAmount(queryVo);

		
		//归还 ,退款总计
		BigDecimal cAmount = new BigDecimal(0.00);
		queryVo.setConsumStatus(HospConstants.CONSUM_STATUS_C);//
		cAmount =  mdsRecordsConptionApi.sumAmount(queryVo);
		if(cAmount!=null){
			iAmount=iAmount.add(cAmount);
		}
		 //预付款
		BigDecimal sAmount = new BigDecimal(0.00);
		queryVo.setConsumStatus(HospConstants.CONSUM_STATUS_I);//
		sAmount =  mdsRecordsConptionApi.sumAmount(queryVo);

		//留存总计
		BigDecimal rAmount = new BigDecimal(0.00);
		if(iAmount!=null){
		 rAmount = sAmount.subtract(iAmount);
		}else{
		rAmount = sAmount;
		}
		//分账总计
		BigDecimal fAmount = new BigDecimal(0.00);
		queryVo.setConsumStatus(HospConstants.CONSUM_STATUS_F);
		fAmount =  mdsRecordsConptionApi.sumAmount(queryVo);
		//BigDecimal fAmount =BigDecimal.ZERO;
		
		//平台收入总计
		BigDecimal pAmount = new BigDecimal(0.00);
		if(fAmount!=null){
		  pAmount = rAmount.subtract(fAmount);
		}else{
		  pAmount = rAmount;
		}
		

		//资金结算列表
		map.put("page", page);
		//预付款总计
		map.put("sAmount", sAmount);
		//归还总计
		map.put("iAmount", iAmount);
		//留存总计
		map.put("rAmount", rAmount);
		//分账总计
		map.put("fAmount", fAmount);
		//平台收入总计
		map.put("pAmount", pAmount);
		
		respVo.setData(map);
		return respVo;
	}
	
	
	/**
	 * TODO 资金结算导出报表
	 * @throws IOException 
	 * */
	@ApiOperation(value = "资金结算导出报表")
	@GetMapping(value = "capitalSettlementExportReport")
	@ResponseBody
	public void capitalSettlementExportReport(QueryVo vo,HttpServletRequest request,HttpServletResponse response) throws IOException 
	{
		logger.info("this method capitalSettlementExportReport>>>>>>资金结算导出报表");
		if (vo.getEndDate() != null) {
			vo.setEndDate(DateOperator.addDaysByDays(vo.getEndDate(), 1));
		}
		//查询页面导出的数据
		PageDown<MdsRecordsConptionVo> conptionVoPage = mdsRecordsConptionApi.findRecordsPage(vo);
		List<MdsRecordsConptionVo> contentList = conptionVoPage.getContent();

		int totalPages = conptionVoPage.getTotalPages()+1;
		String title = "资金结算报表";
		String[] titles = new String[]{"发生时间","租赁订单","归还预付款(元)","收到预付款(元)","收到租金","附加退款","交易类型","租赁设备","医院名称","平台收入比例(%)","备注"};
		String[] attrs = new String[]{"createdDateStr","orderNumber","expenditure","income","obtain","addition","dealType","categoryName","hospitalName","platDistrib","recordsProduct"};
		int [] widths =new int[] {15,15,20,15,15,15,15,15,15,15,15};
		Workbook workbook = PoiExcelUtil.newWorkbook(PoiExcelUtil.WB_TYPE_2007);
		// 生成一个表格
		Sheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为 15 个字节
		sheet.setDefaultColumnWidth(15);
		// 产生表格标题行
		// 生成一个样式
		CellStyle cellStyle = PoiExcelUtil.getCellStyle(workbook);
		/** 创建表头 */
		int[] widthes = new int[titles.length];
		for (int i = 0; i < titles.length; i++) 
		{
			widthes[i] = widths[i];
		}
		PoiExcelUtil.creatHeads(workbook, sheet, titles, widthes);
		int start =0;
		
		while (conptionVoPage!=null && contentList!=null&& contentList.size()>0) 
		{
			PoiExcelUtil.createRows(sheet, conptionVoPage.getContent(), attrs, start, cellStyle,DateOperator.FORMAT_STR);
			conptionVoPage.setTotalPages(totalPages);
			if (conptionVoPage.hasNext()) 
			{
				vo.setPage(vo.getPage()+1);
				conptionVoPage = mdsRecordsConptionApi.findRecordsPage(vo);
				start += vo.getRows();
			}else 
			{
				conptionVoPage=null;
			}
		}
		try 
		{
			PoiExcelUtil.write(workbook, response, title);
		} catch (IOException e) 
		{
		  logger.error(e.getMessage(),e);
		}
	}
}
