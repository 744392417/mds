//package com.spt.mds.open.offer;
//
//import java.io.BufferedReader;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
//
//import com.github.wxpay.sdk.WXPay;
//import com.github.wxpay.sdk.WXPayUtil;
//import com.hsoft.commutil.redis.RedisUtil;
//import com.spt.hospital.client.api.IMdsLeaseInfoApi;
//import com.spt.hospital.client.api.IMdsProductPwdApi;
//import com.spt.hospital.client.constant.HospConstants;
//import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
//import com.spt.hospital.client.entity.MdsProductPwdEntity;
//import com.spt.hospital.client.util.DateParser;
//import com.spt.hospital.client.vo.MdsLeaseInfoVo;
//import com.spt.hospital.client.vo.MdsProductPwdVo;
//import com.spt.hospital.client.vo.QueryVo;
//import com.spt.hospital.client.vo.TemplateData;
//import com.spt.hospital.client.vo.WxMssVo;
//import com.spt.mds.open.vo.RespVo;
//import com.spt.mds.web.util.MdsWXConfigUtil;
//import com.spt.mds.web.util.StringUtils;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import net.sf.json.JSONObject;
///**
// * @author hanson
// * */
//@Api(value = "微信支付", description = "微信支付,支付,退款,查询")
//@RestController
//@RequestMapping(value = "/mdsWxPay")
//public class WxPayController 
//{
//	@Value("${wx.appid}")
//	String appId;
//
//	@Value("${wx.secret}")
//	String secret;
//	
//	@Value("${wx.notifyurl}")
//	String notifyUrl;
//	
//	@Value("${wx.spbillcreateip}")
//	String spbillCreateIp;
//
//	
//	@Value("${wx.tradetype}")
//	String tradeType;
//	
//	@Value("${wx.message.url}")
//	String wxmessageurl;
//	
//	@Value("${wx.token.url}")
//	String wxtokenurl;
//	
//	//异步接收微信支付结果通知的回调地址
////	private static final String notify_url = "";
//	
//    @Autowired
//    private RestTemplate restTemplate;
//	@Autowired
//	private IMdsLeaseInfoApi mdsLeaseInfoApi;
//	@Autowired
//	private IMdsProductPwdApi mdsProductPwdApi;
//
//	private MdsWXConfigUtil config;
//	
//	private WXPay wxpay;
//	
//	public WxPayController() 
//	{
//		try 
//		{
//			//初始化微信支付客户端
//			config = new MdsWXConfigUtil();
//			wxpay = new WXPay(config);
//		} catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * TODO 预支付接口
//	 * @param order
//	 * @return
//	 * @throws Exception
//	 */
//	@SuppressWarnings("unused")
//	@ApiOperation(value = "微信支付")
//	@PostMapping(value = "pay")
//	public RespVo<Map<String, String>>  pay(@RequestBody QueryVo queryVo,HttpServletResponse response) throws Exception {
//		RespVo<Map<String, String>> respVo = new RespVo<>();
//		//这里执行商户系统创建新的订单操作
////		WxPayOrder order = new WxPayOrder();
////		order.setOut_trade_no(System.currentTimeMillis() + "");
////		wxPayService.createOrder(order);
//		
//		String spbillCreateIp = "";
//		String orderNo = "123456789";// "H"+DateParser.formatDateSS(new Date());
//		String money = "1";// 支付金额，单位：分，这边需要转成字符串类型，否则后面的签名会失败
//		String totalFee = "1";
//		String openid = "";
//		//设置请求参数
//        Map<String, String> data = new HashMap<String, String>(50);
//        data.put("body", "微信支付测试");
//        //随机字符串
//        data.put("out_trade_no",StringUtils.getRandomOrderId());
//        data.put("nonce_str", WXPayUtil.generateNonceStr());
//        data.put("fee_type", "CNY");
//        // 支付金额，这边需要转成字符串类型，否则后面的签名会失败
//        data.put("total_fee", money);
//        data.put("spbill_create_ip",spbillCreateIp);
//        data.put("notify_url", "http://localhost:8088/mdsWxPay/notifyUrl");
//        data.put("trade_type","NATIVE");//        data.put("trade_type","JSAPI");
//        //微信用户ID
////        data.put("openid", "OPENID");
//        data.put("mch_id", config.getMchID());
//        //获取第一次签名
//        String sign = WXPayUtil.generateSignature(data, config.getKey());
//        data.put("sign",sign);
//        try {
//        	//发起支付
//            Map<String, String> resp = wxpay.unifiedOrder(data);
//            // 返回状态码
//            String returnCode = (String) resp.get("return_code");
//            //返回信息
//			String returnMsg = (String) resp.get("return_msg");
//			if ("SUCCESS".equals(resp.get("return_code"))) 
//			{
//				/**
//				 * 重要的事情说三遍 小程序支付 所有的字段必须大写 驼峰模式 严格按照小程序支付文档
//				 * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_7&index=3#
//				 * ******* 我当初就因为timeStamp中S没大写弄了3个小时 **********
//				 */
//				// 再次签名
//				Map<String, String>  reData = new HashMap<>(50);
//				reData.put("appId", config.getAppID());
//				reData.put("nonceStr", resp.get("nonce_str"));
//				String newPackage = "prepay_id=" + resp.get("prepay_id");
//				reData.put("package", newPackage);
//				reData.put("signType", "MD5");
//				reData.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000));
//				//获取第二次签名
//				String newSign = WXPayUtil.generateSignature(reData, config.getKey());
//				resp.put("paySign", newSign);
//				resp.put("timeStamp", reData.get("timeStamp"));
//				resp.put("nonceStr", reData.get("nonce_str"));
//				//更新订单信息
//				this.updPrePayOrder(money, orderNo, queryVo, resp);
//				//获取开锁密码
//				MdsProductPwdEntity pwdEntity = mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
//				MdsProductPwdVo vo = new MdsProductPwdVo();
//				BeanUtils.copyProperties(pwdEntity, vo);
//				//密码状态，使用中
//				pwdEntity.setPwdStatus(HospConstants.PWD_STATUS_1);
//				//修改密码状态
//				mdsProductPwdApi.updatePwdStatus(vo);
//				//设置开锁密码
//				resp.put("unlockPwd", vo.getUnlockPwd());
//				
//				respVo.setData(resp);
//			} else 
//			{
//			}
////            //获取二维码URL
////            String code_url = resp.get("code_url");
////            //根据url生成二维码
////            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
////            // 设置二维码参数
////            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
////            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
////            BitMatrix bitMatrix = multiFormatWriter.encode(code_url, BarcodeFormat.QR_CODE, 300, 300, hints);
////            //返回二维码
////            MatrixToImageWriter.writeToStream(bitMatrix, "jpg", response.getOutputStream());
//        } catch (Exception e) 
//        {
//            e.printStackTrace();
//        }
//        return  respVo;
//	}
//	/**
//	 * TODO 支付结果回调
//	 * @return
//	 * @throws Exception 
//	 */
//	@ApiOperation(value = "支付结果回调")
//	@PostMapping(value = "notifyUrl")
//	public void notifyUrl(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		// 读取回调内容
//		InputStream inputStream;
//		StringBuffer sb = new StringBuffer();
//		inputStream = request.getInputStream();
//		String s;
//		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
//		while ((s = in.readLine()) != null) 
//		{
//			sb.append(s);
//		}
//		in.close();
//		inputStream.close();
//		
//		// 支付结果通知的xml格式数据
//		String notifyData = sb.toString(); 
//
//        // 转换成map
//        Map<String, String> notifyMap = WXPayUtil.xmlToMap(notifyData);
//
//        //支付确认内容
//        String resXml = "";
//        //验证签名 // 签名正确
//        if (wxpay.isPayResultNotifySignatureValid(notifyMap))
//        {		
//    //    	WxPayOrder order = wxPayService.getOrder(notifyMap.get("out_trade_no"));
//        	if(notifyMap.get("out_trade_no") != null) 
//        	{
//        		//交易成功
//        		if("SUCCESS".equals(notifyMap.get("result_code"))) 
//        		{	
//        			//更新订单
//        			QueryVo	queryVo  = new QueryVo();
//        			queryVo.setOrderNumber(notifyMap.get("out_trade_no"));
//          			queryVo.setOrderNumber(notifyMap.get("openid"));
//        			List<MdsLeaseInfoEntity> ls =	mdsLeaseInfoApi.findLeaseByOrderNumber(queryVo);
//        			MdsLeaseInfoEntity mdsLeaseInfoEntity = ls.get(0);
//        			//WX返回时间
//        			mdsLeaseInfoEntity.setWxIncomeDate(new Date());
//        			//WX返回付款状态
//        			mdsLeaseInfoEntity.setIncomeSatus(HospConstants.RESULT_SUCCESS);
//        			//生成订单
//        			mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
//        			System.out.println("订单" + notifyMap.get("out_trade_no") + "微信支付成功");
//        		} else 
//        		{	//交易失败
//        			System.out.println("订单" + notifyMap.get("out_trade_no") + "微信支付失败");
//        		}
//        	}
//            // 注意特殊情况：订单已经退款，但收到了支付结果成功的通知，不应把商户侧订单状态从退款改成支付成功
//        	
//        	//设置成功确认内容
//        	resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>" + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
//        }
//        else 
//        {  // 签名错误，如果数据里没有sign字段，也认为是签名错误
//        	//设置失败确认内容
//        	resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg></return_msg>" + "</xml> ";
//        	System.out.println("订单" + notifyMap.get("out_trade_no") + "微信支付失败");
//        }
//        //发送通知
//        response.getWriter().println(resXml);
//	}
//	/**
//	 * TODO 微信申请退款接口
//	 * @param out_trade_no		订单号
//	 * @throws Exception
//	 */
//	@ApiOperation(value = "微信申请退款接口")
//	@PostMapping(value = "refund")
//	public void refund(String outTradeNo) throws Exception
//	{
//		//设置请求参数
//		HashMap<String, String> data = new HashMap<String, String>(50);
//        data.put("out_trade_no", outTradeNo);
//        data.put("out_refund_no", outTradeNo);
//        data.put("total_fee", "1");
//        data.put("refund_fee", "1");
//        data.put("refund_fee_type", "CNY");
//        data.put("op_user_id", config.getMchID());
//
//        try 
//        {
//        	//调用sdk发起退款
//            Map<String, String> result = wxpay.refund(data);
//            if("SUCCESS".equals(result.get("result_code"))) 
//            {
//            	//更新订单
//            	QueryVo	queryVo  = new QueryVo();
//    			queryVo.setOrderNumber(result.get("out_trade_no"));
//    			
//            	List<MdsLeaseInfoEntity> ls =	mdsLeaseInfoApi.findLeaseByOrderNumber(queryVo);
//    			MdsLeaseInfoEntity mdsLeaseInfoEntity = ls.get(0);
//    			//WX返回时间
//    			mdsLeaseInfoEntity.setWxSpendDate(new Date());
//    			//WX返回付款状态
//    			mdsLeaseInfoEntity.setWxSpendStatus(HospConstants.RESULT_SUCCESS);
//    			mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
//    			queryVo.setProductId(mdsLeaseInfoEntity.getProductId());
//    			MdsProductPwdEntity pwd = mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
//				MdsProductPwdVo vo = new MdsProductPwdVo();
//				BeanUtils.copyProperties(pwd, vo);
//				//关密码
//				pwd.setShutPwd(queryVo.getShutPwd());
//				mdsProductPwdApi.updatePwdStatus(vo);
//            	System.out.println("订单" + outTradeNo + "微信退款成功");
//            }
//        } catch (Exception e) 
//        {
//            e.printStackTrace();
//        }
//	}
//	/**
//	 * TODO 微信退款回调接口
//	 * @param out_trade_no		订单号
//	 * @throws Exception
//	 */
//	@SuppressWarnings("unused")
//	@ApiOperation(value = "微信申请退款 回调接口")
//	@PostMapping(value = "refundCallback")
//    public void refundCallback(HttpServletRequest request, HttpServletResponse response) throws Exception
//	{
//		System.out.println("退款  微信回调接口方法 start");
//		// 读取回调内容
//		InputStream inputStream;
//		StringBuffer sb = new StringBuffer();
//		inputStream = request.getInputStream();
//		String s;
//		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
//		while ((s = in.readLine()) != null)
//		{
//			sb.append(s);
//		}
//		in.close();
//		inputStream.close();
//		
//		// 支付结果通知的xml格式数据
//		String notifyData = sb.toString(); 
//		//支付确认内容
//        String resXml = "";
//        // 转换成map
//        Map<String, String> notifyMap = WXPayUtil.xmlToMap(notifyData);
//	
//            //判断 退款是否成功
//            if("SUCCESS".equals(notifyMap.get("return_code")))
//            {
//            	System.out.println("退款  微信回调返回是否退款成功：是");
////		    	//获得 返回的商户订单号
////		    	String passMap = AESUtil.decryptData(map.get("req_info"));
////		    	//拿到解密信息
////		    	map = doXMLParse(passMap);
//                //拿到解密后的订单号
//                String outTradeNo = notifyMap.get("out_trade_no");
//                String refundId = notifyMap.get("refund_id");//微信退款单号
//                QueryVo	queryVo  = new QueryVo();
//    			queryVo.setOrderNumber(outTradeNo);
//    			
//            	List<MdsLeaseInfoEntity> ls =	mdsLeaseInfoApi.findLeaseByOrderNumber(queryVo);
//    			MdsLeaseInfoEntity mdsLeaseInfoEntity = ls.get(0);
//    			//WX返回时间
//    			mdsLeaseInfoEntity.setWxIncomeDate(new Date());
//    			mdsLeaseInfoEntity.setWxSpendNumber(refundId);
//    			//WX返回付款状态
//    			mdsLeaseInfoEntity.setWxSpendStatus(HospConstants.RESULT_SUCCESS);
//    			mdsLeaseInfoEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
//    			//生成订单
//    			mdsLeaseInfoApi.save(mdsLeaseInfoEntity);
//    			queryVo.setProductId(mdsLeaseInfoEntity.getProductId());
//    			
//    			MdsProductPwdEntity pwd = mdsProductPwdApi.getMdsProductPwdInfo(queryVo);
//				MdsProductPwdVo vo = new MdsProductPwdVo();
//				BeanUtils.copyProperties(pwd, vo);
//				//已使用
//				pwd.setPwdStatus(HospConstants.PWD_STATUS_2);
//				mdsProductPwdApi.updatePwdStatus(vo);
//                
//                System.out.println("退款  微信回调返回商户订单号："+notifyMap.get("out_trade_no"));
//                //支付成功 修改订单状态 通知微信成功回调
////                int sqlRow = orderJpaDao.updateOrderStatus("refunded",new Timestamp(System.currentTimeMillis()), outTradeNo);
////                if(sqlRow == 1) {
////                    log.info("退款 微信回调 更改订单状态成功");
////                }
//            }else 
//            {
//                //获得 返回的商户订单号
////                String passMap = AESUtil.decryptData(map.get("req_info"));
////                //拿到解密信息
////                map = doXMLParse(passMap);
//                //拿到解密后的订单号
//                String outTradeNo = notifyMap.get("out_trade_no");
//                //更改 状态为取消
////                int sqlRow = orderJpaDao.updateOrderStatus("canceled",new Timestamp(System.currentTimeMillis()), outTradeNo);
////                if(sqlRow == 1) {
////                    log.info("退款 微信回调返回是否退款成功：否");
////                }
//            }
//            resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>" + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
//            //给微信服务器返回 成功标示 否则会一直询问 咱们服务器 是否回调成功
//         	response.getWriter().write(resXml.toString());
//	}        
//	/**
//	 * TODO 订单查询
//	 * @param out_trade_no		订单号
//	 * @throws Exception
//	 */
//	@ApiOperation(value = "微信订单查询：")
//	@PostMapping(value = "orderQuery")
//	public void orderQuery(String outTradeNo) 
//	{
//		   Map<String, String> data = new HashMap<String, String>(50);
//	       data.put("out_trade_no", "2016090910595900000012");
//
//	       try 
//	       {
//	           Map<String, String> resp = wxpay.orderQuery(data);
//	           System.out.println(resp);
//	       } catch (Exception e) 
//	       {
//	           e.printStackTrace();
//	       }
//	}
//	/**
//	 * TODO  微信小程序推送单个用户
//	 */
//	public String pushOneUser(String openid, String formid) 
//	{
//		// 获取access_token
//		String accessToken =  RedisUtil.get("mdsAccessToken");
//		
//	    if(accessToken==null)
//	    {
//	    	accessToken = this.getAccessToken();
//	    }
//	
//		String url = wxmessageurl + "?access_token=" + accessToken;
//
//		// 拼接推送的模版
//		WxMssVo wxMssVo = new WxMssVo();
//		wxMssVo.setTouser(openid);// 用户openid
//		wxMssVo.setTemplate_id("LzeDP0G5PLgHoOjCMfhu44wfUluhW11Zeezu3r_dC24");// 模版id
//		wxMssVo.setForm_id(formid);// formid
//
//		Map<String, TemplateData> m = new HashMap<>(5);
//
//		// keyword1：订单类型，keyword2：下单金额，keyword3：配送地址，keyword4：取件地址，keyword5备注
//		TemplateData keyword1 = new TemplateData();
//		keyword1.setValue("新下单待抢单");
//		m.put("keyword1", keyword1);
//
//		TemplateData keyword2 = new TemplateData();
//		keyword2.setValue("这里填下单金额的值");
//		m.put("keyword2", keyword2);
//		wxMssVo.setData(m);
//
//		TemplateData keyword3 = new TemplateData();
//		keyword3.setValue("这里填配送地址");
//		m.put("keyword3", keyword3);
//		wxMssVo.setData(m);
//
//		TemplateData keyword4 = new TemplateData();
//		keyword4.setValue("这里填取件地址");
//		m.put("keyword4", keyword4);
//		wxMssVo.setData(m);
//
//		TemplateData keyword5 = new TemplateData();
//		keyword5.setValue("这里填备注");
//		m.put("keyword5", keyword5);
//		wxMssVo.setData(m);
//
//		ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, wxMssVo, String.class);
//		System.out.println("小程序推送结果={}"+responseEntity.getBody());
//		return responseEntity.getBody();
//	}
//	/**
//	 * TODO 获取access_token appid和appsecret到小程序后台获取，当然也可以让小程序开发人员给你传过来
//	 */
//	public String getAccessToken()
//	{
//		// 获取access_token
//		String url = wxtokenurl+"?grant_type=client_credential&appid=" + appId + "&secret=" + secret;
//		String json = restTemplate.getForObject(url, String.class);
//		JSONObject accessToken = JSONObject.fromObject(json);
//		String accessTokenStr =(String) accessToken.get("access_token");
//		String expiresInStr =(String) accessToken.get("expires_in");
//		RedisUtil.setex("mdsAccessToken",Integer.valueOf(expiresInStr),accessTokenStr);
//		return  (String) accessToken.get("access_token");
//	}
//	/**
//	 * TODO  私有：更新微信预支付后订单信息
//	 * */
//	private void updPrePayOrder(String money,String orderNo,QueryVo queryVo,Map<String, String> resp)
//	{
//		// 更新订单信息
//		MdsLeaseInfoVo mdsLeaseInfoVo = new MdsLeaseInfoVo();
//		BigDecimal leaseDeposit = BigDecimal.ZERO;
//		BigDecimal moneyB = new BigDecimal(money) ;
//		leaseDeposit = moneyB.divide(BigDecimal.valueOf(100),2,RoundingMode.UP);
//		//订单号码
//		mdsLeaseInfoVo.setOrderNumber(orderNo);
//		//预付款
//		mdsLeaseInfoVo.setLeaseDeposit(leaseDeposit);
//		//开始时间
//		mdsLeaseInfoVo.setLeaseStartDate(new Date());
//		//医院id
//		mdsLeaseInfoVo.setHospitalId(queryVo.getHospitalId());
//		//医院名称
//		mdsLeaseInfoVo.setHospitalName(queryVo.getHospitalName());
//		//所属科室
//		mdsLeaseInfoVo.setHospitalDepart(queryVo.getHospitalDepart());
//		//品类
//		mdsLeaseInfoVo.setCategoryId(queryVo.getCategoryId());
//		//品类名称
//		mdsLeaseInfoVo.setCategoryName(queryVo.getCategoryName());
//		//产品ID
//		mdsLeaseInfoVo.setProductId(queryVo.getProductId());
//		//产品编号
//		mdsLeaseInfoVo.setProductCode(queryVo.getProductCode());
//		//产品二维码
//		mdsLeaseInfoVo.setProductQrcode(queryVo.getProductQrcode());
//		//产品数量
//		mdsLeaseInfoVo.setProductNumber(queryVo.getProductNumber());
//		//解锁密码
//		mdsLeaseInfoVo.setUnlockPwd(queryVo.getUnlockPwd());
//		//流水
//		String platIncomeNumber = DateParser.formatDateSS(new Date());
//		//预付款流水
//		mdsLeaseInfoVo.setPlatIncomeNumber(platIncomeNumber);
//		//平台发起时间
//		mdsLeaseInfoVo.setPlatIncomeDate(new Date());
//		// 预付款WX流水(微信支付订单号)
//		mdsLeaseInfoVo.setWxIncomeNumber(resp.get("transaction_id"));
//		//openid 用户openid
//		mdsLeaseInfoVo.setOpenid(resp.get("openid"));
//	}
//	
//}
