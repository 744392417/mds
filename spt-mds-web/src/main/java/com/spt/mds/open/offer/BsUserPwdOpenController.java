//package com.spt.mds.open.offer;
//package com.spt.ucs.open.offer;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.hsoft.admin.client.cache.DictUtil;
//import com.hsoft.admin.client.entity.AppDictData;
//import com.hsoft.commutil.exception.ErrorResp;
//import com.hsoft.commutil.redis.RedisUtil;
//import com.spt.credit.client.api.IBsUserAccountApi;
//import com.spt.credit.client.constant.OfferConstants;
//import com.spt.credit.client.entity.BsUserAccount;
//import com.spt.credit.client.vo.UserAccountSendRequestVo;
//import com.spt.credit.client.vo.UserAccountVo;
//import com.spt.ucs.open.vo.RespVo;
//import com.spt.ucs.open.vo.UserPwdParamVo;
//import com.spt.ucs.web.config.BasicErrorController;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//
//@Api(value = "用户密码管理", description = "用户密码管理")
//@RestController
//@RequestMapping(value = "open/bs/userPwd")
//public class BsUserPwdOpenController {
//	
//	private Logger logger = LoggerFactory.getLogger(getClass());
//	
//	@Autowired
//	private IBsUserAccountApi userAccountApi;
//	
//	/***
//	 * 根据邮箱找回密码
//	 * @param vo
//	 * @param request
//	 */
//	@ApiOperation(value = "根据邮箱找回密码", notes = "参数说明：{email:邮箱地址,必传}; 返回说明:code=0 时 没有找到邮箱")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//			@ApiResponse(code = 1054, message = "邮箱不存在")
//		})
//	@PostMapping(value = "updatePwdByEmail")
//	public RespVo<Map<String,Object>> updatePwdByEmail(@RequestBody UserPwdParamVo vo,HttpServletRequest request){
//		RespVo<Map<String,Object>> resultVo = new RespVo<>();
//		Map<String,Object> map=new HashMap<>();
//		try {
//			//根据邮箱获得账户信息
//			String email=vo.getEmail();
//			BsUserAccount userAccount = userAccountApi.findUserByEmail(email);
//			if(userAccount != null){
//				UserAccountSendRequestVo sendvo =new UserAccountSendRequestVo();
//				sendvo.setEmail(email);
//				sendvo.setId(userAccount.getId());
//				userAccountApi.sendUserEmail(sendvo);			
//				RedisUtil.set("updatePwdEmail", vo.getEmail());
//				
//				AppDictData data = DictUtil.getListByCategory(OfferConstants.SAAS_EMAIL).get(0);
//				String companyEmailUrl = data.getRemark();
//				map.put("companyEmailUrl", companyEmailUrl);
//				resultVo.setData(map);
//			}else{
//				//邮箱没有找到
//				resultVo.setCode(1054);			
//				resultVo.setMessage("邮箱不存在！");
//			}
//		} catch (Exception e) {
//			logger.error("updatePwdByEmail", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			if(errsp.getMessage().equals("1055")){
//				resultVo.setMessage("邮箱只能为商品通企业邮箱！");
//				resultVo.setCode(1055);
//			}else{
//				resultVo.setFail(errsp);
//			}
//		}			
//		 return  resultVo;
//	}
//	
//	/**
//	 * 激活邮箱
//	 * @param email
//	 * @param mailActiveCode
//	 * @return
//	 */
//	@ApiOperation(value = "修改密验证邮箱", notes = "email(邮箱,String类型),mailActiveCode(随机码,String类型)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功"),
//		@ApiResponse(code = 501, message = "服务器内部异常"),
//        @ApiResponse(code = 503, message = "权限不足"),
//        @ApiResponse(code = 1054, message = "邮箱验证失败！")
//        })
//	@PostMapping(value = "activeEmail")
//	public RespVo<?> activeEmail(@RequestBody UserAccountSendRequestVo vo,HttpServletRequest request){
//		RespVo<?> resultVo = new RespVo<>();
//		try {
//			String email=RedisUtil.get("updatePwdEmail");
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱验证失败,验证错误!");
//				throw new IllegalAccessError();
//			}
//			
//			if(StringUtils.isNotBlank(email)&&StringUtils.isNotBlank(vo.getMailActiveCode())){
//				String mailActive = email+","+vo.getMailActiveCode();
//				boolean result = userAccountApi.activeEmail(mailActive);
//			    if(!result){
//			    	resultVo.setCode(1054);
//			    	throw new IllegalArgumentException();
//			    }
//			}	    
//		} catch (Exception e) {
//			logger.error("activeEmail", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	 }
//	
//	
//	/**
//	 * 重新发送邮件
//	 * @param email
//	 * @return
//	 */
//	@ApiOperation(value = "重新发送邮件", notes = "email(邮箱,String类型)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功")})
//	@PostMapping(value = "resendEmail")
//	public RespVo<?> resendEmail(@RequestBody UserAccountSendRequestVo vo){
//		RespVo<?> resultVo = new RespVo<>();
//		try {
//			String email=RedisUtil.get("updatePwdEmail");
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱不能为空!");
//				throw new IllegalArgumentException();
//			}
//			
//			BsUserAccount user=userAccountApi.findUserByEmail(email);
//			vo.setEmail(vo.getEmail());
//			vo.setId(user.getId());
//			userAccountApi.sendUserEmail(vo);    
//		} catch (Exception e) {
//			logger.error("resendEmail", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	 }
//	
//	/**
//	 * 根据邮箱修改密码
//	 * @param email
//	 * @param mailActiveCode
//	 * @param request
//	 * @param response
//	 * @param model
//	 * @return
//	 */
//	@ApiOperation(value = "根据邮箱修改密码", notes = "email(邮箱,String类型)必传,loginPwd(密码,String类型,修改新密码(例:123456)必传")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功")})
//	@PostMapping(value = "resetPassword")
//	public RespVo<?> resetPassword(@RequestBody UserAccountSendRequestVo vo){
//		RespVo<?> resultVo = new RespVo<>();
//		try {
//			String email=RedisUtil.get("updatePwdEmail");
//			if(email==null){
//				resultVo.setCode(1052);
//				resultVo.setMessage("邮箱不能为空!");
//				throw new IllegalArgumentException();
//			}
//			BsUserAccount userAccount= userAccountApi.findUserByEmail(email);
//			if(userAccount!=null){
//				userAccount.setPlainPassword(vo.getLoginPwd());
//				userAccountApi.save(userAccount);
//			}
//		} catch (Exception e) {
//			logger.error("resetPassword", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	 }
//	
//	
//	/***
//	 * 根据手机号找回密码
//	 * @param vo
//	 * @return
//	 */
//	@ApiOperation(value = "根据手机号发送短信验证码", notes = "参数说明：{mobile:手机号,必传}; 返回说明:code=0 时 没有找到手机号码")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功")})
//	@PostMapping(value = "sendSmsToPwd")
//	public RespVo<?> sendSmsToPwd(@RequestBody UserPwdParamVo vo){
//		RespVo<?> resultVo = new RespVo<>();
//		try {
//			String mobile=vo.getMobile();
//			BsUserAccount account=userAccountApi.findUserByMobile(mobile);
//			if(null!=account){
//				UserAccountSendRequestVo sendvo =new UserAccountSendRequestVo();
//				sendvo.setMobile(mobile);
//				userAccountApi.sendSms(sendvo);
//			}else{				//手机号没有找到
//				resultVo.setCode(0);
//			}
//		} catch (Exception e) {
//			logger.error("sendSmsToPwd", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//		
//	}
//	
//	/**
//	 * 手机验证码验证
//	 * @param vo
//	 * @return
//	 */
//	@ApiOperation(value = "手机验证码验证", notes = "参数说明：{(mobile:手机号,必传),(smsMsg:手机短信验证码,必传)}; 返回说明:code=0 时 没有找到手机号码")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功")})
//	@PostMapping(value="smsValidate")
//	public RespVo<?> smsValidate(@RequestBody UserPwdParamVo vo){
//		RespVo<?> resultVo = new RespVo<>();
//		try {
//			UserAccountVo account =new UserAccountVo();
//			account.setMobileNumber(vo.getMobile());
//			account.setSms(vo.getSmsMsg());
//			boolean result=userAccountApi.smsValidate(account);
//			if(!result){			//验证码不正确
//				resultVo.setCode(0);
//			}		
//		} catch (Exception e) {
//			logger.error("smsValidate", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//	
//	
//	/**
//	 * 根据手机号修改密码
//	 * @param username
//	 * @param password
//	 * @param response
//	 */
//	@ApiOperation(value = "根据手机号修改密码", notes = "mobile(手机号,String类型,例:13477086901)必传,loginPwd(密码,String类型,修改新密码(例:123456)")
//	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功")})
//	@PostMapping(value="resetPasswordMobile")
//	public RespVo<?> resetPasswordMobile(@RequestBody UserAccountSendRequestVo vo){
//		RespVo<?> resultVo = new RespVo<>();		
//		try {
//			 userAccountApi.resetPasswordMobile(vo);
//		} catch (Exception e) {
//			logger.error("resetPasswordMobile", e);
//			ErrorResp errsp = BasicErrorController.getErrorResp(e);
//			resultVo.setFail(errsp);
//		}
//		return resultVo;
//	}
//}
