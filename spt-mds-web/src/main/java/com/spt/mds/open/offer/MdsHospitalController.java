package com.spt.mds.open.offer;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.PageDown;
import com.hsoft.file.client.remote.FileRemote;
import com.spt.hospital.client.api.IMdsHospitalPriceApi;
import com.spt.hospital.client.api.IMdsProductCategoryApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.product.ProductInfoByStoreManagerVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * @author hanson
 * */
@Api(value = "门店管理", description = "门店管理")
@RestController
@RequestMapping(value = "/mdsHospital")
public class MdsHospitalController
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private IMdsHospitalPriceApi mdsHospitalPriceApi;
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	@Autowired
	private IMdsProductCategoryApi mdsProductCategoryApi;
	
	@SuppressWarnings("unused")
	@Autowired
	private FileRemote fileRemote;
	
	/***
	 * TODO 门店管理 查询门店列表
	 */
	@ApiOperation(value = "门店管理   查询门店列表")
	@PostMapping(value = "findhospitalProductList")
	public RespVo<Map<String, Object>> findhospitalProductList(@RequestBody QueryVo vo) 
	{
		logger.info("门店管理    查询门店列表>>>>>>>>>>>>>>>>>>>>findhospitalProductList");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		Map<String, Object> map = new HashMap<>();
		PageDown<MdsPropertyInfoVo> page = mdsPropertyInfoApi.findPropertyListPage(vo);
		map.put("page", page);
		respVo.setData(map);
		return respVo;
	}
	/***
	 * TODO 门店管理    编辑设备中查询的列表
	 */
	@ApiOperation(value = "门店管理    编辑设备中查询的列表 必传(hospitalId)")
	@PostMapping(value = "findProductListByStore")
	public RespVo<Map<String, Object>> findProductListByStore(@RequestBody QueryVo queryVo) 
	{
		logger.info("门店管理    编辑设备  查询列表>>>>>>>>>>>>>>>>>>>>findProductListByStore");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//查询设备列表
		
		if (queryVo.getEndDate() != null) {
			queryVo.setEndDate(DateParser.addDaysByDays(queryVo.getEndDate(), 1));
		}
	
	
		Page<ProductInfoByStoreManagerVo> byStoreVoPage = mdsProductInfoApi.findProductListByStore(queryVo);
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("page",byStoreVoPage);
		respVo.setData(map);
		return respVo;
	}
	/***
	 * TODO 门店管理    查询门店信息
	 */
	@ApiOperation(value = "查询门店信息详情 必传(hospitalId)")
	@PostMapping(value = "findHospitalInfoByStore")
	public RespVo<Map<String, Object>> findHospitalInfoByStore(@RequestBody QueryVo queryVo) 
	{
		logger.info("门店管理    查询门店信息详情>>>>>>>>>>>>>>>>>>>>findHospitalInfoByStore");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//查询门店数据
		MdsPropertyInfoEntity entity = mdsPropertyInfoApi.getEntity(queryVo.getHospitalId());
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("entity", entity);
		respVo.setData(map);
		return respVo;
	}
	/***
	 * TODO 门店管理    查询医院收费信息列表
	 */
	@ApiOperation(value = "查询医院收费信息列表 必传(hospitalId)")
	@PostMapping(value = "findHospitalPriceList")
	public RespVo<Map<String, Object>> findHospitalPriceList(@RequestBody QueryVo queryVo) 
	{
		logger.info("门店管理  查询医院收费信息列表>>>>>>>>>>>>>>>>>>>>findHospitalPriceList");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		//查询收费规则
		List<MdsHospitalPriceEntity> priceList = mdsHospitalPriceApi.findListByHospitalId(queryVo);
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("priceList", priceList);
		respVo.setData(map);
		return respVo;
	}
	/***
	 * TODO 编辑设备 编辑收费规则  代码待优化
	 */
	@ApiOperation(value = " 编辑设备 编辑收费规则",
			notes="操作类型:operType,门店Id:hospitalId,类别id:categoryId,价格信息Id:hospitalPriceId")
	@PostMapping(value = "saveHospitalPrice")
	public RespVo<Map<String, Object>> saveHospitalPrice(@RequestBody QueryVo queryVo) 
	{
		logger.info("编辑设备 编辑收费规则>>>>>>>>>>>>>>>>>>>>saveHospitalPrice");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		if(null != queryVo.getOperType())
		{
			if(HospConstants.OPER_TYPE_A.equals(queryVo.getOperType()))
			{
				if(null != queryVo.getHospitalId() || null != queryVo.getCategoryId())
				{
					MdsHospitalPriceEntity hospitalPriceEntity = new MdsHospitalPriceEntity();
					//设备类别信息
					MdsProductCategoryEntity productCategory = mdsProductCategoryApi.getEntity(queryVo.getCategoryId());
					hospitalPriceEntity.setCategoryId(productCategory.getId());
					hospitalPriceEntity.setCategoryFileId(productCategory.getCategoryFileId());
					hospitalPriceEntity.setCategoryName(productCategory.getCategoryName());
					hospitalPriceEntity.setCategoryCode(productCategory.getCategoryCode());
					//门店信息
					MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoApi.getEntity(queryVo.getHospitalId());
					hospitalPriceEntity.setHospitalCode(propertyInfo.getHospitalCode());
					hospitalPriceEntity.setHospitalName(propertyInfo.getHospitalName());
					hospitalPriceEntity.setHospitalId(queryVo.getHospitalId());
					transHospitalPrice(hospitalPriceEntity,queryVo);
					mdsHospitalPriceApi.save(hospitalPriceEntity);
					updateProducStatus(queryVo);
				}else
				{
					respVo.setCode(400);
					respVo.setMessage("HospitalId is null Or CategoryId is null !");
				}
			}
			if(HospConstants.OPER_TYPE_U.equals(queryVo.getOperType()))
			{
				if(null != queryVo.getHospitalPriceId())
				{
					MdsHospitalPriceEntity hospitalPriceEntity = new MdsHospitalPriceEntity();
					//设备类别信息
					MdsProductCategoryEntity productCategory = mdsProductCategoryApi.getEntity(queryVo.getCategoryId());
					hospitalPriceEntity.setCategoryId(productCategory.getId());
					hospitalPriceEntity.setCategoryFileId(productCategory.getCategoryFileId());
					hospitalPriceEntity.setCategoryName(productCategory.getCategoryName());
					hospitalPriceEntity.setCategoryCode(productCategory.getCategoryCode());
					//门店信息
					MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoApi.getEntity(queryVo.getHospitalId());
					hospitalPriceEntity.setHospitalCode(propertyInfo.getHospitalCode());
					hospitalPriceEntity.setHospitalName(propertyInfo.getHospitalName());
					hospitalPriceEntity.setHospitalId(queryVo.getHospitalId());
					transHospitalPrice(hospitalPriceEntity, queryVo);
					//设置Id
					hospitalPriceEntity.setId(queryVo.getHospitalPriceId());
					mdsHospitalPriceApi.save(hospitalPriceEntity);
					updateProducStatus(queryVo);
				}else
				{
					respVo.setCode(400);
					respVo.setMessage("hospitalPriceId is null !");
				}
			}
		}
		return respVo;
	}
	
	
	public void updateProducStatus(QueryVo queryVo){
		MdsHospitalPriceEntity entity = mdsHospitalPriceApi.findPrice(queryVo);
		BigDecimal A = new BigDecimal(0.00);
		if (entity.getLeaseDeposit().compareTo(BigDecimal.ZERO) == 1 && entity.getLeaseUnit().compareTo(BigDecimal.ZERO) == 1&& entity.getMinHour()> 0){
			MdsProductInfoVo productInfoVo = new MdsProductInfoVo();
			productInfoVo.setHospitalId(queryVo.getHospitalId());
			productInfoVo.setCategoryId(queryVo.getCategoryId());
			productInfoVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
			List<MdsProductInfoEntity> entityls = mdsProductInfoApi.findProductUseStatusByhospitalId(productInfoVo);
			if(entityls!=null && entityls.size()>0){
				for(MdsProductInfoEntity entitys:entityls){
					entitys.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
					mdsProductInfoApi.save(entitys);
				}
				
			}
	
		}
	}
	
	
	/**
	 * TODO 私有：门店价格属性赋值
	 *  <= minHour ; 25 topBalance ： 起步价 ; leaseUnit:3 单价
	 * */
	public MdsHospitalPriceEntity transHospitalPrice(MdsHospitalPriceEntity priceEntity, QueryVo queryVo) 
	{
		priceEntity.setLeaseDeposit(queryVo.getLeaseDeposit());//预付款
		priceEntity.setLeaseUnit(queryVo.getLeaseUnit());
		priceEntity.setTopBalance(queryVo.getTopBalance());
		priceEntity.setDecreasPrice(queryVo.getDecreasPrice());
		priceEntity.setMinimumPrice(queryVo.getMinimumPrice());
		priceEntity.setMinHour(queryVo.getMinHour());
		priceEntity.setOutHour(queryVo.getOutHour());
		return priceEntity;
	}
}
