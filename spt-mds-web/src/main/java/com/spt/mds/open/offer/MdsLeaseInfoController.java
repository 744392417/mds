package com.spt.mds.open.offer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.util.DateOperator;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsRepairInfoApi;
import com.spt.hospital.client.api.IMdsUserFeedBackApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.leaseInfo.LeaseInfoDetailVo;
import com.spt.hospital.client.vo.repair.RepairInfoDetailVo;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "租赁订单", description = "租赁订单")
@RestController
@RequestMapping(value = "/mdsLeaseInfo")
public class MdsLeaseInfoController 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	@Autowired
	private IMdsRepairInfoApi mdsRepairInfoApi;
	
	@Autowired
	private IMdsUserFeedBackApi mdsUserFeedBackApi;
	
	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	
	@Value("${pdf.file.path}")
	private String filePath;
	
	/***
	 * TODO 租赁订单查询
	 */
	@ApiOperation(value = "租赁订单查询列表")
	@PostMapping(value = "findLeaseInfoPage")
	public RespVo<Map<String, Object>> findLeaseInfoPage(@RequestBody QueryVo queryVo) 
	{
		logger.info("this controller method : findLeaseInfoPage >>>>>租赁订单查询列表>>>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		
		if (queryVo.getEndDate() != null) {
			queryVo.setEndDate(DateOperator.addDaysByDays(queryVo.getEndDate(), 1));
		}
		
		if(StringUtils.isNotBlank(queryVo.getType()))
		{
			MdsUserInfoEntity userEntity = mdsUserInfoApi.getEntity(queryVo.getUserId());
			//如果用户类型不是平台方且物业Id不为空
			if(!HospConstants.USER_TYPE_M.equals(userEntity.getUserType()))
			{
				queryVo.setHospitalId(userEntity.getHospitalId());
				//根据用户物业Id，查询分页数据
				Page<MdsLeaseInfoVo> page = mdsLeaseInfoApi.findLeaseInfoPageByProperty(queryVo);
				Map<String, Object> map = new HashMap<>();
				map.put("page", page);
				respVo.setData(map);
			}
		}else
		{
			Page<MdsLeaseInfoVo> page = mdsLeaseInfoApi.findLeaseInfoPageByProperty(queryVo);
			Map<String, Object> map = new HashMap<>();
			map.put("page", page);
			respVo.setData(map);
		}
		return respVo;
	}
	/***
	 * TODO 租赁订单查询 - 详情
	 */
	@ApiOperation(value = "租赁订单 详情 必传：leaseInfoId 租赁订单号")
	@PostMapping(value = "findLeaseInfo")
	public RespVo<Map<String, Object>> findLeaseInfo(@RequestBody QueryVo queryVo) 
	{
		logger.info("this method : findLeaseInfo>>>>>>>>>>>>>>>>>>>>");
		RespVo<Map<String, Object>> respVo = new RespVo<>();
		LeaseInfoDetailVo sendVo = new LeaseInfoDetailVo();
		//订单详情页
		MdsLeaseInfoEntity leaseInfoentity = mdsLeaseInfoApi.findByOrderNumber(queryVo);
		BeanUtils.copyProperties(leaseInfoentity, sendVo);
		//获取用户信息添加到VO
		List<MdsUserInfoEntity> userInfoEntityList = mdsUserInfoApi.findListByOpenid(leaseInfoentity.getOpenid());
	//BeanUtils.copyProperties(userInfoEntityList.get(0), sendVo);
		if(userInfoEntityList!=null && userInfoEntityList.size()>0){
			sendVo.setNickName(userInfoEntityList.get(0).getNickName());
			sendVo.setUserPhone(userInfoEntityList.get(0).getUserPhone());
		}
		
		MdsPropertyInfoEntity  propertyInfoEntity = mdsPropertyInfoApi.getEntity(leaseInfoentity.getHospitalId());

		sendVo.setHospitalName(propertyInfoEntity.getHospitalName());
		
		MdsProductInfoEntity  mdsProductInfoEntity = mdsProductInfoApi.getEntity(leaseInfoentity.getProductId());
		if(sendVo.getHospitalWard()==null || "".equals(sendVo.getHospitalWard())){
			sendVo.setHospitalWard(mdsProductInfoEntity.getHospitalWard());
		}
		
		
		Map<String, Object> map = new HashMap<>();
		//获取订单设备报修信息

		List<MdsRepairInfoEntity> repairInfoList = mdsRepairInfoApi.findByOrderNumber(queryVo);

		RepairInfoDetailVo repairInfoDetailVo = new RepairInfoDetailVo();
		if(repairInfoList!=null && repairInfoList.size()>0){
			
			MdsRepairInfoEntity repairInfo = repairInfoList.get(0);
			BeanUtils.copyProperties(repairInfo, repairInfoDetailVo);
			
			List<MdsUserInfoEntity> userInfoList = mdsUserInfoApi.findListByOpenid(leaseInfoentity.getOpenid());
			
			if (userInfoList != null && userInfoList.size() > 0) {
				repairInfoDetailVo.setNickName(userInfoList.get(0).getNickName());
			}
			if (repairInfo.getFileId() != null && !repairInfo.getFileId().equals("")) {
				repairInfoDetailVo.setImageUrl(filePath + repairInfo.getFileId());
			}
		}
		
		
		List<MdsUserFeedBackEntity>  feedBackls = mdsUserFeedBackApi.findByOrderNum(queryVo);
		if(feedBackls!=null && feedBackls.size()>0){
			MdsUserFeedBackEntity feedBack = feedBackls.get(0);
			sendVo.setFeedbackContent(feedBack.getFeedbackContent());
			sendVo.setDescribe(feedBack.getRemark());
		}

		map.put("repairInfo", repairInfoDetailVo);
		map.put("entity", sendVo);
		respVo.setData(map);
		return respVo;
	}
}
