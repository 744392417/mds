package com.spt.mds.open.vo;

import com.hsoft.commutil.exception.ErrorResp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "返回参数")
public class RespVo<T> {

	@ApiModelProperty(value = "httpCode",example="200")
	private Integer code = 200; // 200代表成功
	@ApiModelProperty(value = "中文信息",example="success")
	private String message ="success";
	@ApiModelProperty(value = "返回的对象")
	private T data;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	public void setFail(ErrorResp resp){
		if(resp!=null){
			this.message = resp.getMessage();
			this.code = resp.getErrorId();
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	
}
