package com.spt.mds.open.offer;


import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.PageDown;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.exception.ErrorResp;
import com.hsoft.commutil.shiro.ShiroUser;
import com.hsoft.commutil.util.DateOperator;
import com.hsoft.commutil.util.PoiExcelUtil;
import com.hsoft.file.client.remote.FileRemote;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsPropertyInfoApi;
import com.spt.hospital.client.api.IMdsUserInfoApi;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;
import com.spt.hospital.client.vo.user.MdsUserInfoMngVo;
import com.spt.mds.open.vo.RespVo;
import com.spt.mds.web.config.BasicErrorController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * @author hanson
 * */
@Api(value = "后台用户登录管理", description = "后台用户登录管理")
@RestController
@RequestMapping(value = "/mdsUserInfo")
public class MdsUserInfoController 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsUserInfoApi mdsUserInfoApi;
	
	@Autowired
	private IMdsPropertyInfoApi mdsPropertyInfoApi;
	
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	@SuppressWarnings("unused")
	@Autowired
	private FileRemote fileRemote;
	
	@Value("${file.bucket}")
	private String filebucket;
	@Value("${pdf.file.path}")
	private String filePath;
	
	/**
	 * TODO 用户管理 - 用户列表查询
	 * */
	@ApiOperation(value = "用户管理 - 用户列表查询" ,notes=""
			+ "参数:{用户名： nickName 起始时间:startDate,结束时间:endDate}")
	@PostMapping(value = "findUserInfoList")
	public RespVo<Map<String,Object>> findUserInfoList(@RequestBody QueryVo vo) throws ApplicationException 
	{
		if (vo.getEndDate() !=null) {
			vo.setEndDate(DateOperator.addDaysByDays(vo.getEndDate(), 1));
		}
		RespVo<Map<String,Object>> resultVo = new RespVo<>();
		logger.info("findUserInfoList>>>>>>>>>>>>>>>>>用户管理 - 用户列表查询");
		Page<MdsUserInfoMngVo> page = mdsUserInfoApi.findPageByUser(vo);
		Map<String, Object> map = new HashMap<>(50);
		map.put("page", page);
		resultVo.setData(map);
		return resultVo;
	}
	/**
	 * TODO 用户管理 - 详情
	 * */
	@ApiOperation(value = "用户管理 - 用户信息详情查询 必传(openid)" ,notes=""
			+ "参数:{用户openid:openid}")
	@PostMapping(value = "findUserInfo")
	public RespVo<Map<String,Object>> findUserInfo(@RequestBody QueryVo vo) throws ApplicationException 
	{
		RespVo<Map<String,Object>> resultVo = new RespVo<>();
		logger.info("findUserInfo>>>>>>>>>>>>>>>>>用户管理 - 用户信息详情查询");
		MdsUserInfoMngVo mdsUserInfoMngVo = mdsUserInfoApi.findUserInfoDetail(vo);
		
		Page<MdsLeaseInfoVo> pge = mdsLeaseInfoApi.findLeaseInfoPageByProperty(vo);

		Map<String, Object> map = new HashMap<>(50);
		map.put("mdsUserInfoMngVo", mdsUserInfoMngVo);
		map.put("UserInfoDetail", pge);
		resultVo.setData(map);
		return resultVo;
	}
	/**
	 * TODO 用户登录
	 * */
	@ApiOperation(value = "用户登录" ,notes="参数:{用户名:loginName,密码:loginPwd}")
	@PostMapping(value = "login")
	public RespVo<Map<String,Object>> login(@RequestBody MdsUserInfoVo loginVo) throws ApplicationException {
		RespVo<Map<String,Object>> resultVo = new RespVo<>();
		try 
		{
			Map<String,Object> map=new HashMap<String,Object>();
			ShiroUser user=mdsUserInfoApi.login(loginVo);
			map.put("userInfo", user);
			resultVo.setData(map); 
		} catch (Exception e) 
		{
			logger.error("login", e);
			ErrorResp errsp = BasicErrorController.getErrorResp(e);
			resultVo.setFail(errsp);
		}
		return resultVo;
	}
	/**
	 * TODO 用户注销 必传：id
	 * */
	@ApiOperation(value = "用户注销 必传：id")
	@PostMapping(value = "logout")
	public RespVo<Map<String,Object>> logout(@RequestBody MdsUserInfoVo loginVo) throws ApplicationException 
	{
		RespVo<Map<String,Object>> resultVo = new RespVo<>();
		try 
		{
			Map<String,Object> map=new HashMap<String,Object>();
			mdsUserInfoApi.logout(loginVo.getId());
			resultVo.setData(map); 
		} catch (Exception e) {
			logger.error("login", e);
			ErrorResp errsp = BasicErrorController.getErrorResp(e);
			resultVo.setFail(errsp);
		}
		return resultVo;
	}
	
	
	/**
	 * TODO 用户-导出报表
	 * @throws IOException 
	 * */
	@ApiOperation(value = "用户-导出报表")
	@GetMapping(value = "userExportReport")
	@ResponseBody
	public void userExportReport(QueryVo vo,HttpServletRequest request,HttpServletResponse response) throws IOException 
	{
		logger.info("this method productExportReport>>>>>>设备管理导出报表");
		//查询页面导出的数据
		if(vo.getEndDate() != null) {
			vo.setEndDate(DateOperator.addDaysByDays(vo.getEndDate(), 1));
		}
		
		PageDown<MdsUserInfoMngVo> productInfoVoPage = mdsUserInfoApi.findPageByUser(vo);
		int totalPages = productInfoVoPage.getTotalPages()+1;
		String title = "用户报表";
		String[] titles = new String[]{"用户名","手机号","注册时间","设备种类","订单数","预计收入总计(元)","实际收入总计(元)"};
		String[] attrs = new String[]{"nickName","userPhone","registerDateStr","categoryCount","orderCount","prepareCostCount","leaseCostCount"};
		int [] widths =new int[] {15,15,20,15,15,25,25};
		Workbook workbook = PoiExcelUtil.newWorkbook(PoiExcelUtil.WB_TYPE_2007);
		// 生成一个表格
		Sheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为 15 个字节
		sheet.setDefaultColumnWidth(15);
		// 产生表格标题行
		// 生成一个样式
		CellStyle cellStyle = PoiExcelUtil.getCellStyle(workbook);
		/** 创建表头 */
		int[] widthes = new int[titles.length];
		for (int i = 0; i < titles.length; i++) 
		{
			widthes[i] = widths[i];
		}
		PoiExcelUtil.creatHeads(workbook, sheet, titles, widthes);
		int start =0;
		
		while (productInfoVoPage!=null && productInfoVoPage.getContent()!=null&& productInfoVoPage.getContent().size()>0) 
		{
			PoiExcelUtil.createRows(sheet, productInfoVoPage.getContent(), attrs, start, cellStyle,DateOperator.FORMAT_STR);
			productInfoVoPage.setTotalPages(totalPages);
			if (productInfoVoPage.hasNext()) 
			{
				vo.setPage(vo.getPage()+1);
				productInfoVoPage = mdsUserInfoApi.findPageByUser(vo);
				start += vo.getRows();
			}else 
			{
				productInfoVoPage=null;
			}
		}
		try 
		{
			PoiExcelUtil.write(workbook, response, title);
		} catch (IOException e) 
		{
		  logger.error(e.getMessage(),e);
		}
	}

	
	
	
	
	
}
