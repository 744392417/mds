package com.spt.mds.open.vo;

public class LoginParamVo {
	private String loginName;
	private String email;
	private String mobile;
	private String mailActive;
	private Long deptId;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMailActive() {
		return mailActive;
	}

	public void setMailActive(String mailActive) {
		this.mailActive = mailActive;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}		
}
