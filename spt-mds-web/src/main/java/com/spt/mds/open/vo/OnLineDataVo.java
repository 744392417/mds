package com.spt.mds.open.vo;

import java.math.BigDecimal;

/**
 * 线上数据传输实体
 * */
public class OnLineDataVo 
{
	private Integer distanceNowDay;							//首次登录距今天数
	private BigDecimal everyDayLoginFrequency;		//日均登录频率
	private BigDecimal everyDayLoginHours;				//日均登录时长（小时）
	private Integer inquiryOrSellCount;						//询价和挂单次数
	private Integer otherInquiryOrSellCount;				//被应单次数
	private Integer orderReceivingNumber;			 	//应单次数
	private BigDecimal tradeNumberCount;				//成交量（吨）
	private BigDecimal tradeMoney;							//成交额（万元）
	private Integer resaleOrderCount;						//转售挂单次数
	private Integer resaleDealCount;							//转售成交次数
	private BigDecimal resaleDealProfit;						//转售成交利润（万元）
	private BigDecimal payPoundage;						//支付手续费总额（万元）
	private BigDecimal everyDayFreeze;						//日均冻结沉淀资金（万元）
	private BigDecimal everyDayAvailableFreeze;		//日均可用资金沉淀（万元）
	private Integer chasingInsuranceHours;				//追保时效（据截止时间小时数）
	private Integer deliveryCount;								//交收次数
	private Integer originalSellOnCreditRepayHours;	//往期赊销还款时效（据截止时间小时数）
	private Integer originalPaymentHours;					//往期现金付款时效（据截止时间小时数）
	private Integer ticketOpeningHours;						//开票时效（据截止时间小时数）
	private Integer chasingInsuranceCount;				//追保次数
	private Integer chasingInsuranceMoney;				//追保金额
	
	public Integer getDistanceNowDay() {
		return distanceNowDay;
	}
	public void setDistanceNowDay(Integer distanceNowDay) {
		this.distanceNowDay = distanceNowDay;
	}
	public BigDecimal getEveryDayLoginFrequency() {
		return everyDayLoginFrequency;
	}
	public void setEveryDayLoginFrequency(BigDecimal everyDayLoginFrequency) {
		this.everyDayLoginFrequency = everyDayLoginFrequency;
	}
	public BigDecimal getEveryDayLoginHours() {
		return everyDayLoginHours;
	}
	public void setEveryDayLoginHours(BigDecimal everyDayLoginHours) {
		this.everyDayLoginHours = everyDayLoginHours;
	}
	public Integer getInquiryOrSellCount() {
		return inquiryOrSellCount;
	}
	public void setInquiryOrSellCount(Integer inquiryOrSellCount) {
		this.inquiryOrSellCount = inquiryOrSellCount;
	}
	public Integer getOtherInquiryOrSellCount() {
		return otherInquiryOrSellCount;
	}
	public void setOtherInquiryOrSellCount(Integer otherInquiryOrSellCount) {
		this.otherInquiryOrSellCount = otherInquiryOrSellCount;
	}
	public Integer getOrderReceivingNumber() {
		return orderReceivingNumber;
	}
	public void setOrderReceivingNumber(Integer orderReceivingNumber) {
		this.orderReceivingNumber = orderReceivingNumber;
	}
	public BigDecimal getTradeNumberCount() {
		return tradeNumberCount;
	}
	public void setTradeNumberCount(BigDecimal tradeNumberCount) {
		this.tradeNumberCount = tradeNumberCount;
	}
	public BigDecimal getTradeMoney() {
		return tradeMoney;
	}
	public void setTradeMoney(BigDecimal tradeMoney) {
		this.tradeMoney = tradeMoney;
	}
	public Integer getResaleOrderCount() {
		return resaleOrderCount;
	}
	public void setResaleOrderCount(Integer resaleOrderCount) {
		this.resaleOrderCount = resaleOrderCount;
	}
	public Integer getResaleDealCount() {
		return resaleDealCount;
	}
	public void setResaleDealCount(Integer resaleDealCount) {
		this.resaleDealCount = resaleDealCount;
	}
	public BigDecimal getResaleDealProfit() {
		return resaleDealProfit;
	}
	public void setResaleDealProfit(BigDecimal resaleDealProfit) {
		this.resaleDealProfit = resaleDealProfit;
	}
	public BigDecimal getPayPoundage() {
		return payPoundage;
	}
	public void setPayPoundage(BigDecimal payPoundage) {
		this.payPoundage = payPoundage;
	}
	public BigDecimal getEveryDayFreeze() {
		return everyDayFreeze;
	}
	public void setEveryDayFreeze(BigDecimal everyDayFreeze) {
		this.everyDayFreeze = everyDayFreeze;
	}
	public BigDecimal getEveryDayAvailableFreeze() {
		return everyDayAvailableFreeze;
	}
	public void setEveryDayAvailableFreeze(BigDecimal everyDayAvailableFreeze) {
		this.everyDayAvailableFreeze = everyDayAvailableFreeze;
	}
	public Integer getChasingInsuranceHours() {
		return chasingInsuranceHours;
	}
	public void setChasingInsuranceHours(Integer chasingInsuranceHours) {
		this.chasingInsuranceHours = chasingInsuranceHours;
	}
	public Integer getDeliveryCount() {
		return deliveryCount;
	}
	public void setDeliveryCount(Integer deliveryCount) {
		this.deliveryCount = deliveryCount;
	}
	public Integer getOriginalSellOnCreditRepayHours() {
		return originalSellOnCreditRepayHours;
	}
	public void setOriginalSellOnCreditRepayHours(Integer originalSellOnCreditRepayHours) {
		this.originalSellOnCreditRepayHours = originalSellOnCreditRepayHours;
	}
	public Integer getOriginalPaymentHours() {
		return originalPaymentHours;
	}
	public void setOriginalPaymentHours(Integer originalPaymentHours) {
		this.originalPaymentHours = originalPaymentHours;
	}
	public Integer getTicketOpeningHours() {
		return ticketOpeningHours;
	}
	public void setTicketOpeningHours(Integer ticketOpeningHours) {
		this.ticketOpeningHours = ticketOpeningHours;
	}
	public Integer getChasingInsuranceCount() {
		return chasingInsuranceCount;
	}
	public void setChasingInsuranceCount(Integer chasingInsuranceCount) {
		this.chasingInsuranceCount = chasingInsuranceCount;
	}
	public Integer getChasingInsuranceMoney() {
		return chasingInsuranceMoney;
	}
	public void setChasingInsuranceMoney(Integer chasingInsuranceMoney) {
		this.chasingInsuranceMoney = chasingInsuranceMoney;
	}
}
