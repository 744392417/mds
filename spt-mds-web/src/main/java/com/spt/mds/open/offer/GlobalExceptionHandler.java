package com.spt.mds.open.offer;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.hsoft.commutil.exception.ErrorResp;
import com.spt.mds.open.vo.RespVo;
import com.spt.mds.web.config.BasicErrorController;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public RespVo<?> exceptionHandler(Exception e){
		RespVo<?> respVo=new RespVo<>();
		ErrorResp errsp = BasicErrorController.getErrorResp(e);
		respVo.setFail(errsp);
		return respVo;
	}
	

}
