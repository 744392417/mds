package com.spt.mds.open.offer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hsoft.commutil.props.PropertiesUtil;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.file.client.constant.FileConstant;
import com.hsoft.file.client.remote.FileRemote;
import com.hsoft.file.client.vo.Base64DataVo;
import com.hsoft.file.client.vo.FileRespVo;
import com.hsoft.file.client.vo.FileUploadBase64Request;
import com.spt.mds.open.vo.RespVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import sun.misc.BASE64Encoder;

@Api(value = "上传文件", description = "上传文件")
@RestController
@RequestMapping(value = "/mds/fileUpload")
public class FileUploadController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private FileRemote fileRemote;

	@Value("${file.bucket}")
	private String filebucket;

	@Value("${pdf.file.path}")
	private String filePath;


	@ApiOperation(value = "上传图片文件接口")
	@PostMapping(value = "fileUploadFile")
	public RespVo<FileRespVo> fileUploadFile(@RequestParam("file") MultipartFile file) {
		logger.info("fileUploadFile>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		RespVo<FileRespVo> resultVo = new RespVo<>();
		FileRespVo fVo = null;
		if (file.isEmpty() == false) {
			try {
				String fileName = file.getOriginalFilename();
				// 获取扩展
				String extName = fileName.substring(fileName.lastIndexOf("."));// .jpg
				String uuid = UUID.randomUUID().toString().replace("-", "");
				// 新名称
				String newName = uuid + extName;
				InputStream is = file.getInputStream();
				fVo = uploadFileImg(is, newName);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		resultVo.setData(fVo);
		return resultVo;
	}

	public FileRespVo uploadFileImg(InputStream input, String filename) {
		FileRespVo vo = new FileRespVo();
		try {
			FileUploadBase64Request fileRequest = new FileUploadBase64Request();
			fileRequest.setFilePath(filebucket);
			fileRequest.setAppCode(PropertiesUtil.getProperty(FileConstant.FILE_BUCKET));
			List<Base64DataVo> dataList = new ArrayList<Base64DataVo>();
			// 获取rar包字节流
			String imgData = getBytes(input);
			// 将上传的文件转为base64
			Base64DataVo dataVo = new Base64DataVo();
			dataVo.setFileName(filename);
			dataVo.setBase64Data(imgData);
			dataList.add(dataVo);
			fileRequest.setDataList(dataList);
			vo = fileRemote.uploadBase64(fileRequest);
			System.out.println(JsonUtil.obj2Json(vo));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}

	private String getBytes(InputStream input) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024 * 1024];
		int byteread = 0;
		while ((byteread = input.read(buffer)) != -1) {
			out.write(buffer, 0, byteread);
			out.flush();
		}
		byte[] buffer_read = out.toByteArray();

		return new BASE64Encoder().encode(buffer_read);
	}

}
