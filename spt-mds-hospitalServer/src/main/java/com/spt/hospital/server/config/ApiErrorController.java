package com.spt.hospital.server.config;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hsoft.commutil.constants.CommonErrorId;
import com.hsoft.commutil.exception.ErrorResp;
import com.hsoft.commutil.exception.SystemException;
import com.hsoft.commutil.exception.WebApplicationException;
import com.hsoft.commutil.util.JsonUtil;

@Controller
public class ApiErrorController implements ErrorController {
	private static final String ERR_STATUSCODE = "javax.servlet.error.status_code";
	private static final String ERR_EXCEPTION = "javax.servlet.error.exception";

	private static Logger log = LoggerFactory.getLogger(ApiErrorController.class);

	@Value("${error.path:/error}")
	private String errorPath;

	@Override
	public String getErrorPath() {
		return errorPath;
	}

	@RequestMapping(value = "${error.path:/error}", produces = "application/json")
	public @ResponseBody ResponseEntity<ErrorResp> error(HttpServletRequest request) {
		
		final int status = getErrorStatus(request);
		final ErrorResp resp = getErrorMessage(request);

		return ResponseEntity.status(status).body(resp);
	}

	private int getErrorStatus(HttpServletRequest request) {
		Integer statusCode = (Integer) request.getAttribute(ERR_STATUSCODE);

		log.info(String.format(">>>>ApiErrorController.getErrorStatus - %s", statusCode));

		return statusCode != null ? statusCode : HttpStatus.INTERNAL_SERVER_ERROR.value();
	}

	private ErrorResp getErrorMessage(HttpServletRequest request) {
		ErrorResp error = new ErrorResp();
		String uri =(String) request.getAttribute("javax.servlet.forward.request_uri");
		error.setUri(uri);
		Throwable exc = (Throwable) request.getAttribute(ERR_EXCEPTION);
		if (exc == null) {
			error.setErrorId(CommonErrorId.ERROR_UNKNOWN);
			error.setMessage("未知错误");
			return error;
		}
		// String causeMsg =exc.getLocalizedMessage();
		String msg = null;
		exc = getCauseException(exc);
		if (exc instanceof SystemException) {
			SystemException sysEx = (SystemException) exc;
			int errorId = sysEx.getErrorId();
			String message = sysEx.getMessage();
			error.setErrorId(errorId);
			error.setMessage(message);
			// error.setCauseMsg(causeMsg);
		}if (exc instanceof WebApplicationException) {
			WebApplicationException sysEx = (WebApplicationException) exc;
			error = sysEx.getResp();
			// error.setCauseMsg(causeMsg);
		} else {
			msg = exc != null ? exc.getMessage() : "Unexpected error occurred";
			error.setErrorId(CommonErrorId.ERROR_UNKNOWN);
			error.setMessage(msg);
		}

		log.info(">>>>ApiErrorController.getErrorMessage - {}", JsonUtil.obj2Json(error));

		return error;
	}
	
	private Throwable getCauseException(Throwable e){
		Throwable exc = e.getCause();
		if (exc!=null){
			if (exc instanceof SystemException) {
				return exc;
			} else {
				return getCauseException(exc);
			}
			
		}else{
			return e;
		}
	}
}