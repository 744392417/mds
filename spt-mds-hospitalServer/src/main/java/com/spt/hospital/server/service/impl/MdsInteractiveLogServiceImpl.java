package com.spt.hospital.server.service.impl;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsInteractiveLogEntity;
import com.spt.hospital.server.dao.MdsInteractiveLogDao;
import com.spt.hospital.server.service.IMdsInteractiveLogService;


@Component
@Transactional(readOnly = false)
public class MdsInteractiveLogServiceImpl extends BaseService<MdsInteractiveLogEntity> implements IMdsInteractiveLogService 
{
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsInteractiveLogServiceImpl.class);
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Autowired
	private MdsInteractiveLogDao mdsInteractiveLogDao;
	
	@Override
	public BaseDao<MdsInteractiveLogEntity> getBaseDao() 
	{
		return mdsInteractiveLogDao;
	}

}
