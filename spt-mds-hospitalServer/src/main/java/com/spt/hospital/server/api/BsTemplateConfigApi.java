//package com.spt.credit.server.api;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.hsoft.commutil.base.BaseApi;
//import com.hsoft.commutil.base.IBaseService;
//import com.spt.offer.client.entity.BsTemplateConfig;
//import com.spt.offer.client.vo.DictDataVo;
//import com.spt.offer.client.vo.TemplateQueryVo;
//import com.spt.offer.server.service.IBsTemplateConfigService;
//import com.spt.offer.server.util.TemplateContentUtility;
//
//
//@RestController
//@RequestMapping(value = "api/bs/templateConfig")
//public class BsTemplateConfigApi extends BaseApi<BsTemplateConfig> {
//	@Autowired
//	private IBsTemplateConfigService bsTemplateConfigService;
//	
//	@Override
//	public IBaseService<BsTemplateConfig> getService() {
//		return bsTemplateConfigService;
//	}
//	
//	@PostMapping("templateMap")
//	public Map<String, Object> getTemplateMap(@RequestBody TemplateQueryVo queryVo){
//		return bsTemplateConfigService.getTemplateMap(queryVo);
//	}
//	
//	@PostMapping("templateByDictCd")	
//	public List<DictDataVo> getTemplateByDictCd(@RequestBody TemplateQueryVo queryVo){
//		return bsTemplateConfigService.getTemplateByDictCd(queryVo);
//	}
//	
//	@PostMapping("queryTemplate")
//	BsTemplateConfig queryTemplate(@RequestBody TemplateQueryVo queryVo){
//		return TemplateContentUtility.getTemplate(queryVo.getTemplateCat(), queryVo.getTypeCd(), queryVo.getLang());
//	}
//	
//	
//}
//
