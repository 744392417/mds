package com.spt.hospital.server.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.vo.MdsProductCategoryVo;
import com.spt.hospital.client.vo.QueryVo;


public interface IMdsProductCategoryService extends IBaseService<MdsProductCategoryEntity>{

	MdsProductCategoryEntity findByName(MdsProductCategoryVo vo);
	
	List<MdsProductCategoryEntity> findByCategoryinfo();

	MdsProductCategoryEntity findByCategory(@RequestBody QueryVo queryVo);
}

