package com.spt.hospital.server.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsMoneyTransferEntity;

import com.spt.hospital.server.dao.MdsMoneyTransferDao;
import com.spt.hospital.server.service.IMdsMoneyTransferService;



@Component
@Transactional(readOnly = false)
public class MdsMoneyTransferServiceImpl extends BaseService<MdsMoneyTransferEntity> implements IMdsMoneyTransferService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsMoneyTransferServiceImpl.class);

	@Autowired
	private MdsMoneyTransferDao mdsMoneyTransferDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsMoneyTransferEntity> getBaseDao() {
		return mdsMoneyTransferDao;
	}
	
}
