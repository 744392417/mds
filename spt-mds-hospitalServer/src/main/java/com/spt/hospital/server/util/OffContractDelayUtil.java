/*package com.spt.credit.server.util;

import java.util.Date;
import java.util.List;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsoft.commutil.util.SpringContextHolder;
import com.spt.credit.server.service.IOffContractService;
import com.spt.offer.client.entity.OffContract;

public class OffContractDelayUtil {

	private static final Logger logger = LoggerFactory.getLogger(OffContractDelayUtil.class);

	private static final ExecutorService threadPool = Executors.newSingleThreadExecutor();
	private static final ExecutorService threadPoolRun = Executors.newSingleThreadExecutor();

	private static final DelayQueue<OffContract> delayQueue = new DelayQueue<OffContract>();

	private static volatile boolean running = true;

	private static IOffContractService offContractService = SpringContextHolder.getBean(IOffContractService.class);

	public static void init() {
		logger.info(">>>>>> start contract DelayQueen! <<<<<<");
		running = true;
		initExpireMonitor();
		threadPool.execute(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					if (!running)
						break;
					try {
						OffContract delay = delayQueue.take();
						final Long contractId = delay.getId();
						logger.info("取得成交订单,contractId:{}",contractId);
						threadPoolRun.execute(new Runnable() {

							@Override
							public void run() {
								try {
									offContractService.invalidConractStatus(contractId);
								} catch (Exception e) {
								}
							}
						});
					} catch (Exception e) {
						logger.warn("offContract invalid error ", e);
					}
				}
			}
		});
	}

	public void destroy() throws Exception {

		running = false;
		delayQueue.clear();
		threadPool.shutdown();
	}

	public static void addExpireMonitor(Long targetId,Date endTime) {
		OffContract delay = new OffContract();
		delay.setId(targetId);
		delay.setValidTime(endTime);
		boolean result = delayQueue.add(delay);
		logger.info("addExpireMonitor targetId:{},result:{},endTime:{}",targetId,result,endTime);
	}
	
	public static void removeExpireMonitor(Long treatyId) {
		
		OffContract delay = new OffContract();
		delay.setId(treatyId);
		boolean result = delayQueue.remove(delay);
		logger.info("removeExpireMonitor treatyId:{},result:{}",treatyId,result);
	}

	
	private static void initExpireMonitor() {
		//获取有效的竞价记录
		List<OffContract> list = offContractService.findPreTardeContractList();
		if (CollectionUtils.isEmpty(list))
			return;
		logger.info("initExpireMonitor size:{}",list.size());
		for (OffContract entity : list) {
			if(entity.getValidTime().compareTo(new Date())<0){
				offContractService.invalidConractStatus(entity.getId());
			}else{
				addExpireMonitor(entity.getId(),entity.getValidTime());
			}
		}
	}

}
*/