package com.spt.hospital.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class UUIDUtil {
	/**
	 * 
	 * @return String UUID
	 */
	public static String getUUID() {
		String s = UUID.randomUUID().toString();
		return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
	}

	/**
	 * @param number
	 *            int
	 * @return String[] UUID
	 */
	public static String[] getUUID(int number) {
		if (number < 1) {
			return null;
		}
		String[] ss = new String[number];
		for (int i = 0; i < number; i++) {
			ss[i] = getUUID();
		}
		return ss;
	}
	public  static void main(String[]argss){
		System.out.println(UUIDUtil.getUUID());
	}
	
	/**
	 * 
	 * @return 产品编号(日期+随机数)
	 */
	public static String getDateUUID() {
		 SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");  
	        String temp = sf.format(new Date());  
	       //获取6位随机数
	        int random=(int) ((Math.random()+1)*100000);  
	        return temp+random;
	}

}
