/**
 * 
 */
package com.spt.hospital.server.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.commutil.CommandExecutor;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.server.cache.UserCache;

/**
 * 应用启动监听器
 * 
 * @author huangjian
 *
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private CommandExecutor executor;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// 初始化缓存
		DictUtil.init(HospConstants.PROJECT_CODE);
		UserCache.init();
/*		TemplateContentUtility.init();
		ProductTypeUtility.init();
		BrandUtil.init();
		FactoryUtil.init();
		OffPriceDelayUtil.init();
		OffContractDelayUtil.init();*/
		logger.info("---Application已启动---");
		new Thread(executor).start();
	}

}