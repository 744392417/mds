package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsCustomerServiceEntity;
import com.spt.hospital.server.service.IMdsCustomerService;




@RestController
@RequestMapping(value = "api/mdsCustomer")
public class MdsCustomerApi extends BaseApi<MdsCustomerServiceEntity> {
	@Autowired
	private IMdsCustomerService mdsCustomerService;

	@Override
	public IBaseService<MdsCustomerServiceEntity> getService() {
		return mdsCustomerService;
	}

}
