package com.spt.hospital.server.api;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.util.PriceUtils;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.service.IMdsPropertyInfoService;
import com.spt.hospital.server.service.IMdsRecordsConptionService;
import com.spt.hospital.server.service.IMdsRepairInfoService;
import com.spt.hospital.server.service.IMdsUserFeedBackService;
import com.spt.hospital.server.service.IMdsUserInfoService;

@RestController
@RequestMapping(value = "api/mdsLeaseInfo")
public class MdsLeaseInfoApi extends BaseApi<MdsLeaseInfoEntity> 
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsLeaseInfoService mdsLeaseInfoService;
	@Autowired
	private IMdsPropertyInfoService mdsPropertyInfoService;
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	
	@Autowired
	private IMdsRepairInfoService mdsRepairInfoService;
	
	@Autowired
	private IMdsUserFeedBackService mdsUserFeedBackService;
	
	@Autowired
	private IMdsUserInfoService mdsUserInfoService;	
	
	@Autowired
	private IMdsRecordsConptionService mdsRecordsConptionService;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("${fileurl}")
	String fileurl;
	
	@Override
	public IBaseService<MdsLeaseInfoEntity> getService() 
	{
		return mdsLeaseInfoService;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findByOpenid")
	public Page<MdsLeaseInfoVo> findByOpenid(@RequestBody QueryVo queryVo) {
		
		if (StringUtils.isNotBlank(queryVo.getOrderStatus())) {
				transList(queryVo);
			
		}
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);

		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  l  from  MdsLeaseInfoEntity l  where 1=1 ");
		// openId
		if (StringUtils.isNotBlank(queryVo.getOpenid())) {
			sql.append(" and l.openid = :openid");
			searchParams.put("openid", queryVo.getOpenid());
		}
		// 订单编号
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) {
			sql.append(" and l.orderNumber = :orderNumber");
			searchParams.put("orderNumber", queryVo.getOrderNumber());
		}
		// 品类名称
		if (StringUtils.isNotBlank(queryVo.getCategoryName())) {
			sql.append(" and l.categoryName = :categoryName");
			searchParams.put("categoryName", queryVo.getCategoryName());
		}
		// 使用状态
		if (queryVo.getOrderStatusList()!=null && queryVo.getOrderStatusList().size()>0) {
			// sql.append(" and l.orderStatus in ('0','2')");
			// searchParams.put("orderStatus", queryVo.getOrderStatus());
			sql.append(" and l.orderStatus in(:orderStatus)");
			searchParams.put("orderStatus", queryVo.getOrderStatusList());
		}
		// 医院名称
		if (StringUtils.isNotBlank(queryVo.getHospitalName())) {
			sql.append(" and l.hospitalName = :hospitalName");
			searchParams.put("hospitalName", queryVo.getHospitalName());
		}
		sql.append(" order by l.leaseStartDate desc ");

		logger.info("findByOpenid>>>>" + sql.toString());

		Page<MdsLeaseInfoEntity> page = (Page<MdsLeaseInfoEntity>) commonDao.findPage(sql.toString(), searchParams,
				pageRequest);

		List<MdsLeaseInfoVo> listVo = new ArrayList<>();

		if (page != null && page.getContent().size() > 0) {
			for (MdsLeaseInfoEntity entity : page.getContent()) {
				MdsLeaseInfoVo leaseInfoVo = new MdsLeaseInfoVo();
				// BeanUtils.copyProperties(entity, leaseInfoVo);
				// MdsHospitalPriceEntity hospitalPrice =
				// mdsHospitalPriceService.findPrice(leaseInfoVo.getCategoryId(),leaseInfoVo.getHospitalId());
				// leaseInfoVo.setLeaseDeposit(hospitalPrice.getLeaseDeposit());
				// leaseInfoVo.setLeaseUnit(hospitalPrice.getLeaseUnit());
				// leaseInfoVo.setTopBalance(hospitalPrice.getTopBalance());
				// entity.setLeaseEndDate(new Date());
				// leaseInfoVo.setLeaseTime(DateParser.getDateHour(entity.getLeaseEndDate(),
				// entity.getLeaseStartDate()));
				// Integer expectedLeaseCost =
				// PriceUtils.calculatePriceNew(hospitalPrice.getLeaseUnit().intValue(),
				// hospitalPrice.getTopBalance().intValue(),
				// hospitalPrice.getMinHour().intValue(),entity.getLeaseStartDate(),
				// entity.getLeaseEndDate());
				// BigDecimal expectedLeaseCostB = new
				// BigDecimal(expectedLeaseCost);
				// leaseInfoVo.setExpectedLeaseCost(expectedLeaseCostB);
				// //leaseInfoVo.setExpectedLeaseCost(hospitalPrice.getLeaseUnit().subtract(BigDecimal.valueOf(leaseInfoVo.getLeaseTime())));
				// leaseInfoVo.setChargingRules("<=
				// "+hospitalPrice.getMinHour()+" 小时
				// "+hospitalPrice.getLeaseUnit().intValue()+" 元/单,超出后
				// "+hospitalPrice.getTopBalance().intValue()+" 元/小时 ");
					trans(leaseInfoVo, entity);
				listVo.add(leaseInfoVo);
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsLeaseInfoVo> pageVo = new PageImpl<>(listVo, pageRequestn, page.getTotalElements());
		return pageVo;
	}
	
	@PostMapping("saveMdsLeaseInfo")
	public void saveMdsLeaseInfo(@RequestBody MdsLeaseInfoVo vo) 
	{
		vo.setCreatedDate(new Date());
		mdsLeaseInfoService.saveMdsLeaseInfo(vo);
	}
	@PostMapping("findListByOpenid")
	public List<MdsLeaseInfoEntity> findListByOpenid(@RequestBody QueryVo vo)
	{
		return mdsLeaseInfoService.findListByOpenid(vo.getOpenid());
	}
	
	
	@PostMapping("findByOrderNumber")
	public MdsLeaseInfoEntity findByOrderNumber(@RequestBody QueryVo vo){
		return mdsLeaseInfoService.findByOrderNumber(vo.getOrderNumber());
	}
	
	@PostMapping("findInuseByProductCode")
	public List<MdsLeaseInfoEntity> findInuseByProductCode(@RequestBody QueryVo queryVo){
		return mdsLeaseInfoService.findInuseByProductCode(queryVo);
	}
	
	@PostMapping("findByCodeAndId")
	public List<MdsLeaseInfoEntity> findByCodeAndId(@RequestBody QueryVo queryVo) {
		return mdsLeaseInfoService.findByCodeAndId(queryVo.getProductId());
	}
	
	@PostMapping("findLeaseInfoByOrderNumber")
	public MdsLeaseInfoVo findLeaseInfoByOrderNumber(@RequestBody QueryVo vo)
	{
		MdsLeaseInfoVo leaseInfoVo = new MdsLeaseInfoVo();
		MdsLeaseInfoEntity entity = mdsLeaseInfoService.findByOrderNumber(vo.getOrderNumber());
		if(entity!=null){
			trans(leaseInfoVo,entity);
		}
		return leaseInfoVo;
		
	}
	
	public MdsLeaseInfoVo trans(MdsLeaseInfoVo leaseInfoVo, MdsLeaseInfoEntity entity) {

		BeanUtils.copyProperties(entity, leaseInfoVo);
		logger.info("leaseInfoVo.getCategoryId()===="+leaseInfoVo.getCategoryId());
		logger.info("leaseInfoVo.getHospitalId()===="+leaseInfoVo.getHospitalId());
		logger.info("====getOrderNumber====="+leaseInfoVo.getOrderNumber());
		MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceService.findPrice(leaseInfoVo.getCategoryId(),
				leaseInfoVo.getHospitalId());
	//	logger.info("====trans====="+(leaseInfoVo==null));
		//leaseInfoVo.setLeaseDeposit(hospitalPrice.getLeaseDeposit());
		
			
		leaseInfoVo.setLeaseUnit(hospitalPrice.getLeaseUnit());
		leaseInfoVo.setTopBalance(hospitalPrice.getTopBalance());
        if(entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)){
        	leaseInfoVo.setLeaseTime(DateParser.getDateHour(entity.getLeaseEndDate(), entity.getLeaseStartDate()));
        }else{
        	leaseInfoVo.setLeaseTime(DateParser.getDateHour(new Date(), entity.getLeaseStartDate()));
        }
		
		BigDecimal expectedLeaseCost = BigDecimal.ZERO;
		 expectedLeaseCost = PriceUtils.calculatePriceNew(hospitalPrice.getLeaseUnit(),
				hospitalPrice.getTopBalance(), hospitalPrice.getMinHour().intValue(),
				entity.getLeaseStartDate(), new Date(),hospitalPrice.getOutHour());
		// logger.info("====getOrderNumber====11="+leaseInfoVo.getOrderNumber());
		//	logger.info("====expectedLeaseCost====="+expectedLeaseCost);
	//	BigDecimal expectedLeaseCostB = new BigDecimal(expectedLeaseCost);
			if(!entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)){
			//	logger.info("====expectedLeaseCost====5555=");
				leaseInfoVo.setExpectedLeaseCost(expectedLeaseCost);//预计租金
				leaseInfoVo.setLeaseCost(expectedLeaseCost);//租金
			}else{
				
			//	logger.info("====expectedLeaseCost====66=");
				leaseInfoVo.setExpectedLeaseCost(leaseInfoVo.getLeaseCost());//预计租金
			}
			//logger.info("====expectedLeaseCost====="+leaseInfoVo.getLeaseCost());
		leaseInfoVo.setMinHour(hospitalPrice.getMinHour());
		leaseInfoVo.setOutHour(hospitalPrice.getOutHour());

		leaseInfoVo.setIntoBalance(leaseInfoVo.getLeaseDeposit().subtract(leaseInfoVo.getLeaseCost()));//退款
		leaseInfoVo.setChargingRules("<= " + hospitalPrice.getMinHour() + " 小时 " + hospitalPrice.getLeaseUnit().intValue()
						+ " 元/单,超出后 " + hospitalPrice.getTopBalance().intValue() + " 元/小时 ");
		
		return leaseInfoVo;

	}
	
	
	
	
	public QueryVo transList(QueryVo queryVo) {

		List<String> orderStatusList = new ArrayList<String>();
		if (queryVo.getOrderStatus().indexOf(",") != -1) {
			String[] orderStatusArr = queryVo.getOrderStatus().split(",");
			for (int i = 0; i < orderStatusArr.length; i++) {
				orderStatusList.add(orderStatusArr[i]);
			}
		} else {
			orderStatusList.add(queryVo.getOrderStatus());
		}
		queryVo.setOrderStatusList(orderStatusList);

		return queryVo;

	}
	
	
	@PostMapping("returnLeaseInfo")
	public void returnLeaseInfo(@RequestBody MdsLeaseInfoEntity entity) {

		logger.info("=============returnLeaseInfo===getHospitalId===="+entity.getHospitalId());
		logger.info("=============returnLeaseInfo====getOrderNumber==="+entity.getOrderNumber());
		MdsPropertyInfoEntity propertyEntity = mdsPropertyInfoService.getEntity(entity.getHospitalId());
	    //判断是否分账
		List<MdsRecordsConptionEntity> recordsls = mdsRecordsConptionService.findByFOrderNumber(entity.getOrderNumber());
		
	//	logger.info("=============returnLeaseInfo===2222=="+recordsls.size());
		if (recordsls != null && recordsls.size() > 0) {

			for (MdsRecordsConptionEntity records : recordsls) {
		//		logger.info("=============returnLeaseInfo=2222===="+records.getOrderNumber());
				mdsRecordsConptionService.upRecordsConptionEntity(records.getOrderNumber());
			}

		}
		entity = getFashion(entity, propertyEntity);// 分账
		// 退款流水
		try {
			mdsLeaseInfoService.save(entity);
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 各方 分账 分账比例
	public MdsLeaseInfoEntity getFashion(MdsLeaseInfoEntity entityOld, MdsPropertyInfoEntity proEntity) 
	{
		logger.info("=============returnLeaseInfo====getOrderNumber==="+entityOld.getOrderNumber());
		BigDecimal hospitalFashion = BigDecimal.ZERO;
		BigDecimal properFashion = BigDecimal.ZERO;
		BigDecimal agentFashion = BigDecimal.ZERO;
		BigDecimal platFashion = BigDecimal.ZERO;
		// 平台收入分配比例
		Integer platDistribution = 0;
	//	logger.info("总共收入>>>>>>entityOld.getLeaseCost()>>>>>>>"+entityOld.getLeaseCost());
			
		// 医院分账
		if (proEntity.getDistributionType().indexOf(HospConstants.DISTRI_TYPE_H)!=-1) {
		//logger.info("proEntity.getHospitalDistribution()>>>>>>>"+proEntity.getHospitalDistribution());
		if(proEntity.getHospitalDistribution()>0){
			entityOld.setHospitalDistribution(proEntity.getHospitalDistribution());// 比例
			hospitalFashion = entityOld.getLeaseCost().multiply(BigDecimal.valueOf(proEntity.getHospitalDistribution())
					.divide(BigDecimal.valueOf(100), 2, RoundingMode.UP));
		//	logger.info("proEntity.getHospitalDistribution()>>>>hospitalFashion>>>"+hospitalFashion);
			entityOld.setHospitalFashion(hospitalFashion);// 返佣
			//分账
			MdsRecordsConptionEntity entityp = new MdsRecordsConptionEntity();
			entityp.setOrderNumber(entityOld.getOrderNumber());
			entityp.setOpenid(entityOld.getOpenid());
			entityp.setHospitalId(entityOld.getHospitalId());
			entityp.setHospitalName(entityOld.getHospitalName());
			entityp.setLeaseCost(hospitalFashion);
			getRecordsFashion(entityp);
		}
		}
		
		
		// 物业分账
		if (proEntity.getDistributionType().indexOf(HospConstants.DISTRI_TYPE_P)!=-1) {
	//	logger.info("proEntity.getProperDistribution()>>>>>>>"+proEntity.getProperDistribution());
		if(proEntity.getProperDistribution()>0){
		entityOld.setProperDistribution(proEntity.getProperDistribution());// 比例
		properFashion = entityOld.getLeaseCost().multiply(BigDecimal.valueOf(proEntity.getProperDistribution())
				.divide(BigDecimal.valueOf(100), 2, RoundingMode.UP));
		//logger.info("proEntity.getHospitalDistribution()>>>>properFashion>>>"+properFashion);
		entityOld.setProperFashion(properFashion);// 返佣
		//分账
		MdsRecordsConptionEntity entityp = new MdsRecordsConptionEntity();
		entityp.setOrderNumber(entityOld.getOrderNumber());
		entityp.setOpenid(entityOld.getOpenid());
		entityp.setHospitalId(entityOld.getHospitalId());
		entityp.setHospitalName(entityOld.getHospitalName());
		entityp.setLeaseCost(properFashion);
		getRecordsFashion(entityp);

		}
		}
		
		// 代理商分账
		if (proEntity.getDistributionType().indexOf(HospConstants.DISTRI_TYPE_A)!=-1) {}
	//	logger.info("proEntity.getAgentDistribution()>>>>>>>"+proEntity.getAgentDistribution());
		if(proEntity.getAgentDistribution()>0){
		entityOld.setAgentDistribution(proEntity.getAgentDistribution());// 比例
		agentFashion = entityOld.getLeaseCost().multiply(BigDecimal.valueOf(proEntity.getAgentDistribution())
				.divide(BigDecimal.valueOf(100), 2, RoundingMode.UP));
		//logger.info("proEntity.agentFashion()>>>>>>>"+agentFashion);
		entityOld.setAgentFashion(agentFashion);// 返佣
		
		//分账
		MdsRecordsConptionEntity entityp = new MdsRecordsConptionEntity();
		entityp.setOrderNumber(entityOld.getOrderNumber());
		entityp.setOpenid(entityOld.getOpenid());
		entityp.setHospitalId(entityOld.getHospitalId());
		entityp.setHospitalName(entityOld.getHospitalName());
		entityp.setLeaseCost(properFashion);
		getRecordsFashion(entityp);
		}
		
		// 平台分账
		platDistribution = 100 - proEntity.getHospitalDistribution() - proEntity.getProperDistribution()
				- proEntity.getAgentDistribution();
		platFashion = entityOld.getLeaseCost().subtract(hospitalFashion).subtract(properFashion).subtract(agentFashion);
	//	logger.info("proEntity.platDistribution()>>>>>>>"+platDistribution);
		entityOld.setPlatDistribution(platDistribution);
	//	logger.info("proEntity.platFashion()>>>>>>>"+platFashion);
		entityOld.setPlatFashion(platFashion);

		return entityOld;
	}
	
	  // 各方 分账 分账比例
		public void getRecordsFashion(MdsRecordsConptionEntity entityp){
			
			
			// 返回余款
			
			logger.info("mdsRecords getRecordsFashion:====分账====");
			entityp.setPlatPayDate(new Date());
			entityp.setPlatPayNumber("F"+DateParser.formatDateSS(new Date()));
			entityp.setWxPayDate(new Date());
			entityp.setPaySystem("WX");
			entityp.setConsumStatus(HospConstants.CONSUM_STATUS_F);
			entityp.setOrderDescribe("分账");
			entityp.setUpdatedDate(new Date());
		//	logger.info("mdsRecords =分账======分账===");
			try {
				mdsRecordsConptionService.save(entityp);
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findLeaseInfoPageByProperty")
	public Page<MdsLeaseInfoVo> findLeaseInfoPageByProperty(@RequestBody QueryVo queryVo) 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		MdsUserInfoEntity user=null;
		logger.info("queryVo.getUserId()===="+queryVo.getUserId());
		if (queryVo.getUserId()!=null){
			 user = mdsUserInfoService.getEntity(queryVo.getUserId());
		 } 
		logger.info("==="+(user!=null)+queryVo.getHospitalId());
		
		
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		//LEFT JOIN MdsPropertyInfoEntity p on l.hospitalId = p.id LEFT JOIN MdsUserInfoEntity u on (u.hospitalId = p.agentId or u.hospitalId = p.properId ) where 1=1 
		sql.append("select  l  from  MdsLeaseInfoEntity l where 1=1 ");
		//订单号
		if (queryVo.getOrderNumber() != null && !"".equals(queryVo.getOrderNumber())) 
		{
			sql.append(" and l.orderNumber like :orderNumber");
			searchParams.put("orderNumber", "%" + queryVo.getOrderNumber() + "%");
			sql.append(" or l.userPhone like :userPhone");
			searchParams.put("userPhone", "%" + queryVo.getOrderNumber() + "%");
		}
		
//		//登录名
//		if (user!=null  && !user.getUserType().equals(HospConstants.USER_TYPE_M)){
//			sql.append(" and u.loginName = :loginName");
//			searchParams.put("loginName",queryVo.getLoginName());
//		}
		// openId
		if (StringUtils.isNotBlank(queryVo.getOpenid())) {
			sql.append(" and l.openid = :openid");
			searchParams.put("openid", queryVo.getOpenid());
		}
		
		//二维码
		if (StringUtils.isNotBlank(queryVo.getProductQrcode())) 
		{
			sql.append(" and l.productQrcode like :productQrcode");
			searchParams.put("productQrcode", "%" + queryVo.getProductQrcode() + "%");
		}
		//物业Id
		if (user!=null  && !user.getUserType().equals(HospConstants.USER_TYPE_M)){
		if(queryVo.getHospitalId() != null && !"".equals(queryVo.getHospitalId()))
		{
			sql.append(" and (l.properId = :properId  or l.agentId = :agentId)");
			searchParams.put("properId", queryVo.getHospitalId().toString());
			searchParams.put("agentId", queryVo.getHospitalId().toString());
		}
		}
		//品类名
		if (queryVo.getCategoryName() != null && !"".equals(queryVo.getCategoryName())) 
		{
			sql.append(" and l.categoryName = :categoryName");
			searchParams.put("categoryName", queryVo.getCategoryName());
		}
		
		//手机号
		if (StringUtils.isNotBlank(queryVo.getUserPhone())) 
		{
			sql.append(" and l.userPhone like :userPhone");
			searchParams.put("userPhone", "%" + queryVo.getUserPhone() + "%");
		}
		
		
		//医院名
		if (queryVo.getHospitalId() != null && !"".equals(queryVo.getHospitalId())) 
		{
			sql.append(" and l.hospitalId = :hospitalId");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		// 租赁开始时间
		if (queryVo.getStartDate() != null && !"".equals(queryVo.getStartDate())) 
		{
			sql.append("  and  l.leaseStartDate >=:leaseStartDate ");
			searchParams.put("leaseStartDate", queryVo.getStartDate());
			logger.info("getStartDate>>" + queryVo.getStartDate());
			
		}
		// 租赁结束时间
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append(" and  l.leaseEndDate <=:leaseEndDate");
			searchParams.put("leaseEndDate", queryVo.getEndDate());
			logger.info("getEndDate>>" + queryVo.getEndDate());
		}
		sql.append(" order by l.leaseStartDate desc ");
	
		logger.info("findPropertyListPage - sql :>>>>" + sql.toString());

		Page<MdsLeaseInfoEntity> page = (Page<MdsLeaseInfoEntity>) commonDao.findPage(sql.toString(), searchParams,
				pageRequest);
		List<MdsLeaseInfoVo> listVo = new ArrayList<>();

		if (page != null && page.getContent().size() > 0) 
		{
			
			logger.info("entity>>>>>>>page.getContent()>>=="+page.getContent().size());
			for (MdsLeaseInfoEntity entity : page.getContent()) 
			{
				logger.info("entity>>>>>>>>>>>getOrderNumber>>>>=="+entity.getOrderNumber());
				MdsLeaseInfoVo leaseInfoVo = new MdsLeaseInfoVo();
				BeanUtils.copyProperties(entity, leaseInfoVo);
				MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoService.getEntity(entity.getHospitalId());
				if(propertyInfo!=null){
					leaseInfoVo.setHospitalName(propertyInfo.getHospitalName());
					entity.setHospitalName(propertyInfo.getHospitalName());
				}
				
				//费用占比
			//	logger.info("=========kk==getOrderNumber=="+entity.getOrderNumber());
			//	logger.info("=========kk===="+entity.getOrderStatus());
				if (!entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_2)) {
			//		logger.info("=========kk=22===" + entity.getOrderStatus());
			//		logger.info("=========kk=getIntoBalance===" + entity.getIntoBalance());
					
					MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceService.findPrice(entity.getCategoryId(),
							entity.getHospitalId());
					if (hospitalPrice != null) {
						BigDecimal yb = new BigDecimal(100);
						if (entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_1)) {
                           if(entity.getLeaseCost().compareTo(BigDecimal.ZERO)==1){
   							Integer leaseCostProportion = entity.getLeaseCost().divide(entity.getLeaseDeposit(),2,RoundingMode.HALF_UP).multiply(yb).intValue();
   				//			logger.info("费用占比：>>>>结束订单>>>>>===" + leaseCostProportion);
   							leaseInfoVo.setCostProportion(String.valueOf(leaseCostProportion));
                           }else{
                       		leaseInfoVo.setCostProportion(String.valueOf(0));
                           }

						} else {

							BigDecimal leaseCostnew = PriceUtils.calculatePriceNew(hospitalPrice.getLeaseUnit(),
									hospitalPrice.getTopBalance(), hospitalPrice.getMinHour(),
									entity.getLeaseStartDate(), new Date(), hospitalPrice.getOutHour());
							Integer leaseCostProportion = leaseCostnew.divide(entity.getLeaseDeposit(),2,RoundingMode.HALF_UP).multiply(yb)
									.intValue();
					//		logger.info("费用占比：>>>>>>>>>===" + leaseCostProportion);
							leaseInfoVo.setCostProportion(String.valueOf(leaseCostProportion));
							leaseInfoVo.setLeaseCost(leaseCostnew);
							leaseInfoVo.setExpectedLeaseCost(leaseCostnew);//预计租金
							leaseInfoVo.setIntoBalance(leaseInfoVo.getLeaseDeposit().subtract(leaseInfoVo.getExpectedLeaseCost()));//退款

						}

					}
				}
	
				
				//添加报修信息
				List<MdsRepairInfoEntity>  repairls = mdsRepairInfoService.findByOrderNumber(entity.getOrderNumber());
				if(repairls!=null && repairls.size()>0){
					MdsRepairInfoEntity repairEntity = repairls.get(0);
					leaseInfoVo.setRepairfileId(fileurl+repairEntity.getFileId());	
					leaseInfoVo.setRepairReason(repairEntity.getReason());
					leaseInfoVo.setRepairRemark(repairEntity.getRemark());
					leaseInfoVo.setRepairDate(repairEntity.getCreatedDate());//描述
					leaseInfoVo.setServiceReason(repairEntity.getServiceReason());
				}
				List<MdsUserInfoEntity> userInfols =mdsUserInfoService.findListByUserPhone(entity.getOpenid());
				if(userInfols!=null && userInfols.size()>0){
				//	logger.info("userInfols>>>>>>>entity.getNickName()>>>>>>>>=="+userInfols.get(0).getNickName());
					leaseInfoVo.setNickName(userInfols.get(0).getNickName());
					if (entity.getUserPhone() == null || entity.getUserPhone().equals("")) {
						for (MdsUserInfoEntity ent : userInfols) {
				//			logger.info("userInfols>>>>>>>entity.getNickName()>>>>1111>>>>=="+ent.getUserPhone());
                             if(ent.getUserPhone()!=null && !ent.getUserPhone().equals("")){
                   //         logger.info("userInfols>>>>>>>entity.getUserPhone()>>>>>>>>=="+ent.getUserPhone());
                            	 leaseInfoVo.setUserPhone(ent.getUserPhone());
                            	 entity.setUserPhone(ent.getUserPhone());
                             }
						}
					}
				
				}
				
				QueryVo vo = new QueryVo();
				vo.setOrderNumber(entity.getOrderNumber());
				//logger.info("lsFeedBack>>>>>>>entity.getOrderNumber()>>>>>>>>=="+entity.getOrderNumber());
				List<MdsUserFeedBackEntity>  lsFeedBack = mdsUserFeedBackService.findByOrderNum(vo);
				
			//	logger.info("lsFeedBack>>>>>>>>>>>>>>>=="+lsFeedBack.size());
				if(lsFeedBack!=null && lsFeedBack.size()>0){
					MdsUserFeedBackEntity feedBackEntity = lsFeedBack.get(0);
					leaseInfoVo.setFeedbackContent(feedBackEntity.getFeedbackContent());///*反馈内容*/
					leaseInfoVo.setDescribe(feedBackEntity.getRemark());//描述
				}
			//设置返佣比例
				//leaseInfoVo.setPlatDistribution(platDistrib);
				
				//trans
				if(entity.getOrderStatus().equals(HospConstants.ORDER_STATUS_0)){
					trans(leaseInfoVo,entity);	
				}
			   //开始结束时间转换
				if(entity.getLeaseStartDate()!=null){
					leaseInfoVo.setLeaseStartDateStr(DateParser.formatDatesS1(entity.getLeaseStartDate()));
				}
				if(entity.getLeaseEndDate()!=null){
					leaseInfoVo.setLeaseEndDateStr(DateParser.formatDatesS1(entity.getLeaseEndDate()));
				}
				
				if (HospConstants.ORDER_STATUS_0.equals(entity.getOrderStatus())) {
					leaseInfoVo.setOrderStatusValue("使用中");
				}else if (HospConstants.ORDER_STATUS_1.equals(entity.getOrderStatus())) {
					leaseInfoVo.setOrderStatusValue("已完成");
				}else if (HospConstants.ORDER_STATUS_2.equals(entity.getOrderStatus())) {
					leaseInfoVo.setOrderStatusValue("待付款");
				}else if (HospConstants.ORDER_STATUS_4.equals(entity.getOrderStatus())) {
					leaseInfoVo.setOrderStatusValue("报修中");
				}else if (HospConstants.ORDER_STATUS_5.equals(entity.getOrderStatus())) {
					leaseInfoVo.setOrderStatusValue("余额不足");
				}
				
				
				listVo.add(leaseInfoVo);
				logger.info("======end==============");
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsLeaseInfoVo> pageVo = new PageImpl<>(listVo, pageRequestn, page.getTotalElements());

		return pageVo;
	}
	
	/**
	 * stayABug
	 * */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findMdsLeaseInfoPage")
	public Page<MdsLeaseInfoVo> findMdsLeaseInfoPage(@RequestBody QueryVo queryVo) 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);

		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  l  from  MdsLeaseInfoEntity l  where 1=1 ");
		//订单号
		if (queryVo.getOrderNumber() != null && !"".equals(queryVo.getOrderNumber())) 
		{
			sql.append(" and l.orderNumber = :orderNumber");
			searchParams.put("orderNumber", queryVo.getOrderNumber());
		}
		//品类名
		if (queryVo.getCategoryName() != null && !"".equals(queryVo.getCategoryName())) 
		{
			sql.append(" and l.categoryName = :categoryName");
			searchParams.put("categoryName", queryVo.getCategoryName());
		}
		//医院名
		if (queryVo.getHospitalName() != null && !"".equals(queryVo.getHospitalName())) 
		{
			sql.append(" and l.hospitalName = :hospitalName");
			searchParams.put("hospitalName", queryVo.getHospitalName());
		}
		// 租赁开始时间
		if (queryVo.getStartDate() != null && !"".equals(queryVo.getStartDate())) 
		{
			sql.append("  and  l.leaseStartDate >=:leaseStartDate ");
			searchParams.put("leaseStartDate", queryVo.getStartDate());
		}
		// 租赁结束时间
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append(" and  l.leaseEndDate <=:leaseEndDate");
			searchParams.put("leaseEndDate", queryVo.getEndDate());
		}
		sql.append(" order by l.leaseStartDate desc ");
		logger.info("findPropertyListPage - sql :>>>>" + sql.toString());

		Page<MdsLeaseInfoEntity> page = (Page<MdsLeaseInfoEntity>) commonDao.findPage(sql.toString(), searchParams,
				pageRequest);
		List<MdsLeaseInfoVo> listVo = new ArrayList<>();

		if (page != null && page.getContent().size() > 0) 
		{
			for (MdsLeaseInfoEntity entity : page.getContent()) 
			{
				MdsLeaseInfoVo leaseInfoVo = new MdsLeaseInfoVo();
				BeanUtils.copyProperties(entity, leaseInfoVo);
				// 平台收入百分比
				MdsPropertyInfoEntity proEntity = mdsPropertyInfoService.getEntity(entity.getId());
				Integer platDistrib = 0;
				// 返佣类型 医院H,物业P,代理A,医院+物业HP,医院+代理HA,医院+物业+代理HPA,自营ALL
				if(null != proEntity)
				{
					if(StringUtils.isNotBlank(proEntity.getDistributionType()))
					{
						if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_H)) 
						{
							platDistrib = 100 - proEntity.getHospitalDistribution();
						} else if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_P)) 
						{
							platDistrib = 100 - proEntity.getProperDistribution();
						} else if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_A)) 
						{
							platDistrib = 100 - proEntity.getAgentDistribution();
						} else  if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_HP)) 
						{
							platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getProperDistribution();
						} else if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_HA)) 
						{
							platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getAgentDistribution();
						}
						if (proEntity.getDistributionType().equals(HospConstants.DISTRI_TYPE_HAP)) 
						{
							platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getProperDistribution()
									- proEntity.getAgentDistribution();
						} else 
						{
							platDistrib = 100;
						}
					}
				}
				//设置返佣比例
				leaseInfoVo.setPlatDistribution(platDistrib);
				listVo.add(leaseInfoVo);
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsLeaseInfoVo> pageVo = new PageImpl<>(listVo, pageRequestn, page.getTotalElements());

		return pageVo;
	}

	
	@SuppressWarnings("unchecked")
	@PostMapping("findLeaseByOrderNumber")
	public List<MdsLeaseInfoEntity> findLeaseByOrderNumber(@RequestBody QueryVo queryVo) {
		if (StringUtils.isNotBlank(queryVo.getOrderStatus())) {
			transList(queryVo);

		}

		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  l  from  MdsLeaseInfoEntity l  where 1=1 ");
		// openId
		if (StringUtils.isNotBlank(queryVo.getOpenid())) {
			sql.append(" and l.openid = :openid");
			searchParams.put("openid", queryVo.getOpenid());
		}
		// 订单号
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) {
			sql.append(" and l.orderNumber = :orderNumber");
			searchParams.put("orderNumber", queryVo.getOrderNumber());
		}
		// 设备Id
		if (StringUtils.isNotBlank(queryVo.getProductCode())) {
			sql.append(" and l.productCode = :productCode");
			searchParams.put("productCode", queryVo.getProductCode());
		}
		// 使用状态
		if (queryVo.getOrderStatusList() != null && queryVo.getOrderStatusList().size() > 0) {
			// sql.append(" and l.orderStatus in ('0','2')");
			// searchParams.put("orderStatus", queryVo.getOrderStatus());
			sql.append(" and l.orderStatus in(:orderStatus)");
			searchParams.put("orderStatus", queryVo.getOrderStatusList());
		}
		// 医院名
		if (StringUtils.isNotBlank(queryVo.getHospitalName())) {
			sql.append(" and l.hospitalName like :hospitalName");
			searchParams.put("hospitalName", "%" + queryVo.getHospitalName() + "%");
		}
		// 医院id
		if (queryVo.getHospitalId() != null && !"".equals(queryVo.getHospitalId())) {
			sql.append(" and l.hospitalId = :hospitalId");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		sql.append(" order by l.leaseStartDate desc ");
		logger.info("findLeaseByOrderNumber - sql : >>>>" + sql.toString());
		return commonDao.findBy(sql.toString(), searchParams);
	}
	
	@PostMapping("info")
	public MdsLeaseInfoEntity info(@RequestBody Long productId){
		return mdsLeaseInfoService.info(productId);	
	}
	
	@PostMapping("findOrderNumber")
	public MdsLeaseInfoEntity findOrderNumber(@RequestBody String orderNumber){
		return mdsLeaseInfoService.findOrderNumber(orderNumber);	
	}
	
	
	
	@PostMapping("categoryNumber")
	public Long categoryNumber(@RequestBody String openid){
		return mdsLeaseInfoService.categoryNumber(openid);
	}
	@PostMapping("orderNumber")
	public Long orderNumber(@RequestBody String openid){
		return mdsLeaseInfoService.orderNumber(openid);
	}
	@PostMapping("prepareCost")
	public BigDecimal prepareCost(@RequestBody String openid){
		return mdsLeaseInfoService.prepareCost(openid);
	}
	@PostMapping("leaseCost")
	public BigDecimal leaseCost( String openid){
		return mdsLeaseInfoService.leaseCost(openid);
	}
//	public static void main(String[] args) {
//		BigDecimal a =new  BigDecimal("1.00");
//		BigDecimal b =new  BigDecimal("1.01");
//		BigDecimal c =new  BigDecimal("100");
//
//		logger.info((a.compareTo(b)!=-1));
//	}

}