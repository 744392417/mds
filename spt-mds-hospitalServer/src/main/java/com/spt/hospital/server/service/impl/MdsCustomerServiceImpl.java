package com.spt.hospital.server.service.impl;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsCustomerServiceEntity;
import com.spt.hospital.server.dao.MdsCustomerServiceDao;
import com.spt.hospital.server.service.IMdsCustomerService;


@Component
@Transactional(readOnly = false)
public class MdsCustomerServiceImpl extends BaseService<MdsCustomerServiceEntity> implements IMdsCustomerService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsCustomerServiceImpl.class);

	@Autowired
	private MdsCustomerServiceDao mdCustomerServiceDao;

	@Override
	public BaseDao<MdsCustomerServiceEntity> getBaseDao() {
		return mdCustomerServiceDao;
	}

	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;



	


}
