package com.spt.hospital.server.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import net.sf.json.JSONObject;
import sun.misc.BASE64Encoder;
import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.util.HTTPUtility;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.vo.MdsProductPwdVo;
import com.spt.hospital.server.dao.MdsProductPwdDao;
import com.spt.hospital.server.service.IMdsProductPwdService;
import com.spt.hospital.server.util.SptApiCallUtility;
import com.spt.hospital.server.vo.MdsProductPwdDto;




@Component
@Transactional(readOnly = false)
public class MdsProductPwdServiceImpl extends BaseService<MdsProductPwdEntity> implements IMdsProductPwdService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsProductPwdServiceImpl.class);

	@Autowired
	private MdsProductPwdDao mdsProductPwdDao;

	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;

	@Override
	public BaseDao<MdsProductPwdEntity> getBaseDao() {
		return mdsProductPwdDao;
	}

	@Autowired
	private CommonDao commonDao;
	
	@Value("${pwd.url}")
	String pwdUrl;

	@SuppressWarnings("unchecked")
	@Override
	public List<MdsProductPwdEntity> findBylist(@RequestBody MdsProductPwdVo mdsProductPwdVo) {
		Map<String, Object> searchParams = new HashMap<>();

		StringBuffer sql = new StringBuffer();
		sql.append("select  l  from  MdsProductPwdEntity l  where 1=1 ");

		if (StringUtils.isNotBlank(mdsProductPwdVo.getProductCode())) {
			sql.append(" and l.productCode = :productCode");
			searchParams.put("productCode", mdsProductPwdVo.getProductCode());
		}

		if (StringUtils.isNotBlank(mdsProductPwdVo.getProductQrcode())) {
			sql.append(" and l.productQrcode = :productQrcode");
			searchParams.put("productQrcode", mdsProductPwdVo.getProductQrcode());
		}

		if (StringUtils.isNotBlank(mdsProductPwdVo.getUnlockPwd())) {
			sql.append(" and l.unlockPwd = :unlockPwd");
			searchParams.put("unlockPwd", mdsProductPwdVo.getUnlockPwd());
		}
		
		if (StringUtils.isNotBlank(mdsProductPwdVo.getUnlockPwd())) {
			sql.append(" and l.pwdStatus = :pwdStatus");
			searchParams.put("pwdStatus", mdsProductPwdVo.getPwdStatus());
		}

		if (mdsProductPwdVo.getProductId() != null && !"".equals(mdsProductPwdVo.getProductId())) {
			sql.append(" and l.productId = :productId");
			searchParams.put("productId", mdsProductPwdVo.getProductId());
		}
		return commonDao.findBySql(sql.toString(), searchParams);

	}

	@Override
	public MdsProductPwdEntity findByPwd(@RequestBody MdsProductPwdVo mdsProductPwdVo) {

		List<MdsProductPwdEntity> ls = this.findBylist(mdsProductPwdVo);

		if (ls != null && ls.size() > 0) {
			return ls.get(0);
		} else {
			return null;
		}

	}


	@Override
	public void updatePwdStatus(MdsProductPwdVo vo) {
		mdsProductPwdDao.updatePwdStatus(vo.getProductCode(), vo.getProductQrcode(), vo.getPwdStatus());
	}

	// 定时更新密码
	@Override
	public void updatePwd(MdsProductPwdVo vo) {

		List<MdsProductPwdEntity> ls = mdsProductPwdDao.findByCode(vo.getProductCode(), HospConstants.PWD_STATUS_2);
		for (MdsProductPwdEntity pwd : ls) {
			Integer unlockPwd = (int) ((Math.random() * 9 + 1) * 10000);// 开
			Integer shutPwd = (int) ((Math.random() * 9 + 1) * 10000);// 关
			pwd.setUnlockPwd(String.valueOf(unlockPwd));
			pwd.setShutPwd(String.valueOf(shutPwd));
			pwd.setPwdStatus(HospConstants.PWD_STATUS_0);
			mdsProductPwdDao.save(pwd);
		}

	}

	// 添加设备产生新 密码
	@Override
	public void saveProductPwd(MdsProductPwdVo vo) {
		MdsProductPwdEntity productPwdEntity = new MdsProductPwdEntity();
		BeanUtils.copyProperties(vo, productPwdEntity);
		for (int i = 0; i < HospConstants.PWD_NUMBER; i++) {
			Integer unlockPwd = (int) ((Math.random() * 9 + 1) * 10000);// 开
			Integer shutPwd = (int) ((Math.random() * 9 + 1) * 10000);// 关
			productPwdEntity.setUnlockPwd(String.valueOf(unlockPwd));
			productPwdEntity.setShutPwd(String.valueOf(shutPwd));
			productPwdEntity.setPwdStatus(HospConstants.PWD_STATUS_0);
			mdsProductPwdDao.save(productPwdEntity);
		}

	}

	
	@Override
	public MdsProductPwdEntity getPushKey(MdsProductPwdVo vo) {
		//推送密码接口pwdUrl+"/key/pushKey"+"?serialNo=011812999999&type=2&userId=1"
		String uuidJson = null;
		MdsProductPwdEntity entity = new MdsProductPwdEntity();
		try {
		    System.out.println("getPushKey>>>>>>>>>url>>>>>"+pwdUrl+"/key/pushKey"+"?serialNo="+vo.getProductCode()+"&type=2&userId="+vo.getOpenid()); 
			uuidJson = HTTPUtility.doGet(pwdUrl+"/key/pushKey"+"?serialNo="+vo.getProductCode()+"&type=2&userId="+vo.getOpenid());
			System.out.println("------------uuidJson-----"+uuidJson);
			JSONObject jsonObj=  JSONObject.fromObject(uuidJson);		
			Integer code = (Integer) jsonObj.get("code");
			if(code==10000){
				Object data = (Object) jsonObj.get("data");
				JSONObject jsonhead = JSONObject.fromObject(data);
				String openKey = (String) jsonhead.get("openKey");
				String returnKey = (String) jsonhead.get("returnKey");
				String keyId = (String) jsonhead.get("keyId");
				entity.setUnlockPwd(openKey);//开
				entity.setShutPwd(returnKey);//关
				entity.setKeyId(keyId);
				entity.setProductId(vo.getId());
				entity.setProductQrcode(vo.getProductQrcode());
				entity.setProductCode(vo.getProductCode());
				mdsProductPwdDao.save(entity);
				return entity;
			}
			
		} catch (ApplicationException e) {
			e.printStackTrace();
		}
//		MdsProductPwdEntity entity = new MdsProductPwdEntity();
//		entity.setUnlockPwd("98526");
//		entity.setShutPwd("456");
		return entity;

	}
	
	
	@Override
	public void sendReturnKey(MdsProductPwdVo vo) {
		//密码归档接口pwdUrl+"/key/returnKey"+"?serialNo=011812999999&keyId="+vo.getKeyId()
		String returnssss;
		try {
			logger.info("sendReturnKey>>>>>="+pwdUrl+"/key/returnKey"+"?serialNo="+vo.getProductCode()+"&keyId="+vo.getKeyId());
			returnssss = HTTPUtility.doGet(pwdUrl+"/key/returnKey"+"?serialNo="+vo.getProductCode()+"&keyId="+vo.getKeyId());
			logger.info("------------uuidJsonre-----"+returnssss);
			JSONObject jsonObjreturn=  JSONObject.fromObject(returnssss);		
			Integer code = (Integer) jsonObjreturn.get("code");
			if(code==10000){
				mdsProductPwdDao.updatePwdStatusByKeyId(vo.getKeyId(), vo.getProductCode(), HospConstants.PWD_STATUS_2);
			}
		} catch (ApplicationException e) {
			e.printStackTrace();
		}


	}
	
	
}
