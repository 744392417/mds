package com.spt.hospital.server.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsBankMasterEntity;
import com.spt.hospital.server.dao.MdsBankMasterDao;
import com.spt.hospital.server.service.IMdsBankMasterService;



@Component
@Transactional(readOnly = false)
public class MdsBankMasterServiceImpl extends BaseService<MdsBankMasterEntity> implements IMdsBankMasterService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsBankMasterServiceImpl.class);

	@Autowired
	private MdsBankMasterDao mdsBankMasterDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsBankMasterEntity> getBaseDao() {
		return mdsBankMasterDao;
	}
	
}
