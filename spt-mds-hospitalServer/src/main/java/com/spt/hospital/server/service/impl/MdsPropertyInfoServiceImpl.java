package com.spt.hospital.server.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsPropertyInfoDao;
import com.spt.hospital.server.service.IMdsPropertyInfoService;



@Component
@Transactional(readOnly = false)
public class MdsPropertyInfoServiceImpl extends BaseService<MdsPropertyInfoEntity> implements IMdsPropertyInfoService {
	
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsPropertyInfoServiceImpl.class);

	@Autowired
	private MdsPropertyInfoDao mdsPropertyInfoDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsPropertyInfoEntity> getBaseDao() {
		return mdsPropertyInfoDao;
	}
	
	@Override
	public List<MdsPropertyInfoEntity> findBytype(String type){
		return 	mdsPropertyInfoDao.findBytype(type);
	}

	@Override
	public List<String> findAllDepart() {
		return mdsPropertyInfoDao.findAllDepart();
	}

	@Override
	public MdsPropertyInfoEntity byHospitalName(MdsPropertyInfoVo vo) {
		
		return mdsPropertyInfoDao.byHospitalName(vo.getHospitalName());
	}
	
	@Override
	public List<MdsPropertyInfoEntity>  findByIsfashionable(QueryVo vo) {
		
		return mdsPropertyInfoDao.findByIsfashionable(vo.getHospitalId());
	}
	
	
	

}
