package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsInteractiveLogEntity;
import com.spt.hospital.server.service.IMdsInteractiveLogService;


@RestController
@RequestMapping(value = "api/mdsInteractiveLog")
public class MdsInteractiveLogApi extends BaseApi<MdsInteractiveLogEntity>
{
	@Autowired
	private IMdsInteractiveLogService mdsInteractiveLog;
	
	@Override
	public IBaseService<MdsInteractiveLogEntity> getService() 
	{
		return mdsInteractiveLog;
	}
}
