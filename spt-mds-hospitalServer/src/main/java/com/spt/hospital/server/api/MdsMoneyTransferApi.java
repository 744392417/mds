package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsMoneyTransferEntity;
import com.spt.hospital.server.service.IMdsMoneyTransferService;




@RestController
@RequestMapping(value = "api/mdsMoneyTransfer")
public class MdsMoneyTransferApi extends BaseApi<MdsMoneyTransferEntity> {
	@Autowired
	private IMdsMoneyTransferService mdsMoneyTransferService;

	@Override
	public IBaseService<MdsMoneyTransferEntity> getService() {
		return mdsMoneyTransferService;
	}

}
