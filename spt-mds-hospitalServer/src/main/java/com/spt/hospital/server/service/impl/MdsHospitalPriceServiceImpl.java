package com.spt.hospital.server.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsHospitalPriceDao;
import com.spt.hospital.server.service.IMdsHospitalPriceService;



@Component
@Transactional(readOnly = false)
public class MdsHospitalPriceServiceImpl extends BaseService<MdsHospitalPriceEntity> implements IMdsHospitalPriceService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsHospitalPriceServiceImpl.class);

	@Autowired
	private MdsHospitalPriceDao mdsHospitalPriceDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsHospitalPriceEntity> getBaseDao() {
		return mdsHospitalPriceDao;
	}
	
	@Override
	public MdsHospitalPriceEntity findPrice(Long categoryId,Long hospitalId) {
		return mdsHospitalPriceDao.findPrice(categoryId,hospitalId);
	}

	@Override
	public List<MdsHospitalPriceEntity> findListByHospitalId(Long hospitalId) {
		return mdsHospitalPriceDao.findListByHospitalId(hospitalId);
	}
	
	@Override
	public void updateCategoryName(QueryVo queryVo) {
		mdsHospitalPriceDao.updateCategoryName(queryVo.getCategoryId(), queryVo.getCategoryFileId(),
				queryVo.getCategoryName());
	}
}
