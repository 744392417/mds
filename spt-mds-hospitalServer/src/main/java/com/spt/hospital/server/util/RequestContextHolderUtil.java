package com.spt.hospital.server.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * 获取调用和使用Session,Request,Response等方法
 * */
public class RequestContextHolderUtil 
{
	/**
	 * 获取  ServletRequestAttributes
	 * RequestContextHolder.getRequestAttributes()
	 * */
	public static ServletRequestAttributes getRequestAttributes() 
	{
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
    }
	/**
	 * 获取 HttpServletRequest
	 * */
	public static HttpServletRequest getRequest() 
	{
	      return getRequestAttributes().getRequest();
	}
	/**
	 * 获取 HttpServletResponse
	 * */
    public static HttpServletResponse getResponse() 
    {
        return getRequestAttributes().getResponse();
    }
    /**
	 * 获取 HttpSession
	 * */
    public static HttpSession getSession() 
    {
        return getRequest().getSession();
    }
    /**
	 * 获取 ServletContext
	 * */
    public static ServletContext getServletContext() 
    {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext();
    }
}
