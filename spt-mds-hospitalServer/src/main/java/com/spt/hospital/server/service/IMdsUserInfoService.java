package com.spt.hospital.server.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.IBaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.shiro.ShiroUser;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.user.MdsUserInfoMngVo;


public interface IMdsUserInfoService extends IBaseService<MdsUserInfoEntity> 
{
	ShiroUser login(MdsUserInfoVo mdsUserInfoVo) throws ApplicationException;

	MdsUserInfoEntity findByToken(String token);

	public MdsUserInfoEntity findByRoleHospital(String roleCode, Long hospitalId);

	void updateHospRole(MdsUserInfoVo vo);
	
	void updateUserLoginInfo(MdsUserInfoVo vo);

	void updateStatus(MdsUserInfoVo vo);

	List<MdsUserInfoEntity> findByHospital(Long id);
	/**
	 * 根据Openid获取用户信息
	 * */
	MdsUserInfoEntity findByOpenid(String thisOpenid);

	List<MdsUserInfoEntity> findListByOpenid(String openid);

	MdsUserInfoEntity findByloginName(QueryVo queryVo);
	
	MdsUserInfoEntity verifyUserName(String loginName);

	void updatePhone(MdsUserInfoVo mdsUserInfoVo);

	Page<MdsUserInfoMngVo> findPageByUser(QueryVo queryVo) throws ApplicationException;

	List<MdsUserInfoEntity> findListByUserPhone(String openid);
}

