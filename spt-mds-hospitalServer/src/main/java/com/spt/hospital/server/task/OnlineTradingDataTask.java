package com.spt.hospital.server.task;



import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hsoft.commutil.exception.ApplicationException;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsProductInfoService;

/**
 * 定时任务
 * */
@Component
public class OnlineTradingDataTask{
	
	private Logger logger = LoggerFactory.getLogger(OnlineTradingDataTask.class);

	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	
	
	

	@Scheduled(cron = "0 0 12 * * ?")//每天中午12点触发
	public void syncOnlineTradingData() {
		logger.info("Task=========设备管理-超过一周空闲-设备异常状态======");
	//	QueryVo freeVo = new QueryVo();
	//	freeVo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
	//List<MdsProductInfoEntity> productls = mdsProductInfoService.findByfreeStatus(freeVo);
//		for (MdsProductInfoEntity product : productls) {
//
//			long daynum = DateParser.getDays(product.getUpdatedDate(), new Date());
//			logger.info("Task==设备管理==最后使用时间=="+product.getUpdatedDate());
//			logger.info("Task====设备管理==当前时间=="+DateParser.formatDate(new Date()));
//			logger.info("Task====设备管理==两时间相隔=="+daynum);
//			if (daynum >= 7) {
//				logger.info("Task==设备管理==最后使用时间=="+product.getUpdatedDate());
//				logger.info("Task====设备管理==当前时间=="+DateParser.formatDate(new Date()));
//					try {
//					product.setProductStatus(HospConstants.PRODUCT_STATUS_2);
//					mdsProductInfoService.save(product);
//				} catch (ApplicationException e) {
//					e.printStackTrace();
//				}
//			}
//
//		}
	}
}
