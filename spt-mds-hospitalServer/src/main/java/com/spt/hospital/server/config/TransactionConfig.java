package com.spt.hospital.server.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class TransactionConfig {
//	private Logger logger = LoggerFactory.getLogger(TransactionConfig.class);
	
	@Autowired
	private JpaProperties jpaProperties;
	
	@Autowired
	private DataSource dataSource;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.spt.hospital.client.entity");
		factory.setDataSource(dataSource);
		factory.setJpaPropertyMap(jpaProperties.getHibernateProperties(new HibernateSettings()));
		return factory;
	}

	@Bean("transactionManager")
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory().getObject());
		
		return txManager;
	}

//	@Bean(name = "transactionManager")
//	@Primary
//	public DataSourceTransactionManager rdsTransactionManager(DataSource rdsDataSource) {
//		return new DataSourceTransactionManager(rdsDataSource);
//	}

	// @Autowired
	// private Environment env;

	/*@Bean(name = "sqlSessionFactory")
	@Primary
	public SqlSessionFactory rdsSqlSessionFactory(DataSource rdsDataSource) throws Exception {
		logger.info("*************************sqlSessionFactory:begin***********************");

		VFS.addImplClass(SpringBootVFS.class);

		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(rdsDataSource);
		sessionFactory.setTypeAliasesPackage("com.spt.master.entity,com.spt.master.dto,com.spt.commutil.entity");
//		sessionFactory.setTypeAliases(new Class<?>[] { TblSptDictionaryEntity.class });
		// sessionFactory.setTypeHandlersPackage(properties.typeHandlerPackage);
		PaginationInterceptor interceptor = new PaginationInterceptor();
		Properties props = new Properties();
		props.setProperty("jdbc.type", "mysql");
		interceptor.setProperties(props);
		sessionFactory.setPlugins(new Interceptor[] { interceptor });
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setMapperLocations(resolver.getResources("classpath:mybatis/mappers/*.xml"));

		// sessionFactory
		// .setConfigLocation(new
		// PathMatchingResourcePatternResolver().getResource(properties.configLocation));

		SqlSessionFactory resultSessionFactory = sessionFactory.getObject();

		// logger.info("===typealias==>" +
		// resultSessionFactory.getConfiguration().getTypeAliasRegistry().getTypeAliases());

		logger.info("*************************sqlSessionFactory:successs:" + resultSessionFactory);

		return resultSessionFactory;
	}*/

}
