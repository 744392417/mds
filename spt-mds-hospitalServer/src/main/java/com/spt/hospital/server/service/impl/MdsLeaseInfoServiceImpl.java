package com.spt.hospital.server.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.util.PriceUtils;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.SendTemplateMessage;
import com.spt.hospital.client.vo.TemplateData;
import com.spt.hospital.server.dao.MdsHospitalPriceDao;
import com.spt.hospital.server.dao.MdsLeaseInfoDao;
import com.spt.hospital.server.dao.MdsRepairInfoDao;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.util.Httprequests;

import net.sf.json.JSONObject;




@Component
@Transactional(readOnly = false)
public class MdsLeaseInfoServiceImpl extends BaseService<MdsLeaseInfoEntity> implements IMdsLeaseInfoService 
{
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsLeaseInfoServiceImpl.class);

	@Autowired
	private MdsLeaseInfoDao mdsLeaseInfoDao;
	
	@Autowired
	private MdsHospitalPriceDao mdsHospitalPriceDao;
	
	@Autowired
	private MdsRepairInfoDao mdRepairInfoDao;

	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Value("${wx.appid}")
	String appId;
	@Value("${wx.secret}")
	String secret;
	
	@Value("${wx.tempamessage.url}")
	String templateMessageUrl;
	
	@Value("${wx.lackbalance.template}")
	String lackbalanceTemplateId;
	
	@Value("${wx.orderend.template}")
	String orderendTemplateId;

	@Override
	public BaseDao<MdsLeaseInfoEntity> getBaseDao() {
		return mdsLeaseInfoDao;
	}
	@Override
	public void saveMdsLeaseInfo(MdsLeaseInfoVo vo) {
		MdsLeaseInfoEntity entity = new MdsLeaseInfoEntity();
		BeanUtils.copyProperties(vo, entity);
		mdsLeaseInfoDao.save(entity);
	}
	@Override
	public void returnLeaseInfo(MdsLeaseInfoEntity entity) {
		mdsLeaseInfoDao.save(entity);
	}
	@Override
	public List<MdsLeaseInfoEntity> findListByUserManager(String openid) {
		return mdsLeaseInfoDao.findListByUserManager(openid);
	}
	@Override
	public List<MdsLeaseInfoEntity> findByCodeAndId(Long id) {
		return mdsLeaseInfoDao.findByCodeAndId(id);
	}
	
//	@Override
//	public List<MdsLeaseInfoEntity> findByProductCode(String productCode, Long id) {
//		return mdsLeaseInfoDao.findByProductCode(productCode,id);
//	}
//	
	
	@Override
	public MdsLeaseInfoEntity findByOrderNumber(String orderNumber) {
		return mdsLeaseInfoDao.findByOrderNumber(orderNumber);
	}
	@Override
	public List<MdsLeaseInfoEntity> findListByOpenid(String openid) {
		return mdsLeaseInfoDao.findListByOpenid(openid);
	}
//	@Override
//	public MdsHospitalPriceEntity moeys(Long id) {
//		return mdsLeaseInfoDao.moeys(id);
//	}
	@Override
	public MdsLeaseInfoEntity info(Long productId) {
		
		return mdsLeaseInfoDao.info(productId);
	}
	
	@Override
	public List<MdsLeaseInfoEntity> findInuseByProductCode(QueryVo queryVo) {
		
		return mdsLeaseInfoDao.findInuseByProductCode(queryVo.getProductCode());
	}
	@Override
	public MdsLeaseInfoEntity findOrderNumber(String orderNumber) {
		return mdsLeaseInfoDao.findByOrderNumber(orderNumber);
	}
	@Override
	public Long categoryNumber(String openid) {
		
		return mdsLeaseInfoDao.categoryNumber(openid);
	}
	@Override
	public Long orderNumber(String openid) {
		
		return mdsLeaseInfoDao.orderNumber(openid);
	}
	@Override
	public BigDecimal prepareCost(String openid) {
		
		return mdsLeaseInfoDao.prepareCost(openid);
	}
	@Override
	public BigDecimal leaseCost(String openid) {
		
		return mdsLeaseInfoDao.leaseCost(openid);
	}

	@Override
	public List<MdsLeaseInfoEntity> findByInuse() {
		
		return mdsLeaseInfoDao.findByInuse();
	}
	
	@Override
	public void uporderStatus(QueryVo queryVo) {

		mdsLeaseInfoDao.uporderStatus(queryVo.getOrderNumber(), queryVo.getOrderStatus());
	}
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findByOpenid")
	public Page<MdsLeaseInfoVo> findByOpenid(@RequestBody QueryVo queryVo) {
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);

		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  l  from  MdsLeaseInfoEntity l  where 1=1 ");
		// openId
		if (StringUtils.isNotBlank(queryVo.getOpenid())) {
			sql.append(" and l.openid = :openid");
			searchParams.put("openid", queryVo.getOpenid());
		}
		// 订单编号
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) {
			sql.append(" and l.orderNumber = :orderNumber");
			searchParams.put("orderNumber", queryVo.getOrderNumber());
		}
		// 品类名称
		if (StringUtils.isNotBlank(queryVo.getCategoryName())) {
			sql.append(" and l.categoryName = :categoryName");
			searchParams.put("categoryName", queryVo.getCategoryName());
		}
		sql.append(" order by l.leaseStartDate desc ");

		logger.info("findByOpenid>>>>" + sql.toString());

		Page<MdsLeaseInfoEntity> page = (Page<MdsLeaseInfoEntity>) commonDao.findPage(sql.toString(), searchParams,
				pageRequest);

		List<MdsLeaseInfoVo> listVo = new ArrayList<>();

		if (page != null && page.getContent().size() > 0) {
			for (MdsLeaseInfoEntity mdsLeaseInfoEntity : page.getContent()) {
				MdsLeaseInfoVo leaseInfoVo = new MdsLeaseInfoVo();

				BeanUtils.copyProperties(mdsLeaseInfoEntity, leaseInfoVo);
				//添加预计收入
				if(null != leaseInfoVo.getCategoryId() && null != leaseInfoVo.getHospitalId())
				{
					MdsHospitalPriceEntity findPrice = mdsHospitalPriceDao.findPrice(leaseInfoVo.getCategoryId(), leaseInfoVo.getHospitalId());
					//Integer prepareCost = 0;
					BigDecimal prepareCost = BigDecimal.ZERO;
					//日期不为空 ， 计算收益
					if(null != leaseInfoVo.getLeaseStartDate() && null != leaseInfoVo.getLeaseEndDate())
					{
						prepareCost = PriceUtils.calculatePriceNew(
								findPrice.getLeaseUnit(), findPrice.getTopBalance(), findPrice.getMinHour(), 
								leaseInfoVo.getLeaseStartDate(), leaseInfoVo.getLeaseEndDate(),findPrice.getOutHour());
					}
					leaseInfoVo.setPrepareCost(prepareCost);
				}else//如果不符合获取预计收入条件，添加实际收入为预计收入
				{
					leaseInfoVo.setPrepareCost(leaseInfoVo.getLeaseCost());
				}
				//如果查询有报修记录
	          	List<MdsRepairInfoEntity> repairls =mdRepairInfoDao.findByOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
						
				if(null != repairls && repairls.size()>0)
				{
					MdsRepairInfoEntity findOnlyRepariInfo=	repairls.get(0);
					//设置报修信息
					leaseInfoVo.setReason(findOnlyRepariInfo.getReason());
					leaseInfoVo.setRepairRemark(findOnlyRepariInfo.getRemark());
					leaseInfoVo.setRepairfileId(findOnlyRepariInfo.getFileId());
					leaseInfoVo.setServiceReason(findOnlyRepariInfo.getServiceReason());
				}
				listVo.add(leaseInfoVo);
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsLeaseInfoVo> pageVo = new PageImpl<>(listVo, pageRequestn, page.getTotalElements());
		return pageVo;
	}
	


	/**
	 * TODO 获取accessToken
	 * */
	private String getAccessToken(){
		RestTemplate restTemplate = new RestTemplate();
		// 进行网络请求,访问url接口
		String getTokenUrl="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appId+"&secret="+secret;
		logger.info("getAccessToken=============="+getTokenUrl);
		ResponseEntity<String> responseEntity = restTemplate.exchange(getTokenUrl, HttpMethod.GET, null, String.class);
	//	ResponseEntity<String> responseEntity = restTemplate.getForEntity(getTokenUrl,null);
		String map = responseEntity.getBody();
		Map<String, Object> mapTypes = JSON.parseObject(map);  
		String token = (String) mapTypes.get("access_token");
		logger.info("token=============="+token);
		return token;
	}
//	

	/**
     * 发送模板消息sendTemplateMessage
     * 小程序模板消息,发送服务通知
     * @param touser 接收者（用户）的 openid
     * @param template_id 所需下发的模板消息的id
     * @param page 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
     * @param formid 表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id
     * @return
     */
	@Override
	public JSONObject sendTemplateMessage(MdsLeaseInfoEntity leaseInfo) {
		logger.info("sendTemplateMessage==============");
		Map<String, TemplateData> templateMap = new HashMap<>();
		templateMap.put("keyword1", new TemplateData(String.valueOf(leaseInfo.getIntoBalance())));
		templateMap.put("keyword2", new TemplateData(DateParser.formatDates(leaseInfo.getLeaseStartDate())));
		templateMap.put("keyword3", new TemplateData(DateParser.formatDates(new Date())));

		SendTemplateMessage sendTemplateMessage = new SendTemplateMessage();
		// 拼接数据
		sendTemplateMessage.setTouser(leaseInfo.getOpenid());
		sendTemplateMessage.setTemplate_id(lackbalanceTemplateId);
		//sendTemplateMessage.setPage(page);
		sendTemplateMessage.setForm_id(leaseInfo.getWxIncomeNumber());
		sendTemplateMessage.setData(templateMap);
		//sendTemplateMessage.setEmphasis_keyword("");
		
		
		JSONObject json = JSONObject.fromObject(sendTemplateMessage);
		logger.info("##模版发送JSON数据:  " + json.toString());
		String accessToken = getAccessToken();
		logger.info("##模版发送accessToken: " + accessToken);
		logger.info("##模版发送URL: " + templateMessageUrl + accessToken);
		String ss = Httprequests.sendPostWXMessages(templateMessageUrl + accessToken, json.toString());
		JSONObject jsonObj = JSONObject.fromObject(ss);
		logger.info("接收参数消息模版返回值:  " + jsonObj.toString());
		return jsonObj;
	}
	
	/**
     * 发送模板消息sendTemplateMessage
     * 小程序模板消息,发送服务通知
     * @param touser 接收者（用户）的 openid
     * @param template_id 所需下发的模板消息的id
     * @param page 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
     * @param formid 表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id
     * @return
     */
	@Override
	public JSONObject sendTemplateGuanbiMessage(MdsLeaseInfoEntity leaseInfo) {
		logger.info("sendTemplateMessage==============");
		Map<String, TemplateData> templateMap = new HashMap<>();
		templateMap.put("keyword1", new TemplateData(leaseInfo.getOrderNumber()));
		templateMap.put("keyword2", new TemplateData(leaseInfo.getCategoryName()));
		templateMap.put("keyword3", new TemplateData(String.valueOf(leaseInfo.getLeaseDeposit())));
		templateMap.put("keyword4", new TemplateData(String.valueOf(leaseInfo.getLeaseCost())));
		templateMap.put("keyword5", new TemplateData(DateParser.formatDates(leaseInfo.getLeaseStartDate())));
		templateMap.put("keyword6", new TemplateData(DateParser.formatDates(leaseInfo.getLeaseEndDate())));
		
		
		SendTemplateMessage sendTemplateMessage = new SendTemplateMessage();
		// 拼接数据
		sendTemplateMessage.setTouser(leaseInfo.getOpenid());
		sendTemplateMessage.setTemplate_id(orderendTemplateId);//订单结束模版ID
		//sendTemplateMessage.setPage(page);
		sendTemplateMessage.setForm_id(leaseInfo.getWxIncomeNumber());
		sendTemplateMessage.setData(templateMap);
		//sendTemplateMessage.setEmphasis_keyword("");
		
		
		JSONObject json = JSONObject.fromObject(sendTemplateMessage);
		logger.info("##模版发送JSON数据:  " + json.toString());
		String accessToken = getAccessToken();
		logger.info("##模版发送accessToken: " + accessToken);
		logger.info("##模版发送URL: " + templateMessageUrl + accessToken);
		String ss = Httprequests.sendPostWXMessages(templateMessageUrl + accessToken, json.toString());
		JSONObject jsonObj = JSONObject.fromObject(ss);
		logger.info("接收参数消息模版返回值:  " + jsonObj.toString());
		return jsonObj;
	}

}
