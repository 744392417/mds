package com.spt.hospital.server.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;


public interface MdsPropertyInfoDao extends BaseDao<MdsPropertyInfoEntity> {

	@Query("select a from MdsPropertyInfoEntity a where a.hospitalType =?1 ")
	public List<MdsPropertyInfoEntity> findBytype(String hospitalType);

	@Query("select DISTINCT a.hospitalDepart  from MdsPropertyInfoEntity a where a.hospitalDepart != '' ")
	public List<String> findAllDepart();

	@Query("select a from MdsPropertyInfoEntity a where a.hospitalName =?1 and a.status <>'0' ")
	public MdsPropertyInfoEntity byHospitalName(String hospitalName);

	@Query("select a from MdsPropertyInfoEntity a where 1=1 and (a.id =?1 and a.hospitalDistribution>0)  or   (a.properId =?1 and a.properDistribution>0) and   (a.agentId =?1 and a.agentDistribution>0) ")
	public List<MdsPropertyInfoEntity> findByIsfashionable(Long hospitalId);
}

