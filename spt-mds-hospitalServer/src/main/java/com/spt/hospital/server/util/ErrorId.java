package com.spt.hospital.server.util;

/**
 * 错误ID
 * 
 */
public interface ErrorId
{
	final int ERROR_OFFSET = 9000;
	final int ERROR_LOGIN_ERROR = ERROR_OFFSET + 1; // 登录失败，用户名或密码错误
}
