package com.spt.hospital.server.dao;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.QueryVo;


public interface MdsUserInfoDao extends BaseDao<MdsUserInfoEntity> 
{
	/**
	 * 修改信息
	 * */
	@Transactional
	@Modifying
	@Query("update MdsUserInfoEntity u set u.loginName=?1 ,u.loginPwd = ?2 , u.salt=?3  where hospitalId=?4  and roleCode=?5 ")
	public void updateHospRole(String loginName,String loginPwd,String salt,Long hospitalId,String roleCode);
	/**
	 * 修改信息
	 * */
	@Transactional
	@Modifying
	@Query("update MdsUserInfoEntity u set u.loginName=?1 ,u.loginPwd = ?2 , u.salt=?3  where id=?4  ")
	public void updateUserLoginInfo(String loginName,String loginPwd,String salt,Long id);
	/**
	 * 修改 状态
	 * */
	@Transactional
	@Modifying
	@Query("update MdsUserInfoEntity u set u.status=?1  where hospitalId=?2  and roleCode=?3 ")
	public void updateStatus(String status,Long hospitalId,String roleCode);
	/**
	 * 根据用户登录名修改用户密码
	 * */
	@Transactional
	@Modifying
	@Query("update MdsUserInfoEntity u set u.loginPwd = ?2 , u.salt=?3 where u.loginName=?1")
	public void updatePassword(String loginName,String loginPwd,String salt);
	
	
	/**
	 * 修改 手机号
	 * */
	@Transactional
	@Modifying
	@Query("update MdsUserInfoEntity u set u.userPhone=?1,u.remark=?2  where openid=?3 ")
	public void updatePhone(String userPhone,String remark,String openid);

	/**
	 * 根据用户登录用户名密码，获取用户
	 * */
	@Query("select a from MdsUserInfoEntity a where a.loginName =?1 ")
	public  MdsUserInfoEntity  findByloginName(String loginName);
	
	@Query("select a from MdsUserInfoEntity a where a.roleId =?1 ")
	public  List<MdsUserInfoEntity>  findByRole(String roleId);
	
	@Query("select a from MdsUserInfoEntity a where a.token =?1 ")
	public  MdsUserInfoEntity  findByToken(String token);
	
	@Query("select a from MdsUserInfoEntity a where a.roleCode =?1 and a.hospitalId =?2")
	public MdsUserInfoEntity  findByRoleHospital(String roleCode,Long hospitalId);
	
	@Query("select a from MdsUserInfoEntity a where a.hospitalId =?1")
	public List<MdsUserInfoEntity> findByHospital(Long id);
	
	//@Query("select a from MdsUserInfoEntity a where a.openid =?1")
	public MdsUserInfoEntity findByOpenid(String openid);
	
	@Query("select a from MdsUserInfoEntity a where a.openid =?1 order by updatedDate desc ")
	public List<MdsUserInfoEntity> findListByOpenid(String openid);
	
	@Query("select a from MdsUserInfoEntity a where a.loginName =?1")
	MdsUserInfoEntity verifyUserName(String loginName);
	
	@Query("select a from MdsUserInfoEntity a where a.openid =?1 and a.userPhone is not null order by updatedDate desc ")
	public List<MdsUserInfoEntity> findListByUserPhone(String openid);
}

