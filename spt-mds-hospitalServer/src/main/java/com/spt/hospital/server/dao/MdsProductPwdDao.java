package com.spt.hospital.server.dao;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsProductPwdEntity;


public interface MdsProductPwdDao extends BaseDao<MdsProductPwdEntity> {

	/**
	 * 修改密码状态
	 */
	@Transactional
	@Modifying
	@Query("update MdsProductPwdEntity u set u.pwdStatus = ?3  where u.productCode=?1 or productQrcode =?2 ")
	public void updatePwdStatus(String productCode, String productQrcode, String pwdStatus);

	@Query("select a from MdsProductPwdEntity a where a.productCode=?1 and a.pwdStatus =?2 ")
	public List<MdsProductPwdEntity> findByCode(String productCode, String pwdStatus);

	@Transactional
	@Modifying
	@Query("update MdsProductPwdEntity u set u.pwdStatus =?3  where u.keyId=?1 and  u.productCode=?2")
	public void updatePwdStatusByKeyId(String keyId,String productCode,String pwdStatus);

}

