package com.spt.hospital.server.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsUserLoginLogEntity;
import com.spt.hospital.server.dao.MdsUserLoginLogDao;
import com.spt.hospital.server.service.IMdsUserLoginLogService;



@Component
@Transactional(readOnly = false)
public class MdsUserLoginLogServiceImpl extends BaseService<MdsUserLoginLogEntity> implements IMdsUserLoginLogService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsUserLoginLogServiceImpl.class);

	@Autowired
	private MdsUserLoginLogDao mdsUserLoginLogDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsUserLoginLogEntity> getBaseDao() {
		return mdsUserLoginLogDao;
	}

	@Override
	public List<MdsUserLoginLogEntity> findByUserId(Long loginId) {
		return mdsUserLoginLogDao.findByloginId(loginId);
	}
	
}
