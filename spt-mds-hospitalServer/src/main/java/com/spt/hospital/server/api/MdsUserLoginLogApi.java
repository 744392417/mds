package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsUserLoginLogEntity;
import com.spt.hospital.server.service.IMdsUserLoginLogService;


@RestController
@RequestMapping(value = "api/mdsUserLoginLog")
public class MdsUserLoginLogApi extends BaseApi<MdsUserLoginLogEntity> {
	@Autowired
	private IMdsUserLoginLogService mdsUserLoginLogService;

	@Override
	public IBaseService<MdsUserLoginLogEntity> getService() {
		return mdsUserLoginLogService;
	}

}
