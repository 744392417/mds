package com.spt.hospital.server.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;




@RestController
@RequestMapping(value = "api/mdsHospitalPrice")
public class MdsHospitalPriceApi extends BaseApi<MdsHospitalPriceEntity> 
{
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	@Override
	public IBaseService<MdsHospitalPriceEntity> getService() {
		return mdsHospitalPriceService;
	}

	@PostMapping("findPrice")
	public MdsHospitalPriceEntity findPrice(@RequestBody QueryVo queryVo) 
	{
		return mdsHospitalPriceService.findPrice(queryVo.getCategoryId(),queryVo.getHospitalId());
	}
	
	@PostMapping("findListByHospitalId")
	public List<MdsHospitalPriceEntity> findListByHospitalId(@RequestBody QueryVo queryVo)
	{
		return mdsHospitalPriceService.findListByHospitalId(queryVo.getHospitalId());
	}
}
