package com.spt.hospital.server.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;


public interface MdsUserFeedBackDao extends BaseDao<MdsUserFeedBackEntity> {
	
	@Query("select a from MdsUserFeedBackEntity a where a.orderNumber=?1 and a.openid =?2 ")
	public List<MdsUserFeedBackEntity> findByOpenidOrdernum(String orderNumber, String openid);
	
	
	@Query("select a from MdsUserFeedBackEntity a where a.orderNumber=?1  order by createdDate desc ")
	public List<MdsUserFeedBackEntity> findByOrderNum(String orderNumber);
}

