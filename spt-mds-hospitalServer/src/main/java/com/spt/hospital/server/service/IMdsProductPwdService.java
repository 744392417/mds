package com.spt.hospital.server.service;

import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.vo.MdsProductPwdVo;


public interface IMdsProductPwdService extends IBaseService<MdsProductPwdEntity> {

	void updatePwdStatus(MdsProductPwdVo vo);

	void updatePwd(MdsProductPwdVo vo);

	void saveProductPwd(MdsProductPwdVo vo);

	MdsProductPwdEntity findByPwd(MdsProductPwdVo mdsProductPwdVo);

	List<MdsProductPwdEntity> findBylist(MdsProductPwdVo mdsProductPwdVo);

	MdsProductPwdEntity getPushKey(MdsProductPwdVo vo);

	void sendReturnKey(MdsProductPwdVo vo);

}

