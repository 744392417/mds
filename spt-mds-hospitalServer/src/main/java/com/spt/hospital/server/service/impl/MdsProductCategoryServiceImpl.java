package com.spt.hospital.server.service.impl;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.vo.MdsProductCategoryVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsProductCategoryDao;
import com.spt.hospital.server.service.IMdsProductCategoryService;



@Component
@Transactional(readOnly = false)
public class MdsProductCategoryServiceImpl extends BaseService<MdsProductCategoryEntity> implements IMdsProductCategoryService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsProductCategoryServiceImpl.class);

	@Autowired
	private MdsProductCategoryDao mdsProductCategoryDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsProductCategoryEntity> getBaseDao() {
		return mdsProductCategoryDao;
	}

	@Override
	public MdsProductCategoryEntity findByName(MdsProductCategoryVo vo) {
		return mdsProductCategoryDao.findByName(vo.getCategoryName());
	}

	@Override
	public List<MdsProductCategoryEntity> findByCategoryinfo() {
		return mdsProductCategoryDao.findByCategoryinfo();
	}

	@Override
	public MdsProductCategoryEntity findByCategory(QueryVo queryVo) {
		
		return mdsProductCategoryDao.findByCategory(queryVo.getCategoryName());
	}
	

}
