package com.spt.hospital.server.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.commutil.constants.Constant;
import com.hsoft.commutil.encrypt.Digests;
import com.hsoft.commutil.encrypt.Encodes;
import com.hsoft.commutil.encrypt.TokenUtil;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.shiro.ShiroUser;
import com.hsoft.commutil.util.JsonUtil;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.user.MdsUserInfoMngVo;
import com.spt.hospital.server.dao.MdsLeaseInfoDao;
import com.spt.hospital.server.dao.MdsPropertyInfoDao;
import com.spt.hospital.server.dao.MdsUserInfoDao;
import com.spt.hospital.server.service.IMdsUserInfoService;
import com.spt.hospital.server.util.ErrorId;
import com.spt.hospital.server.util.HexSHAUtil;
import com.spt.hospital.server.util.UserLogUtil;


@Component
@Transactional(readOnly = false)
public class MdsUserInfoServiceImpl extends BaseService<MdsUserInfoEntity> implements IMdsUserInfoService
{
	private Logger logger = LoggerFactory.getLogger(MdsUserInfoServiceImpl.class);

	private static final int SALT_SIZE = 8;
	public static final int HASH_INTERATIONS = 1024;
	
	@Autowired
	private MdsUserInfoDao mdsUserInfoDao;
	
	@Autowired
	private MdsPropertyInfoDao mdsPropertyInfoDao;
	
	@Autowired
	private MdsLeaseInfoDao mdsLeaseInfoDao;
	
	
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsUserInfoEntity> getBaseDao() {
		return mdsUserInfoDao;
	}
	
	@Override
	public ShiroUser login(MdsUserInfoVo loginVo) throws ApplicationException 
	{
		logger.info("loginVo:{}", JsonUtil.obj2Json(loginVo));
		ShiroUser shiroUser = null;
		MdsUserInfoEntity user = null;
		boolean isPwdLogin = false;// 密码登录
		if (StringUtils.isNotBlank(loginVo.getLoginName())) 
		{
			user = mdsUserInfoDao.findByloginName(loginVo.getLoginName());
	        if(user == null) 
	        {
				ApplicationException ex = new ApplicationException(ErrorId.ERROR_LOGIN_ERROR, "用户名不存在");
				throw ex;
			}
	    	isPwdLogin = StringUtils.isNotBlank(loginVo.getLoginPwd()) && isPwdEqual(loginVo.getLoginPwd(), user);// 用户名密码登录
		} else
		{
			ApplicationException ex = new ApplicationException(ErrorId.ERROR_LOGIN_ERROR, "用户名为空");
			throw ex;
		}

		if (isPwdLogin)
		{
			shiroUser = new ShiroUser(user.getId(), user.getLoginName(), user.getUserName());
			shiroUser.addProp("loginName", user.getLoginName());
			shiroUser.addProp("userCode", user.getUserCode());
			shiroUser.addProp("userType", user.getUserType());//医院H 物业P 代理商A  平台 M  用户 U
			shiroUser.addProp("userName", user.getUserName());
			shiroUser.addProp("rolrId", user.getRoleId());
			shiroUser.addProp("roleCode", user.getRoleCode());
			shiroUser.addProp("roleName", user.getRoleName());
			shiroUser.addProp("hospitalId", user.getHospitalId());
			shiroUser.addProp("hospitalCode", user.getHospitalCode());
			shiroUser.addProp("hospitalName", user.getHospitalName());
			shiroUser.addProp("hospitalAddress", user.getHospitalAddress());
			shiroUser.addProp("status", user.getStatus());
			List<MdsPropertyInfoEntity> Isfashionablels = mdsPropertyInfoDao.findByIsfashionable(user.getHospitalId());
			if(Isfashionablels!=null && Isfashionablels.size()>0){
				shiroUser.addProp("isfashionable", "Y");
			}else{
				shiroUser.addProp("isfashionable", "N");
			}
			
			try 
			{
				String json = JsonUtil.obj2Json(shiroUser.getProp());
				Map<String, Object> map = new HashMap<>();
				map.put("userInfo", json);
				String userToken = TokenUtil.createToken(HospConstants.SUBJECT_USER, map, HospConstants.SECRETKEY);
				shiroUser.addProp(Constant.GATE_ACCESSTOKEN, userToken);
				//保存Token
				user.setToken(userToken);
				mdsUserInfoDao.save(user);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		if (null == shiroUser) 
		{
			ApplicationException ex = new ApplicationException(ErrorId.ERROR_LOGIN_ERROR, "用户名或密码错误");
			throw ex;
		}
		// 登录日志表
		UserLogUtil.saveLoginLog(shiroUser.id);
		return shiroUser;

	}
	/** 判断2个密码是否一致 */
	private boolean isPwdEqual(String loginPwd, MdsUserInfoEntity user) {
		Boolean ifExitUser = HexSHAUtil.validatePassword(user.getSalt(), loginPwd, user.getLoginPwd());
		return ifExitUser;
	}
	/**
	 * 密码加密
	 * @param user
	 */
	private void entryptPassword(MdsUserInfoVo user) 
	{
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		user.setSalt(Encodes.encodeHex(salt));
		user.setLoginPwd(getEncodePwd(user.getLoginPwd(), salt));
	}

	private String getEncodePwd(String plainPwd, byte[] salt)
	{
		byte[] hashPassword = Digests.sha1(plainPwd.getBytes(), salt, HASH_INTERATIONS);
		return Encodes.encodeHex(hashPassword);
	}
	@Override
	public MdsUserInfoEntity findByToken(String token)
	{
		return mdsUserInfoDao.findByToken(token);
	}
	@Override
	public MdsUserInfoEntity findByRoleHospital(String roleCode, Long hospitalId)
	{
		return mdsUserInfoDao.findByRoleHospital(roleCode, hospitalId);
	}
	@Override
	public void updateHospRole(MdsUserInfoVo vo) 
	{
		entryptPassword(vo);
		mdsUserInfoDao.updateHospRole(vo.getLoginName(), vo.getLoginPwd(), vo.getSalt(), vo.getHospitalId(),
				vo.getRoleCode());
	}
	@Override
	public void updateStatus(MdsUserInfoVo vo) 
	{
		mdsUserInfoDao.updateStatus(vo.getStatus(),vo.getHospitalId(),vo.getRoleCode());
	}
	
	@Override
	public void updatePhone(MdsUserInfoVo mdsUserInfoVo){
		mdsUserInfoDao.updatePhone(mdsUserInfoVo.getUserPhone(),mdsUserInfoVo.getRemark(),mdsUserInfoVo.getOpenid());
	}
	
	@Override
	public List<MdsUserInfoEntity> findByHospital(Long id) {
		return mdsUserInfoDao.findByHospital(id);
	}

	@Override
	public void updateUserLoginInfo(MdsUserInfoVo vo) 
	{
		entryptPassword(vo);
		mdsUserInfoDao.updateUserLoginInfo(vo.getLoginName(), vo.getLoginPwd(), vo.getSalt(), vo.getId());
	}
	/**
	 * 根据Openid获取用户信息
	 * */
	@Override
	public MdsUserInfoEntity findByOpenid(String openid) 	{
		return mdsUserInfoDao.findByOpenid(openid);
	}

	@Override
	public List<MdsUserInfoEntity> findListByOpenid(String openid) {
		return mdsUserInfoDao.findListByOpenid(openid);
	}
	
	
	@Override
	public List<MdsUserInfoEntity> findListByUserPhone(String openid) {
		return mdsUserInfoDao.findListByUserPhone(openid);
	}
	
	
	
	@Override
	public MdsUserInfoEntity findByloginName(QueryVo queryVo) {
		return mdsUserInfoDao.findByloginName(queryVo.getLoginName());
	}

	@Override
	public MdsUserInfoEntity verifyUserName(String loginName) {
		return mdsUserInfoDao.verifyUserName(loginName);
	}
	
	/**
	 * TODO 查询数据到用户管理
	 * */
	
	@Override
	public Page<MdsUserInfoMngVo> findPageByUser(QueryVo queryVo) throws ApplicationException 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append(" select distinct (u.openid) as openid  from   (select m.* from tbl_user_info m group by m.openid having m.created_date = min(m.created_date)) u left join  tbl_lease_info l on u.openid =l.openid  where 1=1  and u.user_Type='U' ");

		if(queryVo.getUserHospId()!=null && !queryVo.getUserHospId().equals("") && !queryVo.getUserType().equals(HospConstants.USER_TYPE_M))
		{
			sql.append(" and ( (l.hospital_Id ="+queryVo.getUserHospId().longValue()+" and l.hospital_Distribution>0 )");
			sql.append(" or (l.proper_Id ="+queryVo.getUserHospId().longValue()+" and l.proper_Distribution>0 )");
			sql.append(" or (l.agent_Id = "+queryVo.getUserHospId().longValue()+" and l.agent_Distribution >0))");
		}
		
		if(StringUtils.isNotBlank(queryVo.getNickName()))
		{
			sql.append(" and (u.nick_Name like :nickName or u.user_Phone like :userPhone)");
			searchParams.put("nickName",  "%" +  queryVo.getNickName()+ "%");
			searchParams.put("userPhone", "%" +  queryVo.getNickName()+ "%");
		}
		
		//logger.info("sql>>>findPageByUser>>>getStartDate>>>"+queryVo.getStartDate());
		if(null != queryVo.getStartDate() && !"".equals(queryVo.getStartDate()))
		{
			sql.append(" and u.created_Date >=:start_Date");
			searchParams.put("start_Date", queryVo.getStartDate());
		}
		//logger.info("sql>>>findPageByUser>>>getEndDate>>>"+queryVo.getEndDate());

		// 租赁结束时间
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append(" and  u.created_Date <=:end_Date");
			searchParams.put("end_Date", queryVo.getEndDate());
		}

		logger.info("sql>>>findPageByUser>>>>>>"+sql.toString());
		Page<String> userInfoPage =this.findPage(sql.toString(),searchParams, pageRequest);
		//遍历添加到展示列表
		//List<MdsUserInfoEntity> content = userInfoPage.getContent();
		List<MdsUserInfoMngVo> addToPageVoList = new ArrayList<MdsUserInfoMngVo>();
		logger.info("======"+userInfoPage.getTotalElements());
		if(userInfoPage!=null && userInfoPage.getContent()!=null){
			logger.info("======"+userInfoPage.getContent().size());
			List<String> ls = userInfoPage.getContent();
             if(ls!=null && ls.size()>0){
       
			for (String obj : ls){
				String openid = obj;
				MdsUserInfoMngVo vo =new MdsUserInfoMngVo();
				logger.info("findListByOpenid>>>>>openid>>>="+openid);
				List<MdsUserInfoEntity> userls = mdsUserInfoDao.findListByOpenid(openid);
				MdsUserInfoEntity userInfoEntity = userls.get(0);
				BeanUtils.copyProperties(userInfoEntity, vo);
				logger.info("findListByOpenid>>>>>openid>>>="+openid);
				Long categoryCount = mdsLeaseInfoDao.categoryNumber(openid);
				Long orderCount = mdsLeaseInfoDao.orderNumber(openid);
				BigDecimal prepareCostCount = mdsLeaseInfoDao.prepareCost(openid);
				BigDecimal leaseCostCount = mdsLeaseInfoDao.leaseCost(openid);
				
				vo.setCategoryCount(categoryCount);
				vo.setOrderCount(orderCount);
				vo.setPrepareCostCount(prepareCostCount);
				vo.setLeaseCostCount(leaseCostCount);
				vo.setRegisterDate(userInfoEntity.getCreatedDate());
				//注册时间转换
				vo.setRegisterDateStr(DateParser.formatDatesS1(userInfoEntity.getCreatedDate()));
				
				List<MdsLeaseInfoEntity> leasels = mdsLeaseInfoDao.findListByOpenid(openid);
				if(leasels!=null && leasels.size()>0){
					MdsLeaseInfoEntity leaseInfoEntity = leasels.get(0);
					vo.setProductCode(leaseInfoEntity.getProductCode());
					vo.setCategoryName(leaseInfoEntity.getCategoryName());
					vo.setHospitalName(leaseInfoEntity.getHospitalName());
					vo.setLeaseStartDate(leaseInfoEntity.getLeaseStartDate());
					vo.setLeaseEndDate(leaseInfoEntity.getLeaseEndDate());
					logger.info(vo.getCategoryName());
				}
				
		
		
				
				addToPageVoList.add(vo);
			}
		}
		}

		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsUserInfoMngVo> pageVo = new PageImpl<>(addToPageVoList, pageRequestn, userInfoPage.getTotalElements());
		return pageVo;
	}
	

	public  Page  findPage(String  qlString,  Map<String,  Object>  parameter,  Pageable  pageable)  {
		Query  query  =  em.createNativeQuery(qlString);
		//  query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(OffPrice.class));
		for  (String  param  :  parameter.keySet())  {
		query.setParameter(param,  parameter.get(param));
		}
		/*
		  *  nativeQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.
		  *  aliasToBean(  Class  resultType));  //返回对象  List<T>  resultList  =
		  *  nativeQuery.getResultList();
		  */

		return  pageable  ==  null  ?  new  PageImpl(query.getResultList())  :  readPage(qlString,  query,  pageable,  parameter);
		}

		private  <T>  Page<T>  readPage(String  qlString,  Query  query,  Pageable  pageable,  Map<String,  Object>  parameter)  {

		query.setFirstResult((int)  pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		Long  total  =  count(qlString,  parameter);
		List<T>  content  =  total  >  pageable.getOffset()  ?  query.getResultList()  :  Collections.<T>emptyList();

		return  new  PageImpl<T>(content,  pageable,  total);
		}

		public  Long  count(String  qlString,  Map<String,  Object>  parameter)  {
		String  fromHql  =  qlString;
		//  select子句与order  by子句会影响count查询,进行简单的排除.
		fromHql  =  "from  ("  +  fromHql  +  ")  a";
		String  countHql  =  "select  count(*)  "  +  fromHql;
		Query  query  =  em.createNativeQuery(countHql);
		for  (String  param  :  parameter.keySet())  {
		query.setParameter(param,  parameter.get(param));
		}
		long  total  =  Long.valueOf(String.valueOf(query.getSingleResult()));
		return  total;
		}
	
}
