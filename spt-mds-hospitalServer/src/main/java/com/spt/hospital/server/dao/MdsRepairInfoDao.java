package com.spt.hospital.server.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;


public interface MdsRepairInfoDao extends BaseDao<MdsRepairInfoEntity> 
{
	// StayaBUG -  more data
	@Query("select r from MdsRepairInfoEntity r where  r.productCode = ?1")
	MdsRepairInfoEntity findByProductCode(String productCode);

	// StayaBUG -  more data
	@Query("select r from MdsRepairInfoEntity r where  r.productCode = ?1 and r.openid = ?2")
	MdsRepairInfoEntity findByCodeAndOpenId(String productCode, String openid);

	@Query("select r from MdsRepairInfoEntity r where  r.productCode = ?1")
	List<MdsRepairInfoEntity> findListByProductCode(String productCode);
	
	@Query("select r from MdsRepairInfoEntity r where  r.productCode = ?1 and r.orderNumber =?2   order by  createdDate desc ")
	List<MdsRepairInfoEntity> findByIdAndOrder(String productCode, String orderNumber);
	
	@Query("select r from MdsRepairInfoEntity r where  r.orderNumber = ?1 order by  updatedDate desc  ")
	List<MdsRepairInfoEntity>  findByOrderNumber(String orderNumber);
}

