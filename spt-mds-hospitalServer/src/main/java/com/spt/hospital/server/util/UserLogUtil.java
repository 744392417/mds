package com.spt.hospital.server.util;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.util.SpringContextHolder;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.entity.MdsUserLoginLogEntity;
import com.spt.hospital.server.service.IMdsUserInfoService;
import com.spt.hospital.server.service.IMdsUserLoginLogService;
import com.spt.hospital.server.service.impl.MdsUserInfoServiceImpl;
import com.spt.hospital.server.service.impl.MdsUserLoginLogServiceImpl;


public class UserLogUtil {
	private static final Logger log=LogManager.getLogger(); 
	private static IMdsUserInfoService mdsUserInfoService = SpringContextHolder.getBean(MdsUserInfoServiceImpl.class);
	private static IMdsUserLoginLogService mdsUserLoginLogService= SpringContextHolder.getBean(MdsUserLoginLogServiceImpl.class);
	public static void saveLoginLog(Long id){
		MdsUserLoginLogEntity loginLog = new MdsUserLoginLogEntity();
		MdsUserInfoEntity entity = mdsUserInfoService.getEntity(id);
		loginLog.setLoginId(entity.getId());
		loginLog.setHospitalId(entity.getHospitalId());
		try {
			mdsUserLoginLogService.save(loginLog);
		} catch (ApplicationException e) {
			log.info(e); 
		}
	}
	public static void updateLoginLog(Long id){
		//登录日志表
		MdsUserInfoEntity entity = mdsUserInfoService.getEntity(id);
		MdsUserLoginLogEntity loginLog = mdsUserLoginLogService.findByUserId(entity.getId()).get(0);
		//计算登录时长
		long hours = 1000 * 60 * 60;//每小时毫秒数
		Date createdDate = loginLog.getCreatedDate();
		Date date = new Date(); 
		long time = date.getTime()-createdDate.getTime();
		BigDecimal gap =BigDecimal.valueOf(time).divide(BigDecimal.valueOf(hours), 2, BigDecimal.ROUND_HALF_UP);
		loginLog.setLoginHours(gap);
		try {
			mdsUserLoginLogService.save(loginLog);
		} catch (ApplicationException e) {
			log.info(e); 
		}
	}
}
