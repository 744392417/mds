package com.spt.hospital.server.service;

import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsUserLoginLogEntity;


public interface IMdsUserLoginLogService extends IBaseService<MdsUserLoginLogEntity>{

	List<MdsUserLoginLogEntity> findByUserId(Long loginId);
}

