package com.spt.hospital.server.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.util.HTTPUtility;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.ReceiveProductVo;
import com.spt.hospital.server.dao.MdsProductInfoDao;
import com.spt.hospital.server.service.IMdsProductInfoService;
import com.spt.hospital.server.util.JsonUtility;




@Component
@Transactional(readOnly = false)
public class MdsProductInfoServiceImpl extends BaseService<MdsProductInfoEntity> implements IMdsProductInfoService 
{
	private Logger logger = LoggerFactory.getLogger(MdsProductInfoServiceImpl.class);
	@Autowired
	private MdsProductInfoDao mdsProductInfoDao;
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;

	@Value("")
	private String sendMdsUrl;
	
	@Override
	public BaseDao<MdsProductInfoEntity> getBaseDao() {
		return mdsProductInfoDao;
	}
	
	@Override
	public MdsProductInfoEntity findByCode(QueryVo vo) {
		return mdsProductInfoDao.findByCode(vo.getProductCode());
	}

//	@Override
//	public List<MdsProductInfoEntity> findByPropertyName(QueryVo vo) {
//		return null;
//	}

	@Override
	public MdsProductInfoEntity findByQrcode(String productQrcode) {
		return mdsProductInfoDao.findByQrcode(productQrcode);
	}

	@Override
	public List<MdsProductInfoEntity> findByCategoryId(MdsProductInfoVo vo) {

		return mdsProductInfoDao.findByCategoryId(vo.getCategoryId(),vo.getProductUseStatus());
	}
	
	@Override
	public List<MdsProductInfoEntity> findproductStatusByCategoryId(MdsProductInfoVo vo) {

		return mdsProductInfoDao.findproductStatusByCategoryId(vo.getCategoryId(),vo.getProductStatus());
	}
	
	

	
	@Override
	public List<MdsProductInfoEntity> findByHospitalId(MdsProductInfoVo vo) {

		return mdsProductInfoDao.findByHospitalId(vo.getCategoryId(), vo.getHospitalId());
	}
	
	
	@Override
	public List<MdsProductInfoEntity> findProductByhospitalId(MdsProductInfoVo vo) {

		return mdsProductInfoDao.findProductByhospitalId(vo.getHospitalId(),vo.getCategoryId());
	}
	
	
	
	/**
	 * 发送设备版本升级请求
	 * */
	@Override
	public void sendVersionUpgradeNews(ReceiveProductVo receiveProductVo) 
	{
		try 
		{
			String uuidJson = HTTPUtility.doPostBody(sendMdsUrl+"/open/bank/account/updateStatus", receiveProductVo, null);
			ReceiveProductVo receiveMessage = JsonUtility.toJavaObject(uuidJson, ReceiveProductVo.class);
			logger.info("==code=====" + receiveMessage.getCode() + "==message=====" + receiveMessage.getMessage());
		} catch (ApplicationException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public List<MdsProductInfoEntity> findByOnlyCategoryId(MdsProductInfoVo vo) {
		return mdsProductInfoDao.findByOnlyCategoryId(vo.getCategoryId());
	}

	@Override
	public MdsProductInfoEntity findByCodeAndId(QueryVo vo) {
		return mdsProductInfoDao.findByCodeAndId(vo.getProductCode(),vo.getProductId());
	}
	
	@Override
	public List<MdsProductInfoEntity> findByfreeStatus(QueryVo vo) {
		return mdsProductInfoDao.findByfreeStatus(vo.getProductUseStatus());
	}
	
	@Override
	public List<MdsProductInfoEntity> findProductUseStatusByhospitalId(MdsProductInfoVo vo) {
		return mdsProductInfoDao.findProductUseStatusByhospitalId(vo.getHospitalId(),vo.getCategoryId(), vo.getProductUseStatus());
	}
	
	@Override
	public List<MdsProductInfoEntity> findProductStatusByhospitalId(MdsProductInfoVo vo) {
		return mdsProductInfoDao.findProductStatusByhospitalId(vo.getHospitalId(), vo.getCategoryId(),vo.getProductStatus());
	}

	@Override
	public void batchReceiveProductStatus(QueryVo vo){
		String product_use_status = null;
		if (HospConstants.BEARCH_RECEIVE_S.equals(vo.getBatchType())) {
			product_use_status = HospConstants.PRODUCT_USE_STATUS_3;// "3"		
		}else{
			product_use_status = HospConstants.PRODUCT_USE_STATUS_2;//"2"
		}
		mdsProductInfoDao.mdsProductInfoEntityStatus(vo.getProductId(),product_use_status);
	}

	@Override
	public Integer equipmentNumber(Long id) {
		return mdsProductInfoDao.equipmentNumber(id); 
	}


	@Override
	public List<MdsProductInfoEntity> allocatedList(QueryVo queryVo) {

		return mdsProductInfoDao.allocatedList(queryVo.getHospitalId());
	}

	@Override
	public void upDatestatus(QueryVo queryVo) {
		 mdsProductInfoDao.upDatestatus(queryVo.getHospitalName(), queryVo.getProductUseStatus(), queryVo.getProductQrcode(),queryVo.getHospitalId(),queryVo.getHospitalDepart(),queryVo.getHospitalWard());
	}

	@Override
	public MdsProductInfoEntity getByHospitalId(QueryVo vo) {
		
		return mdsProductInfoDao.getByHospitalId(vo.getHospitalId(),vo.getProductId());
	}	
	
	@Override
	public int findByhospitalId(Long hospitalId){
		return mdsProductInfoDao.findByhospitalId(hospitalId);
	}
	
	
	@Override
	public List<MdsProductInfoEntity> findByHospitalId(QueryVo vo){
		return mdsProductInfoDao.findByHospitalId(vo.getHospitalId());
	}
	
	
	@Override
	public void updateCategoryName(QueryVo vo) {
		mdsProductInfoDao.updateCategoryName(vo.getCategoryId(), vo.getCategoryFileId(), vo.getCategoryName());
	}

	
	
	@Override
	public void mdsProductStatus(QueryVo vo) {
		mdsProductInfoDao.mdsProductStatus(vo.getProductId(), vo.getProductStatus());
	}


}
