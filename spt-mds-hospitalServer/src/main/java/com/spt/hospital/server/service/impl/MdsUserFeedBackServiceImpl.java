package com.spt.hospital.server.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsUserFeedBackDao;
import com.spt.hospital.server.service.IMdsUserFeedBackService;



@Component
@Transactional(readOnly = false)
public class MdsUserFeedBackServiceImpl extends BaseService<MdsUserFeedBackEntity> implements IMdsUserFeedBackService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsUserFeedBackServiceImpl.class);

	@Autowired
	private MdsUserFeedBackDao mdsUserFeedBackDao;

	@Override
	public BaseDao<MdsUserFeedBackEntity> getBaseDao() {
		return mdsUserFeedBackDao;
	}

	@Override
	public List<MdsUserFeedBackEntity> findByOpenidOrdernum(QueryVo vo) {
		return mdsUserFeedBackDao.findByOpenidOrdernum(vo.getOrderNumber(), vo.getOpenid());
	}
	
	@Override
	public List<MdsUserFeedBackEntity> findByOrderNum(QueryVo vo) {
		return mdsUserFeedBackDao.findByOrderNum(vo.getOrderNumber());
	}


}
