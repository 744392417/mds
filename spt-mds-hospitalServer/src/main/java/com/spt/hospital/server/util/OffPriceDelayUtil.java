///**
// * 
// */
//package com.spt.credit.server.util;
//
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.DelayQueue;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import org.apache.commons.collections.CollectionUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.hsoft.commutil.util.SpringContextHolder;
//import com.spt.credit.server.dao.OffPriceDao;
//import com.spt.credit.server.service.IOffPriceService;
//import com.spt.offer.client.entity.OffPrice;
//
///**
// *
// */
//public class OffPriceDelayUtil {
//
//	private static final Logger logger = LoggerFactory.getLogger(OffPriceDelayUtil.class);
//
//	private static final ExecutorService threadPool = Executors.newSingleThreadExecutor();
//	private static final ExecutorService threadPoolRun = Executors.newSingleThreadExecutor();
//
//	private static final DelayQueue<OffPrice> delayQueue = new DelayQueue<OffPrice>();
//
//	private static volatile boolean running = true;
//
//	private static OffPriceDao offPriceDao = SpringContextHolder.getBean(OffPriceDao.class);
//	private static IOffPriceService offPriceService = SpringContextHolder.getBean(IOffPriceService.class);
//
//	public static void init() {
//		logger.info(">>>>>> start DelayQueen! <<<<<<");
//		running = true;
//		initExpireMonitor();
//		threadPool.execute(new Runnable() {
//			@Override
//			public void run() {
//				while (!Thread.interrupted()) {
//					if (!running)
//						break;
//					try {
//						OffPrice delay = delayQueue.take();
//						final Long priceId = delay.getId();
//						logger.info("取得报价订单,priceId:{}",priceId);
//						threadPoolRun.execute(new Runnable() {
//
//							@Override
//							public void run() {
//								try {
//									offPriceService.invalidPriceStatus(priceId);
//								} catch (Exception e) {
//								}
//							}
//						});
//					} catch (Exception e) {
//						logger.warn("offPrice invalid error ", e);
//					}
//				}
//			}
//		});
//	}
//
//	public void destroy() throws Exception {
//
//		running = false;
//		delayQueue.clear();
//		threadPool.shutdown();
//	}
//
//	public static void addExpireMonitor(Long targetId,Date endTime) {
//		OffPrice delay = new OffPrice();
//		delay.setId(targetId);
//		delay.setValidTime(endTime);
//		boolean result = delayQueue.add(delay);
//		logger.info("addExpireMonitor targetId:{},result:{},endTime:{}",targetId,result,endTime);
//	}
//	
//	public static void removeExpireMonitor(Long treatyId) {
//		
//		OffPrice delay = new OffPrice();
//		delay.setId(treatyId);
//		boolean result = delayQueue.remove(delay);
//		logger.info("removeExpireMonitor treatyId:{},result:{}",treatyId,result);
//	}
//
//	
//	private static void initExpireMonitor() {
//		//获取有效的竞价记录
//		List<OffPrice> list = offPriceDao.findValidOffPriceList();
//		if (CollectionUtils.isEmpty(list))
//			return;
//		logger.info("initExpireMonitor size:{}",list.size());
//		for (OffPrice entity : list) {
//			addExpireMonitor(entity.getId(),entity.getValidTime());
//		}
//	}
//
//}
