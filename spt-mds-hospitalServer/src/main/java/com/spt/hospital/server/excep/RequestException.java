package com.spt.hospital.server.excep;

@SuppressWarnings("serial")
public class RequestException extends RuntimeException{
	
	
	public RequestException() {
		super();
	}
	
	public RequestException(String s) {
		super(s);
	}
	
}
