//package com.spt.credit.server.open;
//
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import org.springframework.web.bind.annotation.RestController;
//
//
//import com.hsoft.commutil.web.PageSearchVo;
//import com.spt.credit.client.entity.BsUserAccount;
//
//import com.spt.credit.server.api.BsUserAccountApi;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//
//@Api(value = "用户帐号管理", description = "用户帐号管理")
//@RestController
//@RequestMapping(value = "open/bs/userAccount")
//public class BsUserAccountOpen {
//	@Autowired
//	private BsUserAccountApi userAccountApi;
//
//	@ApiOperation(value = "分页查询", notes = "分页查询，自定义查询条件")
//	@PostMapping(value = "findPage")
//	public Page<BsUserAccount> findPage(@RequestBody PageSearchVo queryVo) {
//		return userAccountApi.findPage(queryVo);
//	}
//
//	@ApiOperation(value = "获取实体信息", notes = "")
//	@PostMapping(value = "getEntity")
//	public BsUserAccount getEntity(@RequestBody Long id) {
//		return userAccountApi.getEntity(id);
//	}
//
//
//}
