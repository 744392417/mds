package com.spt.hospital.server.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsPropertyInfoDao;
import com.spt.hospital.server.dao.MdsRecordsConptionDao;
import com.spt.hospital.server.service.IMdsRecordsConptionService;



@Component
@Transactional(readOnly = false)
public class MdsRecordsConptionServiceImpl extends BaseService<MdsRecordsConptionEntity> implements IMdsRecordsConptionService{
	private Logger logger = LoggerFactory.getLogger(MdsRecordsConptionServiceImpl.class);

	@Autowired
	private MdsRecordsConptionDao mdsRecordsConptionDao;
	
	@Autowired
	private MdsPropertyInfoDao mdsPropertyInfoDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsRecordsConptionEntity> getBaseDao() 
	{
		return mdsRecordsConptionDao;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Page<MdsRecordsConptionEntity> findByOpenid(QueryVo vo) 
	{
		Pageable pageable = new PageRequest(vo.getPage(), vo.getRows());
		return mdsRecordsConptionDao.findByOpenid(pageable, vo.getOpenid());
	}
	/**
	 * 查询资金结算页面
	 * */
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@Override
	public Page<MdsRecordsConptionVo> findRecordsPage(QueryVo queryVo) 
	{
		logger.info("findRecordsPage  >>>>>>>>>>>>>>>");
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();

		StringBuffer sql = new StringBuffer("select  r.createdDate, r.leaseCost, r.consumStatus, r.orderNumber ");
		sql.append(" , l.categoryName,l.hospitalId, l.hospitalDistribution, l.hospitalFashion, l.properName ");
		sql.append(" , l.properDistribution, l.properFashion, l.agentName, l.agentDistribution ");
		sql.append(" , l.agentFashion, l.platDistribution, l.productName,r.id ");
		sql.append("  from MdsRecordsConptionEntity r left join MdsLeaseInfoEntity l ");
		sql.append("  on r.orderNumber = l.orderNumber where  1=1  and r.consumStatus in ('S','I','P','C') and  r.incomeSatus is null ");
	//	System.out.println("queryVo.getUserHospId()===="+queryVo.getUserHospId());
		if(queryVo.getUserHospId()!=null && !queryVo.getUserHospId().equals("") && !queryVo.getUserType().equals(HospConstants.USER_TYPE_M))
		{
			sql.append(" and ( (l.hospitalId ="+queryVo.getUserHospId().longValue()+" and l.hospitalDistribution>0 )");
			sql.append(" or (l.properId ="+queryVo.getUserHospId().longValue()+" and l.properDistribution>0 )");
			sql.append(" or (l.agentId ="+queryVo.getUserHospId().longValue()+" and l.agentDistribution >0))");
		}
		
		//门店名称
		/*if (StringUtils.isNotBlank(queryVo.getHospitalName())) 
		{
			sql.append(" and r.hospitalName like :hospitalName");
			searchParams.put("hospitalName", "%" + queryVo.getHospitalName() + "%");
		}*/
		//订单编号
		if (StringUtils.isNotBlank(queryVo.getOrderNumber())) 
		{
			sql.append(" and r.orderNumber like :orderNumber");
			searchParams.put("orderNumber", "%" + queryVo.getOrderNumber() + "%");
		}
		//门店Id
		if (queryVo.getHospitalId()!=null && !"".equals(queryVo.getHospitalId())) 
		{
			sql.append(" and r.hospitalId =:hospitalId ");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		
		
		
		//开始时间
		if (null != queryVo.getStartDate() && !"".equals(queryVo.getStartDate())) 
		{
			sql.append(" and  r.createdDate >=:startDate ");
			searchParams.put("startDate", queryVo.getStartDate());
		}
		//结束时间
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append(" and  r.createdDate <=:endDate ");
			searchParams.put("endDate", queryVo.getEndDate());
		}
		
		//品类名称
		if (StringUtils.isNotBlank(queryVo.getCategoryName())) 
		{
			sql.append(" and l.categoryName = :categoryName ");
			searchParams.put("categoryName",queryVo.getCategoryName());
		}
		
		//品类名称
		if (queryVo.getCategoryId() != null && !"".equals(queryVo.getCategoryId())) 
		{
			sql.append(" and l.categoryId = :categoryId ");
			searchParams.put("categoryId",queryVo.getCategoryId());
		}
		
		//收款类别
		if (StringUtils.isNotBlank(queryVo.getConsumStatus())) 
		{
			sql.append(" and r.consumStatus = :consumStatus ");
			searchParams.put("consumStatus",queryVo.getConsumStatus());
		}
		
		
		
		
//		//收支类型 stayAbug - 命名
//		if(StringUtils.isNotBlank(queryVo.getBudgetType()) && StringUtils.isNotBlank(queryVo.getBudgetType()))
//		{
//			if(queryVo.getBudgetType().equals("支出"))
//			{
//				sql.append(" and r.consumStatus ='S' ");
//			}else
//			{
//				sql.append(" and r.consumStatus in ('I','"+queryVo.getConsumStatus()+"' ) ");
//			}
//		}else
//		{
//			//交易类型
//			if(StringUtils.isNotBlank(queryVo.getConsumStatus()))
//			{
//				sql.append(" and r.consumStatus = :consumStatus ");
//				searchParams.put("consumStatus", queryVo.getConsumStatus());
//			}else if(StringUtils.isNotBlank(queryVo.getBudgetType()))//收支类型
//			{
//				if(queryVo.getBudgetType().equals("支出"))
//				{
//					sql.append(" and r.consumStatus ='S' ");
//				}else
//				{
//					sql.append(" and r.consumStatus in ('I') ");
//				}
//			}
//		}
		sql.append(" order by r.").append(queryVo.getSort() + " " + queryVo.getOrder());
		logger.info("findRecordsPage  >>>>>>>>>11>>>>>>"+sql.toString());
		Page<MdsRecordsConptionVo> page = commonDao.findPage(sql.toString(), searchParams, pageRequest);

		List<MdsRecordsConptionVo> listVo = new ArrayList<MdsRecordsConptionVo>();
		//取出数据
		for (Object obj : page.getContent())
		{
			Object[] objs = (Object[]) obj;
			MdsRecordsConptionVo recordsConptionVo = new MdsRecordsConptionVo();
			//0 创建日期
			recordsConptionVo.setCreatedDate((Date) objs[0]);
			//1 租赁费用
			if(null != objs[1]) { recordsConptionVo.setLeaseCost((BigDecimal) objs[1]); }
			//2 消费状态状态 预付款S 退款 I 收入P , 分账 F
			if(null != objs[2]) { recordsConptionVo.setConsumStatus((String) objs[2]); }
			//3 订单编号
			if(null != objs[3]) { recordsConptionVo.setOrderNumber((String) objs[3]); }
			//4 品类名称
			if(null != objs[4]) { recordsConptionVo.setCategoryName((String) objs[4]); }
			//5 医院 门店
			if (null != objs[5]) {
				MdsPropertyInfoEntity propertyEntity = mdsPropertyInfoDao.findOne((Long) objs[5]);
				if (propertyEntity != null && propertyEntity.getHospitalName() != null) {
					recordsConptionVo.setHospitalName(propertyEntity.getHospitalName());
				}

			}
			//6 医院分账金额
			if(null != objs[6]) { recordsConptionVo.setHospitalFashion(new BigDecimal(objs[6].toString())); }
			//7 医院返佣比例
			if(null != objs[7]) 
			{ 
				Float objs7 = Float.parseFloat(objs[7].toString());
				recordsConptionVo.setHospitalDistribution(objs7.intValue()); 
			}
			//8 物业名称
			if(null != objs[8]) { recordsConptionVo.setProperName((String) objs[8]); }
			//9 物业返佣金额
			if(null != objs[9]) { recordsConptionVo.setProperFashion(new BigDecimal(objs[9].toString())); }
			//10 物业返佣比例
			if(null != objs[10]) 
			{ 
				Float objs10 = Float.parseFloat(objs[10].toString());
				recordsConptionVo.setProperDistribution(objs10.intValue());
			}
			//11 所属代理商
			if(null != objs[11]) { recordsConptionVo.setAgentName((String) objs[11]); }
			//12 代理商返佣金额
			if(null != objs[12]) { recordsConptionVo.setAgentFashion(new BigDecimal(objs[12].toString())); }
			//13 代理商返佣比例
			if(null != objs[13]) 
			{ 
				Float objs13 = Float.parseFloat(objs[13].toString());
				recordsConptionVo.setAgentDistribution(objs13.intValue());
			}
			//14 平台收入百分比
			if(null != objs[14]) 
			{ 
				Float objs14 = Float.parseFloat(objs[14].toString());
				recordsConptionVo.setPlatDistrib(objs14.intValue());
			}
			//15 设备名称
			if(null != objs[15]) { recordsConptionVo.setProductName(objs[15].toString()); }
			String recordsProduct = "";
			//支付分账
			if(recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_F))
			{
				if(StringUtils.isNotBlank(recordsConptionVo.getHospitalName()))
				{
					recordsProduct = "支付"+recordsConptionVo.getHospitalName()+"分账";
				}
				else if(StringUtils.isNotBlank(recordsConptionVo.getProperName()))
				{
					recordsProduct = "支付"+recordsConptionVo.getHospitalName()+"分账";
				}
				else if(StringUtils.isNotBlank(recordsConptionVo.getAgentName()))
				{
					recordsProduct = "支付"+recordsConptionVo.getHospitalName()+"分账";
				}
			}
			//支出
			if(recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_S))
			{
				if(StringUtils.isNotBlank(recordsConptionVo.getProductName()))
				{
					recordsProduct = "归还"+recordsConptionVo.getHospitalName()+"预付款";
				}
			}
	
			//汇入
			if(recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_I))
			{
				if(StringUtils.isNotBlank(recordsConptionVo.getProductName()))
				{
					recordsProduct = "收到"+recordsConptionVo.getHospitalName()+"预付款";
				}
			}
			if(null != objs[16]) { recordsConptionVo.setId((Long)objs[16]); }
	
			//设置项目
			recordsConptionVo.setRecordsProduct(recordsProduct);
			
			recordsConptionVo.setCreatedDateStr(DateParser.formatDatesS1(recordsConptionVo.getCreatedDate()));
			
			
			
			if (recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_I)) {
				recordsConptionVo.setIncome(recordsConptionVo.getLeaseCost());
				recordsConptionVo.setDealType("收到预付款");
		}

		if (recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_P)) {
			recordsConptionVo.setObtain(recordsConptionVo.getLeaseCost());
			recordsConptionVo.setDealType("收到租金  ");
		}
		if (recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_S)) {
			recordsConptionVo.setExpenditure(recordsConptionVo.getLeaseCost());
			recordsConptionVo.setDealType("归还预付款");
		}

		if (recordsConptionVo.getConsumStatus().equals(HospConstants.CONSUM_STATUS_C)) {
			recordsConptionVo.setAddition(recordsConptionVo.getLeaseCost());
			recordsConptionVo.setDealType("附加退款");
		}
			
			
			listVo.add(recordsConptionVo);
		}
		System.out.println("========888===6767676676===8888====eeee=44444449===="+listVo.size());
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page pageVo = new PageImpl(listVo, pageRequestn, page.getTotalElements());
		return pageVo;
	}
	@SuppressWarnings({ "deprecation", "unused", "rawtypes" })
	@Override
	public BigDecimal sumAmount(QueryVo queryVo){

		logger.info("================sumAmount=============queryVo.getConsumStatus()============"+queryVo.getConsumStatus());
		// 状态 预付款S 退款 I 收入P , 分账 F
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer("select sum(r.leaseCost) as leaseCost ");
		sql.append("  from MdsRecordsConptionEntity r left join  MdsLeaseInfoEntity l on r.orderNumber =l.orderNumber  where  1=1 and r.incomeSatus is null ");
		
		
		if(queryVo.getUserHospId()!=null && !"".equals(queryVo.getUserHospId()) && !queryVo.getUserType().equals(HospConstants.USER_TYPE_M))
		{
			sql.append(" and ( (l.hospitalId ="+queryVo.getUserHospId().longValue()+" and l.hospitalDistribution>0 )");
			sql.append(" or (l.properId ="+queryVo.getUserHospId().longValue()+" and l.properDistribution>0 )");
			sql.append(" or (l.agentId ="+queryVo.getUserHospId().longValue()+" and l.agentDistribution >0))");
		}

		// 消费状态状态 预付款S 退款 I 收入P , 分账 F
		if (StringUtils.isNotBlank(queryVo.getConsumStatus())) 
		{
			logger.info("=====sumAmount===queryVo.getConsumStatus==========="+queryVo.getConsumStatus());
				sql.append(" and r.consumStatus = :consumStatus");
				searchParams.put("consumStatus", queryVo.getConsumStatus());
		}

		
		//门店名称
//		if (StringUtils.isNotBlank(queryVo.getHospitalName())) 
//		{
//			logger.info("=====sumAmount===queryVo.getHospitalName==========="+queryVo.getHospitalName());
//			sql.append(" and r.hospitalName like :hospitalName");
//			searchParams.put("hospitalName", "%" + queryVo.getHospitalName() + "%");
//		}
		//门店ID
		if (queryVo.getHospitalId()!=null && !"".equals(queryVo.getHospitalId())) 
		{
			logger.info("=====sumAmount===queryVo.getHospitalId==========="+queryVo.getHospitalId());
			sql.append(" and r.hospitalId =:hospitalId");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		//开始日期
		if (queryVo.getStartDate() != null && !"".equals(queryVo.getStartDate())) 
		{
			sql.append("  and  r.createdDate >=:startDate  ");
			searchParams.put("startDate", queryVo.getStartDate());
		}
		//结束日期
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append("  and  r.createdDate <=:endDate");
			searchParams.put("endDate", queryVo.getEndDate());
		}
//		//品类名称
//		if (StringUtils.isNotBlank(queryVo.getCategoryName())) 
//		{
//			sql.append(" and l.categoryName like :categoryName");
//			searchParams.put("categoryName", "%" + queryVo.getCategoryName() + "%");
//		}
		
		
		//品类名称
//		if (StringUtils.isNotBlank(queryVo.getCategoryName())) 
//		{
//			sql.append(" and l.categoryName = :categoryName");
//			searchParams.put("categoryName",queryVo.getCategoryName());
//		}
		
		//品类名称
		if (StringUtils.isNotBlank(queryVo.getCategoryName())) 
		{
			logger.info("=====sumAmount===queryVo.getCategoryName==========="+queryVo.getCategoryName());
			sql.append(" and l.categoryName = :categoryName");
			searchParams.put("categoryName",queryVo.getCategoryName());
		}
		
		logger.info("========sumAmount==========="+sql.toString());
		List list=commonDao.findBy(sql.toString(),searchParams);
		logger.info("=========sumAmount=========="+(list!=null));
		if(list!=null){
			return ((BigDecimal) list.get(0));
		}else{
			return BigDecimal.ZERO;
		}
		
	}

	@Override
	public List<MdsRecordsConptionEntity> findByFOrderNumber(String orderNumber) {
		return mdsRecordsConptionDao.findByFOrderNumber(orderNumber);
	}
	
	@Override
	public void saveRecords(MdsRecordsConptionEntity record) {
		 mdsRecordsConptionDao.save(record);
	}
	
	@Override
	public void upRecordsConptionEntity(String orderNumber) {
		 mdsRecordsConptionDao.upRecordsConptionEntity(orderNumber);
	}
	
	@Override
	public  MdsRecordsConptionEntity  findByIOrderNumber(QueryVo queryVo){
		 return mdsRecordsConptionDao.findByIOrderNumber(queryVo.getOrderNumber());
	}
}
