package com.spt.hospital.server.dao;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;

public interface MdsRecordsConptionDao extends BaseDao<MdsRecordsConptionEntity> {

	@Query("select a from MdsRecordsConptionEntity a where a.openid =?1 and  a.incomeSatus is null order by updatedDate desc")
	public  Page<MdsRecordsConptionEntity>  findByOpenid(Pageable ageable,String openid);
	
	
	@Query("select a from MdsRecordsConptionEntity a where a.orderNumber =?1 and a.consumStatus='F' ")
	public  List<MdsRecordsConptionEntity>  findByFOrderNumber(String orderNumber);
	
	
	@Transactional
	@Modifying
	@Query("update MdsRecordsConptionEntity a set a.incomeSatus='1'  where a.orderNumber =?1 and a.consumStatus='F' ")
	public void upRecordsConptionEntity(String orderNumber);
	
	
	@Query("select a from MdsRecordsConptionEntity a where a.orderNumber =?1 and a.consumStatus='I' ")
	public  MdsRecordsConptionEntity  findByIOrderNumber(String orderNumber);
	
		
}

