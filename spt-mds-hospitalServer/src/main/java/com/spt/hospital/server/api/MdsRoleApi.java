package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsRoleEntity;
import com.spt.hospital.server.service.IMdsRoleService;




@RestController
@RequestMapping(value = "api/mdsRole")
public class MdsRoleApi extends BaseApi<MdsRoleEntity> {
	@Autowired
	private IMdsRoleService mdsRoleService;

	@Override
	public IBaseService<MdsRoleEntity> getService() {
		return mdsRoleService;
	}

}
