package com.spt.hospital.server.api;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.ReceiveProductVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;
import com.spt.hospital.client.vo.product.ProductInfoByStoreManagerVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.service.IMdsProductInfoService;
import com.spt.hospital.server.service.IMdsPropertyInfoService;
import com.spt.hospital.server.service.IMdsRepairInfoService;


@RestController
@RequestMapping(value = "api/mdsProductInfo")
public class MdsProductInfoApi extends BaseApi<MdsProductInfoEntity> 
{
	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	@Autowired
	private IMdsLeaseInfoService mdsLeaseInfoService;
	@Autowired
	private IMdsPropertyInfoService mdsPropertyInfoService;
	
	@Autowired
	private IMdsRepairInfoService mdsRepairInfoService;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("${fileurl}")
	private String fileurl;
	
	@Override
	public IBaseService<MdsProductInfoEntity> getService() {
		return mdsProductInfoService;
	}

	@PostMapping("findByCode")
	public MdsProductInfoEntity findByCode(@RequestBody QueryVo vo) {
		return mdsProductInfoService.findByCode(vo);
	}
	
	@PostMapping("findByCodeAndId")
	public MdsProductInfoEntity findByCodeAndId(@RequestBody QueryVo vo) {
		return mdsProductInfoService.findByCodeAndId(vo);
	}
	
	@PostMapping("findProductInfoByQrcode")
	public MdsProductInfoEntity findProductInfoByQrcode(@RequestBody QueryVo vo) {
		return mdsProductInfoService.findByQrcode(vo.getProductQrcode());
	}
	
	@PostMapping("batchReceiveProductStatus")//2 空闲 3 暂停 
	public void batchReceiveProductStatus(@RequestBody QueryVo vo){
		mdsProductInfoService.batchReceiveProductStatus(vo);
	}
	
	
	
	/**
	 * TODO findByQrcode
	 * */
	@PostMapping("findByQrcode")
	public MdsProductInfoVo findByQrcode(@RequestBody QueryVo vo)
	{
		MdsProductInfoEntity entity = mdsProductInfoService.findByQrcode(vo.getProductQrcode());
		MdsProductInfoVo mdsProductVo = new MdsProductInfoVo();
		if (entity != null) {
			BeanUtils.copyProperties(entity, mdsProductVo);
			MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceService.findPrice(mdsProductVo.getCategoryId(),
					mdsProductVo.getHospitalId());
			if(hospitalPrice!=null){
				mdsProductVo.setLeaseDeposit(hospitalPrice.getLeaseDeposit());
				mdsProductVo.setLeaseUnit(hospitalPrice.getLeaseUnit());
				mdsProductVo.setTopBalance(hospitalPrice.getTopBalance());
				mdsProductVo.setMinHour(hospitalPrice.getMinHour());
				mdsProductVo.setOutHour(hospitalPrice.getOutHour());
				mdsProductVo.setChargingRules("<= "+hospitalPrice.getMinHour()+" 小时 "+hospitalPrice.getLeaseUnit()+" 元/单,超出后 "+hospitalPrice.getTopBalance()+" 元/小时 ");
//				Long hospitalId = hospitalPrice.getHospitalId();
//				if(hospitalId!=null){
//					//查询病房
//					MdsPropertyInfoEntity propEntity = mdsPropertyInfoService.getEntity(hospitalId);
//					if(propEntity!=null){
//						mdsProductVo.setHospitalWard(propEntity.getHospitalWard());
//					}
//				}
			}
			mdsProductVo.setFileCategotyUrl(fileurl+entity.getFileId());

			//hospitalPrice.getLeaseUnit() +"元/小时,封顶"+ hospitalPrice.getTopBalance() +"元/天 每天递减2元,最低"+hospitalPrice.getMinimumPrice()+"元/天"
			// 开锁密码
//			MdsProductPwdVo mdsProductPwdVo = new MdsProductPwdVo();
//			mdsProductPwdVo.setProductQrcode(vo.getProductQrcode());
//			MdsProductPwdEntity productPwdEntity = mdsProductPwdService.findByPwd(mdsProductPwdVo);
//			if (productPwdEntity != null) {
//				mdsProductVo.setUnlockPwd(productPwdEntity.getUnlockPwd());
//			}
		}
		return mdsProductVo;
	}

	/**
	 * TODO 我的设备 设备详情列表 根据设备品类查询
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findProductInfoListPage")
	public Page<ProductInfoByCategoryVo> findProductInfoListPage(@RequestBody QueryVo queryVo) 
	{	
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		StringBuffer sql = new StringBuffer();
		sql.append("select  p  from  MdsProductInfoEntity p where p.productUseStatus <> '-1' ");
		Map<String, Object> searchParams = new HashMap<>();
		// 设备品类ID
		if (queryVo.getCategoryId() != null && !"".equals(queryVo.getCategoryId())) 
		{
			sql.append(" and p.categoryId ="+queryVo.getCategoryId());
		}
		//设备兼容状态
		if (queryVo.getProductStatus()!=null &&  !"".equals(queryVo.getProductStatus()) && !"undefined".equals(queryVo.getProductStatus())) 
		{
			sql.append(" and p.productStatus  = :productStatus");
			searchParams.put("productStatus", queryVo.getProductStatus());
		}
		//设备使用状态
		if (queryVo.getProductUseStatus()!=null &&  !"".equals(queryVo.getProductUseStatus()) && !"undefined".equals(queryVo.getProductUseStatus())) 
		{
			sql.append(" and p.productUseStatus  = :productUseStatus");
			searchParams.put("productUseStatus", queryVo.getProductUseStatus());
		}
		//所在科室
		if (StringUtils.isNotBlank(queryVo.getHospitalDepart())) 
		{
			sql.append(" and p.hospitalDepart  = :hospitalDepart");
			searchParams.put("hospitalDepart", queryVo.getHospitalDepart());
		}
		//所在医院
		if (null!=queryVo.getHospitalId() && !"".equals(queryVo.getHospitalId())) 
		{
			sql.append(" and p.hospitalId  = :hospitalId ");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		//设备ID关键字
		if (StringUtils.isNotBlank(queryVo.getProductCode())) 
		{
 			sql.append(" and p.productCode like '%"+queryVo.getProductCode()+"%' ");
		}
		//设备ID关键字
		if (StringUtils.isNotBlank(queryVo.getProductQrcode())) 
		{
 			sql.append(" and (p.productQrcode like '%"+queryVo.getProductQrcode()+"%'  ");
 			sql.append(" or p.productCode like '%"+queryVo.getProductQrcode()+"%' ) ");
		}
		//开始结束时间
		if(null != queryVo.getStartDate())
		{
			sql.append(" and p.updatedDate >= :startDate ");
			searchParams.put("startDate", queryVo.getStartDate());

		}
		//开始结束时间
		if(null != queryVo.getEndDate())
		{
			sql.append(" and p.updatedDate <= :endDate ");
			searchParams.put("endDate", queryVo.getEndDate());
		}
		sql.append(" order by updatedDate DESC");
		System.out.println("++++"+sql.toString());
		Page<MdsProductInfoEntity> productInfoPage = (Page<MdsProductInfoEntity>) commonDao.findPage(sql.toString(),
				searchParams, pageRequest);
		//根据设备信息查询订单列表
		List<ProductInfoByCategoryVo> addToPageVoList = new ArrayList<ProductInfoByCategoryVo>();
		List<MdsProductInfoEntity> content = productInfoPage.getContent();
		for (MdsProductInfoEntity mdsProductInfoEntity : content) 
		{
			ProductInfoByCategoryVo addToPageVo = new ProductInfoByCategoryVo();
			BeanUtils.copyProperties(mdsProductInfoEntity, addToPageVo);
			
			
			//获取最近的订单
			if(!mdsProductInfoEntity.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_1)){
				List<MdsLeaseInfoEntity> leaseInfoList =  
						mdsLeaseInfoService.findByCodeAndId(mdsProductInfoEntity.getId());
				if(null != leaseInfoList && leaseInfoList.size()>0)
				{
					MdsLeaseInfoEntity mdsLeaseInfoEntity = leaseInfoList.get(0);
					//设置最近租出的设备的信息
					addToPageVo.setLeaseEndDate(mdsLeaseInfoEntity.getLeaseEndDate());
					addToPageVo.setLeaseStartDate(mdsLeaseInfoEntity.getLeaseStartDate());
					addToPageVo.setLeaseTime(mdsLeaseInfoEntity.getLeaseTime());
				}
			}
			
			
			
			
			if(mdsProductInfoEntity.getHospitalId()!=null && !"".equals(mdsProductInfoEntity.getHospitalId())){
				 MdsPropertyInfoEntity propertyInfo = mdsPropertyInfoService.getEntity(mdsProductInfoEntity.getHospitalId());
			      if (null != propertyInfo && !"".equals(propertyInfo.getHospitalName())) {
			    	  mdsProductInfoEntity.setHospitalName(propertyInfo.getHospitalName());
			      }
			}
		
			//设备状态转换
			
			     productTransStatus(addToPageVo);
			//如果有报修添加报修信息
//			if(mdsProductInfoEntity.getProductStatus().equals(HospConstants.PRODUCT_STATUS_5)){
//				
//			//	mdsRepairInfoService.findByIdAndOrder(mdsProductInfoEntity.getId(), orderNumber)
//				
//				
//				
//				
//			}
//	
			     if(addToPageVo.getLeaseStartDate()!=null){
			    	    addToPageVo.setLeaseStartDateStr(DateParser.formatDatesS1(addToPageVo.getLeaseStartDate()));
			     }
			     
			     if(addToPageVo.getLeaseEndDate()!=null){
				     addToPageVo.setLeaseEndDateStr(DateParser.formatDatesS1(addToPageVo.getLeaseEndDate()));
			     }
			 
			     if(addToPageVo.getUpdatedDate()!=null){
				     addToPageVo.setUpdatedDateStr(DateParser.formatDatesS1(addToPageVo.getUpdatedDate()));
			     }
	
			addToPageVoList.add(addToPageVo);
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<ProductInfoByCategoryVo> pageVo = 
				new PageImpl<>(addToPageVoList, pageRequestn, productInfoPage.getTotalElements());
		return pageVo;
	}
	
	
	
	public ProductInfoByCategoryVo productTransStatus(ProductInfoByCategoryVo addToPageVo) {
         
		if (addToPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_1)) {
			addToPageVo.setProductUseStatusValue("备选");

		} else if (addToPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_2)) {
			addToPageVo.setProductUseStatusValue("空闲");

		} else if (addToPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_3)) {
			addToPageVo.setProductUseStatusValue("暂停");
		} else if (addToPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_4)) {
			addToPageVo.setProductUseStatusValue("使用中");
		} else if (addToPageVo.getProductUseStatus().equals(HospConstants.PRODUCT_USE_STATUS_5)) {
			addToPageVo.setProductUseStatusValue("维修中");

		} else {
			addToPageVo.setProductUseStatusValue("删除");

		}

		if (addToPageVo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_1)) {
			addToPageVo.setProductStatusValue("默认");

		} else if (addToPageVo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_2)) {
			addToPageVo.setProductStatusValue("异常");

		} else if (addToPageVo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_3)) {
			addToPageVo.setProductStatusValue("低电量");
		} else if (addToPageVo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_4)) {
			addToPageVo.setProductStatusValue("逾期");

		} else if (addToPageVo.getProductStatus().equals(HospConstants.PRODUCT_STATUS_5)) {
			addToPageVo.setProductStatusValue("报修");

		} else {
			addToPageVo.setProductStatusValue("删除");

		}
		return addToPageVo;
	}
	
	
	
	@PostMapping("findProductInfoList")
	public List<MdsProductInfoEntity> findProductInfoList(@RequestBody QueryVo queryVo) 
	{
		StringBuffer sql = new StringBuffer();
		sql.append("select  p  from  MdsProductInfoEntity p where p.productUseStatus <> '-1' ");
		Map<String, Object> searchParams = new HashMap<>();
		// 设备品类ID
		if (queryVo.getCategoryId() != null && !"".equals(queryVo.getCategoryId())) 
		{
			sql.append(" and p.categoryId  like '%"+queryVo.getCategoryId()+"%'");
		}
		//设备兼容状态
		if (StringUtils.isNotBlank(queryVo.getProductStatus())) 
		{
			sql.append(" and p.productStatus  = :productStatus");
			searchParams.put("productStatus", queryVo.getProductStatus());
		}
		//设备使用状态
		if (StringUtils.isNotBlank(queryVo.getProductUseStatus())) 
		{
			sql.append(" and p.productUseStatus  = :productUseStatus");
			searchParams.put("productUseStatus", queryVo.getProductUseStatus());
		}
		//所在科室
		if (StringUtils.isNotBlank(queryVo.getHospitalDepart())) 
		{
			sql.append(" and p.hospitalDepart  = :hospitalDepart");
			searchParams.put("hospitalDepart", queryVo.getHospitalDepart());
		}
		//所在医院
		if (StringUtils.isNotBlank(queryVo.getHospitalName())) 
		{
			sql.append(" and p.hospitalName  = :hospitalName");
			searchParams.put("hospitalName", queryVo.getHospitalName());
		}
		//设备ID关键字
		if (StringUtils.isNotBlank(queryVo.getProductCode())) 
		{
 			sql.append(" and p.productCode like '%"+queryVo.getProductCode()+"%' ");
		}
		//开始结束时间
		if(null != queryVo.getStartDate() && null != queryVo.getEndDate())
		{
			sql.append(" and p.updatedDate >= :startDate and p.updatedDate <= :endDate ");
			searchParams.put("startDate", queryVo.getStartDate());
			searchParams.put("endDate", queryVo.getEndDate());
		}
		sql.append("order by createdDate DESC");
		List<MdsProductInfoEntity> productInfoList = commonDao.findBy(sql.toString(), searchParams);
		return productInfoList;
	}
	
	@PostMapping("sendVersionUpgradeNews")
	public void sendVersionUpgradeNews(@RequestBody ReceiveProductVo receiveProductVo) 
	{
		if(StringUtils.isNotBlank(receiveProductVo.getInteractiveType()))
		{
			mdsProductInfoService.sendVersionUpgradeNews(receiveProductVo);
		}
	}
	/**
	 * TODO 查询门店管理设备列表
	 * */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findProductListByStore")
	public Page<ProductInfoByStoreManagerVo> findProductListByStore(@RequestBody QueryVo queryVo)
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		//查询该门店所有的设备信息
		sql.append(" select p  from  MdsProductInfoEntity p  where 1 = 1  ");
		if(null != queryVo.getHospitalId())
		{
			sql.append(" and  p.hospitalId  =  :hospitalId ");
			searchParams.put("hospitalId", queryVo.getHospitalId());
		}
		//设备ID模糊查询
		if(StringUtils.isNotBlank(queryVo.getProductQrcode()))
		{
			sql.append(" and  p.productQrcode  like  '%"+queryVo.getProductQrcode()+"%' ");
		}
		

		if(StringUtils.isNotBlank(queryVo.getCategoryName()))
		{
			sql.append(" and  p.categoryName  like  '%"+queryVo.getCategoryName()+"%' ");
		}
		//开始结束时间
//		if(null != queryVo.getStartDate() && null != queryVo.getEndDate())
//		{
//			sql.append(" and p.updatedDate between :startDate and :endDate ");
//			searchParams.put("startDate", queryVo.getStartDate());
//			searchParams.put("endDate", queryVo.getEndDate());
//		}
		
		//开始结束时间
		if (queryVo.getStartDate() != null && !"".equals(queryVo.getStartDate())) 
		{
			sql.append("  and p.updatedDate >=:startDate ");
			searchParams.put("startDate",queryVo.getStartDate());
		}
		
		if (queryVo.getEndDate() != null && !"".equals(queryVo.getEndDate())) 
		{
			sql.append(" and p.updatedDate <=:endDate   ");
			searchParams.put("endDate",queryVo.getEndDate());
		}
		
		
		
		
		sql.append("and p.productUseStatus in('2','3','4','5')");
		
		Page<MdsProductInfoEntity> productInfoPage = 
				(Page<MdsProductInfoEntity>)commonDao.findPage(sql.toString(),searchParams, pageRequest);
		List<MdsProductInfoEntity> content = productInfoPage.getContent();
		List<ProductInfoByStoreManagerVo> addToPageVoList = new ArrayList<ProductInfoByStoreManagerVo>();
		//根据设备信息查询订单列表
		for (MdsProductInfoEntity mdsProductInfoEntity : content) 
		{
			ProductInfoByStoreManagerVo addToPageVo = new ProductInfoByStoreManagerVo();
			BeanUtils.copyProperties(mdsProductInfoEntity, addToPageVo);
			//获取最近的订单
			List<MdsLeaseInfoEntity> leaseInfoList =  
					mdsLeaseInfoService.findByCodeAndId(mdsProductInfoEntity.getId());
			if(null != leaseInfoList && leaseInfoList.size()>0)
			{
				MdsLeaseInfoEntity mdsLeaseInfoEntity = leaseInfoList.get(0);
				//设置最近租出的设备的信息 租赁时长，开始结束时间
				addToPageVo.setLeaseEndDate(mdsLeaseInfoEntity.getLeaseEndDate());
				addToPageVo.setLeaseStartDate(mdsLeaseInfoEntity.getLeaseStartDate());
				addToPageVo.setLeaseTime(mdsLeaseInfoEntity.getLeaseTime());
				//设置医院信息 地址，合作租赁方
				MdsPropertyInfoEntity propertyInfoentity = mdsPropertyInfoService.getEntity(queryVo.getHospitalId());
				addToPageVo.setHospitalAddress(propertyInfoentity.getHospitalAddress());
				addToPageVo.setHospitalPartners(propertyInfoentity.getHospitalPartners());
			}
			addToPageVoList.add(addToPageVo);
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<ProductInfoByStoreManagerVo> pageVo = 
				new PageImpl<>(addToPageVoList, pageRequestn, productInfoPage.getTotalElements());
		return pageVo;
	}
	
//	@PostMapping("moeys")
//	public MdsHospitalPriceEntity moeys(@RequestBody Long id){
//		return mdsLeaseInfoService.moeys(id);
//	}
//	
	@PostMapping("undistributedList")
	public List<MdsProductInfoEntity> undistributedList(@RequestBody QueryVo queryVo){
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append(" select p  from  MdsProductInfoEntity p  where p.productUseStatus='1' ");
		if (queryVo.getMinimum() !=null) {
			sql.append(" and p.serial >"+queryVo.getMinimum());
		}
		if (queryVo.getMaximum() != null) {
			sql.append(" and p.serial <"+queryVo.getMaximum());
		}
		if (queryVo.getProductQrcode() != null) {
			sql.append(" and  p.productQrcode  like  '%"+queryVo.getProductQrcode()+"%' ");
		}
		
		 @SuppressWarnings("unchecked")
		List<MdsProductInfoEntity> productInfoPage =  commonDao.findBy(sql.toString(), searchParams);
				
		return	productInfoPage;
	}
	@PostMapping("allocatedList")
	public List<MdsProductInfoEntity> allocatedList(@RequestBody QueryVo queryVo){
		return mdsProductInfoService.allocatedList(queryVo);
	}
	@PostMapping("upDatestatus")
	public void upDatestatus(@RequestBody QueryVo queryVo){
		mdsProductInfoService.upDatestatus(queryVo);
	}
	
	@PostMapping("getByHospitalId")
	public MdsProductInfoEntity getByHospitalId(@RequestBody QueryVo vo){
		return mdsProductInfoService.getByHospitalId(vo);
	}
	
	@PostMapping("findByhospitalId")
	public int findByhospitalId(@RequestBody Long hospitalId){
		return mdsProductInfoService.findByhospitalId(hospitalId);
	}
	
	@PostMapping("findByHospitalId")
	public List<MdsProductInfoEntity> findByHospitalId(@RequestBody QueryVo vo){
		return mdsProductInfoService.findByHospitalId(vo);
	}
	
	@PostMapping("findProductUseStatusByhospitalId")
	public List<MdsProductInfoEntity> findProductUseStatusByhospitalId(@RequestBody MdsProductInfoVo vo){
		return mdsProductInfoService.findProductUseStatusByhospitalId(vo);
	}
	
	@PostMapping("mdsProductStatus")
	public void mdsProductStatus(@RequestBody QueryVo vo){
		mdsProductInfoService.mdsProductStatus(vo);
	}
}
