package com.spt.hospital.server.api;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.hsoft.commutil.shiro.ShiroUser;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.util.PriceUtils;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.MdsUserInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.user.MdsUserInfoMngVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.service.IMdsProductInfoService;
import com.spt.hospital.server.service.IMdsRepairInfoService;
import com.spt.hospital.server.service.IMdsUserInfoService;
import com.spt.hospital.server.util.UserLogUtil;

@RestController
@RequestMapping(value = "api/mdsUserInfo")
public class MdsUserInfoApi extends BaseApi<MdsUserInfoEntity> {
	@Autowired
	private IMdsUserInfoService mdsUserInfoService;
	@Autowired
	private IMdsLeaseInfoService mdsLeaseInfoService;
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	@Autowired
	private IMdsRepairInfoService mdsRepairInfoService;
	
	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	
	
	@Autowired
	private CommonDao commonDao;
	@Override
	public IBaseService<MdsUserInfoEntity> getService() {
		return mdsUserInfoService;
	}
	/**
	 * TODO 查询数据到用户详情
	 * */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findUserInfoDetail")
	public MdsUserInfoMngVo findUserInfoDetail(@RequestBody QueryVo queryVo)
	{
//		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
//		Map<String, Object> searchParams = new HashMap<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append(" select distinct u.openid  from  MdsUserInfoEntity u  where u.userType='U' ");
//		if(StringUtils.isNotBlank(queryVo.getOpenid()))
//		{
//			sql.append(" and u.openid = :openid");
//			searchParams.put("openid", queryVo.getOpenid());
//		}
//		Page<String> userInfoPage =(Page<String>)commonDao.findPage(sql.toString(),searchParams, pageRequest);
		
		
		List<MdsUserInfoEntity> userls = mdsUserInfoService.findListByOpenid(queryVo.getOpenid());
		MdsUserInfoEntity userInfoEntity = userls.get(0);
		MdsUserInfoMngVo addToPageListVo = new MdsUserInfoMngVo();
		addToPageListVo.setNickName(userInfoEntity.getNickName());
		//BeanUtils.copyProperties(userInfoEntity, addToPageListVo);
		//添加用户注册时间
		     addToPageListVo.setRegisterDate(userInfoEntity.getCreatedDate());
		      List<MdsLeaseInfoVo> leaseInfoVols = new ArrayList<>();
		  	 BigDecimal leaseCostAll =BigDecimal.ZERO;
		  	 
		  	BigDecimal leaseCostCount = mdsLeaseInfoService.leaseCost(queryVo.getOpenid());
		 	addToPageListVo.setLeaseCostAll(leaseCostCount);
				//根据openid获取用户所有订单的信息
//				List<MdsLeaseInfoEntity> findByUserManager = mdsLeaseInfoService.findListByUserManager(queryVo.getOpenid());
//				//循环列表添加数据
//				for (MdsLeaseInfoEntity mdsLeaseInfoEntity : findByUserManager) 
//				{
//					MdsLeaseInfoVo leaseInfoVo =new MdsLeaseInfoVo();
//					BeanUtils.copyProperties(mdsLeaseInfoEntity, leaseInfoVo);
//					//添加预计收入
//					if(null != leaseInfoVo.getCategoryId() && null != leaseInfoVo.getHospitalId())
//					{
//						MdsHospitalPriceEntity findPrice = mdsHospitalPriceService.findPrice(leaseInfoVo.getCategoryId(), leaseInfoVo.getHospitalId());
//						//Integer prepareCost = 0;
//						BigDecimal prepareCost = BigDecimal.ZERO;
//						//日期不为空 ， 计算收益
//						if(null != leaseInfoVo.getLeaseStartDate() && null != leaseInfoVo.getLeaseEndDate())
//						{
//							prepareCost = PriceUtils.calculatePriceNew(
//									findPrice.getLeaseUnit(), findPrice.getTopBalance(), findPrice.getMinHour(), 
//									leaseInfoVo.getLeaseStartDate(), leaseInfoVo.getLeaseEndDate(),findPrice.getOutHour());
//						}
//						leaseInfoVo.setPrepareCost(prepareCost);
//					}else//如果不符合获取预计收入条件，添加实际收入为预计收入
//					{
//						leaseInfoVo.setPrepareCost(mdsLeaseInfoEntity.getLeaseCost());
//					}
//					
//					leaseCostAll.add(leaseInfoVo.getPrepareCost());
//					//如果查询有报修记录
//		          	List<MdsRepairInfoEntity> repairls =mdsRepairInfoService.findByOrderNumber(mdsLeaseInfoEntity.getOrderNumber());
//							
//					if(null != repairls && repairls.size()>0)
//					{
//						MdsRepairInfoEntity findOnlyRepariInfo=	repairls.get(0);
//						//设置报修信息
//						leaseInfoVo.setReason(findOnlyRepariInfo.getReason());
//						leaseInfoVo.setRepairRemark(findOnlyRepariInfo.getRemark());
//						leaseInfoVo.setRepairfileId(findOnlyRepariInfo.getFileId());
//						leaseInfoVo.setServiceReason(findOnlyRepariInfo.getServiceReason());
//					}
//					//填入列表
//					leaseInfoVols.add(leaseInfoVo);
//				}
//				
//				
//			addToPageListVo.setLeaseInfoVo(leaseInfoVols);
//				addToPageListVo.setLeaseCostAll(leaseCostAll);
//			}
//		}

		return addToPageListVo;
	}
	
	/**
	 * TODO 查询数据到用户管理
	 * */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findPageByUserInfoMng")
	public Page<MdsUserInfoMngVo> findPageByUserInfoMng(@RequestBody QueryVo queryVo) throws ApplicationException 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append(" select u  from   (select m from MdsUserInfoEntity m group by m.openid having m.createdDate = min(m.createdDate)) u  join MdsLeaseInfoEntity l on l.openid = u.openid  where u.userType='U' ");
		if(StringUtils.isNotBlank(queryVo.getProductName()))
		{
			sql.append(" and l.productName = :productName");
			searchParams.put("productName", queryVo.getProductName());
		}
		if(StringUtils.isNotBlank(queryVo.getHospitalName()))
		{
			sql.append(" and l.hospitalName = :hospitalName");
			searchParams.put("hospitalName", queryVo.getHospitalName());
		}
		

		//开始结束时间
		if(null != queryVo.getStartDate())
		{
			//logger.info("sql>>findPageByUserInfoMng>>getStartDate>>>>>"+queryVo.getStartDate());
			sql.append(" and u.createdDate >= :startDate ");
			searchParams.put("startDate", queryVo.getStartDate());

		}
		//开始结束时间
		if(null != queryVo.getEndDate())
		{
			//logger.info("sql>>findPageByUserInfoMng>>getEndDate>>>>>"+queryVo.getEndDate());
			sql.append(" and u.createdDate <= :endDate ");
			searchParams.put("endDate", queryVo.getEndDate());
		}
		
//		if(null != queryVo.getStartDate() && null != queryVo.getEndDate())
//		{
//			sql.append(" and l.leaseStartDate BETWEEN :leaseStartDate");
//			searchParams.put("leaseStartDate", queryVo.getStartDate());
//			
//			sql.append(" and  :leaseEndDate");
//			searchParams.put("leaseEndDate", queryVo.getEndDate());
//		}
		logger.info("sql>>findPageByUserInfoMng>>>>>>>"+sql.toString());
		Page<MdsUserInfoEntity> userInfoPage =(Page<MdsUserInfoEntity>)commonDao.findPage(sql.toString(),searchParams, pageRequest);
		//遍历添加到展示列表
		List<MdsUserInfoEntity> content = userInfoPage.getContent();
		List<MdsUserInfoMngVo> addToPageVoList = new ArrayList<MdsUserInfoMngVo>();
		for (MdsUserInfoEntity mdsUserInfoEntity : content)
		{
			//用户信息
			MdsUserInfoMngVo addToPageListVo = new MdsUserInfoMngVo();
			BeanUtils.copyProperties(mdsUserInfoEntity, addToPageListVo);
			//用户最新订单信息
			String openid = mdsUserInfoEntity.getOpenid();
			List<MdsLeaseInfoEntity> findByUserManager = mdsLeaseInfoService.findListByUserManager(openid);
			MdsLeaseInfoEntity mdsLeaseInfoEntity = findByUserManager.get(0);
			BeanUtils.copyProperties(mdsLeaseInfoEntity, addToPageListVo);
			//添加至列表
			addToPageListVo.setRegisterDate(mdsUserInfoEntity.getCreatedDate());
			//预计收入
			if(null != addToPageListVo.getCategoryId() && null != addToPageListVo.getHospitalId())
			{
				MdsHospitalPriceEntity findPrice = mdsHospitalPriceService.findPrice(addToPageListVo.getCategoryId(), addToPageListVo.getHospitalId());
				if(null != findPrice)
				{
					//Integer prepareCost = 0;
					BigDecimal prepareCost = BigDecimal.ZERO;
					if(null != addToPageListVo.getLeaseStartDate() && null != addToPageListVo.getLeaseEndDate())
					{
						prepareCost = PriceUtils.calculatePriceNew(
								findPrice.getLeaseUnit(), findPrice.getTopBalance(), findPrice.getMinHour(), 
								addToPageListVo.getLeaseStartDate(), addToPageListVo.getLeaseEndDate(),findPrice.getOutHour());
					}
					addToPageListVo.setPrepareCost(prepareCost);
				}
			}else//如果不符合获取预计收入条件，添加实际收入
			{
				addToPageListVo.setPrepareCost(addToPageListVo.getLeaseCost());
			}
			//填入列表
			addToPageVoList.add(addToPageListVo);
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsUserInfoMngVo> pageVo = new PageImpl<>(addToPageVoList, pageRequestn, userInfoPage.getTotalElements());
		return pageVo;
	}
	
	/**
	 * TODO 查询数据到用户管理
	 * */
	
	@PostMapping("findPageByUser")
	public Page<MdsUserInfoMngVo> findPageByUser(@RequestBody QueryVo queryVo) throws ApplicationException 
	{
		return mdsUserInfoService.findPageByUser(queryVo);
	}
		
	@PostMapping("login")
	public ShiroUser login(@RequestBody MdsUserInfoVo mdsUserInfoVo) throws ApplicationException 
	{
			return mdsUserInfoService.login(mdsUserInfoVo);
	}
	@PostMapping(value = "logout")
	public void logout(@RequestBody Long userId) throws ApplicationException 
	{
		UserLogUtil.updateLoginLog(userId);
	}
	@PostMapping("findByToken")
	public MdsUserInfoEntity findByToken(@RequestBody String token) {
		return mdsUserInfoService.findByToken(token);
	}
	@PostMapping("updateHospRole")
	public void updateHospRole(@RequestBody MdsUserInfoVo mdsUserInfoVo) {
		mdsUserInfoService.updateHospRole(mdsUserInfoVo);
	}
	@PostMapping("updateUserLoginInfo")
	public void updateUserLoginInfo(@RequestBody MdsUserInfoVo mdsUserInfoVo)
	{
		mdsUserInfoService.updateUserLoginInfo(mdsUserInfoVo);
	}
	@PostMapping("updateStatus")
	public void updateStatus(@RequestBody MdsUserInfoVo mdsUserInfoVo) {
		mdsUserInfoService.updateStatus(mdsUserInfoVo);
	}
	/**
	 * 根据Openid获取用户信息
	 * */
	@PostMapping("findByOpenid")
	public MdsUserInfoEntity findByOpenid(@RequestBody String openid)
	{
		return mdsUserInfoService.findByOpenid(openid);
	}
	/**
	 * 根据Openid获取用户信息列表
	 * */
	@PostMapping("findListByOpenid")
	public List<MdsUserInfoEntity> findListByOpenid(@RequestBody String openid)
	{
		return mdsUserInfoService.findListByOpenid(openid);
	}
	
	/**
	 * 根据Openid 修改手机号
	 * */
	@PostMapping("updatePhone")
	public  void updatePhone(@RequestBody MdsUserInfoVo mdsUserInfoVo){
		 mdsUserInfoService.updatePhone(mdsUserInfoVo);
	}
	
	/**
	 * 根据findByloginName获取用户信息列表
	 * */
	@PostMapping("findByloginName")
	public MdsUserInfoEntity findByloginName(@RequestBody QueryVo queryVo){
		return mdsUserInfoService.findByloginName(queryVo);
	}
	
	
	/**
	 * 验证用户名是否存在
	 */
	@PostMapping("verifyUserName")
	public MdsUserInfoEntity verifyUserName(@RequestBody QueryVo queryVo){
		return mdsUserInfoService.verifyUserName(queryVo.getLoginName());
	}

	
}
