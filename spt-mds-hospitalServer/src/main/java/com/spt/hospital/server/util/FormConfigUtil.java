/*package com.spt.credit.server.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.hsoft.admin.client.cache.DictUtil;
import com.spt.offer.client.entity.BsTemplateConfig;
import com.spt.offer.client.vo.DictDataVo;
import com.spt.offer.client.vo.NumberCfgVo;
import com.spt.offer.client.vo.PriceCfgVo;

public class FormConfigUtil {

	public static Map<String, Object> findByTypeCd(String templateCat,String typeCd,String lang, List<String> dictCds) {
		Map<String, Object> rtn = new HashMap<String, Object>();
		BsTemplateConfig template =TemplateContentUtility.findTemplate(templateCat, "FMC_"+typeCd, lang);
		if(template!=null){
			String content = template.getContent();
			Map<String, String> obj = (Map<String, String>)JSONUtils.parse(content);
			for(String dictCd:dictCds){
				String values =(String) obj.get(dictCd);
				if(dictCd.equals("saaszsPriceCfg")){
					PriceCfgVo vo = JSON.parseObject(values, PriceCfgVo.class);
					rtn.put(dictCd, vo);
				}else if(dictCd.equals("saaszsNumberCfg")){
					NumberCfgVo vo = JSON.parseObject(values, NumberCfgVo.class);
					rtn.put(dictCd, vo);
				}else if(dictCd.equals("saaszsPackageSpec")){
					rtn.put(dictCd, values);
				}else{
					List<DictDataVo> dataList = new ArrayList<DictDataVo>();
					for(String value:values.split(",")){
						if(value.indexOf("_")>0){
							String[] str=value.split("\\_");
							DictDataVo dict= new DictDataVo();
							dict.setDictCd(value);
							dict.setDictName(DictUtil.getValue(dictCd, str[0]));
							dataList.add(dict);	
						}else{
							DictDataVo dict= new DictDataVo();
							dict.setDictCd(value);
							dict.setDictName(DictUtil.getValue(dictCd, value));
							dataList.add(dict);	
						}
						
					}
					rtn.put(dictCd, dataList);
				}
				
			}
		}
		return rtn;
	}

	public static List<DictDataVo> findByTypeCd(String templateCat, String typeCd, String lang, String dictCd) {
		List<DictDataVo> dataList = new ArrayList<DictDataVo>();
		BsTemplateConfig template =TemplateContentUtility.findTemplate(templateCat, "FMC_"+typeCd, lang);
		if(template!=null){
			String content = template.getContent();
			Map<String, String> obj = (Map<String, String>)JSONUtils.parse(content);
			String values =(String) obj.get(dictCd);
			for(String value:values.split(",")){
				DictDataVo dict= new DictDataVo();
				dict.setDictCd(value);
				dict.setDictName(DictUtil.getValue(dictCd, value));
				dataList.add(dict);
			}
		}
		return dataList;
	}

}
*/