package com.spt.hospital.server.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsUserLoginLogEntity;


public interface MdsUserLoginLogDao extends BaseDao<MdsUserLoginLogEntity> {

	@Query("select a from MdsUserLoginLogEntity a where  a.loginId =?1 order by createdDate desc ")
	public List<MdsUserLoginLogEntity> findByloginId(Long loginId);

}

