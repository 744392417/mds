package com.spt.hospital.server.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsRepairInfoService;


@RestController
@RequestMapping(value = "api/mdsRepairInfo")
public class MdsRepairInfoApi extends BaseApi<MdsRepairInfoEntity> 
{
	@Autowired
	private IMdsRepairInfoService mdsRepairInfoService;

	@Override
	public IBaseService<MdsRepairInfoEntity> getService() 
	{
		return mdsRepairInfoService;
	}

	@PostMapping("findByProductCode")
	public MdsRepairInfoEntity findByProductCode(@RequestBody QueryVo vo) {
		return mdsRepairInfoService.findByProductCode(vo);
	}
	@PostMapping("findListByProductCode")
	List<MdsRepairInfoEntity> findListByProductCode(@RequestBody String productCode)
	{
		return mdsRepairInfoService.findListByProductCode(productCode);
	}
	
	@PostMapping("findByOrderNumber")
	List<MdsRepairInfoEntity> findByOrderNumber(@RequestBody QueryVo vo){
		return mdsRepairInfoService.findByOrderNumber(vo.getOrderNumber());
	}
}
