package com.spt.hospital.server.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsRoleEntity;
import com.spt.hospital.server.dao.MdsRoleDao;
import com.spt.hospital.server.service.IMdsRoleService;



@Component
@Transactional(readOnly = false)
public class MdsRoleServiceImpl extends BaseService<MdsRoleEntity> implements IMdsRoleService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsRoleServiceImpl.class);

	@Autowired
	private MdsRoleDao mdsRoleDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsRoleEntity> getBaseDao() {
		return mdsRoleDao;
	}
	
}
