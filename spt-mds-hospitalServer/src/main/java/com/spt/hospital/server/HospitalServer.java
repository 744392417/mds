/**
 * 
 */
package com.spt.hospital.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.hsoft.admin.client.config.AdminClientConfig;
import com.hsoft.commutil.config.CommConfig;
import com.hsoft.push.client.config.PushClientConfig;
import com.spt.hospital.server.task.MdsSocketServer;

/**
 * @author huangjian
 *
 */
// 这里的配置文件必须另外起个名字，不能直接使用application.properties，框架自动会以application.properties里面的配置项最为最高优先级
@PropertySource(value = { "classpath:/config.properties","classpath:/jdbc.properties" , "file:d:/deploy/config.properties",
		"file:/data/product/deploy/config.properties"}, ignoreResourceNotFound = true)
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages={"com.spt.hospital.client.api"})
@Import({CommConfig.class,AdminClientConfig.class,PushClientConfig.class})
@EnableScheduling
public class HospitalServer{
	public static void main(String[] args) {
		SpringApplication.run(HospitalServer.class, args);
		
//		//起socket服务
//	         	MdsSocketServer mdsSocketServer = new MdsSocketServer();
//	         	mdsSocketServer.startSocketServer();

	}
}
