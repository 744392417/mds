package com.spt.hospital.server.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsUserFeedBackService;


@RestController
@RequestMapping(value = "api/mdsUserFeedBack")
public class MdsUserFeedBackApi extends BaseApi<MdsUserFeedBackEntity> {
	@Autowired
	private IMdsUserFeedBackService mdsUserFeedBackService;

	@Override
	public IBaseService<MdsUserFeedBackEntity> getService() {
		return mdsUserFeedBackService;
	}

	
	@PostMapping("findByOpenidOrdernum")
	public List<MdsUserFeedBackEntity> findByOpenidOrdernum(@RequestBody QueryVo vo) {
		return mdsUserFeedBackService.findByOpenidOrdernum(vo);
	}
	
	@PostMapping("findByOrderNum")
	public List<MdsUserFeedBackEntity> findByOrderNum(@RequestBody QueryVo vo) {
		return mdsUserFeedBackService.findByOrderNum(vo);
	}
	
}
