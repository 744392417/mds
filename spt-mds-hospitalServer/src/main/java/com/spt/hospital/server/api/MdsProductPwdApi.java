package com.spt.hospital.server.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsProductPwdEntity;
import com.spt.hospital.client.vo.MdsProductPwdVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsProductPwdService;




@RestController
@RequestMapping(value = "api/mdsProductPwd")
public class MdsProductPwdApi extends BaseApi<MdsProductPwdEntity> {
	@Autowired
	private IMdsProductPwdService mdsProductPwdService;

	@Override
	public IBaseService<MdsProductPwdEntity> getService() {
		return mdsProductPwdService;
	}
	
	@Autowired
	private CommonDao commonDao;
	
	
	
	@PostMapping("updatePwdStatus")
	public void updatePwdStatus(@RequestBody MdsProductPwdVo vo) {
		 mdsProductPwdService.updatePwdStatus(vo);
	}
	
	@PostMapping("getPushKey")
	public MdsProductPwdEntity getPushKey(@RequestBody MdsProductPwdVo vo) {
		return  mdsProductPwdService.getPushKey(vo);
	}
	
	
	@PostMapping("sendReturnKey")
	public void sendReturnKey(@RequestBody MdsProductPwdVo vo) {
		 mdsProductPwdService.sendReturnKey(vo);
	}
	
	
	
	@SuppressWarnings("unchecked")
	@PostMapping("getMdsProductPwdInfo")
	public MdsProductPwdEntity getMdsProductPwdInfo(@RequestBody QueryVo queryVo) 
	{
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  c  from  MdsProductPwdEntity c where 1=1 and  pwdStatus = '0' ");
		if (queryVo.getProductId()!=null && !"".equals(queryVo.getProductId())) 
		{
			sql.append(" and  c.productId  = :productId  ");
			searchParams.put("productId", queryVo.getProductId());
		}
		if (StringUtils.isNotBlank(queryVo.getProductCode())) 
		{
			sql.append(" and  c.productCode  = :productCode  ");
			searchParams.put("productCode", queryVo.getProductCode());//设备上的编码 (ID号)
		}
		if (StringUtils.isNotBlank(queryVo.getProductQrcode())) 
		{
			sql.append(" and  c.productQrcode  = :productQrcode  ");
			searchParams.put("productQrcode", queryVo.getProductQrcode());//设备上的二维码
		}
		if (StringUtils.isNotBlank(queryVo.getProductName())) 
		{
			sql.append(" and  c.productName  = :productName  ");
			searchParams.put("productName", queryVo.getProductName());
		}
		if (StringUtils.isNotBlank(queryVo.getUnlockPwd())) 
		{
			sql.append(" and  c.unlockPwd  = :unlockPwd  ");
			searchParams.put("unlockPwd", queryVo.getUnlockPwd());
		}
		sql.append(" order by c.updatedDate desc ");
		System.out.println("getMdsProductPwdInfo>>>>" + sql.toString());

		List<MdsProductPwdEntity> lst =commonDao.findBy(sql.toString(),searchParams);
		if(lst!=null && lst.size()>0)
		{
			return lst.get(0);
		}else
		{
			return null;
		}
	}
}
