package com.spt.hospital.server.util;

import java.util.ArrayList;
import java.util.List;

public class ResConditionParser {
	public enum MatchType {
		EQ, NEQ, LIKE, NLIKE, LT, GT, LE, GE, IN;
	}

	public static final String AND = "_AND_";
	private static final String or = "_or_";// 高优先级
	private static final String OR = "_OR_";// 低优先级
	public static final String TRUE = "true";

	/**
	 * 将指定字符串转化为条件类
	 * <ul>
	 * <li>amount_GT_10000：金额大于1万
	 * <li>amount_LT_10000_AND_price_GT_1000：金额小于1万，并且单价大于1000
	 * </ul>
	 * 
	 * @param conditionValue
	 * @return
	 */
	public static List<List<List<ConditionField>>> parse(String conditionValue) {
		List<List<List<ConditionField>>> listOr = new ArrayList<List<List<ConditionField>>>();
		if (conditionValue != null && !conditionValue.equals("无")) {
			String[] conditionsOr = conditionValue.split(OR);
			for (String conTmp : conditionsOr) {
				List<List<ConditionField>> lstAnd = new ArrayList<List<ConditionField>>();
				String[] conditionsAnd = conTmp.split(AND);
				for (String conAnd : conditionsAnd) {
					List<ConditionField> lstItem = new ArrayList<ConditionField>();
					String[] conditions_or = conAnd.split(or);
					for (String conItem : conditions_or) {
						String[] condition = conItem.split("_");
						if (condition.length == 3) {
							MatchType matchType = MatchType.valueOf(condition[1]);
							lstItem.add(new ConditionField(condition[0], matchType, condition[2]));
						} else if (condition.length == 1) {
							lstItem.add(new ConditionField(condition[0], MatchType.EQ, TRUE));
						}
					}
					lstAnd.add(lstItem);
				}
				listOr.add(lstAnd);
			}
		}
		return listOr;
	}
}
