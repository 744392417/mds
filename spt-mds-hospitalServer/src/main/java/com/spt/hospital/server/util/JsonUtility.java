package com.spt.hospital.server.util;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
//import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;

public final class JsonUtility
{
    private static final String SerialWriterString = "���л�JSON�������쳣";
    private static final HashMap<String, String> DefaultTypeMap = new HashMap<String, String>();
    
    //private static SerializeConfig mapping = new SerializeConfig();  
    //private static String dateFormat;  
    static {  
        //dateFormat = "yyyy-MM-dd HH:mm:ss";  
        //mapping.put(Date.class, new SimpleDateFormatSerializer(dateFormat));  
    }  
    
    private JsonUtility()
    {
    }

    public static <T> T toJavaObject(String s, Class<T> clazz)
    {
    	return JSONObject.parseObject(s, clazz);
    }
    
    public static <T> T toJavaObject(String s, Type clazz)
    {
    	return JSONObject.parseObject(s, clazz);
    }
    
    public static <T> T toJavaObject2(String s, TypeReference<T> t)
    {
    	return JSONObject.parseObject(s, t);
    }
    
    public static <T> List<T> toJavaObjectArray(String s, Class<T> clazz)
    {
    	return JSONObject.parseArray(s, clazz);
    }
    
    public static String toJSONString(Object o)
    {
        try
        {
        	if (o instanceof List){
        		 return JSONArray.toJSONString(o);
        	}else{
        		return JSONObject.toJSONString(o);
        	}
        }
        catch (Exception e)
        {
            return SerialWriterString;
        }
    }

    /**
     * @param o
     * @param prettyFormat ��ʽ�����JSONString
     * @return
     */
    public static String toJSONString(Object o, boolean prettyFormat)
    {
        try
        {
            return JSONObject.toJSONString(o, prettyFormat);
        }
        catch (Exception e)
        {
            return SerialWriterString;
        }
    }
    /**
     * 
     * @param o
     * @return
     */
    public static String toWriteNullJSONString(Object o)
    {
        SerializerFeature[] features = { SerializerFeature.WriteMapNullValue, // ��������ֶ�
                SerializerFeature.WriteNullListAsEmpty,
                // list�ֶ����Ϊnull�����Ϊ[]��������null
                SerializerFeature.WriteNullNumberAsZero,
                // ��ֵ�ֶ����Ϊnull�����Ϊ0��������null
                SerializerFeature.WriteNullBooleanAsFalse,
                // Boolean�ֶ����Ϊnull�����Ϊfalse��������null
                SerializerFeature.WriteNullStringAsEmpty
                // �ַ������ֶ����Ϊnull�����Ϊ""��������null
        };
        
        try
        {
            return JSONObject.toJSONString(o, features);
        }
        catch (Exception e)
        {
            return SerialWriterString;
        }
    }
    
    public static <K, V> String map2JSONString(Map<K, V> map)
    {
        return toJSONString(map);
    }
    
    public static <T> String list2JSONString(List<T> list)
    {
        try
        {
            //return JSONArray.toJSONString(list, mapping);
            return JSONArray.toJSONString(list);
        }
        catch (Exception e)
        {
            return SerialWriterString;
        }
    }
    
    /**
     * ��һ��json�ַ���ת����֧���б�Ƕ�׽ṹ��MAP������һ��Ƕ�ף�
     * û����typeMap�ж������͵ģ�Ĭ��һ��ת������ΪString
     * 
     * @param s				json�ַ���
     * @param typeMap		����map��
     * 						key: 	���������磺sr_country
     * 						value:	��������֧�֣�BigDecimal,Date,Integer,String
     * @return
     */
    @SuppressWarnings("unchecked")
	public static Map<String, Object> toObjectMap(String s, Map<String, String> typeMap)
    {
    	if (typeMap == null)
    	{
    		typeMap = DefaultTypeMap;
    	}
    	
    	Map<String, Object> map = (Map<String, Object>)toJavaObject(s, Map.class);
    	Object obj;
    	for (String key : map.keySet())
    	{
    		obj = map.get(key);
    		if (obj instanceof JSONArray) //��Ƕ��List<Map<String, Object>>����
    		{
    			JSONArray arr = (JSONArray)obj;
    			List<Map<String, Object>> subList = new ArrayList<Map<String, Object>>();
    			for (Object o : arr)
    			{
    				JSONObject jobj = (JSONObject)o;
    				//�ݹ������map
    				Map<String, Object> subMap = toObjectMap(jobj.toJSONString(), typeMap);
    				subList.add(subMap);
    			}
    			map.put(key, subList);
    		}
    		else
    		{
    			if (!typeMap.containsKey(key)) //Ĭ�ϲ���String����
    			{
    				map.put(key, String.valueOf(obj));
    			}
    			else
    			{
    				String type = typeMap.get(key);
    				if (type.equalsIgnoreCase("BigDecimal"))
    				{
    					map.put(key, new BigDecimal(String.valueOf(obj)));
    				}
    				else if (type.equalsIgnoreCase("Date"))
    				{
    					map.put(key, new Date(Long.valueOf(String.valueOf(obj))));
    				}
    				else if (type.equalsIgnoreCase("Integer"))
    				{
    					map.put(key, Integer.valueOf(String.valueOf(obj)));
    				}
    				else
    				{
    					map.put(key, String.valueOf(obj));
    				}
    			}
    		}
    	}
    	
    	return map;
    }
    
    /**
     * ��һ��json�ַ���ת����Map<String, String[]>����
     * @param s
     * @return
     */
    @SuppressWarnings("unchecked")
	public static Map<String, String[]> toStringArrayMap(String s)
    {
    	Map<String, Object> map = (Map<String, Object>)toJavaObject(s, Map.class);
    	Map<String, String[]> rtnMap = new HashMap<String, String[]>();
    	Object obj;
    	for (String key : map.keySet())
    	{
    		obj = map.get(key);
    		if (obj instanceof JSONArray) 
    		{
    			Object[] objArr = ((JSONArray)obj).toArray();
    			if (objArr != null && objArr.length > 0)
    			{
    				int len = objArr.length;
    				String[] strArr = new String[len];
    				for (int i = 0; i < len; i++)
    				{
    					strArr[i] = String.valueOf(objArr[i]);
    				}
    				rtnMap.put(key, strArr) ;
    			}
    		}
    	}
    	return rtnMap;
    }
}
