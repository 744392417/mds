package com.spt.hospital.server.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsBankMasterEntity;
import com.spt.hospital.server.service.IMdsBankMasterService;




@RestController
@RequestMapping(value = "api/mdsBankMaster")
public class MdsBankMasterApi extends BaseApi<MdsBankMasterEntity> {
	@Autowired
	private IMdsBankMasterService mdsBankMasterService;

	@Override
	public IBaseService<MdsBankMasterEntity> getService() {
		return mdsBankMasterService;
	}

}
