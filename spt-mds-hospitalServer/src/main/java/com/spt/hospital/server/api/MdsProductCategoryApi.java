package com.spt.hospital.server.api;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.MdsProductCategoryVo;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsProductCategoryService;
import com.spt.hospital.server.service.IMdsProductInfoService;




@RestController
@RequestMapping(value = "api/mdsCategory")
public class MdsProductCategoryApi extends BaseApi<MdsProductCategoryEntity> {
	@Autowired
	private IMdsProductCategoryService mdsProductCategoryService;

	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	
	

	@Override
	public IBaseService<MdsProductCategoryEntity> getService() {
		return mdsProductCategoryService;
	}

	@Autowired
	private CommonDao commonDao;

	@Value("${fileurl}")
	private String filePath;
	
	@PostMapping("findByName")
	public MdsProductCategoryEntity findByName(@RequestBody MdsProductCategoryVo vo) {
		return mdsProductCategoryService.findByName(vo);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findPageProductCategory")
	public Page<MdsProductCategoryVo> findPageProductCategory(@RequestBody QueryVo queryVo) {
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  c  from  MdsProductCategoryEntity c where c.status=1  ");
		//设备名称
		if (StringUtils.isNotBlank(queryVo.getCategoryName()))
		{
			sql.append(" and  c.categoryName  like  '%"+queryVo.getCategoryName()+"%'  ");
		}
		sql.append(" order by c.").append(queryVo.getSort() + " " + queryVo.getOrder());
		System.out.println("findPageUcsCompany>>>>" + sql.toString());
		
		Page<MdsProductCategoryEntity> page = (Page<MdsProductCategoryEntity>) commonDao.findPage(sql.toString(),
				searchParams, pageRequest);
		List<MdsProductCategoryVo> productCategoryVoList = new ArrayList<>();
		if (page != null && page.getContent().size() > 0) 
		{
			for (MdsProductCategoryEntity entity : page.getContent()) 
			{
				MdsProductCategoryVo caterotyVo = new MdsProductCategoryVo();
				BeanUtils.copyProperties(entity, caterotyVo);
				
				MdsProductInfoVo vo = new MdsProductInfoVo();
				vo.setCategoryId(entity.getId());

				// 1、备选：在设备管理中录入，还没有分配医院的设备
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);
				caterotyVo.setBeixuanNum(this.getProductNumber(vo));
				// 2、空闲：在门店管理中配置好的设备，还没有使用，或使用后已经归还。
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
				caterotyVo.setKongxuanNum(this.getProductNumber(vo));
				// 3、暂停：只有空闲状态可以进入暂停状态，暂停状态设备不可使用。
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
				caterotyVo.setZhantingNum(this.getProductNumber(vo));
				// 4、使用中：正在使用中
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
				caterotyVo.setShiyongNum(this.getProductNumber(vo));
				// 5、维修中：客户通过系统进行报修，平台确认后，进入报修状态，用s能使用
				vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
				caterotyVo.setWeixiuzhong(this.getProductNumber(vo));

				
				
				/*// 6、异常：如果设备处于空闲状态超过2周（暂定），设备属性为异常。
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_2);
				caterotyVo.setYichangNum(this.getProductStatuNumber(vo));*/
				/*// 7、低电量：设备通信次数*0.002（通信500次）+待机天数*0.002（待机500天）=1就为低电量状态
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_3);
				caterotyVo.setDidianliangNum(this.getProductStatuNumber(vo));*/
				/*// 8、逾期：客户预计收入/押金>80%，状态就是逾期，予以标记。
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_4);
				caterotyVo.setYuqiNum(this.getProductStatuNumber(vo));*/

				//10.默认
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
				caterotyVo.setMoren(this.getProductStatuNumber(vo));
				
				//9.报修中  
				vo.setProductStatus(HospConstants.PRODUCT_STATUS_5);
				caterotyVo.setBaoxiuNum(this.getProductStatuNumber(vo));
				
				//设置总数量
				List<MdsProductInfoEntity> findByCategoryId = mdsProductInfoService.findByOnlyCategoryId(vo);
				caterotyVo.setCountNumber(findByCategoryId.size());
				//设置图片
				String imageUrl = filePath+caterotyVo.getCategoryFileId();
				caterotyVo.setCategoryImage(imageUrl);
				productCategoryVoList.add(caterotyVo);
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsProductCategoryVo> pageVo = new PageImpl<>(productCategoryVoList, pageRequestn, page.getTotalElements());
		return pageVo;
	}
	/**
	 * TODO 获取设备数量
	 * */
	public Integer getProductNumber(MdsProductInfoVo vo) 
	{
		List<MdsProductInfoEntity> ls = mdsProductInfoService.findByCategoryId(vo);
		if (ls != null && ls.size() > 0) 
		{
			return ls.size();
		} else 
		{
			return 0;
		}
	}
	
	
	/**
	 * TODO 获取设备兼容状态数量
	 * */
	public Integer getProductStatuNumber(MdsProductInfoVo vo) 
	{
		System.out.println("getCategoryId="+vo.getCategoryId());
		System.out.println("getProductStatus="+vo.getProductStatus());
		List<MdsProductInfoEntity> ls = mdsProductInfoService.findproductStatusByCategoryId(vo);
	
		if (ls != null && ls.size() > 0) 
		{
			System.out.println("ls.size()="+ls.size());
			return ls.size();
		} else 
		{
			return 0;
		}
	}
	
	
	/**
	 * TODO 删除设备品类是许查找改设备品类下是否有设备 如果有则不允许删除此设备品类
	 */
	@PostMapping("equipmentNumber")
	public Integer equipmentNumber(@RequestBody Long id){
		return mdsProductInfoService.equipmentNumber(id);	
	}
	
	
	@PostMapping("findByCategoryinfo")
	public List<MdsProductCategoryEntity> findByCategoryinfo(){
		return mdsProductCategoryService.findByCategoryinfo();	
	}
	
	@PostMapping("findByCategory")
	public MdsProductCategoryEntity findByCategory(@RequestBody QueryVo queryVo){
		return mdsProductCategoryService.findByCategory(queryVo);	
	}
	
	@PostMapping("updateAllCategoryInfo")
	public void updateAllCategoryInfo(@RequestBody QueryVo queryVo) {
		mdsProductInfoService.updateCategoryName(queryVo);
		mdsHospitalPriceService.updateCategoryName(queryVo);
	}
}
