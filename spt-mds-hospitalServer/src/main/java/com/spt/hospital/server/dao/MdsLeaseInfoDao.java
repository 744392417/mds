package com.spt.hospital.server.dao;
import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;


public interface MdsLeaseInfoDao extends BaseDao<MdsLeaseInfoEntity> 
{
	@Query("select a from MdsLeaseInfoEntity a where a.openid =?1 order by a.leaseStartDate desc")
	public List<MdsLeaseInfoEntity> findListByUserManager(String openid);

	@Query("select a from MdsLeaseInfoEntity a where  a.productId = ?1 order by a.leaseStartDate desc")
	public List<MdsLeaseInfoEntity> findByCodeAndId(Long productId);

	@Query("select a from MdsLeaseInfoEntity a where a.orderNumber =?1 ")
	public MdsLeaseInfoEntity findByOrderNumber(String orderNumber);
	
	@Query("select a from MdsLeaseInfoEntity a where a.openid =?1 order by a.leaseStartDate desc")
	public List<MdsLeaseInfoEntity> findListByOpenid(String openid);
	
//	@Query("select a from MdsHospitalPriceEntity a where a.id =?1")
//	public MdsHospitalPriceEntity moeys(Long id);
	
	@Query("select a from MdsLeaseInfoEntity a where a.productId =?1  ")
	public MdsLeaseInfoEntity info(Long productId);
	
	@Query("select a from MdsLeaseInfoEntity a where a.productCode =?1  and orderStatus='0' ")
	public List<MdsLeaseInfoEntity> findInuseByProductCode(String productCode);
	
//	@Query("select a from MdsLeaseInfoEntity a where a.productCode =?1  and orderStatus='4' ")
//	public List<MdsLeaseInfoEntity> findByProductCode(String productCode);
	
	@Query("select count(DISTINCT l.categoryId) FROM MdsLeaseInfoEntity l where l.openid =?1")
	public Long categoryNumber(String openid);
	
	@Query("select count(l.orderNumber) FROM MdsLeaseInfoEntity l where l.openid =?1")
	public Long orderNumber(String openid);
	
	@Query("select sum(l.leaseDeposit) FROM MdsLeaseInfoEntity l where l.openid =?1")
	public BigDecimal prepareCost(String openid);
	
	@Query("select sum(l.leaseCost) FROM MdsLeaseInfoEntity l where l.openid =?1")
	public BigDecimal leaseCost(String openid);
	
	
	@Query("select a from MdsLeaseInfoEntity a where  orderStatus in('0','4','5') ")
	public List<MdsLeaseInfoEntity> findByInuse();
	
	@Transactional
	@Modifying
	@Query("update MdsLeaseInfoEntity u set u.orderStatus =?2 where  u.orderNumber =?1")
	public void uporderStatus(String orderNumber,String orderStatus);

}

