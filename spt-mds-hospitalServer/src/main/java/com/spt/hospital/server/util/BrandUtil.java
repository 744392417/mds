//package com.spt.credit.server.util;
//
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.google.common.cache.CacheBuilder;
//import com.google.common.cache.CacheLoader;
//import com.google.common.cache.LoadingCache;
//import com.hsoft.commutil.cache.LocalCacheManager;
//import com.hsoft.commutil.util.SpringContextHolder;
//import com.spt.credit.client.entity.BsBrand;
//
//
//public class BrandUtil {
//	
//	private final static Logger log = LoggerFactory.getLogger(BrandUtil.class);
//
//	private static LoadingCache<String, List<BsBrand>> bsBrandCache;
//	
//	public static void init(){
//		bsBrandCache = CacheBuilder.newBuilder().refreshAfterWrite(10, TimeUnit.MINUTES)
//				.build(new CacheLoader<String,List<BsBrand>>(){
//
//					@Override
//					public List<BsBrand> load(String productCd) throws Exception {
//						//IBsBrandService brandService = SpringContextHolder.getBean(IBsBrandService.class);
//				//		return brandService.findByProductCd(productCd);
//						return null ;
//					}
//					
//				});
//		LocalCacheManager.register(bsBrandCache);
//	}
//	
//	public static void refresh(String productCd){
//		bsBrandCache.refresh(productCd);
//		log.info("refresh {} finished.", productCd);
//	}
//	
//	public static void cleanUp() {
//		if (bsBrandCache != null) {
//			bsBrandCache.cleanUp();
//		}
//	}
//	
//	public static List<BsBrand> findByProductCd(String productCd){
//		List<BsBrand> brandList = null;
//		try {
//			brandList = bsBrandCache.get(productCd);
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}
//		return brandList;
//	}
//}
