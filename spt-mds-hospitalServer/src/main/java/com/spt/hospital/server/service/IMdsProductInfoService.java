package com.spt.hospital.server.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.client.vo.ReceiveProductVo;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;


public interface IMdsProductInfoService extends IBaseService<MdsProductInfoEntity> {

	MdsProductInfoEntity findByCode(QueryVo vo);

	MdsProductInfoEntity findByQrcode(String productQrcode);

	List<MdsProductInfoEntity> findByCategoryId(MdsProductInfoVo vo);

	List<MdsProductInfoEntity> findByHospitalId(MdsProductInfoVo vo);

	void sendVersionUpgradeNews(ReceiveProductVo receiveProductVo);

	List<MdsProductInfoEntity> findByOnlyCategoryId(MdsProductInfoVo vo);

	MdsProductInfoEntity findByCodeAndId(QueryVo vo);
	
	void batchReceiveProductStatus(QueryVo vo);
	
	Integer equipmentNumber(Long id);

	List<MdsProductInfoEntity> allocatedList(QueryVo queryVo);
	
	void upDatestatus(QueryVo queryVo);

	List<MdsProductInfoEntity> findproductStatusByCategoryId(MdsProductInfoVo vo);

	int findByhospitalId(Long hospitalId);
	
	MdsProductInfoEntity getByHospitalId(QueryVo vo);

	List<MdsProductInfoEntity> findByHospitalId(QueryVo vo);

	List<MdsProductInfoEntity> findByfreeStatus(QueryVo vo);

	List<MdsProductInfoEntity> findProductUseStatusByhospitalId(MdsProductInfoVo vo);

	List<MdsProductInfoEntity> findProductStatusByhospitalId(MdsProductInfoVo vo);

	List<MdsProductInfoEntity> findProductByhospitalId(MdsProductInfoVo vo);

	void updateCategoryName(QueryVo vo);

	void mdsProductStatus(QueryVo vo);
	
}

