package com.spt.hospital.server.service;

import java.util.List;
import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;



public interface IMdsPropertyInfoService extends IBaseService<MdsPropertyInfoEntity>{

	List<MdsPropertyInfoEntity> findBytype(String type);

	List<String> findAllDepart();


	MdsPropertyInfoEntity byHospitalName(MdsPropertyInfoVo vo);

	List<MdsPropertyInfoEntity> findByIsfashionable(QueryVo vo);
	 
}

