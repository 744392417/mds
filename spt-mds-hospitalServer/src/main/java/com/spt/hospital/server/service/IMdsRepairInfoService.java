package com.spt.hospital.server.service;


import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.vo.QueryVo;


public interface IMdsRepairInfoService extends IBaseService<MdsRepairInfoEntity>
{
	MdsRepairInfoEntity findByProductCode(QueryVo vo);

	MdsRepairInfoEntity findByCodeAndOpenId(String productCode, String openid);

	List<MdsRepairInfoEntity> findListByProductCode(String productCode);
	
	List<MdsRepairInfoEntity> findByIdAndOrder(String productCode, String orderNumber);

	List<MdsRepairInfoEntity> findByOrderNumber(String orderNumber);
}

