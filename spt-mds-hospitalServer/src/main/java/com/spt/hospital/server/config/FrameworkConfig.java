package com.spt.hospital.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.hsoft.commutil.CommandExecutor;
import com.hsoft.commutil.dao.TaskMonitorDao;
import com.hsoft.commutil.interceptor.ServiceAop;
import com.hsoft.commutil.interceptor.TaskInterceptor;
import com.hsoft.commutil.redis.RedisDataSource;

@Configuration
public class FrameworkConfig {
//	private Logger logger = LoggerFactory.getLogger(FrameworkConfig.class);

	@Autowired
	private Environment env;
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate template = new RestTemplate();
		SimpleClientHttpRequestFactory factory = (SimpleClientHttpRequestFactory) template.getRequestFactory();
		factory.setConnectTimeout(5000);
		factory.setReadTimeout(30000);
		template.getMessageConverters().add(new FormHttpMessageConverter());
		return template;
	}
	
	@Bean
	public ServiceAop serviceAop() {
		return new ServiceAop();
	}
	@Bean
	public TaskInterceptor taskInterceptor() {
		return new TaskInterceptor();
	}
	@Bean
	public TaskMonitorDao taskMonitorDao() {
		return new TaskMonitorDao();
	}
	
	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
		ThreadPoolTaskExecutor executor =new ThreadPoolTaskExecutor();
		return executor;
	}
	@Bean
	public RedisDataSource redisDataSource(){
		RedisDataSource redisDataSource =new RedisDataSource();
		redisDataSource.setHost(env.getProperty("spring.redis.host"));
		redisDataSource.setPort(Integer.valueOf(env.getProperty("spring.redis.port")));
		return redisDataSource;
	}
	@Bean
	public CommandExecutor commandExecutor() {
		return new CommandExecutor();
	}
	
}
