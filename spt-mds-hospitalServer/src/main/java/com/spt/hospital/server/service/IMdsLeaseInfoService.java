package com.spt.hospital.server.service;

import java.math.BigDecimal;
import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.vo.MdsLeaseInfoVo;
import com.spt.hospital.client.vo.QueryVo;

import net.sf.json.JSONObject;



public interface IMdsLeaseInfoService extends IBaseService<MdsLeaseInfoEntity>
{
	void saveMdsLeaseInfo(MdsLeaseInfoVo vo);

	void returnLeaseInfo(MdsLeaseInfoEntity entity);

	List<MdsLeaseInfoEntity> findListByUserManager(String openid);

	List<MdsLeaseInfoEntity> findByCodeAndId(Long productId);

	MdsLeaseInfoEntity findByOrderNumber(String orderNumber);

	List<MdsLeaseInfoEntity> findListByOpenid(String openid);

	// MdsHospitalPriceEntity moeys(Long id);

	MdsLeaseInfoEntity info(Long productId);

	List<MdsLeaseInfoEntity> findInuseByProductCode(QueryVo queryVo);

	MdsLeaseInfoEntity findOrderNumber(String orderNumber);

	Long categoryNumber(String openid);

	Long orderNumber(String openid);

	BigDecimal prepareCost(String openid);

	BigDecimal leaseCost(String openid);

	List<MdsLeaseInfoEntity> findByInuse();

	JSONObject sendTemplateMessage(MdsLeaseInfoEntity leaseInfo);

	JSONObject sendTemplateGuanbiMessage(MdsLeaseInfoEntity leaseInfo);

	void uporderStatus(QueryVo queryVo);

}

