package com.spt.hospital.server.util;


import com.hsoft.commutil.encrypt.Digests;
import com.hsoft.commutil.encrypt.Encodes;
import com.spt.hospital.client.entity.MdsUserInfoEntity;


/**
 * 对密码进行加密，解密操作
 * */
public class HexSHAUtil 
{
	private static final Integer SALT_SIZE = 8;
	private static final Integer HASH_INTERATIONS = 1024;
	
	/**
	 * 对密码进行加密处理   hexSHA1散列加密
	 * 返回方法有加密后的salt   需要保存或者更新
	 * */
	public static MdsUserInfoEntity entryptPassword(MdsUserInfoEntity entity) 
	{
		
		byte[] salt = Digests.generateSalt(SALT_SIZE);
        byte[] hashPassword = Digests.sha1(entity.getLoginPwd().getBytes(), salt, HASH_INTERATIONS);
        
		entity.setSalt(Encodes.encodeHex(salt));
		entity.setLoginPwd(Encodes.encodeHex(hashPassword));
		return entity;
	}
	/**
	 * 解密判断密码是否正确
	 * 参数：添加时存储的salt,新输入密码,数据库原密码
	 * */
	public static Boolean validatePassword(String salt,String entryPwd,String dbExistingPassword) 
	{
		//传入salt,获取对应的加密编码
        byte[] saltBye =Encodes.decodeHex(salt);
        //解析密码
		byte[] hashPassword = Digests.sha1(entryPwd.getBytes(), saltBye, 1024);
		String breakPwdString = Encodes.encodeHex(hashPassword);
		//判断解析之后的密码是否跟数据库密码一致
		Boolean result=breakPwdString.equals(dbExistingPassword);
		return result;
	}
}
