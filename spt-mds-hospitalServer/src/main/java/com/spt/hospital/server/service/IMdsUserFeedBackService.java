package com.spt.hospital.server.service;

import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsUserFeedBackEntity;
import com.spt.hospital.client.vo.QueryVo;


public interface IMdsUserFeedBackService extends IBaseService<MdsUserFeedBackEntity> {

	List<MdsUserFeedBackEntity> findByOpenidOrdernum(QueryVo vo);

	List<MdsUserFeedBackEntity> findByOrderNum(QueryVo vo);

}

