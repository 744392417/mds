package com.spt.hospital.server.vo;


/**
 * 获取密码传递
 * 
 * */
public class MdsPwdDto {
	
	private String openKey;
	private String returnKey;
	private String keyId;
	
	public String getOpenKey() {
		return openKey;
	}
	public void setOpenKey(String openKey) {
		this.openKey = openKey;
	}
	public String getReturnKey() {
		return returnKey;
	}
	public void setReturnKey(String returnKey) {
		this.returnKey = returnKey;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	} 
	
}
