package com.spt.hospital.server.util;


import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.hsoft.commutil.props.PropertiesUtil;
import com.hsoft.commutil.util.JsonUtil;
import com.spt.hospital.client.vo.ParamVo;
import com.spt.hospital.server.util.HttpClientUtil.ApplicationType;



/**
 * 调用 接口 工具类
 *
 */
public class SptApiCallUtility {

	public static String sendData(String OfferContName, String methodStr, String jsonStr){

		HttpClientUtil httpClientUtil = new HttpClientUtil();
		//String url = PropertiesUtil.getProperty(OfferContName)+methodStr;
		String url = OfferContName+methodStr;
		System.out.println("url....>>>>>>>>>>>>>>>>"+url);
		String str =null;
		try {
			ParamVo vo =new ParamVo();
			vo.setErrorId("0");
			vo.setObjJson(jsonStr);
			StringEntity se = new StringEntity(JsonUtil.obj2Json(vo),"utf-8");
			HttpPost post = httpClientUtil.createPost(url, se, null);
			post.addHeader("Content-Type", ApplicationType.JSON.val());
			str = httpClientUtil.fetchData(post);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;

	}
}
