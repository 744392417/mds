package com.spt.hospital.server.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.service.IMdsRecordsConptionService;

@RestController
@RequestMapping(value = "api/mdsRecord")
public class MdsRecordsConptionApi extends BaseApi<MdsRecordsConptionEntity> {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IMdsRecordsConptionService mdsRecordsConptionService;
	@Autowired
	private IMdsLeaseInfoService mdsLeaseInfoService;
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	@Autowired
	private CommonDao commonDao;
	@Override
	public IBaseService<MdsRecordsConptionEntity> getService()
	{
		return mdsRecordsConptionService;
	}
	
	
	@PostMapping("saveMdsRecords")
	public void saveMdsRecords(@RequestBody MdsRecordsConptionEntity en){
		 try {
		//	 logger.info("saveMdsRecords==getConsumStatus==="+en.getConsumStatus());
		//	 logger.info("saveMdsRecords==getLeaseCost==="+en.getLeaseCost());
			mdsRecordsConptionService.save(en);
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@PostMapping("findByOpenid")
	public Page<MdsRecordsConptionEntity> findByOpenid(@RequestBody QueryVo vo) 
	{
		return mdsRecordsConptionService.findByOpenid(vo);
	}
	
	@PostMapping("findByIOrderNumber")
	public MdsRecordsConptionEntity findByIOrderNumber(@RequestBody QueryVo vo) {
		return mdsRecordsConptionService.findByIOrderNumber(vo);
	}
	
	
	@PostMapping("findRecordsPage")
	public Page<MdsRecordsConptionVo> findRecordsPage(@RequestBody QueryVo queryVo) 
	{
		return mdsRecordsConptionService.findRecordsPage(queryVo);
	}
	@PostMapping("sumAmount")
	public BigDecimal sumAmount(@RequestBody QueryVo queryVo) 
	{
		return mdsRecordsConptionService.sumAmount(queryVo);
	}
	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findPageMdsRecords")
	public Page<MdsRecordsConptionEntity> findPageMdsRecords(@RequestBody QueryVo queryVo) 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		
		sql.append("select  c  from  MdsRecordsConptionEntity c left join  MdsLeaseInfoEntity l on r.orderNumber =l.orderNumber  where  1=1 and r.incomeSatus is null  ");

//		// 设备
//		if (queryVo.getPropertyName() != null && !"".equals(queryVo.getPropertyName())) 
//		{
//			sql.append(" and c.propertyName like :companyName");
//			searchParams.put("propertyName", "%" + queryVo.getPropertyName() + "%");
//		}
		// 医院
		if (queryVo.getPropertyName() != null && !"".equals(queryVo.getPropertyName()))
		{
			sql.append(" and c.propertyName like :companyName");
			searchParams.put("propertyName", "%" + queryVo.getPropertyName() + "%");
		}
		
		// 医院
		if (queryVo.getCategoryId() != null && !"".equals(queryVo.getCategoryId()))
		{
			sql.append(" and l.categoryId =:categoryId");
			searchParams.put("categoryId",queryVo.getCategoryId());
		}
		
		
		// 收支类型
		if (queryVo.getConsumStatus() != null && !"".equals(queryVo.getConsumStatus())) 
		{
			sql.append(" and c.consumStatus =:consumStatus");
			searchParams.put("consumStatus", queryVo.getConsumStatus());
		}
		//开始结束时间
		if (queryVo.getStartDate() != null && queryVo.getEndDate() != null && !"".equals(queryVo.getStartDate())
				&& !"".equals(queryVo.getEndDate())) 
		{
			sql.append("  and  c.payDate >=:payDateStart  and  c.payDate <=:payDateEnd");
			searchParams.put("payDateStart", queryVo.getStartDate());
			searchParams.put("payDateEnd", queryVo.getEndDate());
		}
		sql.append(" order by c.").append(queryVo.getSort() + " " + queryVo.getOrder());

		System.out.println("findPageMdsRecords>>>>" + sql.toString());
		Page<MdsRecordsConptionEntity> page = (Page<MdsRecordsConptionEntity>) commonDao.findPage(sql.toString(),
				searchParams, pageRequest);
		return page;
	}

	@PostMapping("findRecordsPageByUser")
	public Page<MdsRecordsConptionVo> findRecordsPageByUser(@RequestBody QueryVo queryVo)
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);
		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		if(StringUtils.isNotBlank(queryVo.getOpenid()))
		{
			sql.append("select  c  from  MdsRecordsConptionEntity c    where 1=1 and leaseCost>0 and c.incomeSatus is null");
			sql.append(" and c.openid = :openid order by createdDate desc ");
			searchParams.put("openid", queryVo.getOpenid());
		}
		//查询数据
		Page<MdsRecordsConptionEntity> page = (Page<MdsRecordsConptionEntity>) commonDao.findPage(sql.toString(),
				searchParams, pageRequest);
		List<MdsRecordsConptionVo> addToPageVoList = new ArrayList<>();
		if (page != null && page.getContent().size() > 0)
		{
			for (MdsRecordsConptionEntity entity : page.getContent()) 
			{
				MdsRecordsConptionVo addToPageVo = new MdsRecordsConptionVo();
				
				BeanUtils.copyProperties(entity, addToPageVo);
				if(entity.getConsumStatus().equals(HospConstants.CONSUM_STATUS_S)){
					addToPageVo.setConsumStatusValue("返回余款");
				}else if(entity.getConsumStatus().equals(HospConstants.CONSUM_STATUS_I)){
					addToPageVo.setConsumStatusValue("支付预付款");
				}else  if(entity.getConsumStatus().equals(HospConstants.CONSUM_STATUS_P)){
					addToPageVo.setConsumStatusValue("支付租金");
				}else{
					 addToPageVo.setConsumStatusValue("附加退款");
				}
				
				
				
				
				//订单信息
//				MdsLeaseInfoEntity leaseInfoEntity = mdsLeaseInfoService.findByOrderNumber(entity.getOrderNumber());
//				addToPageVo.setLeaseTime(leaseInfoEntity.getLeaseTime());
//				addToPageVo.setLeaseStartDate(leaseInfoEntity.getLeaseStartDate());
//				addToPageVo.setLeaseEndDate(leaseInfoEntity.getLeaseEndDate());
//				addToPageVo.setProductCode(leaseInfoEntity.getProductCode());
//				addToPageVo.setProductName(leaseInfoEntity.getProductName());
//				addToPageVo.setLeaseCost(leaseInfoEntity.getLeaseDeposit());
//				addToPageVo.setLeaseDeposit(leaseInfoEntity.getLeaseDeposit());
//				addToPageVo.setIntoBalance(leaseInfoEntity.getIntoBalance());
//				addToPageVo.setHospitalId(leaseInfoEntity.getHospitalId());
//				addToPageVo.setHospitalName(leaseInfoEntity.getHospitalName());
				
				
				
				
				
//				//收费规则
//				MdsHospitalPriceEntity findPriceEntity = mdsHospitalPriceService.findPrice(leaseInfoEntity.getCategoryId(), leaseInfoEntity.getHospitalId());
//				if(null != findPriceEntity)
//				{
//					addToPageVo.setChargingRules(findPriceEntity.getLeaseUnit()+","+findPriceEntity.getTopBalance());
//				}
				//添加到列表
				addToPageVoList.add(addToPageVo);
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsRecordsConptionVo> pageVo = new PageImpl<>(addToPageVoList, pageRequestn, page.getTotalElements());
		return pageVo;
	}
	

}
