package com.spt.hospital.server.api;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsoft.admin.client.cache.DictUtil;
import com.hsoft.commutil.base.BaseApi;
import com.hsoft.commutil.base.CommonDao;
import com.hsoft.commutil.base.IBaseService;
import com.hsoft.commutil.exception.ApplicationException;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsPropertyInfoEntity;
import com.spt.hospital.client.entity.MdsUserInfoEntity;
import com.spt.hospital.client.vo.MdsProductInfoVo;
import com.spt.hospital.client.vo.MdsPropertyInfoVo;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsProductInfoService;
import com.spt.hospital.server.service.IMdsPropertyInfoService;
import com.spt.hospital.server.service.IMdsUserInfoService;




@RestController
@RequestMapping(value = "api/mdsPropertyInfo")
public class MdsPropertyInfoApi extends BaseApi<MdsPropertyInfoEntity> {
	@Override
	public IBaseService<MdsPropertyInfoEntity> getService() {
		return mdsPropertyInfoService;
	}

	@Autowired
	private IMdsPropertyInfoService mdsPropertyInfoService;
	
	@Autowired
	private IMdsUserInfoService mdsUserInfoService;
	
	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;

	@Autowired
	private CommonDao commonDao;

	@PostMapping("findBytype")
	public List<MdsPropertyInfoEntity> findBytype(@RequestBody MdsPropertyInfoVo vo) 
	{
		return mdsPropertyInfoService.findBytype(vo.getHospitalType());
	}
	
	
	@PostMapping("findByIsfashionable")
	public List<MdsPropertyInfoEntity> findByIsfashionable(@RequestBody QueryVo vo) 
	{
		return mdsPropertyInfoService.findByIsfashionable(vo);
	}
	
	
	
	@PostMapping("findAllDepart")
	public List<String> findAllDepart() 
	{
		return mdsPropertyInfoService.findAllDepart();
	}

	@PostMapping("saveOrUpdateProperty")
	public void saveOrUpdateProperty(@RequestBody MdsPropertyInfoEntity entity) 
	{
		try 
		{
			mdsPropertyInfoService.save(entity);
		} catch (ApplicationException e) 
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@PostMapping("findPropertyListPage")
	public Page<MdsPropertyInfoVo> findPropertyListPage(@RequestBody QueryVo queryVo) 
	{
		Pageable pageRequest = new PageRequest(queryVo.getPage() - 1, queryVo.getRows(), null);

		Map<String, Object> searchParams = new HashMap<>();
		StringBuffer sql = new StringBuffer();
		//页面  医院名称等关键字查询
		sql.append("select  c  from  MdsPropertyInfoEntity as c where 1=1 and status <> '-1' ");
		
		if(StringUtils.isNotBlank(queryVo.getHospitalName()))
		{
			sql.append("and c.hospitalName like  "+" '%"+queryVo.getHospitalName()+"%'  ");
		}
		if (StringUtils.isNotBlank(queryVo.getStatus()))
		{
			sql.append(" and c.status = :status");
			searchParams.put("status", queryVo.getStatus());
		}
		// 类型 :医院H 物业 P 代理商 A 平台M
		if (StringUtils.isNotBlank(queryVo.getType())) 
		{
			sql.append(" and c.hospitalType = :hospitalType");
			searchParams.put("hospitalType", queryVo.getType());
		}
		sql.append(" order by c.updatedDate ");
		
		System.out.println("findPropertyListPage>>>>" + sql.toString());

		Page<MdsPropertyInfoEntity> page = (Page<MdsPropertyInfoEntity>) commonDao.findPage(sql.toString(),
				searchParams, pageRequest);

		List<MdsPropertyInfoVo> listVo = new ArrayList<>();

		if (page != null && page.getContent().size() > 0)
		{
			for (MdsPropertyInfoEntity proEntity : page.getContent()) 
			{
				MdsPropertyInfoVo propertyVo = new MdsPropertyInfoVo();
				BeanUtils.copyProperties(proEntity, propertyVo);
				Integer platDistrib=0;
				// 返佣类型 医院H,医院+物业HP,医院+代理HA,自营ALL
//				String ctScoreString = DictUtil.getValue(HospConstants.DISTRIBUTION_TYPE, proEntity.getDistributionType());
//				propertyVo.setDistributionName(ctScoreString);
				reverseDistributionType(propertyVo);
				System.out.println("==getDistributionType=="+propertyVo.getDistributionType());
				System.out.println("getHospitalName=="+propertyVo.getHospitalName());
				System.out.println("getDistributionType=="+propertyVo.getDistributionType());
				//计算返佣比例
				if (HospConstants.DISTRI_TYPE_H.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getHospitalDistribution();
				} else if (HospConstants.DISTRI_TYPE_P.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getProperDistribution();
				} else if (HospConstants.DISTRI_TYPE_A.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getAgentDistribution();
				} else if (HospConstants.DISTRI_TYPE_HP.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getProperDistribution();
				} else if (HospConstants.DISTRI_TYPE_HA.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getAgentDistribution();
				}else if (HospConstants.DISTRI_TYPE_HAP.equals(proEntity.getDistributionType())){
					platDistrib = 100 - proEntity.getHospitalDistribution() - proEntity.getProperDistribution()
							- proEntity.getAgentDistribution();
				} else{
					platDistrib = 100;
				}
				System.out.println("platDistrib==="+platDistrib);
				// 设置平台收入
				propertyVo.setPlatDistrib(platDistrib);
				propertyVo.setPlatDistribution(platDistrib); 
				// 管理员账号密码
				List<MdsUserInfoEntity> userInfoList = mdsUserInfoService.findByHospital(propertyVo.getId());
				propertyVo.setUserInfoList(userInfoList);
				// 设备 类别
				//List<MdsHospitalPriceEntity> ls = mdsHospitalPriceService.findListByHospitalId(propertyVo.getId());
				List<MdsProductInfoVo> hpVols = new ArrayList<>(); 
				
				List<MdsHospitalPriceEntity> hospitalPricels = mdsHospitalPriceService.findListByHospitalId(propertyVo.getId());
				System.out.println("hospitalPricels=="+hospitalPricels.size());
				if(hospitalPricels!=null && hospitalPricels.size()>0){
					for(MdsHospitalPriceEntity hospitalPriceEntity:hospitalPricels){
						
		
						MdsProductInfoVo queryTVo = new MdsProductInfoVo();
					System.out.println("propertyVo.getId()==="+propertyVo.getId());
					System.out.println("propertyVo.hospitalType()==="+propertyVo.getHospitalType());
					queryTVo.setHospitalId(propertyVo.getId());
					queryTVo.setCategoryId(hospitalPriceEntity.getCategoryId());
					//List<MdsProductInfoEntity> productInfols = mdsProductInfoService.findProductByhospitalId(queryTVo);
					//System.out.println("propertyVo.productInfols()==="+productInfols.size());
					
					
//					for (MdsProductInfoEntity productInfoEntity : productInfols) 
//					{
						MdsProductInfoVo vo = new MdsProductInfoVo();
						vo.setCategoryName(hospitalPriceEntity.getCategoryName());
						vo.setCategoryId(hospitalPriceEntity.getCategoryId());
						vo.setHospitalId(propertyVo.getId());
						//复制类别信息
					//	BeanUtils.copyProperties(productInfoEntity, vo);
						// 1、备选：在设备管理中录入，还没有分配医院的设备
						vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_1);
						vo.setBeixuanNum(this.getProductNumber(vo));
						// 2、空闲：在门店管理中配置好的设备，还没有使用，或使用后已经归还。
						vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
						vo.setKongxuanNum(this.getProductNumber(vo));
						// 3、暂停：只有空闲状态可以进入暂停状态，暂停状态设备不可使用。
						vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_3);
						vo.setZhantingNum(this.getProductNumber(vo));
						// 4、使用中：正在使用中
						vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_4);
						vo.setShiyongNum(this.getProductNumber(vo));
						
						vo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_5);
						vo.setWeixiuzhong(this.getProductNumber(vo));
				
						//10.默认
						vo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
						vo.setMoren(this.getProductStatuNumber(vo));
						// 6、异常：如果设备处于空闲状态超过2周（暂定），设备属性为异常。
						vo.setProductStatus(HospConstants.PRODUCT_STATUS_2);
						vo.setYichangNum(this.getProductStatuNumber(vo));
						// 7、低电量：设备通信次数*0.002（通信500次）+待机天数*0.002（待机500天）=1就为低电量状态
						vo.setProductStatus(HospConstants.PRODUCT_STATUS_3);
						vo.setDidianliangNum(this.getProductStatuNumber(vo));
						// 8、逾期：客户预计收入/押金>80%，状态就是逾期，予以标记。
						vo.setProductStatus(HospConstants.PRODUCT_STATUS_4);
						vo.setYuqiNum(this.getProductStatuNumber(vo));
						// 5、报修：客户通过系统进行报修，平台确认后，进入报修状态，用能使用
						vo.setProductStatus(HospConstants.PRODUCT_STATUS_5);
						vo.setBaoxiuNum(this.getProductStatuNumber(vo));

	                   
						List<MdsProductInfoEntity> mdspls = mdsProductInfoService.findProductByhospitalId(vo);
						if (mdspls != null && mdspls.size() > 0) 
						{
							vo.setAllNum(mdspls.size());
						}
						// 预付款
			            vo.setLeaseDeposit(vo.getLeaseDeposit()); 
			            //收费规则
			     		//vo.setChargingRules(vo.getLeaseUnit()+","+vo.getTopBalance());
			     		//vo.setChargingRules("<= "+vo.getMinHour()+" 小时 "+vo.getLeaseUnit().intValue()+" 元/单,超出后 "+vo.getTopBalance().intValue()+" 元/小时 ");
			     		if (null == hospitalPriceEntity.getMinHour() || 0 == hospitalPriceEntity.getMinHour()||
			     				hospitalPriceEntity.getLeaseUnit().compareTo(BigDecimal.ZERO)==-1 || 
			     			hospitalPriceEntity.getTopBalance().compareTo(BigDecimal.ZERO)==-1) {
			     			vo.setChargingRules("");
						}else{
							vo.setChargingRules(hospitalPriceEntity.getTopBalance().stripTrailingZeros().toPlainString()+"("+hospitalPriceEntity.getMinHour()+")+"+hospitalPriceEntity.getLeaseUnit().stripTrailingZeros().toPlainString()+"n");
						}
			     		//递减价格(元/天)
//			     		vo.setDecreasPrice(vo.getDecreasPrice());
//			     		//最低(元/天)
//			     		vo.setMinimumPrice(vo.getMinimumPrice());
						hpVols.add(vo);
					//}
				}
					propertyVo.setMdsProductList(hpVols);
					
				}

				listVo.add(propertyVo);
				// 设备信息
			}
		}
		PageRequest pageRequestn = new PageRequest(queryVo.getPage() - 1, queryVo.getRows());
		Page<MdsPropertyInfoVo> pageVo = new PageImpl<>(listVo, pageRequestn, page.getTotalElements());
		//转换返佣类型
		//pageVo = this.reverseDistributionType(pageVo);
		return pageVo;
	}
	/**
	 * TODO 私有方法：转换返佣类型
	 * */
	public MdsPropertyInfoVo reverseDistributionType(MdsPropertyInfoVo mdsPropertyInfoVo)
	{
           System.out.println(mdsPropertyInfoVo.getDistributionType());
           System.out.println(HospConstants.DISTRI_TYPE_H.equals(mdsPropertyInfoVo.getDistributionType()));
			if(HospConstants.DISTRI_TYPE_H.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("医院");
			}else	if(HospConstants.DISTRI_TYPE_P.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("物业");
			}else	if(HospConstants.DISTRI_TYPE_A.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("代理商");
			}else	if(HospConstants.DISTRI_TYPE_HP.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("医院+物业");
			}else	if(HospConstants.DISTRI_TYPE_HA.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("医院+代理商");
			}else	if(HospConstants.DISTRI_TYPE_HAP.equals(mdsPropertyInfoVo.getDistributionType()))
			{
				mdsPropertyInfoVo.setDistributionName("医院+物业+代理商");
			}else
			{
				mdsPropertyInfoVo.setDistributionName("平台自营");
			}

		return mdsPropertyInfoVo;
	}
	/**
	 * 
	 * */
	public Integer getProductNumber(MdsProductInfoVo vo) {

		List<MdsProductInfoEntity> ls = mdsProductInfoService.findProductUseStatusByhospitalId(vo);
		if (ls != null && ls.size() > 0) {
			return ls.size();
		} else {
			return 0;
		}

	}
	
	/**
	 * TODO 获取设备兼容状态数量
	 * */
	public Integer getProductStatuNumber(MdsProductInfoVo vo) 
	{
		List<MdsProductInfoEntity> ls = mdsProductInfoService.findProductStatusByhospitalId(vo);
		if (ls != null && ls.size() > 0) 
		{
			return ls.size();
		} else 
		{
			return 0;
		}
	}
	
	
	@PostMapping("byHospitalName")
	public MdsPropertyInfoEntity byHospitalName(@RequestBody MdsPropertyInfoVo vo){
		return mdsPropertyInfoService.byHospitalName(vo);
	}
	
	
}
