package com.spt.hospital.server.dao;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.vo.product.ProductInfoByCategoryVo;


public interface MdsProductInfoDao extends BaseDao<MdsProductInfoEntity> {

	@Query("select a from MdsProductInfoEntity a where a.productCode =?1 ")
	public MdsProductInfoEntity findByCode(String productCode);

	@Query("select a from MdsProductInfoEntity a where a.productQrcode =?1 and productUseStatus<>'-1'")
	public MdsProductInfoEntity findByQrcode(String productQrcode);

	@Query("select a from MdsProductInfoEntity a where a.categoryId =?1 and productUseStatus=?2  and productUseStatus<>'-1'")
	public List<MdsProductInfoEntity> findByCategoryId(Long categoryId, String productUseStatus);
	
	@Query("select a from MdsProductInfoEntity a where a.categoryId =?1 and productStatus=?2  and productStatus<>'-1'")
	public List<MdsProductInfoEntity> findproductStatusByCategoryId(Long categoryId, String productStatus);
	
	@Query("select a from MdsProductInfoEntity a where a.categoryId =?1 and productUseStatus<>'-1' ")
	public List<MdsProductInfoEntity> findByOnlyCategoryId(Long categoryId);
	
	@Query("select a from MdsProductInfoEntity a where a.categoryId =?1 and hospitalId=?2")
	public List<MdsProductInfoEntity> findByHospitalId(Long categoryId,Long hospitalId);
	
	@Query("select a from MdsProductInfoEntity a where a.productCode =?1 and a.id = ?2")
	public MdsProductInfoEntity findByCodeAndId(String productCode,Long id);
	
	/**
	 * 批量启用/暂停
	 */
	@Transactional
	@Modifying
	@Query("update MdsProductInfoEntity u set u.productUseStatus = ?2  where u.id=?1")
	public void mdsProductInfoEntityStatus(Long productId,String product_use_status);
	
	@Query("SELECT COUNT(1) FROM MdsProductInfoEntity a WHERE a.categoryId = ?1 and a.productUseStatus <> '-1' ")
	public Integer equipmentNumber(Long id);
	
	
	@Query("SELECT a FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1 and a.productUseStatus in('2','3','4','5') and a.productUseStatus <> '-1' ")
	public List<MdsProductInfoEntity> allocatedList(Long hospitalId);
	
	@Transactional
	@Modifying
	@Query("update MdsProductInfoEntity u set u.productUseStatus = ?2,u.hospitalName=?1,u.hospitalId=?4,u.hospitalDepart=?5,u.hospitalWard=?6 where u.productQrcode=?3")
	public void upDatestatus(String hospitalName,String productUseStatus,String productQrcode,Long hospitalId,String hospitalDepart,String hospitalWard);

	@Query("SELECT a FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1 and a.id =?2")
	public MdsProductInfoEntity getByHospitalId(Long hospitalId,Long id);
	
	@Query("SELECT a FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1 ")
	public List<MdsProductInfoEntity> findByHospitalId(Long hospitalId);
	
//	@Query("SELECT a FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1 and productUseStatus =?2")
//	public List<MdsProductInfoEntity> findProductUseStatusByHospitalId(Long hospitalId,String productUseStatus);

	
	@Query("SELECT COUNT(1) FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1 and a.productUseStatus <>'-1' ")
	public int findByhospitalId(Long hospitalId);
	
	
	@Query("SELECT a FROM MdsProductInfoEntity a WHERE  a.productUseStatus in('2')")
	public List<MdsProductInfoEntity> findByfreeStatus(String productUseStatus);
	
	
	@Query("SELECT  a  FROM MdsProductInfoEntity a WHERE a.hospitalId = ?1  and a.categoryId =?2 and a.productUseStatus <>'-1' ")
	public List<MdsProductInfoEntity> findProductByhospitalId(Long hospitalId,Long categoryId);
	
	
	@Query("select a from MdsProductInfoEntity a where a.hospitalId =?1 and a.categoryId =?2 and productUseStatus=?3  and productUseStatus<>'-1'")
	public List<MdsProductInfoEntity> findProductUseStatusByhospitalId(Long hospitalId,Long categoryId,String productUseStatus);
	
	@Query("select a from MdsProductInfoEntity a where a.hospitalId =?1  and a.categoryId =?2 and productStatus=?3  and productStatus<>'-1'")
	public List<MdsProductInfoEntity> findProductStatusByhospitalId(Long hospitalId,Long categoryId,String productStatus);
	
	
	/**
	 * 
	 */
	@Transactional
	@Modifying
	@Query("update MdsProductInfoEntity u set u.fileId = ?2, u.categoryName = ?3 where u.categoryId=?1")
	public void updateCategoryName(Long categoryId,String fileId,String categoryName);

	@Transactional
	@Modifying
	@Query("update MdsProductInfoEntity u set u.productStatus = ?2  where u.id=?1")
	public void mdsProductStatus(Long productId,String productStatus);
}

