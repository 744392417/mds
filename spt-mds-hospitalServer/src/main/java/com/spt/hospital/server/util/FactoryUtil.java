/*package com.spt.credit.server.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hsoft.commutil.cache.LocalCacheManager;
import com.hsoft.commutil.util.SpringContextHolder;
import com.spt.credit.server.service.IBsFactoryService;
import com.spt.offer.client.entity.BsFactory;

public class FactoryUtil {
	
	private final static Logger log = LoggerFactory.getLogger(BrandUtil.class);

	private static LoadingCache<String, List<BsFactory>> bsFactoryCache;
	
	private static String factoryId = "factoryId";
	
	public static void init(){
		bsFactoryCache = CacheBuilder.newBuilder().refreshAfterWrite(10, TimeUnit.MINUTES)
				.build(new CacheLoader<String,List<BsFactory>>(){

					@Override
					public List<BsFactory> load(String factoryIdStr) throws Exception {
						//IBsFactoryService factoryService = SpringContextHolder.getBean(IBsFactoryService.class);
						return factoryService.findAll();
						return 
					}
					
				});
		LocalCacheManager.register(bsFactoryCache);
	}
	
	public static void refresh(String productCd){
		bsFactoryCache.refresh(productCd);
		log.info("refresh {} finished.", productCd);
	}
	
	public static void cleanUp() {
		if (bsFactoryCache != null) {
			bsFactoryCache.cleanUp();
		}
	}
	
	public static List<BsFactory> findByFactoryIdStr(String factoryIdStr){
		List<BsFactory> factoryList = new ArrayList<BsFactory>();
		try {
			List<BsFactory> list = bsFactoryCache.get(factoryId);
			String[] strArray = factoryIdStr.split(",");
			for (String str : strArray) {
				Long factoryId = Long.valueOf(str);
				for (BsFactory factory : list) {
					if(factory.getId() == factoryId){
						factoryList.add(factory);
					}
				}
			}
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return factoryList;
	}

}
*/