package com.spt.hospital.server.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.vo.MdsRecordsConptionVo;
import com.spt.hospital.client.vo.QueryVo;


public interface IMdsRecordsConptionService extends IBaseService<MdsRecordsConptionEntity>{
	
	Page<MdsRecordsConptionEntity>  findByOpenid(QueryVo vo);

	Page<MdsRecordsConptionVo> findRecordsPage(QueryVo queryVo);

	BigDecimal sumAmount(QueryVo queryVo);

	void saveRecords(MdsRecordsConptionEntity record);

	void upRecordsConptionEntity(String orderNumber);

	List<MdsRecordsConptionEntity> findByFOrderNumber(String orderNumber);

	MdsRecordsConptionEntity findByIOrderNumber(QueryVo queryVo);
}

