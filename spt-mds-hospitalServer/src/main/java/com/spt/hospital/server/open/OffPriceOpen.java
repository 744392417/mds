/*package com.spt.credit.server.open;
package com.spt.offer.server.open;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hsoft.commutil.web.PageSearchVo;
import com.spt.offer.client.entity.OffPrice;
import com.spt.offer.client.vo.ComparePriceVo;
import com.spt.offer.client.vo.DeleteOfferParamVo;
import com.spt.offer.client.vo.OfferPriceRequestVo;
import com.spt.offer.client.vo.OffPriceRequestParamVo;
import com.spt.offer.client.vo.ToffpriceVo;
import com.spt.offer.server.api.OffPriceApi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "报价", description = "报价")
@RestController
@RequestMapping(value = "open/off/offPrice")
public class OffPriceOpen {

	@Autowired
	private OffPriceApi offPriceApi;
	
	@ApiOperation(value = "获得智能比价列表")
	@PostMapping("findCompareList")
	public Page<ToffpriceVo> findCompareList(@RequestBody ComparePriceVo paramVo){
		return offPriceApi.findCompareList(paramVo);
		
	}
	
	@ApiOperation(value = "保存我的报价")
	@PostMapping("saveOfferPrice")
	public void saveOfferPrice(@RequestBody OfferPriceRequestVo requestVo){
		offPriceApi.saveOfferPrice(requestVo);
	}
	
	@ApiOperation(value = "获得我的报价列表")
	@PostMapping("findPageVo")
	public Page<ToffpriceVo> findPageVo(@RequestBody PageSearchVo queryVo){
		return offPriceApi.findPageVo(queryVo);		
	}
	
	
	@ApiOperation(value = "标识为常用标价")
	@PostMapping("updateCommonPrice")
	public void updateCommonPrice(@RequestBody DeleteOfferParamVo param){
		offPriceApi.updateCommonPrice(param);
	}
	

	@ApiOperation(value = "发布报价/ 撤销报价")
	@PostMapping("releaseOfferPrice")
	public void releaseOfferPrice(@RequestBody OffPriceRequestParamVo param){
		String sign=param.getSign();
		if(null!=sign){
			if("release".equals(sign)){			//发布报价
				offPriceApi.releaseOfferPrice(param);
			}else if("cancel".equals(sign)){	//撤销报价
				//offPriceApi.revokeOfferPrice(param);
			}		
		}
	}
		
	@ApiOperation(value = "设置智能比价中获取对手方的报价")
	public void setSptOffer(ToffpriceVo vo,OffPrice entity){
		offPriceApi.setSptOffer(vo, entity);
	}
}
*/