package com.spt.hospital.server.service;

import java.util.List;

import com.hsoft.commutil.base.IBaseService;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.vo.QueryVo;


public interface IMdsHospitalPriceService extends IBaseService<MdsHospitalPriceEntity> 
{
	MdsHospitalPriceEntity findPrice(Long categoryId,Long hospitalId);

	List<MdsHospitalPriceEntity> findListByHospitalId(Long hospitalId);

	void updateCategoryName(QueryVo queryVo);
}

