package com.spt.hospital.server.task;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hsoft.commutil.redis.RedisUtil;
import com.spt.hospital.client.api.IMdsLeaseInfoApi;
import com.spt.hospital.client.api.IMdsProductInfoApi;
import com.spt.hospital.client.api.IMdsRecordsConptionApi;
import com.spt.hospital.client.constant.HospConstants;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;
import com.spt.hospital.client.entity.MdsLeaseInfoEntity;
import com.spt.hospital.client.entity.MdsProductInfoEntity;
import com.spt.hospital.client.entity.MdsRecordsConptionEntity;
import com.spt.hospital.client.util.DateParser;
import com.spt.hospital.client.util.PriceUtils;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.service.IMdsHospitalPriceService;
import com.spt.hospital.server.service.IMdsLeaseInfoService;
import com.spt.hospital.server.service.IMdsProductInfoService;

/**
 * 定时任务
 * */
@Component
public class LackbalanceTask{
	
	private Logger logger = LoggerFactory.getLogger(LackbalanceTask.class);

	@Autowired
	private IMdsLeaseInfoService mdsLeaseInfoService;
	
	@Autowired
	private IMdsHospitalPriceService mdsHospitalPriceService;
	
	@Autowired
	private IMdsLeaseInfoApi mdsLeaseInfoApi;
	
	@Autowired
	private IMdsRecordsConptionApi mdsRecordsConptionApi;

	@Autowired
	private IMdsProductInfoApi mdsProductInfoApi;
	
	@Autowired
	private IMdsProductInfoService mdsProductInfoService;
	
	

	@Scheduled(fixedRate = 1000*60*1)  
	public void syncOnlineTradingData() {
		logger.info("Task========余额不足======");
		BigDecimal yb= new BigDecimal("100");
		BigDecimal bs= new BigDecimal("80");
		List<MdsLeaseInfoEntity> leaseInfols =mdsLeaseInfoService.findByInuse();
		logger.info("Task========余额不足==11===="+leaseInfols.size());
		if(leaseInfols!=null && leaseInfols.size()>0){
			
	
		for (MdsLeaseInfoEntity leaseInfo : leaseInfols) {
			logger.info("Task===hospitalPrice.getOrderNumber()"+leaseInfo.getOrderNumber());
			logger.info("Task===hospitalPrice.getCategoryId()"+(leaseInfo.getCategoryId()));
			logger.info("Task===hospitalPrice.getHospitalId()"+leaseInfo.getHospitalId());

			MdsHospitalPriceEntity hospitalPrice = mdsHospitalPriceService.findPrice(leaseInfo.getCategoryId(),leaseInfo.getHospitalId());
		
			BigDecimal expectedLeaseCost = BigDecimal.ZERO;
			logger.info("Task===hospitalPrice.getLeaseUnit()"+(hospitalPrice!=null));
			logger.info("Task===hospitalPrice.getLeaseUnit()"+hospitalPrice.getLeaseUnit());
			logger.info("Task===hospitalPrice.getTopBalance()"+hospitalPrice.getTopBalance());
			logger.info("Task===hospitalPrice.getMinHour()"+hospitalPrice.getMinHour());
			logger.info("Task====getLeaseDeposit===="+leaseInfo.getLeaseDeposit());
			 expectedLeaseCost = PriceUtils.calculatePriceNew(hospitalPrice.getLeaseUnit(),
					hospitalPrice.getTopBalance(), hospitalPrice.getMinHour().intValue(),
					leaseInfo.getLeaseStartDate(), new Date(),hospitalPrice.getOutHour());
			 logger.info("Task====getLeaseDeposit===="+expectedLeaseCost);
			//BigDecimal expectedLeaseCostB = new BigDecimal(expectedLeaseCost);//目前使用成本
			BigDecimal expectedLeaseCostC = new BigDecimal(0.00);//使用占比
			expectedLeaseCostC = (expectedLeaseCost.divide(leaseInfo.getLeaseDeposit(),2,RoundingMode.HALF_UP)).multiply(yb);
			logger.info("Task=====目前使用成本===="+expectedLeaseCost);
			logger.info("Task====警戒线="+bs);
			logger.info("Task===是否超过==="+(expectedLeaseCostC.compareTo(bs)==1));
			
			if(expectedLeaseCostC.compareTo(bs)==1){
				logger.info("=====RedisUtil=="+"balance"+leaseInfo.getOrderNumber());
				String balanceOrder = RedisUtil.get("balance"+leaseInfo.getOrderNumber());
				logger.info("=====balanceOrder=RedisUtil=====");
				if(StringUtils.isBlank(balanceOrder)){
					logger.info("=====balanceOrder==");
					leaseInfo.setLeaseCost(expectedLeaseCost);
					QueryVo queryVo=new QueryVo();
					queryVo.setOrderNumber(leaseInfo.getOrderNumber());
					queryVo.setOrderStatus(HospConstants.ORDER_STATUS_5);
					mdsLeaseInfoService.uporderStatus(queryVo);
					queryVo.setProductId(leaseInfo.getProductId());
					queryVo.setProductStatus(HospConstants.PRODUCT_STATUS_4);
					mdsProductInfoService.mdsProductStatus(queryVo);
					mdsLeaseInfoService.sendTemplateMessage(leaseInfo);
					logger.info("=====balanceOrder=RedisUtil==set===");
					RedisUtil.setex("balance"+leaseInfo.getOrderNumber(),HospConstants.BALANCE_EXPIRE_SECONDS,leaseInfo.getOrderNumber());
					logger.info("=====balanceOrder=RedisUtil=11111=set===");
				}
	
			}
			logger.info("Task==balanceOrder===目前使用成本==expectedLeaseCost=="+expectedLeaseCost);
			logger.info("Task==balanceOrder===目前使用成本==getLeaseDeposit=="+leaseInfo.getLeaseDeposit());
			logger.info("=====balanceOrder=是否关闭订单=====" + ((expectedLeaseCost.compareTo(leaseInfo.getLeaseDeposit())==1)));
			if(expectedLeaseCost.compareTo(leaseInfo.getLeaseDeposit())==1){
				
				MdsLeaseInfoEntity leaseEntity  = mdsLeaseInfoService.getEntity(leaseInfo.getId());
				leaseEntity.setOrderStatus(HospConstants.ORDER_STATUS_1);
				leaseEntity.setLeaseEndDate(new Date());
				leaseEntity.setLeaseTime(DateParser.getDateHour(leaseEntity.getLeaseEndDate(), leaseEntity.getLeaseStartDate()));
				leaseEntity.setLeaseCost(leaseEntity.getLeaseDeposit());
				mdsLeaseInfoApi.returnLeaseInfo(leaseEntity);
				MdsRecordsConptionEntity recordsConption = new MdsRecordsConptionEntity();
				
				MdsProductInfoEntity productInfo = mdsProductInfoApi.getEntity(leaseEntity.getProductId());
				productInfo.setProductUseStatus(HospConstants.PRODUCT_USE_STATUS_2);
				productInfo.setProductStatus(HospConstants.PRODUCT_STATUS_1);
				mdsProductInfoApi.save(productInfo);
				
				//支付租金
				MdsRecordsConptionEntity entity = new MdsRecordsConptionEntity();
				
				logger.info("mdsRecords 支付租金:========");
				entity.setOrderNumber(leaseEntity.getOrderNumber());
				entity.setOpenid(leaseEntity.getOpenid());
				logger.info("mdsRecords 支付租金 :" + leaseEntity.getLeaseCost());
				entity.setLeaseCost(leaseEntity.getLeaseCost());//支付租金
				entity.setConsumStatus(HospConstants.CONSUM_STATUS_P);
				entity.setPaySystem("WX");
				entity.setHospitalId(leaseEntity.getHospitalId());
				entity.setHospitalName(leaseEntity.getHospitalName());
				entity.setPlatPayDate(new Date());
				entity.setPlatPayNumber("p"+DateParser.formatDateSS(new Date()));
				entity.setWxPayDate(new Date());
				entity.setUpdatedDate(new Date());
				entity.setOrderDescribe("支付租金");
				logger.info("mdsRecords 支付租金:=====save===");
				
				mdsRecordsConptionApi.save(recordsConption);
				logger.info("发送关闭消息=="+leaseEntity.getOrderNumber());
				mdsLeaseInfoService.sendTemplateGuanbiMessage(leaseEntity);
				

				
			}
		}
	}
	}
	
//	public static void main(String[] args) {
//		BigDecimal expectedLeaseCostA = new BigDecimal("1.20");
//		BigDecimal expectedLeaseCostB = new BigDecimal("1.00");
//		BigDecimal yb= new BigDecimal("100");
//		System.out.println(expectedLeaseCostA.compareTo(expectedLeaseCostB)==1);
//		
//		BigDecimal	expectedLeaseCostC = (expectedLeaseCostB.divide(expectedLeaseCostA,2,RoundingMode.HALF_UP)).multiply(yb);
//		System.out.println(""+expectedLeaseCostC);
//		
//		
//		
//
//	  
//	}
}
