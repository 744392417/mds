package com.spt.hospital.server.service.impl;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hsoft.commutil.base.BaseDao;
import com.hsoft.commutil.base.BaseService;
import com.hsoft.push.client.remote.PushRemote;
import com.spt.hospital.client.entity.MdsRepairInfoEntity;
import com.spt.hospital.client.vo.QueryVo;
import com.spt.hospital.server.dao.MdsRepairInfoDao;
import com.spt.hospital.server.service.IMdsRepairInfoService;



@Component
@Transactional(readOnly = false)
public class MdsRepairInfoServiceImpl extends BaseService<MdsRepairInfoEntity> implements IMdsRepairInfoService {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(MdsRepairInfoServiceImpl.class);

	@Autowired
	private MdsRepairInfoDao mdRepairInfoDao;
	@SuppressWarnings("unused")
	@Autowired
	private PushRemote pushRemote;
	
	@Override
	public BaseDao<MdsRepairInfoEntity> getBaseDao() {
		return mdRepairInfoDao;
	}
	
	@Override
	public MdsRepairInfoEntity findByProductCode(QueryVo vo) 
	{
		return mdRepairInfoDao.findByProductCode(vo.getProductCode());
	}

	@Override
	public MdsRepairInfoEntity findByCodeAndOpenId(String productCode, String openid) {
		return mdRepairInfoDao.findByCodeAndOpenId(productCode,openid);
	}

	@Override
	public List<MdsRepairInfoEntity> findListByProductCode(String productCode) 
	{
		return mdRepairInfoDao.findListByProductCode(productCode);
	}

	@Override
	public List<MdsRepairInfoEntity> findByIdAndOrder(String productCode, String orderNumber) {

		return mdRepairInfoDao.findByIdAndOrder(productCode, orderNumber);
	}
	
	@Override
	public List<MdsRepairInfoEntity> findByOrderNumber(String orderNumber) {

		return mdRepairInfoDao.findByOrderNumber(orderNumber);
	}
	
	
}
