package com.spt.hospital.server.dao;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsHospitalPriceEntity;


public interface MdsHospitalPriceDao extends BaseDao<MdsHospitalPriceEntity> {

	@Query("select a from MdsHospitalPriceEntity a where a.categoryId =?1 and a.hospitalId =?2 ")
	public MdsHospitalPriceEntity findPrice(Long categoryId, Long hospitalId);

	@Query("select a from MdsHospitalPriceEntity a where  a.hospitalId =?1 ")
	public List<MdsHospitalPriceEntity> findListByHospitalId(Long hospitalId);
	
	
	/**
	 * 
	 */
	@Transactional
	@Modifying
	@Query("update MdsHospitalPriceEntity u set u.categoryFileId = ?2, u.categoryName = ?3 where u.categoryId=?1")
	public void updateCategoryName(Long categoryId,String categoryFileId,String categoryName);

}

