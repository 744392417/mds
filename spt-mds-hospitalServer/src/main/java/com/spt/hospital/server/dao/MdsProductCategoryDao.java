package com.spt.hospital.server.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hsoft.commutil.base.BaseDao;
import com.spt.hospital.client.entity.MdsProductCategoryEntity;


public interface MdsProductCategoryDao extends BaseDao<MdsProductCategoryEntity> {

	@Query("select a from MdsProductCategoryEntity a where a.categoryName =?1 AND a.status='1'")
	public MdsProductCategoryEntity findByName(String categoryName);

	
	@Query("SELECT a FROM MdsProductCategoryEntity a WHERE a.status = '1'")
	public List<MdsProductCategoryEntity> findByCategoryinfo();
	
	
	@Query("SELECT a FROM MdsProductCategoryEntity a WHERE a.categoryName = ?1")
	MdsProductCategoryEntity findByCategory(String categoryName);
}

