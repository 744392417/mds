//package test.spt.offer.server.service;
//
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.web.context.request.RequestAttributes;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import com.alibaba.fastjson.JSONObject;
//import com.hsoft.commutil.exception.ApplicationException;
//import com.hsoft.commutil.util.HTTPUtility;
//import com.spt.hospital.client.vo.UserAccountVo;
//import com.spt.hospital.server.HospitalServer;
//import com.spt.hospital.server.service.IUcsUserAccountService;
//
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = HospitalServer.class)
//public class UserAccountServiceTest
//{
//	private Logger logger=LoggerFactory.getLogger(UserAccountServiceTest.class);
//	@Autowired
//	private IUcsUserAccountService userService;
//
//	//测试API接口，根据名称获取公司
//	@Test
//	public void testGetCompanyByName()
//	{
//		com.spt.credit.client.vo.UcsCompanyCreditVo vo=new com.spt.credit.client.vo.UcsCompanyCreditVo();
//		vo.setCompanyName("测试企业");
//		//查看是否存在此数据
//		UcsCompanyCreditVo ucsCompanyuVo = ucsCompanyApi.findByCompanyName(vo);
//		System.out.println(ucsCompanyuVo.getCompanyName()+ucsCompanyuVo.getCompanyCode()+"-------------------------------");
//	}
//	
//	//测试saas接口---线上数据
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testGetOnlineData()
//	{
//		List<UcsCompany> findAll = ucsCompanyApi.findAll();
//		List<FindOnlineDataVo> sendVoList=new ArrayList<FindOnlineDataVo>();
//		for (UcsCompany ucsCompany : findAll) {
//			FindOnlineDataVo onlineData=new FindOnlineDataVo();
//			BeanUtils.copyProperties(ucsCompany, onlineData);
//			sendVoList.add(onlineData);
//		}
//		try {
//			String uuidJson = HTTPUtility.doPostBody("http://192.168.2.76:81/open/bank/account/findOnLineData", sendVoList, null);
//			System.out.println(uuidJson+"************************------------------------------------*********************************");
//			//接收查看获取的数据
//			JSONObject jsStr = JSONObject.parseObject(uuidJson);
//			List<Map<String,Object>> receiveCreditScoreList=(List<Map<String,Object>>) jsStr.get("data");
//			System.out.println(receiveCreditScoreList);
//			
////			ReceiveCreditScoreVo recs = JsonUtility.toJavaObject(uuidJson, ReceiveCreditScoreVo.class);
////			System.out.println("==code=====" + recs.getCode() + "==message=====" + recs.getMessage());
//			
//			for (int i = 0; i < receiveCreditScoreList.size(); i++) 
//			{
//				UcsCompanyCreditVo companyCreditVo=new UcsCompanyCreditVo();
//				Map<String, Object> map = receiveCreditScoreList.get(i);
//				ReceiveCreditScoreVo receiveCreditScoreVo = new ReceiveCreditScoreVo();
//				
//				receiveCreditScoreVo.setCompanyCode(map.get("companyCode").toString());
//				receiveCreditScoreVo.setCompanyName(map.get("companyName").toString());
//				receiveCreditScoreVo.setCompanyId(Long.valueOf(map.get("companyId").toString()));
//				
//				receiveCreditScoreVo.setDistanceNowDay(Integer.valueOf(map.get("distanceNowDay").toString()));
//				
//				BigDecimal everyDayLoginFrequencyBig=new BigDecimal(map.get("everyDayLoginFrequency").toString());
//				receiveCreditScoreVo.setEveryDayLoginFrequency(everyDayLoginFrequencyBig);
//				
//				BigDecimal everyDayLoginHoursBig=new BigDecimal(map.get("everyDayLoginHours").toString());
//				receiveCreditScoreVo.setEveryDayLoginFrequency(everyDayLoginHoursBig);
//				receiveCreditScoreVo.setOrderReceivingNumber(Integer.valueOf(map.get("orderReceivingNumber").toString()));
//				
//				BigDecimal tradeNumberCountBig=new BigDecimal(map.get("tradeNumberCount").toString());
//				receiveCreditScoreVo.setEveryDayLoginFrequency(tradeNumberCountBig);
//				
//				BigDecimal tradeMoneyBig=new BigDecimal(map.get("tradeMoney").toString());
//				receiveCreditScoreVo.setEveryDayLoginFrequency(tradeMoneyBig);
//				
//				List<UcsCreditScoreVo> onLineDataList = dealCreditScoreAndSaveData(receiveCreditScoreVo);
//				companyCreditVo.setCreditScoreList(onLineDataList);
//				ucsCreditScoreApi.saveCreditList(companyCreditVo);
//			}
//		} catch (ApplicationException e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	
//	
//	/**
//	 *	私有方法： 分配信用评分保存		线上数据类
//	 * */
//	private List<UcsCreditScoreVo> dealCreditScoreAndSaveData(ReceiveCreditScoreVo receiveCreditScoreVo)
//	{
//		List<UcsCreditScoreVo> list=new ArrayList<UcsCreditScoreVo>();
//		
//		//线上数据 -- 首次登陆距今天数
//		UcsCreditScoreVo voDOA=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDOA);
//		voDOA.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDOA.setType("首次登陆距今天数");//类型
//		voDOA.setDataType("D");//金额A 其他数字 D
//		voDOA.setOnlineType("O");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDOA.setFielidSort(0);//排序
//		voDOA.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getDistanceNowDay())//线上数据 -- 首次登陆距今天数
//		{
//			voDOA.setOnlineData(receiveCreditScoreVo.getDistanceNowDay().toString());
//		}
////		voDOA.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDOA);
//		logger.info("List ：add： 首次登陆距今天数>>>>");
//		
//		//线上数据 -- 日均登陆频率
//		UcsCreditScoreVo voDOB=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDOB);
//		voDOB.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDOB.setType("日均登陆频率");//类型
//		voDOB.setDataType("D");//金额A 其他数字 D
//		voDOB.setOnlineType("O");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDOB.setFielidSort(1);//排序
//		voDOB.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getEveryDayLoginFrequency())//线上数据 -- 日均登陆频率
//		{
//			voDOB.setOnlineData(receiveCreditScoreVo.getEveryDayLoginFrequency().toString());
//		}
////		voDOB.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDOB);
//		logger.info("List ：add： 日均登陆频率>>>>");
//		
//		//线上数据 -- 日均登录时长(小时)
//		UcsCreditScoreVo voDOC=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDOC);
//		voDOC.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDOC.setType("日均登录时长(小时)");//类型
//		voDOC.setDataType("D");//金额A 其他数字 D
//		voDOC.setOnlineType("O");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDOC.setFielidSort(2);//排序
//		voDOC.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getEveryDayLoginHours())//线上数据 --  日均登录时长(小时)
//		{
//			voDOC.setOnlineData(receiveCreditScoreVo.getEveryDayLoginHours().toString());
//		}
////		voDOC.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDOC);
//		logger.info("List ：add： 日均登录时长(小时)>>>>");
//		
//		//线上数据 -- 询价和挂单次数
//		UcsCreditScoreVo voDTA=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTA);
//		voDTA.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTA.setType("询价和挂单次数");//类型
//		voDTA.setDataType("D");//金额A 其他数字 D
//		voDTA.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTA.setFielidSort(3);//排序
//		voDTA.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getInquiryOrSellCount())//线上数据 --  询价和挂单次数
//		{
//			voDTA.setOnlineData(receiveCreditScoreVo.getInquiryOrSellCount().toString());
//		}
////		voDTA.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTA);
//		logger.info("List ：add： 询价和挂单次数>>>>");
//		
//		//线上数据 -- 被应单次数
//		UcsCreditScoreVo voDTB=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTB);
//		voDTB.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTB.setType("被应单次数");//类型
//		voDTB.setDataType("D");//金额A 其他数字 D
//		voDTB.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTB.setFielidSort(4);//排序
//		voDTB.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getOtherInquiryOrSellCount())//线上数据 --  被应单次数
//		{
//			voDTB.setOnlineData(receiveCreditScoreVo.getOtherInquiryOrSellCount().toString());
//		}
////		voDTB.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTB);
//		logger.info("List ：add： 被应单次数>>>>");
//		
//		//线上数据 -- 应单次数
//		UcsCreditScoreVo voDTC=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTC);
//		voDTC.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTC.setType("应单次数");//类型
//		voDTC.setDataType("D");//金额A 其他数字 D
//		voDTC.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTC.setFielidSort(5);//排序
//		voDTC.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getOrderReceivingNumber())//线上数据 --  应单次数
//		{
//			voDTC.setOnlineData(receiveCreditScoreVo.getOrderReceivingNumber().toString());
//		}
////		voDTC.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTC);
//		logger.info("List ：add： 应单次数>>>>");
//		
//		//线上数据 -- 成交量(吨)
//		UcsCreditScoreVo voDTD=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTD);
//		voDTD.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTD.setType("成交量(吨)");//类型
//		voDTD.setDataType("D");//金额A 其他数字 D
//		voDTD.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTD.setFielidSort(6);//排序
//		voDTD.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getTradeNumberCount())//线上数据 --   成交量(吨)
//		{
//			voDTD.setOnlineData(receiveCreditScoreVo.getTradeNumberCount().toString());
//		}
////		voDTD.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTD);
//		logger.info("List ：add： 成交量(吨)>>>>");
//		
//		//线上数据 -- 成交额(万元)
//		UcsCreditScoreVo voDTE=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTE);
//		voDTE.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTE.setType("成交额(万元)");//类型
//		voDTE.setDataType("A");//金额A 其他数字 D
//		voDTE.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTE.setFielidSort(7);//排序
//		voDTE.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getTradeMoney())//线上数据 --  成交额(万元)
//		{
//			voDTE.setOnlineData(receiveCreditScoreVo.getTradeMoney().toString());
//		}
////		voDTE.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTE);
//		logger.info("List ：add： 成交额(万元)>>>>");
//		
//		//线上数据 -- 转售挂单次数
//		UcsCreditScoreVo voDTF=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTF);
//		voDTF.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTF.setType("转售挂单次数");//类型
//		voDTF.setDataType("D");//金额A 其他数字 D
//		voDTF.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTF.setFielidSort(8);//排序
//		voDTF.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getResaleOrderCount())//线上数据 --   转售挂单次数
//		{
//			voDTF.setOnlineData(receiveCreditScoreVo.getResaleOrderCount().toString());
//		}
////		voDTF.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTF);
//		logger.info("List ：add： 转售挂单次数>>>>");
//		
//		//线上数据 -- 转售成交次数
//		UcsCreditScoreVo voDTG=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTG);
//		voDTG.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTG.setType("转售成交次数");//类型
//		voDTG.setDataType("D");//金额A 其他数字 D
//		voDTG.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTG.setFielidSort(9);//排序
//		voDTG.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getResaleDealCount())//线上数据 --  转售成交次数
//		{
//			voDTG.setOnlineData(receiveCreditScoreVo.getResaleDealCount().toString());
//		}
////		voDTG.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTG);
//		logger.info("List ：add： 转售成交次数>>>>");
//		
//		//线上数据 -- 转售成交利润(万元)
//		UcsCreditScoreVo voDTH=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTH);
//		voDTH.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTH.setType("转售成交利润(万元)");//类型
//		voDTH.setDataType("A");//金额A 其他数字 D
//		voDTH.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTH.setFielidSort(10);//排序
//		voDTH.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getResaleDealProfit())//线上数据 --  转售成交利润(万元)
//		{
//			voDTH.setOnlineData(receiveCreditScoreVo.getResaleDealProfit().toString());
//		}
////		voDTH.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTH);
//		logger.info("List ：add： 转售成交利润(万元)>>>>");
//		
//		//线上数据 -- 支付手续费总额(万元)
//		UcsCreditScoreVo voDTI=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTI);
//		voDTI.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTI.setType("支付手续费总额(万元)");//类型
//		voDTI.setDataType("A");//金额A 其他数字 D
//		voDTI.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTI.setFielidSort(11);//排序
//		voDTI.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getPayPoundage())//线上数据 -- 支付手续费总额(万元)
//		{
//			voDTI.setOnlineData(receiveCreditScoreVo.getPayPoundage().toString());
//		}
////		voDTI.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTI);
//		logger.info("List ：add： 支付手续费总额(万元)>>>>");
//		
//		//线上数据 -- 日均冻结沉淀资金(万元)
//		UcsCreditScoreVo voDTJ=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTJ);
//		voDTJ.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTJ.setType("日均冻结沉淀资金(万元)");//类型
//		voDTJ.setDataType("A");//金额A 其他数字 D
//		voDTJ.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTJ.setFielidSort(12);//排序
//		voDTJ.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getEveryDayFreeze())//线上数据 -- 日均冻结沉淀资金(万元)
//		{
//			voDTJ.setOnlineData(receiveCreditScoreVo.getEveryDayFreeze().toString());
//		}
////		voDTJ.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTJ);
//		logger.info("List ：add： 日均冻结沉淀资金(万元)>>>>");
//		
//		//线上数据 -- 日均可用资金沉淀(万元)
//		UcsCreditScoreVo voDTK=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDTK);
//		voDTK.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDTK.setType("日均可用资金沉淀(万元)");//类型
//		voDTK.setDataType("A");//金额A 其他数字 D
//		voDTK.setOnlineType("T");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDTK.setFielidSort(13);//排序
//		voDTK.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getEveryDayAvailableFreeze())//线上数据 -- 日均可用资金沉淀(万元)
//		{
//			voDTK.setOnlineData(receiveCreditScoreVo.getEveryDayAvailableFreeze().toString());
//		}
////		voDTK.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDTK);
//		logger.info("List ：add： 日均可用资金沉淀(万元)>>>>");
//		
//		//线上数据 -- 追保时效(据截止时间小时数)
//		UcsCreditScoreVo voDPA=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDPA);
//		voDPA.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDPA.setType("追保时效(据截止时间小时数)");//类型
//		voDPA.setDataType("D");//金额A 其他数字 D
//		voDPA.setOnlineType("P");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDPA.setFielidSort(14);//排序
//		voDPA.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getChasingInsuranceHours())//线上数据 --  追保时效(据截止时间小时数)
//		{
//			voDPA.setOnlineData(receiveCreditScoreVo.getChasingInsuranceHours().toString());
//		}
////		voDPA.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDPA);
//		logger.info("List ：add： 追保时效(据截止时间小时数)>>>>");
//		
//		//线上数据 -- 交收次数
//		UcsCreditScoreVo voDPB=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDPB);
//		voDPB.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDPB.setType("交收次数");//类型
//		voDPB.setDataType("D");//金额A 其他数字 D
//		voDPB.setOnlineType("P");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDPB.setFielidSort(15);//排序
//		voDPB.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getDeliveryCount())//线上数据 --  交收次数
//		{
//			voDPB.setOnlineData(receiveCreditScoreVo.getDeliveryCount().toString());
//		}
////		voDPB.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDPB);
//		logger.info("List ：add： 交收次数>>>>");
//		
//		//线上数据 -- 往期赊销还款时效(据截止时间小时数)
//		UcsCreditScoreVo voDPC=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDPC);
//		voDPC.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDPC.setType("往期赊销还款时效(据截止时间小时数)");//类型
//		voDPC.setDataType("D");//金额A 其他数字 D
//		voDPC.setOnlineType("P");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDPC.setFielidSort(16);//排序
//		voDPC.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getOriginalSellOnCreditRepayHours())//线上数据 --  往期赊销还款时效(据截止时间小时数)
//		{
//			voDPC.setOnlineData(receiveCreditScoreVo.getOriginalSellOnCreditRepayHours().toString());
//		}
////		voDPC.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDPC);
//		logger.info("List ：add： 往期赊销还款时效(据截止时间小时数)>>>>");
//		
//		//线上数据 -- 往期现金付款时间(据截止时间小时数)
//		UcsCreditScoreVo voDPD=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDPD);
//		voDPD.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDPD.setType("往期现金付款时间(据截止时间小时数)");//类型
//		voDPD.setDataType("D");//金额A 其他数字 D
//		voDPD.setOnlineType("P");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDPD.setFielidSort(17);//排序
//		voDPD.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getOriginalPaymentHours())//线上数据 --  往期现金付款时间(据截止时间小时数)
//		{
//			voDPD.setOnlineData(receiveCreditScoreVo.getOriginalPaymentHours().toString());
//		}
////		voDPD.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDPD);
//		logger.info("List ：add： 往期现金付款时间(据截止时间小时数)>>>>");
//		
//		//线上数据 -- 开票时效(据截止时间小时数)
//		UcsCreditScoreVo voDPE=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDPE);
//		voDPE.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDPE.setType("开票时效(据截止时间小时数)");//类型
//		voDPE.setDataType("D");//金额A 其他数字 D
//		voDPE.setOnlineType("P");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDPE.setFielidSort(18);//排序
//		voDPE.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		if(null != receiveCreditScoreVo.getTicketOpeningHours())//线上数据 --  开票时效(据截止时间小时数)
//		{
//			voDPE.setOnlineData(receiveCreditScoreVo.getTicketOpeningHours().toString());
//		}
////		voDPE.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDPE);
//		logger.info("List ：add： 开票时效(据截止时间小时数)>>>>");
//		
//		//线上数据 -- 追保违约次数、金额
//		UcsCreditScoreVo voDDA=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDDA);
//		voDDA.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDDA.setType("追保违约次数、金额");//类型
//		voDDA.setDataType("D");//金额A 其他数字 D
//		voDDA.setOnlineType("D");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDDA.setFielidSort(19);//排序
//		voDDA.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
////		voDDA.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDDA);
//		logger.info("List ：add： 追保违约次数、金额>>>>");
//		
//		//线上数据 -- 交收违约次数、金额
//		UcsCreditScoreVo voDDB=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDDB);
//		voDDB.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDDB.setType("交收违约次数、金额");//类型
//		voDDB.setDataType("D");//金额A 其他数字 D
//		voDDB.setOnlineType("D");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDDB.setFielidSort(20);//排序
//		voDDB.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
////		voDDB.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDDB);
//		logger.info("List ：add： 交收违约次数、金额>>>>");
//		
//		//线上数据 -- 开票违约次数、金额
//		UcsCreditScoreVo voDDC=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDDC);
//		voDDC.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDDC.setType("开票违约次数、金额");//类型
//		voDDC.setDataType("D");//金额A 其他数字 D
//		voDDC.setOnlineType("D");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDDC.setFielidSort(21);//排序
//		voDDC.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
////		voDDC.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDDC);
//		logger.info("List ：add： 开票违约次数、金额>>>>");
//		
//		//线上数据 -- 付款违约次数、金额
//		UcsCreditScoreVo voDDD=new UcsCreditScoreVo();
//		BeanUtils.copyProperties(receiveCreditScoreVo, voDDD);
//		voDDD.setGradingType(OfferConstants.UCS_GRADING_TYPE_D);//评分类型，D，线上数据类
//		voDDD.setType("付款违约次数、金额");//类型
//		voDDD.setDataType("D");//金额A 其他数字 D
//		voDDD.setOnlineType("D");//操作行为(O) 交易(T) 履约(P) 违约(D)
//		voDDD.setFielidSort(22);//排序
//		voDDD.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
////		voDDD.setFileId(receiveCreditScoreVo.getF_B());//文件ID
////		获取诚信分  --
//		list.add(voDDD);
//		logger.info("List ：add： 付款违约次数、金额>>>>");
//		
//		return list;
//	}
//	
//	
//	
//	//测试用户登录
//	@Test
//	public void testUserLogin()
//	{
//		UserAccountVo loginVo=new UserAccountVo();
//		loginVo.setLoginName("133");
//		loginVo.setLoginPwd("123456");
//		UserAccountVo login = userService.login(loginVo);
//		logger.info(login.getLoginName()+"登录名>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//		logger.info(login.getLoginPwd()+"用户密码>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//	}
//	
////	@Autowired
////	private UcsUserAccountServiceImpl test;
////	测试密码解密
////	@Test
////	public void testValidatePassword()
////	{
////		Boolean validatePassword = test.validatePassword("839d7f2581fa89b5","123456", "2b0e66ed40fde92a17f136dc618f783a2f162c0a");
////		log.info(validatePassword+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
////	}
//	
//	//测试修改密码
//	@Test
//	public void testUpdatePassword()
//	{
//		UserValidateVo entity=new UserValidateVo();
//		entity.setMobile("133");
//		entity.setLoginPwd("123456");
//		userService.updatePassword(entity);
//		logger.info("测试完成！------------------------------");
//	}
//	
//	//测试发送短信验证码
//	@Test
//	public void testSendSms()
//	{
//		UserAccountVo userAccountVo=new UserAccountVo();
//		userAccountVo.setMobile("17887945493");
//		userService.sendSms(userAccountVo);
//		
//		
//		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//		String mobile = (String) requestAttributes.getAttribute("mobile",RequestAttributes.SCOPE_SESSION);
//		System.out.println(mobile+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//	}
//	//测试校验短信验证码
////	@Test
//	public void testValidateSms()
//	{
////		String mobile = (String) request.getSession().getAttribute("mobile");
//		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
////		HttpServletRequest request = requestAttributes.getRequest();
//		//从session里面获取对应的值
//		String mobile = (String) requestAttributes.getAttribute("my_value",RequestAttributes.SCOPE_SESSION);
//		System.out.println(mobile+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//	}
///*	
//   @Autowired
//	private IuserService userService;
//	@Autowired
//	private IBsCompanyService  bsCompanyService;
//*/
////	@Test
////	public void loginBas(){
///*		SptUserLoginVo vo = new SptUserLoginVo();
//		BsUserAccount user = userService.getEntity(228l);
//		vo.setUserName("15512345678");
//		vo.setUserPassWord("123456");
//		vo.setMobile("15512345678");
//		vo.setUserRole(user.getRoleCd());;
//		String jsonStr = JsonUtil.obj2Json(vo);
//		String uuidJson = SptApiCallUtility.sendData(OfferConstants.BAS_GATEWAY_URL_KEY,"/open/user/login", jsonStr);
//		System.out.println("-----------------"+uuidJson);
//		JSONObject jsonObj=  JSONObject.parseObject(uuidJson);				
//		String errorId=jsonObj.getString("errorId");
//		String objJson=jsonObj.getString("objJson");
//		System.out.println("-----------3333------"+objJson);
//		//将返回的数据转为Object对象
//		if(!errorId.equals("0")){
//			throw new RequestException(errorId);
//		if(errorId.equals("0")){
//			String jsonStr1 = jsonObj.getString("objJson");
//			Object obj = jsonObj.parse(jsonStr1);
//			System.out.println((String)obj);
//		}
//		
//		
//		*/
//		
////		 try {
////			 ReceiveCreditScoreVo vo = new ReceiveCreditScoreVo();
////			vo.setCompanyName("方正物业");
////			vo.setCreditAmount("2525666");
////			vo.setOpinionStatus("2");
////			vo.setStatus("222");
/////*
////				  String timesptms =  DateOperator.formatDate(new Date(),"yyyyMMdd");
////					String plainPwd =PropertiesUtil.getProperty(OfferConstants.BAS_PRIVATEKEY)+8+timesptms;
////					String token = Md5EncryptUtils.encrypt(plainPwd);
////					vo.set*/
////			String jsonStr = JsonUtil.obj2Json(vo);
////			   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>jsonStr>>>>>>>>"+ jsonStr);
////			    String uuidJson=  HTTPUtility.doPostBody("http://192.168.2.76:8088/open/bank/account/updateStatus", vo, null);
////				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+uuidJson);
//////				JSONObject jsonObj = JSONObject.parseObject(uuidJson);
//////				String errorId = jsonObj.getString("errorId");
////		    } catch (Exception e) {
////		      e.printStackTrace();
////		    }
////	
////	}
//	
//	/**
//	 * 讲产品信息传送 自营审批测试
//	 */
////	@Test
////	public void bas(){
////		
/////*
////			ApplyProductDetailVo voDetail = new ApplyProductDetailVo();
////		
////			List<ApplyProductDetailVo> ls = new ArrayList<ApplyProductDetailVo>();
////			
////			voDetail.setProductName("PP");
////			voDetail.setProductCd("SL_PP_PP");
////			voDetail.setBrandNumber("M250E");
////			voDetail.setFactoryName("上海石化");	
////			voDetail.setWarehouseName("测试仓库111test");//
////			voDetail.setDealNumber(new BigDecimal(10.0000));
////			voDetail.setDealPrice(new BigDecimal(8500.0000));
////			voDetail.setTotalPrice(new BigDecimal(85000.0000));
////			ls.add(voDetail);
////			ApplyBuyVo applyBuyVo = new  ApplyBuyVo();
////			applyBuyVo.setAppDetail(ls);
////			applyBuyVo.setTotalAmount(new BigDecimal(85000.0000));
////			applyBuyVo.setCompanyName("浙江网塑电子商务有限公司");
////			applyBuyVo.setDeliveryType("ZT");
////			*/
////    	    OffMatchVo offMatchVo= new OffMatchVo();
////	    	OffPrice offPrice = offPriceService.getEntity(Long.valueOf(1152));
////            System.out.println(">>>>>"+bsCompanyService.getEntity(offPrice.getCompanyId()).getCompanyName());
////	    	offMatchVo.setCompanyName("测试客户企业");
////	    	offMatchVo.setAppCode("saaszs");
////	    	offPrice.setPriceTotal(offPrice.getDealPrice().multiply(offPrice.getDealNumber()));
////	    	BeanUtils.copyProperties(offPrice, offMatchVo);
////			String jsonStr = JsonUtil.obj2Json(offMatchVo);
////
////			//String uuidJson = SptApiCallUtility.sendData(OfferConstants.BAS_GATEWAY_URL_KEY,"/open/apply/buy/saveAndApply", jsonStr);
////			String uuidJson;
////			try {
////				uuidJson = HTTPUtility.doPostBody("http://192.168.2.55:8088/open/apply/buy/saveAndApply", offMatchVo, null);
////				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+uuidJson);
////				JSONObject jsonObj = JSONObject.parseObject(uuidJson);
////				String errorId = jsonObj.getString("errorId");
////
////			} catch (ApplicationException e) {
////				e.printStackTrace();
////			}
////			
////		
////	}
////	//@Test
////	public void test(){
////		/*BsUserAccount user=userService.findUserByEmail("zhangsan@autrade.com.cn");
////		if(user!=null){*/
////		BsUserAccount user = new BsUserAccount();
////			user.setUserName("ceshi1");
////			user.setPlainPassword("123456");
////			user.setMobile("18392403745");
////			user.setLoginName("18392403745");
////			user.setEmail("dengyanhua12@autrade.com.cn");
////			userService.saveAccount(user);
////		//}
////		
////		
////	}
////	/**
////	 * 商品通登录测试
////	 */
////	//@Test
////	public void loginSpt(){
////		SptUserLoginVo vo = new SptUserLoginVo();
////		BsUserAccount user = userService.getEntity(228l);
////		vo.setUserName(user.getMobile());
////		vo.setUserPassWord("123456");
////		vo.setMobile(user.getMobile());
////		vo.setUserRole(user.getRoleCd());;
////		String jsonStr = JsonUtil.obj2Json(vo);
////		String uuidJson = SptApiCallUtility.sendData(OfferConstants.SPT_GATEWAY_URL_KEY,"/SptMaster/loginInternal", jsonStr);
////		System.out.println("-----------------"+uuidJson);
////		JSONObject jsonObj=  JSONObject.parseObject(uuidJson);				
////		String errorId=jsonObj.getString("errorId");
////		String objJson=jsonObj.getString("objJson");
////		System.out.println("-----------3333------"+objJson);
////		//将返回的数据转为Object对象
////		/*if(!errorId.equals("0")){
////			throw new RequestException(errorId);*/
////		if(errorId.equals("0")){
////			String jsonStr1 = jsonObj.getString("objJson");
////			Object obj = jsonObj.parse(jsonStr1);
////			System.out.println((String)obj);
////		}
////	}
////	
////
////	/**
////	 * 自营审批登录测试
////	 */
////	@Test
////	public void loginBas(){
/////*		SptUserLoginVo vo = new SptUserLoginVo();
////		BsUserAccount user = userService.getEntity(228l);
////		vo.setUserName("15512345678");
////		vo.setUserPassWord("123456");
////		vo.setMobile("15512345678");
////		vo.setUserRole(user.getRoleCd());;
////		String jsonStr = JsonUtil.obj2Json(vo);
////		String uuidJson = SptApiCallUtility.sendData(OfferConstants.BAS_GATEWAY_URL_KEY,"/open/user/login", jsonStr);
////		System.out.println("-----------------"+uuidJson);
////		JSONObject jsonObj=  JSONObject.parseObject(uuidJson);				
////		String errorId=jsonObj.getString("errorId");
////		String objJson=jsonObj.getString("objJson");
////		System.out.println("-----------3333------"+objJson);
////		//将返回的数据转为Object对象
////		if(!errorId.equals("0")){
////			throw new RequestException(errorId);
////		if(errorId.equals("0")){
////			String jsonStr1 = jsonObj.getString("objJson");
////			Object obj = jsonObj.parse(jsonStr1);
////			System.out.println((String)obj);
////		}
////		
////		
////		*/
////		
////		 try {
////			 SptUserLoginVo vo = new SptUserLoginVo();
////			 vo.setUserName("15512345678");
////				vo.setUserPassWord("123456");
////				vo.setMobile("15512345678");
////		
/////*
////				  String timesptms =  DateOperator.formatDate(new Date(),"yyyyMMdd");
////					String plainPwd =PropertiesUtil.getProperty(OfferConstants.BAS_PRIVATEKEY)+8+timesptms;
////					String token = Md5EncryptUtils.encrypt(plainPwd);
////					vo.set*/
////		      HTTPUtility.doPostBody("http://192.168.2.55:8088/open/user/ssoLogin", vo, null);
////		    } catch (Exception e) {
////		      e.printStackTrace();
////		    }
////	
////	}
////	
////	
////	/**
////	 * 开通saas账号
////	 */
////	//@Test
////	public void sptOpenSaasAccount(){
////		   OpenSassAccountRequestVo requestVo = new OpenSassAccountRequestVo();
////		   
////			userService.sptOpenSaasAccount(requestVo);
////	}
////	
////	@Test
////	public void login() throws ApplicationException{
////		   BsUserAccountVo loginVo = new BsUserAccountVo();
////		   loginVo.setLoginName("13248068823");
////		   loginVo.setLoginPwd("123456");
////		   loginVo.setAppCode("BAS");
////		   ShiroUser shiroUser = userService.login(loginVo);
////		   System.out.println(">>>>="+shiroUser.getProp().get(Constant.GATE_ACCESSTOKEN));
////	}
//}
