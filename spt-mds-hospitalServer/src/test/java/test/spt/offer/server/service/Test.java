package test.spt.offer.server.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	
	@SuppressWarnings("unused")
	private static final int SALT_SIZE = 8;
	public static final int HASH_INTERATIONS = 1024;
	
	
public static void main(String[] args) {
	/*AxqContractVo  axqContractData = new AxqContractVo();
	axqContractData.setContractNo("cte_0001");
	axqContractData.setFileId("115");
	System.out.println(JsonUtility.toJSONString(axqContractData));
	String cc 	= JsonUtility.toJSONString(axqContractData);
	
	AxqContractVo  axqContractData2 = new AxqContractVo();
	axqContractData2 =JsonUtility.toJavaObject(cc, AxqContractVo.class);

	System.out.println(""+axqContractData2.getContractNo());
	System.out.println(""+axqContractData2.getFileId());*/
/*	Calendar ca = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
	    ca.setTime(new Date());
		ca.add(Calendar.DATE, -3);
		Date endDate = ca.getTime();
		System.out.println("============"+sdf.format(endDate));*/
		
/*		
		System.out.println(addressResolution("湖北省武汉市洪山区").get("county"));
		System.out.println(addressResolution("湖北省恩施土家族苗族自治州恩施市").get("county"));
		System.out.println(addressResolution("北京市市辖区朝阳区").get("county"));
		System.out.println(addressResolution("内蒙古自治区兴安盟科尔沁右翼前旗").get("county"));
		System.out.println(addressResolution("西藏自治区日喀则地区日喀则市").get("county"));
		System.out.println(addressResolution("海南省省直辖县级行政单位中沙群岛的岛礁及其海域").get("county"));	
		System.out.println(addressResolution("浙江省温州市鹿城区").get("city").substring(0, (addressResolution("浙江省温州市鹿城区").get("city").indexOf("市"))));
		System.out.println(addressResolution("浙江省温州市鹿城区").get("county").substring(0, (addressResolution("浙江省温州市鹿城区").get("county").indexOf("区"))));
		
		*/

	}



/**
 * 解析地址
 * @author lin
 * @param address
 * @return
 */
public static Map<String,String>  addressResolution(String address){
    String regex="(?<province>[^省]+自治区|.*?省|.*?行政区|.*?市)(?<city>[^市]+自治州|.*?地区|.*?行政单位|.+盟|市辖区|.*?市|.*?县)(?<county>[^县]+县|.+区|.+市|.+旗|.+海域|.+岛)?(?<town>[^区]+区|.+镇)?(?<village>.*)";
    Matcher m=Pattern.compile(regex).matcher(address);
    String province=null,city=null,county=null,town=null,village=null;
   // List<Map<String,String>> table=new ArrayList<Map<String,String>>();
    Map<String,String> row=null;
    while(m.find()){
        row=new LinkedHashMap<String,String>();
        province=m.group("province");
      
        row.put("province", province==null?"":province.trim());
     
        city=m.group("city");
     
        row.put("city", city==null?"":city.trim());
        county=m.group("county");
   
        row.put("county", county==null?"":county.trim());
        town=m.group("town");
        row.put("town", town==null?"":town.trim());
        village=m.group("village");
        row.put("village", village==null?"":village.trim());
    
    }

    return row;
}

}
