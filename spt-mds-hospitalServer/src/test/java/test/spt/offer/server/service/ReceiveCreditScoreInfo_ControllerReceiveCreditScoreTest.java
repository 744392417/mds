package test.spt.offer.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 接收 其他系统 提升信用评分资料	测试
 * */
public final class ReceiveCreditScoreInfo_ControllerReceiveCreditScoreTest
{
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	
//	RespVo<?> resultVo = new RespVo<>();
//	UcsCompanyCreditVo dealCreditScoreVo = dealCreditScore(receiveCreditScoreVo);
//	//查看是否存在此数据
//	UcsCompanyCreditVo findByCompanyNameVo = ucsCompanyApi.findByCompanyName(dealCreditScoreVo);
//	//如果存在
//	if (null != findByCompanyNameVo.getId())
//	{
//		//添加一条公司记录
//		UcsCompany ucsCompany = new UcsCompany();
//		BeanUtils.copyProperties(dealCreditScoreVo, findByCompanyNameVo);
//		BeanUtils.copyProperties(findByCompanyNameVo, ucsCompany);
//	//	ucsCompany.setId(findByCompanyNameVo.getId());
//		ucsCompanyApi.save(ucsCompany);
//		//存在此数据   如果条数不为空且大于1  批量保存
//		if (findByCompanyNameVo.getCreditScoreList() != null && findByCompanyNameVo.getCreditScoreList().size() > 0) 
//		{
//			ucsCreditScoreApi.saveCreditList(dealCreditScoreVo);
//		}
//	} else 
//	{
//		//不存在，添加一条公司记录
//		BeanUtils.copyProperties(dealCreditScoreVo, findByCompanyNameVo);
//		ucsCompanyApi.save(findByCompanyNameVo);
//		//存在此数据   如果条数不为空且大于1  批量保存
//		if (dealCreditScoreVo.getCreditScoreList() != null && dealCreditScoreVo.getCreditScoreList().size() > 0) 
//		{
//			ucsCreditScoreApi.saveCreditList(findByCompanyNameVo);
//		}
//	}
//	return resultVo;
	
	
/*	*//**
	 * 私有方法：分配信用评分
	 * *//*
	public UcsCompanyCreditVo dealCreditScore(ReceiveCreditScoreVo receiveCreditScoreVo) 
	{
		UcsCompanyCreditVo voCompany = new UcsCompanyCreditVo();
		voCompany.setCompanyName(receiveCreditScoreVo.getCompanyName());
		
		List<UcsCreditScoreVo> creditScoreList = new ArrayList<>();

		//获取字典表转换Map 获取诚信分
		List<AppDictData> creditscoreList = DictUtil.getListByCategory(OfferConstants.UCS_CREDITSCORE);
		Map<String, String> appDicdataMap = listConvertMap(creditscoreList);
		
		// 程序类(P)
		UcsCreditScoreVo voPR = new UcsCreditScoreVo();
		voPR.setGradingType(OfferConstants.UCS_GRADING_TYPE_P);//评分类型，P，程序类
		voPR.setType("企业工商注册文件");
		if (StringUtils.isNotBlank(appDicdataMap.get(voPR.getType()))) 
		{
			voPR.setScore(Integer.parseInt(appDicdataMap.get(voPR.getType())));// 诚信分
		}
//		voPR.setFileId(receiveCreditScoreVo.getP_R());//文件ID
		creditScoreList.add(voPR);
		logger.info("List ：add：程序类企业工商注册文件>>>>");
		// 背景类(B)
		
		
		// 财务类(F)
		UcsCreditScoreVo voFB = new UcsCreditScoreVo();
		voFB.setGradingType(OfferConstants.UCS_GRADING_TYPE_F);//评分类型，F，财务类
		voFB.setDataType("B");//数据类型
		voFB.setType("经营数据分析报告");
		if (StringUtils.isNotBlank(appDicdataMap.get(voFB.getType()))) 
		{
			voFB.setScore(Integer.parseInt(appDicdataMap.get(voFB.getType())));// 诚信分
		}
		voFB.setFileId(receiveCreditScoreVo.getF_B());//文件ID
		creditScoreList.add(voFB);
		logger.info("List ：add：经营数据分析报告>>>>");
		
		//
		UcsCreditScoreVo voFF = new UcsCreditScoreVo();
		voFF.setGradingType(OfferConstants.UCS_GRADING_TYPE_F);//评分类型，F，财务类
		voFF.setDataType("F");//数据类型
		voFF.setType("审计报告");
		if (StringUtils.isNotBlank(appDicdataMap.get(voFF.getType()))) 
		{
			voFF.setScore(Integer.parseInt(appDicdataMap.get(voFF.getType())));// 诚信分
		}
		voFF.setFileId(receiveCreditScoreVo.getF_R());//文件ID
		creditScoreList.add(voFF);
		logger.info("List ：add：审计报告>>>>");
		
		// 资信类(C)
		UcsCreditScoreVo voCE = new UcsCreditScoreVo();
		voCE.setGradingType(OfferConstants.UCS_GRADING_TYPE_C);//评分类型，C，资信类
		voCE.setType("企业征信查询授权书");
		voCE.setProjectContent("");
		if (StringUtils.isNotBlank(appDicdataMap.get(voCE.getType()))) 
		{
			voCE.setScore(Integer.parseInt(appDicdataMap.get(voCE.getType())));// 诚信分
		}
		voCE.setFileId(receiveCreditScoreVo.getC_E());//文件ID
		creditScoreList.add(voCE);
		logger.info("List ：add：企业征信查询授权书>>>>");
		
		//
		UcsCreditScoreVo voCL = new UcsCreditScoreVo();
		voCL.setGradingType(OfferConstants.UCS_GRADING_TYPE_C);//评分类型，C，资信类
		voCL.setType("法人征信查询授权书");
		voCL.setProjectContent("");
		if (StringUtils.isNotBlank(appDicdataMap.get(voCL.getType()))) 
		{
			voCL.setScore(Integer.parseInt(appDicdataMap.get(voCL.getType())));// 诚信分
		}
		voCL.setFileId(receiveCreditScoreVo.getC_L());//文件ID
		creditScoreList.add(voCL);
		logger.info("List ：add：法人征信查询授权书>>>>");
		
		//
		UcsCreditScoreVo voCP = new UcsCreditScoreVo();
		voCP.setGradingType(OfferConstants.UCS_GRADING_TYPE_C);//评分类型，C，资信类
		voCP.setType("专利说明书");
		voCP.setProjectContent("");
		if (StringUtils.isNotBlank(appDicdataMap.get(voCP.getType()))) 
		{
			voCP.setScore(Integer.parseInt(appDicdataMap.get(voCP.getType())));// 诚信分
		}
		voCP.setFileId(receiveCreditScoreVo.getC_P());//文件ID
		creditScoreList.add(voCP);
		logger.info("List ：add：专利说明书>>>>");
		
		//
		UcsCreditScoreVo voCT = new UcsCreditScoreVo();
		voCT.setGradingType(OfferConstants.UCS_GRADING_TYPE_C);//评分类型，C，资信类
		voCT.setType("商标说明书");
		voCT.setProjectContent("");
		if (StringUtils.isNotBlank(appDicdataMap.get(voCT.getType()))) 
		{
			voCT.setScore(Integer.parseInt(appDicdataMap.get(voCT.getType())));// 诚信分
		}
		voCT.setFileId(receiveCreditScoreVo.getC_T());//文件ID
		creditScoreList.add(voCT);
		logger.info("List ：add：商标说明书>>>>");

		// 担保类(G)
		UcsCreditScoreVo voGL = new UcsCreditScoreVo();
		voGL.setGradingType(OfferConstants.UCS_GRADING_TYPE_G);//评分类型，G，担保类
		voGL.setType("企业法人夫妻双方的个人连带担保函");
		if (StringUtils.isNotBlank(appDicdataMap.get(voGL.getType()))) 
		{
			voGL.setScore(Integer.parseInt(appDicdataMap.get(voGL.getType())));// 诚信分
		}
		voGL.setFileId(receiveCreditScoreVo.getG_L());//文件ID
		creditScoreList.add(voFF);
		logger.info("List ：add：企业法人夫妻双方的个人连带担保函>>>>");
		
		//
		UcsCreditScoreVo voGO = new UcsCreditScoreVo();
		voGO.setGradingType(OfferConstants.UCS_GRADING_TYPE_G);//评分类型，G，担保类
		voGO.setType("其他资产担保");
		if (StringUtils.isNotBlank(appDicdataMap.get(voGO.getType()))) 
		{
			voGO.setScore(Integer.parseInt(appDicdataMap.get(voGO.getType())));// 诚信分
		}
		voGO.setFileId(receiveCreditScoreVo.getG_O());//文件ID
		creditScoreList.add(voGO);
		logger.info("List ：add：其他资产担保>>>>");
		
		//
		UcsCreditScoreVo voGS = new UcsCreditScoreVo();
		voGS.setGradingType(OfferConstants.UCS_GRADING_TYPE_G);//评分类型，G，担保类
		voGS.setType("业务员推荐与保证");
		if (StringUtils.isNotBlank(appDicdataMap.get(voGS.getType()))) 
		{
			voGS.setScore(Integer.parseInt(appDicdataMap.get(voGS.getType())));// 诚信分
		}
		voGS.setFileId(receiveCreditScoreVo.getG_S());//文件ID
		creditScoreList.add(voGS);
		logger.info("List ：add：业务员推荐与保证>>>>");
		
		//
		UcsCreditScoreVo voGM = new UcsCreditScoreVo();
		voGM.setGradingType(OfferConstants.UCS_GRADING_TYPE_G);//评分类型，G，担保类
		voGM.setType("质押抵押记录");
		if (StringUtils.isNotBlank(appDicdataMap.get(voGM.getType()))) 
		{
			voGM.setScore(Integer.parseInt(appDicdataMap.get(voGM.getType())));// 诚信分
		}
		voGM.setFileId(receiveCreditScoreVo.getG_M());//文件ID
		creditScoreList.add(voGM);
		logger.info("List ：add：质押抵押记录>>>>");

		// 重大事件类(E)
		UcsCreditScoreVo voEL = new UcsCreditScoreVo();
		voEL.setGradingType(OfferConstants.UCS_GRADING_TYPE_E);//评分类型，E，重大事件类
		voEL.setType("诉讼记录");
		if (StringUtils.isNotBlank(appDicdataMap.get(voEL.getType()))) 
		{
			voEL.setScore(Integer.parseInt(appDicdataMap.get(voEL.getType())));// 诚信分
		}
//		voEL.setFileId(receiveCreditScoreVo.getE_L());//文件ID
		creditScoreList.add(voEL);
		
		// 保险人意见(O)
		voCompany.setCreditScoreList(creditScoreList);
		// 线上数据(D)
		return voCompany;
	}
	*//**
	 * 私有方法：List类型数据转换成Map类型
	 * *//*
	private Map<String, String> listConvertMap(List<AppDictData> creditscoreList) 
	{
		Map<String, String> appDicdataMap = new HashMap<>();
		for (AppDictData appDicdata : creditscoreList) 
		{
			appDicdataMap.put(appDicdata.getDictCd(), appDicdata.getDictName());
		}
		return appDicdataMap;
	}
	*/
	
	
	
	
	//如果传的文件ID，公司名，评分类型，数据类型，对比数据库不一致，再进行操作
//	UcsCreditScore findByType = ucsCreditScoreApi.findByType(voFR);
//	if(!findByType.getFileId().equals(receiveCreditScoreVo.getF_R()))
//	{
//		voFR.setFielidSort(0);//文件显示排序
//		voFR.setFileId(receiveCreditScoreVo.getF_R());//文件ID
//		voFR.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//		//获取诚信分
//		Integer voFRScore = 0;
//		if(StringUtils.isNotBlank(receiveCreditScoreVo.getF_R()))
//		{
//			String scoreString = getApplicationDicDataMap().get(voFR.getType());
//			if(null != scoreString)
//			{
//				voFRScore=Integer.valueOf(scoreString);//诚信分
//			}
//		}
//		voFR.setScore(voFRScore);
//		list.add(voFR);
//		logger.info("List ：add：审计报告>>>>");
//	}
	
	
	
	
	
	
//	//财务数据 -- 审计报告
//	UcsCreditScoreVo voFR=new UcsCreditScoreVo();
//	BeanUtils.copyProperties(receiveCreditScoreVo, voFR);
//	voFR.setGradingType(OfferConstants.UCS_GRADING_TYPE_F);//评分类型，F，财务类
//	voFR.setDataType("F");//数据类型
//	voFR.setType("审计报告");//类型
//	voFR.setFielidSort(0);//文件显示排序
//	voFR.setWillChoose(OfferConstants.UCS_WILL_CHOOSEE_1);//1 其它系统导入 0 信用系统上传
//	//如果传的文件ID，公司名，评分类型，数据类型，对比数据库不一致，再进行操作
//	UcsCreditScore findByType = ucsCreditScoreApi.findByType(voFR);
//	//是否存在
//	if(null != findByType)
//	{
//		voFR.setId(findByType.getId());
//	}
//	//文件判断
//	if(StringUtils.isNotBlank(receiveCreditScoreVo.getF_R()))
//	{
//		voFR.setFileId(receiveCreditScoreVo.getF_R());//文件ID
//	}
//	//获取诚信分
//	String scoreString = getApplicationDicDataMap().get(voFR.getType());
//	if(null != scoreString)
//	{
//		voFR.setScore(Integer.valueOf(scoreString));
//	}
//	list.add(voFR);
//	logger.info("List ：add：审计报告>>>>");
//	
	
	
	
	
	
	
	
	
//	if (creditScoreold != null) {
//		// 更新
//		if (ucsCreditVo.getDataType() != null && !"".equals(ucsCreditVo.getDataType())) {
//			creditScoreold.setDataType(ucsCreditVo.getDataType());
//		}
//		if (ucsCreditVo.getOnlineData() != null && !"".equals(ucsCreditVo.getOnlineData())) {
//			creditScoreold.setOnlineData(ucsCreditVo.getOnlineData());
//		}
//
//		if (ucsCreditVo.getOnlineType() != null && !"".equals(ucsCreditVo.getOnlineType())) {
//			creditScoreold.setOnlineType(ucsCreditVo.getOnlineType());
//		}
//
//		if (ucsCreditVo.getProjectContent() != null && !"".equals(ucsCreditVo.getProjectContent())) {
//			creditScoreold.setProjectContent(ucsCreditVo.getProjectContent());
//		}
//		if (ucsCreditVo.getFileId() != null && !"".equals(ucsCreditVo.getFileId())) {
//			creditScoreold.setFileId(ucsCreditVo.getFileId());
//		}
//		creditScoreold.setCreditStatus("3");//重新审核
//		
//		
//	} else {
//		// 新增
//		BeanUtils.copyProperties(ucsCreditVo, creditScoreold);
//		creditScoreold.setCreditStatus(OfferConstants.UCS_WILL_CHOOSEE_0);//未审核
//	}
	
}
